# PDL to AQMS message injector

pdl-to-aqms is a stand-alone Python script that reads a
quakeml.xml file, formats its contents into suitable AQMS tables
(event, origin, arrival, amp, etc) and inserts these into the
specified AQMS database.

It can be run directly from the command line, using
the proper command line arguments, or, more importantly,
can be triggered by a PDL client each time it receives
a new PDL message.

When a PDL event origin passes any specified filters and is
therefore suitable for insertion into the database,
pdl-to-aqms will first query the database to see
if that origin already exists in the database, and if so,
will update it.  
<!--
The PDL message --code (e.g., 'us70007nkh')
will get tagged to the AQMS database origin via 
origin.locevid='US70007NKH'.
-->

(Note: focalmech is now in its own repo at:
https://gitlab.com/aqms-swg/aqms-hashwrap)

## Contents
 - [Installation](#installation)
 - [Configuration](#configuration-properties)
 - [Running the Code](#examples)
   - [Cmd Line Usage](#usage)
   - [Run from PDL](#example-run-from-pdl)
     - [Ignore your own messages!](#--ignore-sources---important)
   - [Run from shell script](#example-run-from-shell-script)
 - [Polygon Filtering](#polygon-filtering)
 - [Event Association](#event-association)
 - [Setting rflags](#setting-rflag)
 - [Helper Apps](#helper-apps)
 - [Logging](#logging)

## Installation

### The easiest way:

1. Create a virtual python env using conda or pyenv, e.g,

    ```
    >conda create -n aqms python=3.10     // any python version > 3.7 should work
    >conda activate aqms
    ```

2. Install directly from the gitlab repo:

    ```(aqms)>pip install git+https://gitlab.com/aqms-swg/aqms-pdl.git```

<!--
Note: If you want to use focalmech to compute HASH focalmechs, do this
instead:

    (aqms)>pip install "aqms-pdl[hash] @ git+https://gitlab.com/aqms-swg/aqms-pdl.git"

This will automatically install the necessary requirements (e.g., hashwrap, mplstereonet) for you.
-->

### Or you can download the code first

    >git clone https://gitlab.com/aqms-swg/aqms-pdl.git

### Update the code (if needed)

    >git pull

### Install it

    >pip install .

<!--
Note: If you want to use focalmech to compute HASH focalmechs, do:

    >pip install .[hash]
-->

### Test it out:

      >pdl-to-aqms -h

      usage: pdl-to-aqms    [-h] --status STATUS --code // e.g., --code=hv71386392
                            --directory // path to quakeml.xml --type // e.g.,
                            --type=phase_data --source // e.g., --source=pr
                            [--optional_arg OPTIONAL_ARG]
                            [--action e.g., EVENT_UPDATED, EVENT_UPDATED, PRODUCT_ADDED]
                            [--eventids list of eventids]
                            ...

Note: **pdl2aqms** is another alias for pdl-to-aqms
### Configuration Properties

pdl-to-aqms needs to read a yaml config file to get settings for the database connection, etc.
pdl-to-aqms attempts to find the config file in the following
order:

1. --configfile=path-to-yml-file   // passed on cmd line
2. configfile pointed to in user's env: AQMS_DB_CONFIG=/path/to/configfile
3. config.yml in the current working directory
4. config.yml in the directory where pdl-to-aqms package is installed

### Database Credentials
The config file should contain the
parameters needed to connect to the AQMS database:

  >cat config.yml

    DB_HOST: localhost
    DB_NAME: archdb
    DB_PORT: 5432
    DB_USER: rtem
    DB_PASSWORD: xxxx_xxxx
    # This should be the same as the auth in the local origin/event tables:
    RSN_source: 'PR'
    LOG_DIR: './log'
    LOG_LEVEL: 'INFO'
    pdl-types: ['origin', 'phase-data', 'moment-tensor', 'focal-mechanism', 'unassociated-amplitude', 'internal-moment-tensor']
    assoc_thresh:
        time: 1.1
        km_dist: 10.0
        km_depth: 15.0
    associate:True

If this file is not found, configure.py will try to
set these same variables from the user's environment.
This can be configured directly in configure.py.
For instance, if everything except DB_PASSWORD is set in this file,
pdl-to-aqms will look in the user's env for DB_PASSWORD.

### Event Association
If associate is set to True in the config file, pdl-to-aqms will attempt to associate
the current PDL event with other events in the local db using 2 methods:
1. By PDL indexer association
   If --eventids=[..] contains the eventid of a corresponding pdl_id that
   has already been inserted into the db, pdl-to-aqms will use the existing
   event + origin in the db.  If it determines the origin has changed, it will
   insert a new origin and make it preferred for the event.
   This method is used to associated 2 PDL events with different eventids (all 
   external to the local db) with one another, so it doesn't result in separate
   evids for the same event.  It can also be used to associate an external PDL 
   event with a local RSN evid that was previously injected into PDL.
2. By assoc_thresh
   Regardless of whether or not PDL has associated this event with any other
   event it has seen, pdl-to-aqms will compare the PDL message origin
   with any other origins in the database that are within the threshold
   (dt, dx, dz) specified in config.yml.  If the 2 events are found to be
   within the threshold "distances", they will be flagged as associated.
   This method is used to associate an external PDL event with a local
   RSN evid that may or may not have been injected into PDL.  Currently it is
   not used to associate 2 external PDL eventids with each other as
   method 1 is.
   
When a PDL event is associated with a local RSN event, a new row is
inserted into the aqms assocevents table (evid, evidassoc) and a corresponding
remark is inserted into the Remark table, indicating whether the
association was made by method 1 or 2.


### PDL Message Types
The config file must specify the allowable PDL message types for processing:

    pdl-types: ['origin', 'phase-data', 'moment-tensor', 'focal-mechanism', 'unassociated-amplitude', 'internal-moment-tensor']

Any PDL message types not in this list will be ignored.

### RSN_Source
Used to look for associations between PDL and local events,
determine if an event (origin) is local or regional, etc.

It should be set the RSN network code where the code is being run.
e.g.,

    RSN_source: 'PR'

### --ignore-sources   \*\*Important**
In addition, RSN's will want to use the --ignore-sources=XX cmd line flag with
pdl-to-aqms, where XX is their source code, so that pdl-to-aqms will not try
to insert any of the RSN's own PDL messages back into their aqms db as a PDL event.
If the RSN also wishes to ignore other sources, the syntax is:
--ignore-sources=XX,XY,XZ,...

### Logging

Logging can be configured in several ways of decreasing priority:

1. On the cmd line // This will override all other loglevel settings:

    ```console
       --loglevel=DEBUG     // {DEBUG, INFO, WARNING, ERROR, CRITICAL}
    ```

2. In config.yml:

    ```console
       LOG_DIR: './log'
       LOG_LEVEL: 'INFO'
    ```

3. None - If you don't set it anywhere, it will default to:

    ```console
       LOG_DIR: '.'
       LOG_LEVEL: 'INFO'
    ```

## Some Notes:

### Setting rflag

The following AQMS tables contain rflag column:
    Amp, Arrival, Coda, Origin, Mec, Netmag,
    Assocamm, Assocamo, Assocaro, Assoccom, Assoccoo.

The following obspy objects have evalutation_mode + evaluation_status attributes:
    origin, pick, magnitude, focalmechanism
The rest only have creation_info.author to check

It looks like rflag is not consistently used/checked in AQMS.
    AQMS Amp, Arrival, Netmag check for rflag.upper() in {'A','H','F'}
    AQMS Origin checks for rflag.upper() in {'A','H','F', 'I', 'C'}
    AQMS Mec - doesn't even check rflag.

As a compromise, the code only sets rflag to one of:
    A - Automatic  (Default)
    H - Human/Hand (evaluation_mode == 'manual' but evaluation_status != 'final'
                    or, obspy_object.creation_info.author is set)
    F - Final      (evaluation_mode == 'manual' and evaluation_status == 'final')


## Usage

    >pdl-to-aqms -h
      usage: pdl_to_aqms.py [-h] --status STATUS --code // e.g., --code=hv71386392
                            --directory // path to quakeml.xml --type // e.g.,
                            --type=phase_data --source // e.g., --source=pr
                            [--optional_arg OPTIONAL_ARG]
                            [--action e.g., EVENT_UPDATED, EVENT_UPDATED, PRODUCT_ADDED]
                            [--eventids list of eventids]
                            [--preferred-eventid //e.g., --preferred-eventid=hv71386392]
                            [--preferred-eventsource //e.g., --preferred-eventsource=hv]
                            [--preferred-eventtime PREFERRED_EVENTTIME]
                            [--preferred-magnitude PREFERRED_MAGNITUDE]
                            [--preferred-latitude PREFERRED_LATITUDE]
                            [--preferred-longitude PREFERRED_LONGITUDE]
                            [--preferred-depth PREFERRED_DEPTH]
                            ...
                            [--ignore-latitude-above // Ignore latitude > than this]
                            [--ignore-latitude-below // Ignore latitude < than this]
                            [--ignore-longitude-above // Ignore longitude > than this]
                            [--ignore-longitude-below // Ignore longitude < than this]
                            [--ignore-depth-above // Ignore depth > than this]
                            [--ignore-depth-below // Ignore depth < than this]
                            [--ignore-magnitude-below // Ignore magnitude < than this]
                            [--ignore-magnitude-above Ignore magnitude > than this]
                            [--ignore-sources // Ignore these sources, eg. =HV,US]
                            [--only-listen-to-sources // Only listen for these sources]
                            [--debug DEBUG]
                            [--ignore-arrivals]    // Don't try to inert any arrivals into db
                            [--ignore-amplitudes]  // Don't try to insert any amp into the db
                            [--loglevel // --loglevel=DEBUG or INFO,WARN,ERROR,etc] 
                            [--archive-quakeml]    // If present, set to True
                            [--archive-quakeml-dir    // Top level dir for local quakeml archive]
                            [--configFile // --configFile=config.yml]

      required arguments:
        --status STATUS
        --code // e.g., --code=hv71386392
        --directory // path to quakeml.xml
        --type // e.g., --type=phase_data
        --source // e.g., --source=pr

### Polygon Filtering
[2021-12] Added ability to specify polygon of interest in the config
file:

```
polygon:
  - [-121.25, 34.5]
  - [-117.76, 37.43]
  - [-120, 39]
  - [-120, 42]
  - [-122.7, 42]
  - [-125, 43]
  - [-126, 43]
  - [-126, 40]
  - [-121.25, 34.5]
```

If polygon is specified in the config, the filter will only pass
messages with lat,lon that locate within the polygon for 
processing consideration.

## Examples


### Example: Run from PDL
The following excerpt is from the config.ini file that is read
by the PDL app ProductClient.jar

    [indexer]
    type = gov.usgs.earthquake.indexer.Indexer
    ...
    includeTypes = origin, associate, disassociate, trump, trump-origin, phase-data
    listeners = indexer_listener
    ...
    [indexer_listener]
    type = gov.usgs.earthquake.indexer.ExternalIndexerListener
    command = pdl-to-aqms --ignore-latitude-below=17.0 --ignore-latitude-above=19.0 --ignore-longitude-above=-65.0 --ignore-longitude-below=-68.0 --only-listen-to-source=US


PDL automatically passes many fields as command line arguments to 
pdl_to_aqms.py, including all of the required ones (--status, --source,
etc).
Additional filter parameters can be passed as shown.
pdl_to_aqms.py will use these to determine if a message is suitable for
insertion into the AQMS database.

A full list of filter options can be seen from the help message.


### Example: Run from shell script

    #!/bin/sh

    pdl-to-aqms    --preferred-eventid=ci38452111 --preferred-eventsource=ci \
                   --preferred-eventsourcecode=38452111 \
                   --eventids=ci38452111 --preferred-magnitude=1.35 \
                   --preferred-latitude=33.5318333 --preferred-longitude=-116.6995 \
                   --preferred-depth=5.92 --preferred-eventtime=2019-07-05T14:42:52.650Z \
                   --directory=./1582319336450 \
                   --type=phase-data --code=ci38452111 --source=ci \
                   --updateTime=2020-02-21T21:08:56.450Z --status=UPDATE \


Note that shell scripts can be autogenerated by using the --archive_quakeml flags

    >pdl-to-aqms ... --archive_quakeml --archive_quakeml_dir=/path/to/archive

These params can be set in either the config.yml file or as cmd-line
flags as above (cmd-line will override config.yml settings).

Doing this will create a new dir (if it doesn't already exist):
    /path/to/archive/code/timestamp

where code = the pdl --code (e.g., us70008jr5) and timestamp is the time
in milliseconds of the XML element: eventParameters/creationInfo/creationTime
within the original quakeml file. e.g.,

   /path/to/archive/us70008jr5/1592073888040/

Inside this directory will be 2 files:
  - quakeml.xml - A copy of the original PDL quakeml file that was processed
  - run.sh* - An executable script to rerun this local copy of
    quakeml.xml without the need for PDL. It will have the same cmd line flags as the original run.

## Helper apps

Inside the aqms-pdl/aqms_pdl/helpers directory are several stand-alone scripts that
make use of the package:

0. cmd_line args common to all apps below:

        Usage: >python ... -c /path/to/configfile --loglevel DEBUG --logconsole
        
**Note:** apps will look for the configfile in the usual order:

1. command line --configfile or -c
2. AQMS_DB_CONFIG env
3. local file = ./config.yml
4. file=config.yml in the aqms-pdl INSTALLATION_DIR


Helper apps:

1. associate_origin.py

        Usage: >python associate_origin.py --evid EVID

    This app can be used to look for possible matching PDL origins that may have
    arrived *before* the local RSN event was inserted into the db.
    e.g., say a PDL event arrives before the local event in the db.  At the time the PDL event
    is inserted, there will be no local event to associate with it.  When the local event
    is later inserted (e.g., by RT1 or Jiggle, etc), this code could be triggered to look
    for the association with any previously inserted PDL events.
    Matches within assoc_thresh will trigger insertion into the Assocevents table.
    
2. compare_evids.py 

        Usage: >python compare_evids --evid 1,10
        
    This app will calculate the difference (dt, dx, dz) between two event preferred_origins,
    and output this to console.  This can be useful for debugging why a particular assoc_thresh
    is not matching events.
    This app does not write anything into the db.

3. magprefpriority.py 

        Usage: >python magprefpriority -f ../resources/magprefpriority_NC

    This app can be used to control the Magprefpriority table in the db.
    It will attempt to read the *template* file, which should look nearly identical to the 
    db table itself, and use it to create the magprefpriority table.
    
4. test_connection.py

        Usage: >python test_connection
    
    May be useful to help debug instances when connecting to db fails.
    If sql connection successful, will print out one-line summaries of the 3 latest events.

**Note:** Although these scripts are stand-alone, they must be run from within a conda/venv where the aqms-pdl
package is installed.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Write license
