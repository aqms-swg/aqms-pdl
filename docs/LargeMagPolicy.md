# Large Magnitude Policy Implementation

 - [Policy](#policy)
 - [Steps to Implement The Large Magnitude Policy](#steps-to-implement-the-large-magnitude-policy)

## Large Magnitude Policy
The ANSS recently adopted a “large magnitude policy” (LMP) governing the choice of preferred magnitude for large (Mw>=6.0) earthquakes within the authoritative regions of ANSS Regional Seismic networks (RSNs). 

### Policy:
The NEIC W-phase magnitude (Mww) will be used as the authoritative ANSS magnitude during response when the NEIC Mww is 6.0 or larger.  

### Large Magnitude Policy Implementation Strategy
In deciding how best to implement this policy, we (ANSS working group ?) thought about the following scenarios:

 - An Mww magnitude arrives via Product Data Layer (PDL) after the local RSN has already published into PDL a solution for the event with the magnitude preferred by the RSN.
 - An Mww magnitude arrives via PDL after the local RSN has generated an evid for the event, but before the RSN has published its results into PDL
 - An Mww magnitude arrives before the local RSN has created an evid for the event.

Any implementation would need to consider all of these possible scenarios.  In addition, regardless of when the Mww magnitude arrives relative to the RSN solution, the RSN will likely continue to work on the event and generate new and updated products.  Similarly, NEIC will likely publish updates to the Mww magnitude that must be handled.

With this in mind, we decided on the following approach: The NEIC (auth=US), or any (configurable) foreign auth – where foreign refers to any source that is not the current RSN where the code is running – will be able to push products that translate into AQMS tables (event, origin, netmag, etc), but those products/tables will all be affiliated with a unique event evid which is, in a sense owned by the foreign source.  As an example, if the local RSN (auth=’NC’) is listening/injecting PDL messages for the same event from both auth=’NN’ and auth=’US’, then for each of these auths (NC, NN, US) there will be a unique evid to which all related products are attached (event → origin → netmag, etc).  The code that inserts PDL messages into the AQMS db will not be permitted to alter anything created/owned by the local RSN, with one exception, explained below.

Thus, in most cases, there will be an evid for the local RSN products, the local evid, and there will be other evid(s) (one for US, one for any neighboring networks that insert, etc) for the PDL products, called PDL evids.  The products that hang from these PDL evids (origins, netmags, mecs, etc) will be updated as new PDL messages arrive.

In this way, the two event streams (local v. PDL) will not know anything about each other.  The only place the two streams will cross is in a new table (the Assocevents table, described below), which associates evid_1 with evid_2.  This association can be made automatically, as the PDL products are inserted and also manually.  When a local evid and a US PDL evid containing an Mww magnitude are associated in the Assocevents table, the US Mww magnitude may be selected to be preferred on the local evid.  This selection triggers a new stored procedure which tries to make the Mww magnitude preferred on the local evid, provided the Mww magnitude has higher precedence in the Magprefpriority table than any existing local RSN magnitudes.  This is the only sense in which the two streams – local v. PDL – may cross, and currently, it is only implemented manually; an analyst must manually select (e.g., using the aqms-pdl-app)  US Mww magnitude to be preferred for a local event evid, and even then, the Mww magnitude must have sufficient magprefpriority to become preferred.  In the future, if the RSN’s request it, the code may be modified to make Mww magnitude preferred automatically, as the Mww PDL message is received, subject to the same priority criteria as before.

### Procedure to Make US Mww Preferred For Local Evid
Two things need to happen for the US Mww (or any other foreign magnitude) to be made preferred on a local evid:
1. The local evid and the foreign evid need to be associated in the Assocevents table (this should happen automatically)
2. An analyst must use the aqms-pdl-app to manually select the US Mww magnitude to be applied to the *local* evid.


### Large Magnitude Policy Code Implementation
In order to implement the procedure outlined above, we developed two python packages:

1. pdl-to-aqms - The main app in this package is called pdl-to-aqms (pdl2aqms also works). It is triggered by PDL
   and converts PDL products into AQMS table rows and inserts them into the db.  
   If the PDL event already exists in the db, the code will update the existing event.
   This code is configurable and can automatically associate PDL evids with local evids, either using the PDL associations
   (if they exist), or through brute force association based on a configurable association threshold (dx, dz, dt).
   The package and documentation can be found at:

   https://gitlab.com/aqms-swg/aqms-pdl

2. pdl-import-review - This is a small GUI app that runs in a browser and allows the user to interact with the Assocevents 
   table - e.g., to create/delete associations and/or to make US Mww preferred on a local evid.  The app also has a route 
   for the event table which creates a view into the aqms tables that allows the user to step through each origin affiliated 
   with an evid, and each netmag affiliated with an origin, etc, through a sequence of dropdown tables.
   The package and documentation can be found at:

   https://gitlab.com/aqms-swg/aqms-pdl-app

The two packages were designed to be very flexible; while they accomplish the goal of the large magnitude policy implementation, they can also do a lot more.  For instance, a local RSN may wish to also import event solutions from a neighboring network(s) for comparison.  This can easily be configured.  In addition, the code was tested and improved with an eye for making the local RSN feel safe.  E.g., it will not interfere with their existing codes/tables and the local-foreign association is done in a lightweight manner, through the Assocevents table.

### Steps to Implement The Large Magnitude Policy

1. Install aqms-pdl package

   https://gitlab.com/aqms-swg/aqms-pdl

2. Check that AQMS db is up to date:
   1. If Assocevents table doesn’t exist → Create it using provided sql script
sql/create_assoc.sql - script in aqms-pdl repo to create the assocevents table

   2. Modify your Magprefpriority table to allow Mww to become preferred type ‘w’ magnitude when magnitude >= 6.0
      A script in the aqms-repo exists to help with creation/modification of the magprefpriority db table:
   
      *helpers/magprefpriority.py*
   
      Users can maintain the magprefpriority table as an editable ASCII file which the script then uses to update the db table.

      An example of the static file and magprefpriority table:

    <pre>
    >cat resources/magprefpriority_NC
    archdb1=> select region_name, datetime_on, datetime_off, priority, magtype, auth, subsource, magalgo, minmag, maxmag, minreadings, maxuncertainty, quality from magprefpriority order by priority desc;
    region_name| datetime_on| datetime_off | prior|magt| auth | subsource |    magalgo     | minmag | maxmag |minrd|maxuncer| quality
    -----------+------------+--------------+------+----+------+-----------+----------------+--------+--------+-----+--------+--------
    DEFAULT    | -2209000000|  32504000000 |   99 | w  | US   |           | Mww            |      6 |     10 |     |        |
    DEFAULT    | -2209000000|  32504000000 |   90 | w  | NC   |           | Mw             |      5 |     10 |     |        |
    ...
   </pre>
   
   3. Update Stored Procedure(s) if necessary
   - epref.setprefmag - must be modified to allow best magid from orid != event.preforigin since Mww origin is not part of local evid.

   - Install two new Stored Procedures if not already installed: 
     - magpref.magpref_bestMagidOfEvent - Finds best prefmag considering only RSN orids
     - epref.unsetprefmag - allows unset Mww or other non-RSN prefmag in an atomic way


3. Install aqms-pdl-app package
   
   https://gitlab.com/aqms-swg/aqms-pdl-app


4. Configure aqms-pdl

   pdl2aqms is a stand-alone app that ProductClient (described below) runs when it receives a PDL message that meets
   your criteria.
   pdl2aqms reads a configuration file (./config.yml by default) that tells it, among other things,
   how to connect to your database.

   Details are in the package repo doc:

   https://gitlab.com/aqms-swg/aqms-pdl


5. Download the latest Product Data Layer (PDL) client: 
    
    The latest ProductClient.jar release can be found at: 

	https://github.com/usgs/pdl/releases
 
    The Product Distribution User Guide can be found at:

    https://usgs.github.io/pdl/index.html


6. Modify the ProductClient configuration file (e.g., config.ini) as per the example given in the aqms-pdl repo README:

<pre>
    [indexer]
    type = gov.usgs.earthquake.indexer.Indexer
    includeTypes = origin, associate, disassociate, trump, trump-origin, phase-data
    listeners = indexer_listener
    ...
    [indexer_listener]
    type = gov.usgs.earthquake.indexer.ExternalIndexerListener
    command = pdl-to-aqms --ignore-latitude-below=17.0 --ignore-latitude-above=19.0 --ignore-longitude-above=-65.0 --ignore-longitude-below=-68.0 --only-listen-to-source=US
</pre>
 - Note: ProductClient needs to find that executable, so if you don't see it in your environment (>which pdl-to-aqms),
         then you should put the absolute path to it in the PDL config (e.g., command = /your/path/to/pdl-to-aqms).

7. Start ProductClient.jar (you'll probably want to put this in a bash script and background it)
    >java -jar ProductClient.jar --configFile=config.ini --receive

When a PDL message arrives whose type matches your includeTypes, this will trigger the pdl-to-aqms cmd with the –ignore flags you select (see aqms-pdl documentation for more information).  It is important to either set –only-listen-to-source=US,NN,..  or else –ignore-source=’NC’ in order to not listen/insert your own messages that your RSN may be pushing through PDL (e.g., we don’t want a loop).

The flags to pdl-to-aqms: –ignore-latitude-below, etc., are checked when pdl-to-aqms is run and if the event origin, magnitude, time delay, etc don’t pass these checks, some information about the event is logged but nothing will be injected into the db.  See the aqms-pdl docs for the full set of flags.
Start the aqms-pdl-app to manually review the Assocevents table and assign an Mww magnitude to a local evid (subject to magprefpriority table rules).

