"""A setuptools based setup module for aqms-pdl"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from codecs import open
from os import path
from setuptools import setup, find_packages

#import aqms_pdl
#import versioneer

version = '1.0.0'

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as readme_file:
    readme = readme_file.read()

with open(path.join(here, 'HISTORY.rst'), encoding='utf-8') as history_file:
    history = history_file.read().replace('.. :changelog:', '')

requirements = [
    #'SQLAlchemy==1.4.46',
    'SQLAlchemy<2',
    'psycopg2',
    'PyYAML',
    'timezonefinder',
    'pytz',
]

test_requirements = [
    'tox',
    'pytest',
    'pytest-runner',
]

cli_requirements = [
        #'click',
]
develop_requirements = test_requirements + cli_requirements

hash_requirements = [
    'toml',
    'hashwrap',
    # MTH: mplstereonet at pypi still has np.float which is deprecated
    # MTH: as far as I can tell line below requires commit id (SHA) and there is no alias for "latest" ... ?
    'mplstereonet @ git+https://github.com/joferkington/mplstereonet.git@89a3b5a028703452bfc5dbfc3d77af355f645485',
]

import sys
import site
#print("***** MTH: sys.prefix=[%s] site.USER_BASE=[%s]" % (sys.prefix, site.USER_BASE))
py_version = '%s.%s' % (sys.version_info[0], sys.version_info[1])
path = 'lib/python%s/site-packages/aqms_pdl' % py_version
data_dir = os.path.join(path, 'resources')
data_files = [(data_dir, ["aqms_pdl/resources/example1.toml"])]


setup(
    name='aqms-pdl',
    #version=versioneer.get_version(),
    version=version,
    #version=aqms_pdl.__version__,
    #cmdclass=versioneer.get_cmdclass(),
    description="Listens for PDL messages and inserts quakeml into AQMS db",
    long_description=readme + '\n\n' + history,
    author="Mike Hagerty",
    author_email='mhagerty@isti.com',
    url='https://gitlab.isti.com/sog/aqms-pdl',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    entry_points={
        'console_scripts':[
            'pdl2aqms=aqms_pdl.pdl_to_aqms:main',
            'pdl-to-aqms=aqms_pdl.pdl_to_aqms:main',
            'pdl-to-SA=aqms_pdl.helpers.pdl_to_SA:main',
            'qml2aqms=aqms_pdl.qml_to_aqms:main',
            'hypodd-qml2aqms=aqms_pdl.qml_to_aqms_hypodd:main',
            'play-aqms=aqms_pdl.play_aqms:main',
            'qmlsummary=aqms_pdl.helpers.qml_summary:main',
            'associate_origin=aqms_pdl.helpers.associate_origin:main',
            'associate-origin=aqms_pdl.helpers.associate_origin:main',
            'compare-evids=aqms_pdl.helpers.compare_evids:main',
            'magprefpriority=aqms_pdl.helpers.magprefpriority:main',
            'test-connection=aqms_pdl.helpers.test_connection:main',
            'aqms-catalog=aqms_pdl.aqms_catalog:main',
            #'focalmech=aqms_pdl.helpers.focal_mech:main',
            ],
        },
    include_package_data=True,
    package_data={
        #'seismic_format':['formats/format*']
    },
    #data_files=[('my_data', ['test_data/*'])],
    data_files=data_files,
    install_requires=requirements,
    license="MIT",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    extras_require={ # MTH: eg, >pip install pkg[cli]
        'cli': cli_requirements,
        'develop': develop_requirements,
        'hash': hash_requirements,
        }
)
