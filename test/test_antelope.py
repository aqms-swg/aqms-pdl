#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_pdl2aqms
----------------------------------

Tests for `pdl_to_aqms` module.
"""

import glob
import os
import sys
import unittest
unittest.TestLoader.sortTestMethodsUsing = None

from aqms_pdl.libs.libs_log import read_config
from aqms_pdl.libs.libs_db import (set_Session, fix_leap_seconds,
                             getSequence, get_eventbyid, call_procedure, get_gtype,
                             query_origins_around_origin_time,
                             query_for_pdl_event_in_aqms,
                             query_assocevents,
                             create_and_insert_assocevents,
                             query_origin_by_evid,
                            )
from aqms_pdl.libs.libs_aqms import get_assoc_thresh, associate_RSN_origin_with_PDL_origins
from aqms_pdl.libs.libs_cmd import process_cmd_line

from aqms_pdl import createSession
import aqms_pdl

from aqms_pdl.libs.run_pdl_to_aqms import run_pdl_to_aqms

"""
MTH: To see output print statements do: >pytest -s test_antelope.py
"""

class Test(unittest.TestCase):

    def setUp(self):
        os.environ['AQMS_DB_CONFIG'] = './configs/config_NC.yml'
        #self.config = read_config(configfile='./config_NC.yml')[0]
        #self.config, _ = read_config(configfile='./config_NC.yml')
        #self.config, log_msgs = read_config(requireConfigFile=True, debug=True)
        self.config, log_msgs = read_config(requireConfigFile=True, debug=False)
        #print(self.config)
        #for k, v in log_msgs.items():
            #for msg in v:
                #print(msg)
        createSession(self.config)
        set_Session()

        self.assoc_thresh = get_assoc_thresh(self.config)

    def test_version(self):
        assert(aqms_pdl.__version__)
        print("aqms-pdl ver:%s" % aqms_pdl.__version__)

    def test_ALL(self):

    # 1. Insert PDL Mww, Insert RSN Ml, Test that RSN_evid associates with PDL_evid

    #def test_associate_when_RSN_evid_arrives_after_PDL_evid(self):
        PDL_evid = insert_us_mww(self.config, associate=False)
        PDL_orig = query_origin_by_evid(PDL_evid)
        PDL_orid = PDL_orig.orid
        PDL_Mww_magid = PDL_orig.prefmag
        print("PDL_evid=%d orid=%d" % (PDL_evid, PDL_orid))

        # Insert NC Ml magnitude:
        RSN_evid = insert_nc_1(self.config, associate=False)
        RSN_orig = query_origin_by_evid(RSN_evid)
        RSN_orid = RSN_orig.orid

        print("RSN_evid=%d orid=%d" % (RSN_evid, RSN_orid))

        # MTH: just doing the insert via pdl2aqms, regardless of order, will trigger creation of Assocevents
        #      Unless config.yml associate=False, then the lines below can be tested

        # Test that RSN_evid associated with PDL Mww evid
        assoc_origins = associate_RSN_origin_with_PDL_origins(RSN_orig, self.assoc_thresh)
        create_and_insert_assocevents(RSN_evid, assoc_origins)
        found = query_assocevents(RSN_evid, PDL_evid)
        self.assertTrue(found)

    # 2. Insert RSN Mw, Confirm same RSN_evid, Confirm RSN Mw now preferred on RSN_evid

    #def test_insert_RSN_Mw_mag_and_find_origin_match(self):

        RSN_evid2 = insert_nc_2(self.config, associate=False)
        print("RSN_evid2=%d" % RSN_evid2)

        self.assertEqual(RSN_evid, RSN_evid2)

        RSN_orig2 = query_origin_by_evid(RSN_evid2)
        self.assertEqual(RSN_orig2.orid, RSN_orid)
        print("RSN_orig2.orid=%d" % RSN_orig2.orid)

        RSN_Mw_magid = RSN_orig2.prefmag

        event = get_eventbyid(RSN_evid)
        print("Compare event.prefmag=%d RSN_Mw_magid=%d" % (event.prefmag, RSN_Mw_magid))
        self.assertEqual(event.prefmag, RSN_Mw_magid)


    # 3. Set PDL Mww preferred and confirm
    #def test_setMwwprefmag(self):

        retval = call_procedure('epref.setprefmag_magtype', [RSN_evid, PDL_Mww_magid], self.config)
        print("call epref.setprefmag_magtype(%d, %d) returned:%d" %
              (RSN_evid, PDL_Mww_magid, retval))
        #print("setprefmag_magtype returned retval=[%d]" % retval)
        msg = "ERROR: epref.setprefmag_magtype(%d,%d) returned retval:%d <= 0" % (RSN_evid, PDL_Mww_magid, retval)
        self.assertGreater(retval, 0, msg)

        event = get_eventbyid(RSN_evid)
        self.assertEqual(event.prefmag, PDL_Mww_magid)

    # 4. Unset PDL Mww preferred and confirm magpref falls back to RSN Mw
    #def test_unsetMwwprefmag(self):
        retval = call_procedure('epref.unsetprefmag', [RSN_evid, PDL_Mww_magid], self.config)
        print("call epref.unsetprefmag(%d, %d) returned:%d" %
              (RSN_evid, PDL_Mww_magid, retval))
        msg = "ERROR: epref.unsetprefmag(%d,%d) returned retval:%d <= 0" % (RSN_evid, PDL_Mww_magid, retval)
        self.assertGreater(retval, 0, msg)
        event = get_eventbyid(RSN_evid)
        self.assertEqual(event.prefmag, RSN_Mw_magid)

    def test_insert_nn_msgs(self):
        NN_evid1 = insert_nn_1(self.config)
        NN_evid2 = insert_nn_2(self.config)
        self.assertEqual(NN_evid1, NN_evid2)


    @classmethod
    def tearDownClass(cls):
        print("Call teardownclass")
        files = glob.glob('?.xml')
        for f in files:
            print("Probably <should> remove file:%s" % f)
            #os.remove(f)

def insert_nc_1(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                #'--configFile=./config.yml',
                '--preferred-eventid=nc73584926',
                '--preferred-eventsource=nc',
                '--preferred-eventsourcecode=73584926',
                '--eventids=ew1625784601,pt21189000,nc73584926',
                '--preferred-magnitude=6.15',
                '--preferred-latitude=38.5233345',
                '--preferred-longitude=-119.496666',
                '--preferred-depth=9.83',
                '--preferred-eventtime=2021-07-08T22:49:48.070Z',
                '--directory=test_data/2021-07-08_antelope_valley/archive-qml/nc73584926/1625784820560',
                '--type=origin',
                '--code=nc73584926',
                '--source=nc',
                '--status=UPDATE',
                '--subsource=RT1',
               ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    #args.associate = associate
    config['associate'] = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

def insert_nc_2(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                #'--configFile=config.yml',
                '--preferred-eventid=nc73584926',
                '--preferred-eventsource=nc',
                '--preferred-eventsourcecode=73584926',
                '--eventids=ew1625784601,pt21189000,nc73584926',
                '--preferred-magnitude=5.91',
                '--preferred-latitude=38.5233345',
                '--preferred-longitude=-119.496666',
                '--preferred-depth=9.83',
                '--preferred-eventtime=2021-07-08T22:49:48.070Z',
                '--directory=test_data/2021-07-08_antelope_valley/archive-qml/nc73584926/1625784882760',
                '--type=origin',
                '--code=nc73584926',
                '--source=nc',
                '--status=UPDATE',
                '--subsource=RT2',

               ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    config['associate'] = associate
    return run_pdl_to_aqms(config, quakemlfile, args)


def insert_us_mww(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                #'--configFile=config.yml',
                '--preferred-eventid=nc73584926',
                '--preferred-eventsource=nc',
                '--preferred-eventsourcecode=73584926',
                '--eventids=ew1625784601,ew1625784613,pt21189000,nc73584926,us6000etc0',
                '--preferred-magnitude=5.91',
                '--preferred-latitude=38.5233345',
                '--preferred-longitude=-119.496666',
                '--preferred-depth=9.83',
                '--preferred-eventtime=2021-07-08T22:49:48.070Z',
                '--directory=test_data/2021-07-08_antelope_valley/archive-qml/us_6000etc0_mww/1625785954040',
                '--type=moment-tensor',
                '--code=us_6000etc0_mww',
                '--source=us',
                '--updateTime=2021-07-08T23:12:34.040Z',
                '--status=UPDATE',
                '--action=EVENT_UPDATED',
                '--logdir=./log',
                '--loglevel=INFO',
                ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    config['associate'] = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

def insert_us_EW(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                '--configFile=config.yml',
                '--preferred-eventid=ew1625784601',
                '--preferred-eventsource=ew',
                '--preferred-eventsourcecode=1625784601',
                '--eventids=ew1625784601',
                '--preferred-magnitude=4.8',
                '--preferred-latitude=37.8864',
                '--preferred-longitude=-119.1695',
                '--preferred-depth=9',
                '--preferred-eventtime=2021-07-08T22:50:01.398Z',
                '--directory=./archive-qml/ew1625784601/1625784613626',
                '--type=origin',
                '--code=ew1625784601',
                '--source=ew',
                '--updateTime=2021-07-08T22:50:13.626Z',
                '--status=UPDATE',
                '--action=EVENT_ADDED'
                ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    config['associate'] = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

def insert_nn_1(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                #'--configFile=config.yml',
                '--preferred-eventid=nc73584926',
                '--preferred-eventsource=nc',
                '--preferred-eventsourcecode=73584926',
                '--eventids=nn00811145,ew1625784601,ew1625784613,pt21189000,nc73584926,us6000etc0',
                '--preferred-magnitude=5.91',
                '--preferred-latitude=38.5233345',
                '--preferred-longitude=-119.496666',
                '--preferred-depth=9.83',
                '--preferred-eventtime=2021-07-08T22:49:48.070Z',
                '--directory=test_data/2021-07-08_antelope_valley/archive-qml/nn00811145/1625786801715',
                '--type=origin',
                '--code=nn00811145',
                '--source=nn',
                '--updateTime=2021-07-08T23:26:41.715Z',
                '--status=UPDATE',
                '--action=EVENT_UPDATED',
                ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    config['associate'] = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

def insert_nn_2(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                #'--configFile=config.yml',
                '--preferred-eventid=nc73584926',
                '--preferred-eventsource=nc',
                '--preferred-eventsourcecode=73584926',
                '--eventids=nn00811145,ew1625784601,ew1625784613,pt21189000,nc73584926,us6000etc0',
                '--preferred-magnitude=5.91',
                '--preferred-latitude=38.5233345',
                '--preferred-longitude=-119.496666',
                '--preferred-depth=9.83',
                '--preferred-eventtime=2021-07-08T22:49:48.070Z',
                '--directory=test_data/2021-07-08_antelope_valley/archive-qml/nn00811145/1625787022057',
                '--type=origin',
                '--code=nn00811145',
                '--source=nn',
                '--updateTime=2021-07-08T23:30:22.057Z',
                '--status=UPDATE',
                '--action=EVENT_UPDATED',
                ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    config['associate'] = associate
    return run_pdl_to_aqms(config, quakemlfile, args)


if __name__ == "__main__":

    test = Test()
    test.setUp()
    test.test_version()
    test.test_ALL()
    exit()
    test.test_associate_when_RSN_evid_arrives_after_PDL_evid()
    test.test_insert_RSN_Mw_mag_and_find_origin_match()
    test.test_insert_nn_msgs()
    test.test_setMwwprefmag()
    test.test_unsetMwwprefmag()
    #unittest.main(failfast=True)
