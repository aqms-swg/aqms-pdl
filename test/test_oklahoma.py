#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_pdl2aqms
----------------------------------

Tests for `pdl_to_aqms` module.
"""

import glob
import os
import sys
import unittest

from aqms_pdl.libs.libs_util import archive_quakeml
from aqms_pdl.libs.libs_log import read_config

from aqms_pdl.libs.libs_db import (set_Session, fix_leap_seconds,
                             getSequence, get_eventbyid, call_procedure, get_gtype,
                             query_origins_around_origin_time,
                             query_for_pdl_event_in_aqms,
                             query_assocevents,
                             create_and_insert_assocevents,
                             query_origin_by_evid,
                            )
from aqms_pdl.libs.libs_aqms import get_assoc_thresh, associate_RSN_origin_with_PDL_origins
from aqms_pdl.libs.libs_cmd import process_cmd_line

from aqms_pdl import createSession
import aqms_pdl

from aqms_pdl.libs.run_pdl_to_aqms import run_pdl_to_aqms

class Test(unittest.TestCase):


    def setUp(self):
        #self.config = read_config(configfile='configs/config_OK.yml')
        #os.environ['AQMS_DB_CONFIG'] = './configs/config_OK.yml'
        os.environ['AQMS_DB_CONFIG'] = './configs/config_NC.yml'
        self.config, log_msgs = read_config(requireConfigFile=True, debug=True)
        createSession(self.config)
        print(self.config)
        set_Session()
        self.assoc_thresh = get_assoc_thresh(self.config)
        self.PDL_evid = 1
        self.RSN_evid = 6
        print(self.config)

    def test_version(self):
        #assert(aqms_pdl.__version__ == '0.0.1')
        assert(aqms_pdl.__version__)
        print(aqms_pdl.__version__)

    def test_insert_Mww(self):
        PDL_evid = insert_ok_1(self.config)
        print("1st ok evid:%d" % PDL_evid)
        PDL_evid = insert_ok_2(self.config)
        print("2nd ok evid:%d" % PDL_evid)

    def xtest_insert_RSN(self):
        # OK is not an authoritative region in geo_region,
        #  hence there is no way to make this the local RSN_evid
        RSN_evid = insert_ogs(self.config, associate=True)
        RSN_orig = query_origin_by_evid(RSN_evid)
        assoc_origins = associate_RSN_origin_with_PDL_origins(RSN_orig, self.assoc_thresh)
        create_and_insert_assocevents(RSN_evid, assoc_origins)
        found = query_assocevents(RSN_evid, self.PDL_evid)
        self.assertTrue(found)


    def xtest_setMwwprefmag(self):
        # self.Mww_magid is same self.PDL_evid
        retval = call_procedure('epref.setprefmag_magtype', [self.RSN_evid, self.PDL_evid], self.config)
        print("setprefmag_magtype returned retval=[%d]" % retval)
        msg = "ERROR: epref.setprefmag_magtype(%d,%d) returned retval:%d <= 0" % (self.RSN_evid, self.PDL_evid, retval)
        self.assertGreater(retval, 0, msg)

    def Xtest_unsetMwwprefmag(self):
        retval = call_procedure('epref.unsetprefmag', [self.RSN_evid, self.Mww_magid], self.config)
        print("unsetprefmag returned retval=[%d]" % retval)
        msg = "ERROR: epref.unsetprefmag(%d,%d) returned retval:%d <= 0" % (self.RSN_evid, self.Mww_magid, retval)
        self.assertGreater(retval, 0, msg)


    @classmethod
    def tearDownClass(cls):
        print("Call teardownclass")
        #files = glob.glob('?.xml')
        #for f in files:
            #os.remove(f)

def insert_ok_1(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                '--configFile=./config_OK.yml',
                '--ignore-magnitude-below=4.0',
                '--type=internal-moment-tensor',
                '--preferred-eventid=usauto400063yy',
                '--eventids=usauto400063yy',
                '--preferred-eventsource=usauto',
                '--preferred-eventsourcecode=400063yy',
                '--code=us_400063yy_mww',
                '--source=us',
                '--preferred-magnitude=null',
                '--preferred-latitude=36.9497',
                '--preferred-longitude=-98.1091',
                '--preferred-depth=16.7',
                '--preferred-eventtime=2021-02-19T13:56:57.800Z',
                '--directory=test_data/2021-02-19_Oklahoma/archive-qml/us_400063yy_mww/1613743193040',
                '--updateTime=2021-02-19T13:59:53.040Z',
                '--status=UPDATE',
                '--property-derived-magnitude=5.4',
                ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    #args.associate = associate
    config['associate'] = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

def insert_ok_2(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                '--configFile=./config_OK.yml',
                '--ignore-magnitude-below=4.0',
                '--type=origin',
                '--code=us6000diwi',
                '--source=us',
                '--preferred-eventid=us6000diwi',
                '--preferred-eventsource=us',
                '--preferred-eventsourcecode=6000diwi',
                '--eventids=usauto400063yy,us6000diwi',
                '--preferred-magnitude=4.5',
                '--preferred-latitude=36.9575',
                '--preferred-longitude=-98.0920',
                '--preferred-depth=18.83',
                '--preferred-eventtime=2021-02-19T13:56:57.844Z',
                '--directory=test_data/2021-02-19_Oklahoma/archive-qml/us6000diwi/1613743985040',
                '--updateTime=2021-02-19T14:13:05.040Z',
                '--status=UPDATE',
                '--action=EVENT_UPDATED',
                ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    #args.associate = associate
    config['associate'] = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

def insert_ogs(config, associate=True):
    sys.argv = ['pdl-to-aqms',
                '--configFile=./config_OK.yml',
                '--ignore-magnitude-below=4.0',
                '--type=origin',
                '--code=ogs2021dmpg',
                '--source=ok',
                '--preferred-eventid=ok2021dmpg',
                '--preferred-eventsource=ok',
                '--preferred-eventsourcecode=2021dmpg',
                '--eventids=usauto400063yy,ok2021dmpg,us6000diwi',
                '--preferred-magnitude=4.75',
                '--preferred-latitude=36.96366667',
                '--preferred-longitude=-98.09383333',
                '--preferred-depth=7.0',
                '--preferred-eventtime=2021-02-19T13:56:57.950Z',
                '--directory=test_data/2021-02-19_Oklahoma/archive-qml/ogs2021dmpg/1613744874118',
                '--updateTime=2021-02-19T14:27:54.118Z',
                '--status=UPDATE',
                '--trackerURL=http://ehppdl1.cr.usgs.gov/tracker',
                '--subsource=NOT-PDL',
                '--action=EVENT_UPDATED',
                ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    #args.associate = associate
    config['associate'] = associate
    # Fix code so that local pdl_id looks like an evid (an int):
    args.code = 'ok12345678'
    args.eventids=['usauto400063yy','ok12345678','us6000diwi']
    return run_pdl_to_aqms(config, quakemlfile, args)

if __name__ == "__main__":
    test = Test()
    test.setUp()
    test.test_insert_Mww()
    #unittest.main()
