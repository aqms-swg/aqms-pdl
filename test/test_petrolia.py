#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_pdl2aqms
----------------------------------

Tests for `pdl_to_aqms` module.
"""

import glob
import os
import sys
import unittest

from aqms_pdl.libs.libs_util import read_config, archive_quakeml
from aqms_pdl.libs.libs_db import (set_Session, fix_leap_seconds,
                             getSequence, get_eventbyid, call_procedure, get_gtype,
                             query_origins_around_origin_time,
                             query_for_pdl_event_in_aqms,
                             query_assocevents,
                             create_and_insert_assocevents,
                             query_origin_by_evid,
                            )
from aqms_pdl.libs.libs_aqms import get_assoc_thresh, associate_RSN_origin_with_PDL_origins
from aqms_pdl.libs.libs_cmd import process_cmd_line

from aqms_pdl import createSession
import aqms_pdl

from aqms_pdl.libs.run_pdl_to_aqms import run_pdl_to_aqms

#test_data/2021-12-20_Petrolia/nc73666231

'''
The problem with the 2021-12-20 Petrolia M6.2 PDL sequence is:

myarch=> select * from event;
 evid | prefor | prefmag | prefmec | commid | auth | subsource | etype | selectflag | version |           lddate
------+--------+---------+---------+--------+------+-----------+-------+------------+---------+----------------------------
    1 |      1 |       1 |       1 |        | US   | PDL       | eq    |          1 |       0 | 2021-12-22 20:52:06.219583
    6 |      6 |       6 |         |        | EW   | PDL       | eq    |          1 |       0 | 2021-12-22 20:52:09.206877
   11 |     11 |      11 |       6 |        | US   | PDL       | eq    |          1 |       0 | 2021-12-22 20:52:12.782479
   21 |     21 |      21 |      11 |        | US   | PDL       | eq    |          1 |       0 | 2021-12-22 20:52:17.479095
   16 |     31 |      26 |         |        | NC   | RT1       | eq    |          1 |       0 | 2021-12-22 20:52:15.043921
(5 rows)

(aqms) mth@mikes-mbp [~/mth/python_pkgs/aqms-pdl/test]> compare-evids -e 1,16
origin orid: 1 evid:1  prefmag:1    auth:US datetime:1640031044.8 lat:40.31 lon:-124.76 depth:35.0 subsource:PDL locevid:40008q39
origin orid:31 evid:16 prefmag:None auth:NC datetime:1640031045.4 lat:40.31 lon:-124.74 depth: 9.3 subsource:RT1 locevid:73666231

compare-evids: dt=0.6 (sec) dx=2.0 (km) dz=25.7 (km) ** The first US _mww message is way too deep to associate with NC evid=16

(aqms) mth@mikes-mbp [~/mth/python_pkgs/aqms-pdl/test]> compare-evids -e 6,16
origin orid:6  evid:6  prefmag:6    auth:EW datetime:1640031048.4 lat:40.30 lon:-124.62 depth:9.0 subsource:PDL locevid:1640031020
origin orid:31 evid:16 prefmag:None auth:NC datetime:1640031045.4 lat:40.31 lon:-124.74 depth:9.3 subsource:RT1 locevid:73666231
compare-evids: dt=3.0 (sec) dx=9.9 (km) dz=0.3 (km)  ** The EW message time is 3.0 secs off NC evid=16 time so won't easily associate

origin orid:21 evid:21 prefmag:21   auth:US datetime:1640031042.9 lat:40.35 lon:-124.91 depth:22.0 subsource:PDL locevid:6000gdxq
origin orid:31 evid:16 prefmag:None auth:NC datetime:1640031045.4 lat:40.31 lon:-124.74 depth:9.3  subsource:RT1 locevid:73666231
compare-evids: dt=2.5 (sec) dx=15.1 (km) dz=12.7 (km) ** dt= 2.5 compared to 3rd US _mww message - 3 different systems/pdl_ids

'''


class Test(unittest.TestCase):

    def setUp(self):
        self.config = read_config(configfile='./config_NC.yml')
        createSession(self.config)
        set_Session()
        self.PDL_evid = 1
        self.RSN_evid = 6
        self.Mww_magid = 1
        self.Ml_magid = 6
        self.Mw_magid = 11
        self.assoc_thresh = get_assoc_thresh(self.config)

    def test_version(self):
        assert(aqms_pdl.__version__)
        print(aqms_pdl.__version__)

    def test_insert_msgs_in_order_received(self):
        # msg+6 = 'at' - has no quakeml
        for msg in (msg_1, msg_2, msg_3, msg_4, msg_5, msg_7, msg_8, msg_9, msg_10):
            PDL_evid = msg(self.config)
            print("PDL_evid=%d" % PDL_evid)

    def Xtest_associate_when_RSN_evid_arrives_after_PDL_evid(self):
        PDL_evid = insert_us_mww(self.config)
        # Insert NC Ml magnitude:
        RSN_evid = insert_nc_1(self.config, associate=False)

        #RSN_evid = insert_nc_1(self.config, associate=True)
        #self.assertEqual(PDL_evid, 1)
        #self.assertEqual(RSN_evid, 6)
        # MTH: just doing the insert via pdl2aqms, regardless of order, will trigger creation of Assocevents
        #      Unless config.yml associate=False, then the lines below can be tested

        RSN_orig = query_origin_by_evid(RSN_evid)
        assoc_origins = associate_RSN_origin_with_PDL_origins(RSN_orig, self.assoc_thresh)
        create_and_insert_assocevents(RSN_evid, assoc_origins)
        found = query_assocevents(RSN_evid, PDL_evid)
        self.assertTrue(found)

    def Xtest_insert_RSN_Mw_mag_and_find_origin_match(self):
        RSN_evid = insert_nc_2(self.config, associate=False)
        self.assertEqual(RSN_evid, self.RSN_evid)
        RSN_orig = query_origin_by_evid(RSN_evid)
        self.assertEqual(RSN_orig.orid, self.RSN_evid)
        print("Yes to using RSN_orid:%d" % RSN_orig.orid)

    def Xtest_insert_nn_msgs(self):
        NN_evid1 = insert_nn_1(self.config)
        NN_evid2 = insert_nn_2(self.config)
        self.assertEqual(NN_evid1, NN_evid2)

    def Xtest_setMwwprefmag(self):
        retval = call_procedure('epref.setprefmag_magtype', [self.RSN_evid, self.Mww_magid], self.config)
        #print("setprefmag_magtype returned retval=[%d]" % retval)
        msg = "ERROR: epref.setprefmag_magtype(%d,%d) returned retval:%d <= 0" % (self.RSN_evid, self.Mww_magid, retval)
        self.assertGreater(retval, 0, msg)
        event = get_eventbyid(self.RSN_evid)
        self.assertEqual(event.prefmag, self.Mww_magid)

    def Xtest_unsetMwwprefmag(self):
        retval = call_procedure('epref.unsetprefmag', [self.RSN_evid, self.Mww_magid], self.config)
        #print("unsetprefmag returned retval=[%d]" % retval)
        msg = "ERROR: epref.unsetprefmag(%d,%d) returned retval:%d <= 0" % (self.RSN_evid, self.Mww_magid, retval)
        self.assertGreater(retval, 0, msg)
        event = get_eventbyid(self.RSN_evid)
        self.assertEqual(event.prefmag, self.Mw_magid)


    @classmethod
    def tearDownClass(cls):
        print("Call teardownclass")
        #files = glob.glob('?.xml')
        #for f in files:
            #os.remove(f)

#EVENT_ADDED null => usauto40008q39
def msg_1(config, associate=True):
    sys.argv = ['pdl-to-aqms',
        '--configFile=../config.yml',
        '--preferred-eventid=usauto40008q39',
        '--preferred-eventsource=usauto',
        '--preferred-eventsourcecode=40008q39',
        '--eventids=usauto40008q39',
        '--preferred-magnitude=null',
        '--preferred-latitude=40.3083',
        '--preferred-longitude=-124.7612',
        '--preferred-depth=35',
        '--preferred-eventtime=2021-12-20T20:10:17.800Z',
        '--directory=test_data/2021-12-20_Petrolia/us_40008q39_mww/us/1640031176040',
        '--type=internal-moment-tensor',
        '--code=us_40008q39_mww',
        '--source=us',
        '--updateTime=2021-12-20T20:12:56.040Z',
        '--status=UPDATE',
        '--property-derived-magnitude=6.2',
        '--property-derived-magnitude-type=Mww',
        '--action=EVENT_ADDED'
       ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_UPDATED usauto40008q39 => ew1640031020
def msg_2(config, associate=True):
    sys.argv = ['pdl-to-aqms',
    '--configFile=../config.yml',
    '--preferred-eventid=ew1640031020',
    '--preferred-eventsource=ew',
    '--preferred-eventsourcecode=1640031020',
    '--eventids=ew1640031020,usauto40008q39',
    '--preferred-magnitude=5.5',
    '--preferred-latitude=40.3032',
    '--preferred-longitude=-124.6239',
    '--preferred-depth=9',
    '--preferred-eventtime=2021-12-20T20:10:21.400Z',
    '--directory=test_data/2021-12-20_Petrolia/ew1640031020/ew/1640031046557',
    '--type=origin',
    '--code=ew1640031020',
    '--source=ew',
    '--updateTime=2021-12-20T20:10:46.557Z',
    '--status=UPDATE',
    '--action=EVENT_UPDATED'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_ADDED null => usautoa000h3ts
def msg_3(config, associate=True):
    sys.argv = ['pdl-to-aqms',
    '--configFile=./config.yml',
    '--preferred-eventid=usautoa000h3ts',
    '--preferred-eventsource=usauto',
    '--preferred-eventsourcecode=a000h3ts',
    '--eventids=usautoa000h3ts',
    '--preferred-magnitude=null',
    '--preferred-latitude=40.2748',
    '--preferred-longitude=-124.9751',
    '--preferred-depth=19.7',
    '--preferred-eventtime=2021-12-20T20:10:14.700Z',
    '--directory=test_data/2021-12-20_Petrolia/us_a000h3ts_mww/us/1640031194040',
    '--type=internal-moment-tensor',
    '--code=us_a000h3ts_mww',
    '--source=us',
    '--updateTime=2021-12-20T20:13:14.040Z',
    '--status=UPDATE',
    '--action=EVENT_ADDED',
    '--property-derived-magnitude-type=Mww',
    '--property-derived-magnitude=6.2'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_UPDATED ew1640031020 => nc73666231
def msg_4(config, associate=True, subsource='RT1'):
    sys.argv = ['pdl-to-aqms',
    '--configFile=./config.yml',
    '--preferred-eventid=nc73666231',
    '--preferred-eventsource=nc',
    '--preferred-eventsourcecode=73666231',
    '--eventids=ew1640031020,usauto40008q39,nc73666231',
    '--preferred-magnitude=5.78',
    '--preferred-latitude=40.3148346',
    '--preferred-longitude=-124.7391663',
    '--preferred-depth=9.34',
    '--preferred-eventtime=2021-12-20T20:10:18.440Z',
    '--directory=test_data/2021-12-20_Petrolia/nc73666231/nc/1640031242630',
    '--type=origin',
    '--code=nc73666231',
    '--source=nc',
    '--updateTime=2021-12-20T20:14:02.630Z',
    '--status=UPDATE',
    '--action=EVENT_ADDED'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    args.subsource = subsource
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_ADDED null => usauto6000gdxq
def msg_5(config, associate=True):
    sys.argv = ['pdl-to-aqms',
    '--configFile=./config.yml',
    '--preferred-eventid=usauto6000gdxq',
    '--preferred-eventsource=usauto',
    '--preferred-eventsourcecode=6000gdxq',
    '--eventids=usauto6000gdxq',
    '--preferred-magnitude=null',
    '--preferred-latitude=40.3521',
    '--preferred-longitude=-124.9099',
    '--preferred-depth=22',
    '--preferred-eventtime=2021-12-20T20:10:15.900Z',
    '--directory=test_data/2021-12-20_Petrolia/us_6000gdxq_mww/us/1640031250040',
    '--type=internal-moment-tensor',
    '--code=us_6000gdxq_mww',
    '--source=us',
    '--updateTime=2021-12-20T20:14:10.040Z',
    '--status=UPDATE',
    '--property-derived-magnitude=6.2',
    '--property-derived-magnitude-type=Mww'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_UPDATED nc73666231 => nc73666231
def msg_6(config, associate=True):
    sys.argv = ['pdl-to-aqms',
    '--configFile=./config.yml',
    '--preferred-eventid=nc73666231',
    '--preferred-eventsource=nc',
    '--preferred-eventsourcecode=73666231',
    '--eventids=ew1640031020,at00r4fk16,pt21354001,usauto40008q39,nc73666231',
    '--preferred-magnitude=5.78',
    '--preferred-latitude=40.3148346',
    '--preferred-longitude=-124.7391663',
    '--preferred-depth=9.34',
    '--preferred-eventtime=2021-12-20T20:10:18.440Z',
    '--directory=test_data/2021-12-20_Petrolia/at00r4fk16/at/1640031309642',
    '--type=origin',
    '--code=at00r4fk16',
    '--source=at',
    '--updateTime=2021-12-20T20:15:09.642Z',
    '--status=UPDATE',
    '--action=EVENT_UPDATED'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_UPDATED nc73666231 => nc73666231
def msg_7(config, associate=True, subsource='RT1'):
    sys.argv = ['pdl-to-aqms',
    '--configFile=./config.yml',
    '--preferred-eventid=nc73666231',
    '--preferred-eventsource=nc',
    '--preferred-eventsourcecode=73666231',
    '--eventids=ew1640031020,at00r4fk16,pt21354001,usauto40008q39,nc73666231',
    '--preferred-magnitude=6.22',
    '--preferred-latitude=40.3148346',
    '--preferred-longitude=-124.7391663',
    '--preferred-depth=9.34',
    '--preferred-eventtime=2021-12-20T20:10:18.440Z',
    '--directory=test_data/2021-12-20_Petrolia/nc73666231/nc/1640031329580',
    '--type=origin',
    '--code=nc73666231',
    '--source=nc',
    '--updateTime=2021-12-20T20:15:29.580Z',
    '--status=UPDATE',
    '--action=EVENT_UPDATED'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    args.subsource = subsource
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_UPDATED nc73666231 => nc73666231
def msg_8(config, associate=True, subsource='RT1'):
    sys.argv = ['pdl-to-aqms',
    '--configFile=./config.yml',
    '--preferred-eventid=nc73666231',
    '--preferred-eventsource=nc',
    '--preferred-eventsourcecode=73666231',
    '--eventids=ew1640031020,at00r4fk16,pt21354001,usauto40008q39,nc73666231',
    '--preferred-magnitude=6.22',
    '--preferred-latitude=40.3148346',
    '--preferred-longitude=-124.7391663',
    '--preferred-depth=9.34',
    '--preferred-eventtime=2021-12-20T20:10:18.440Z',
    '--directory=test_data/2021-12-20_Petrolia/nc73666231_fm1/nc/1640031335040',
    '--type=focal-mechanism',
    '--code=nc73666231_fm1',
    '--source=nc',
    '--updateTime=2021-12-20T20:15:35.040Z',
    '--status=UPDATE',
    '--action=EVENT_UPDATED'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    args.subsource = subsource
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_UPDATED nc73666231 => nc73666231
def msg_9(config, associate=True, subsource='RT1'):
    sys.argv = ['pdl-to-aqms',
    '--configFile=./config.yml',
    '--preferred-eventid=nc73666231',
    '--preferred-eventsource=nc',
    '--preferred-eventsourcecode=73666231',
    '--eventids=ew1640031020,at00r4fk16,pt21354001,usauto40008q39,nc73666231',
    '--preferred-magnitude=6.22',
    '--preferred-latitude=40.3148346',
    '--preferred-longitude=-124.7391663',
    '--preferred-depth=9.34',
    '--preferred-eventtime=2021-12-20T20:10:18.440Z',
    '--directory=test_data/2021-12-20_Petrolia/nc73666231_mt1/nc/1640031336340',
    '--type=moment-tensor',
    '--code=nc73666231_mt1',
    '--source=nc',
    '--updateTime=2021-12-20T20:15:36.340Z',
    '--status=UPDATE',
    '--action=EVENT_UPDATED'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    args.subsource = subsource
    return run_pdl_to_aqms(config, quakemlfile, args)

#EVENT_UPDATED nc73666231 => nc73666231
def msg_10(config, associate=True, subsource='RT1'):
    sys.argv = ['pdl-to-aqms',
    '--configFile=./config.yml',
    '--preferred-eventid=nc73666231',
    '--preferred-eventsource=nc',
    '--preferred-eventsourcecode=73666231',
    '--eventids=ew1640031020,at00r4fk16,pt21354001,usauto40008q39,nc73666231',
    '--preferred-magnitude=6.22',
    '--preferred-latitude=40.3148346',
    '--preferred-longitude=-124.7391663',
    '--preferred-depth=9.34',
    '--preferred-eventtime=2021-12-20T20:10:18.440Z',
    '--directory=test_data/2021-12-20_Petrolia/nc73666231_fm2/nc/1640031336370',
    '--type=focal-mechanism',
    '--code=nc73666231_fm2',
    '--source=nc',
    '--updateTime=2021-12-20T20:15:36.370Z',
    '--status=UPDATE',
    '--action=EVENT_UPDATED'
    ]
    quakemlfile, args = process_cmd_line('pdl-to-aqms', config)
    args.associate = associate
    args.subsource = subsource
    return run_pdl_to_aqms(config, quakemlfile, args)

if __name__ == "__main__":
    unittest.main()
