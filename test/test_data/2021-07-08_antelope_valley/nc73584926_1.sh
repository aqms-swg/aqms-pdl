#!/bin/bash

# --ignore-sources=NC \

pdl-to-aqms \
  --configFile=config.yml \
  --preferred-eventid=nc73584926 \
  --preferred-eventsource=nc \
  --preferred-eventsourcecode=73584926 \
  --eventids=ew1625784601,pt21189000,nc73584926 \
  --preferred-magnitude=6.15 \
  --preferred-latitude=38.5233345 \
  --preferred-longitude=-119.496666 \
  --preferred-depth=9.83 \
  --preferred-eventtime=2021-07-08T22:49:48.070Z \
  --directory=archive-qml/nc73584926/1625784820560 \
  --type=origin \
  --code=nc73584926 \
  --source=nc \
  --status=UPDATE \
  --subsource='RT1'
