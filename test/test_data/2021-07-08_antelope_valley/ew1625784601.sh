#!/bin/sh
pdl-to-aqms   \
    --configFile=config.yml    \
    --ignore-magnitude-below=3.5    \
    --ignore-sources=NC    \
    --ignore-latitude-above=42.5    \
    --ignore-latitude-below=34    \
    --ignore-longitude-above=-117.25    \
    --ignore-longitude-below=-128    \
    --preferred-eventid=ew1625784601    \
    --preferred-eventsource=ew    \
    --preferred-eventsourcecode=1625784601    \
    --eventids=ew1625784601    \
    --preferred-magnitude=4.8    \
    --preferred-latitude=37.8864    \
    --preferred-longitude=-119.1695    \
    --preferred-depth=9    \
    --preferred-eventtime=2021-07-08T22:50:01.398Z    \
    --directory=./archive-qml/ew1625784601/1625784613626    \
    --type=origin    \
    --code=ew1625784601    \
    --source=ew    \
    --updateTime=2021-07-08T22:50:13.626Z    \
    --status=UPDATE    \
    --trackerURL=http://ehppdl1.cr.usgs.gov/tracker/    \
    --property-magnitude-type=mw    \
    --property-quakeml-magnitude-publicid=quakeml:ci.anss.org/magnitude/1625784601/Mw    \
    --property-latitude=37.8864    \
    --property-magnitude-num-stations-used=6    \
    --property-eventtime=2021-07-08T22:50:01.398Z    \
    --property-eventsource=ew    \
    --property-latitude-error=0.0791    \
    --property-pdl-client-version=Version    \
    2.3.0    \
    2020-04-04    \
    --property-eventParametersPublicID=quakeml:ci.anss.org/eventParameters/1625784601/1625784613    \
    --property-horizontal-error=12.43    \
    --property-eventsourcecode=1625784601    \
    --property-magnitude=4.8    \
    --property-longitude-error=0.0791    \
    --property-longitude=-119.1695    \
    --property-event-type=earthquake    \
    --property-quakeml-origin-publicid=quakeml:ci.anss.org/origin/1625784601    \
    --property-review-status=automatic    \
    --property-version=0    \
    --property-num-phases-used=0    \
    --property-num-stations-used=6    \
    --property-quakeml-publicid=quakeml:ci.anss.org/event/1625784601    \
    --property-vertical-error=3.535    \
    --property-eventtime-error=1.2073    \
    --property-depth=9    \
    --property-evaluation-status=preliminary    \
    --property-magnitude-error=0.3    \
    --signature=t0qDWugYMo2XB8J68lgmEw5OqlIMmVrMIPIWsAl9liBpsVpDXjlGnLUNtqw1yVgjkx2AG0ThiXnijR47qSFGfpEKlqdIRBZNKuDL82B7XD4+RzGWWfC5lVQ4HTXMAp/yWqZ6NcEB+QHEA5XNYWSwYeNFgDknlqGgc318CTwzHKnOjxeIuBt/JespVldiaefjeUqAZQC0wMTaqiKKErflXn9/To9iFW1W0iZREA/Fmru8nJWkC/UiluFImymnhR/KDhxD7LiOj7TwhLC/wwkRGXffFU5F0StkSriF5e+bEnmokdmuLnPEj1ocPTkMoX0fNtwJEsO5W1JkjwcL/XFIFQ==    \
    --action=EVENT_ADDED
