#!/bin/bash
#                      code = ogs2021dmpg
#         preferred_eventid = ok2021dmpg     eventids = usauto400063yy,ok2021dmpg,us6000diwi \
#                    source = ok
#     preferred_eventsource = ok
# preferred_eventsourcecode = 2021dmpg
#--ignore-event-after=4hrs \

pdl-to-aqms \
	--configFile=./config.yml \
  --ignore-magnitude-below=4.0 \
	--type=origin \
	--code=ogs2021dmpg \
	--source=ok \
	--preferred-eventid=ok2021dmpg \
	--preferred-eventsource=ok \
	--preferred-eventsourcecode=2021dmpg \
	--eventids=usauto400063yy,ok2021dmpg,us6000diwi \
	--preferred-magnitude=4.75 \
	--preferred-latitude=36.96366667 \
	--preferred-longitude=-98.09383333 \
	--preferred-depth=7.0 \
	--preferred-eventtime=2021-02-19T13:56:57.950Z \
	--directory=archive-qml/ogs2021dmpg/1613744874118 \
	--updateTime=2021-02-19T14:27:54.118Z \
	--status=UPDATE \
	--trackerURL=http://ehppdl1.cr.usgs.gov/tracker \
  --ignore-arrivals \
  --ignore-amplitudes \
  --subsource='NOT-PDL' \
	"--property-magnitude-type=ml" "--property-origin-source=OK" "--property-quakeml-magnitude-publicid=smi:org.gfz-potsdam.de/geofon/Magnitude/20210219142122.623045.21674" "--property-latitude=36.96366667" "--property-magnitude-num-stations-used=39" "--property-magnitude-source=OK" "--property-eventtime=2021-02-19T13:56:57.950Z" "--property-eventsource=ok" "--property-latitude-error=0.1414213562" "--property-pdl-client-version=Version 2.1.0 2019-03-07" "--property-standard-error=0.15" "--property-eventParametersPublicID=smi:org.gfz-potsdam.de/geofon/ok/1613744874.118146" "--property-eventsourcecode=2021dmpg" "--property-magnitude=4.75" "--property-longitude-error=0.1414213562" "--property-longitude=-98.09383333" "--property-minimum-distance=0.0" "--property-azimuthal-gap=96.0" "--property-event-type=earthquake" "--property-quakeml-origin-publicid=smi:org.gfz-potsdam.de/geofon/Origin/20210219142001.914184.21566" "--property-depth-type=operator assigned" "--property-review-status=reviewed" "--property-num-phases-used=182" "--property-num-stations-used=98" "--property-quakeml-publicid=smi:org.gfz-potsdam.de/geofon/ogs2021dmpg" "--property-vertical-error=0.3" "--property-depth=7.0" "--property-evaluation-status=confirmed" "--property-magnitude-error=0.23" --signature=MC0CFQCIMnx2CEpjyxG98RXzb2HXJ6itXAIUHYNpUkauSueVn6MkIVOEd2WGz4E= --action=EVENT_UPDATED
