
#                      code = us6000diwi
#         preferred_eventid = us6000diwi     eventids = usauto400063yy, us6000diwi
#                    source = us
#     preferred_eventsource = us
# preferred_eventsourcecode = 6000diwi
#--ignore-event-after=4hrs \

pdl-to-aqms \
	--configFile=./config.yml \
	--ignore-magnitude-below=4.0 \
	--type=origin \
	--code=us6000diwi \
	--source=us \
	--preferred-eventid=us6000diwi \
	--preferred-eventsource=us \
	--preferred-eventsourcecode=6000diwi \
	--eventids=usauto400063yy,us6000diwi \
	--preferred-magnitude=4.5 \
	--preferred-latitude=36.9575 \
	--preferred-longitude=-98.0920 \
	--preferred-depth=18.83 \
	--preferred-eventtime=2021-02-19T13:56:57.844Z \
	--directory=archive-qml/us6000diwi/1613743985040 \
	--updateTime=2021-02-19T14:13:05.040Z \
	--status=UPDATE \
	--trackerURL=http://ehppdl1.cr.usgs.gov/tracker/ \
	"--property-magnitude-type=mb" "--property-origin-source=us" "--property-quakeml-magnitude-publicid=quakeml:us.anss.org/magnitude/6000diwi/mb" "--property-latitude=36.9575" "--property-magnitude-num-stations-used=39" "--property-magnitude-source=us" "--property-eventtime=2021-02-19T13:56:57.844Z" "--property-eventsource=us" "--property-latitude-error=0.0117" "--property-pdl-client-version=Version 2.5.1 2020-06-25" "--property-standard-error=0.36" "--property-eventParametersPublicID=quakeml:us.anss.org/eventparameters/6000diwi/1613743987" "--property-horizontal-error=1.8" "--property-error-ellipse-azimuth=110" "--property-eventsourcecode=6000diwi" "--property-magnitude=4.5" "--property-longitude-error=0.0281" "--property-longitude=-98.0920" "--property-minimum-distance=0.207" "--property-azimuthal-gap=31" "--property-event-type=earthquake" "--property-quakeml-origin-publicid=quakeml:us.anss.org/origin/6000diwi" "--property-error-ellipse-minor=2000" "--property-depth-type=from location" "--property-error-ellipse-intermediate=3600" "--property-error-ellipse-major=5800" "--property-error-ellipse-rotation=153" "--property-review-status=reviewed" "--property-num-phases-used=58" "--property-quakeml-publicid=quakeml:us.anss.org/event/6000diwi" "--property-vertical-error=3.7" "--property-eventtime-error=0.73" "--property-depth=18.83" "--property-evaluation-status=preliminary" "--property-magnitude-error=0.086" "--property-error-ellipse-plunge=74" --signature=MCwCFCT7BAo7frbKiE9cwzyqA0Ya+KPUAhRonQSf7cXYE7AfhPrw3w9QAKLgCA== --action=EVENT_UPDATED
