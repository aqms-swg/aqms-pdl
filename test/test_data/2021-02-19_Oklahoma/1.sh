#!/bin/bash

pdl-to-aqms \
	--configFile=./config.yml \
	--ignore-magnitude-below=4.0 \
	--type=internal-moment-tensor \
	--preferred-eventid=usauto400063yy \
	--eventids=usauto400063yy \
	--preferred-eventsource=usauto \
	--preferred-eventsourcecode=400063yy \
	--code=us_400063yy_mww \
	--source=us \
	--preferred-magnitude=null \
	--preferred-latitude=36.9497 \
	--preferred-longitude=-98.1091 \
	--preferred-depth=16.7 \
	--preferred-eventtime=2021-02-19T13:56:57.800Z \
	--directory=archive-qml/us_400063yy_mww/1613743193040 \
	--updateTime=2021-02-19T13:59:53.040Z \
	--status=UPDATE \
  --property-derived-magnitude=5.4
