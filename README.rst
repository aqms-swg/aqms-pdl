===============================
AQMS PDL Tools
===============================

.. image:: https://img.shields.io/travis/mhagerty/aqms-pdl.svg
        :target: https://travis-ci.org/mhagerty/aqms-pdl

.. image:: https://img.shields.io/pypi/v/aqms-pdl.svg
        :target: https://pypi.python.org/pypi/aqms-pdl


Tools for converting between PDL quakeml and AQMS db

* Free software: MIT license
* Documentation: (COMING SOON!) https://aqms-pdl.readthedocs.org.

Installation
-------------

      >pip install some_path_to_whl

Usage
-----------

The pip install makes the following command-line scripts available on your path:

      >pdl-to-aqms 


