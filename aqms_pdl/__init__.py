# -*- coding: utf-8 -*-
from __future__ import print_function
#from ._version import get_versions

__author__ = 'Mike Hagerty'
__email__ = 'mhagerty@isti.com'
__version__ = '0.0.3'
#__version__ = get_versions()['version']
#del get_versions

import os
def get_format_dir():
    this_dir = os.path.abspath(os.path.dirname(__file__))
    formats_dir = os.path.join(this_dir, "formats")
    return formats_dir

def installation_dir():
  return os.path.dirname(os.path.realpath(__file__))

def resource_dir():
  return os.path.join(installation_dir(), 'resources')


import logging
logger = logging.getLogger()

from sqlalchemy import create_engine, engine_from_config
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError
from sqlalchemy.exc import ProgrammingError

Session = None
def createSession(config, echo=False, readonly=False):
    global Session
    engine = engine_from_config(config, echo=echo)
    try:
        conn = engine.connect()
        if readonly:
            conn = conn.execution_options(
                #isolation_level="SERIALIZABLE",
                postgresql_readonly=True,
                #postgresql_deferrable=True,
            )
    except OperationalError as e:
        _msg = f"createSession: Unable to connect to db. Caught error:{e}"
        raise Exception(_msg)

    Session = sessionmaker(bind=engine)
    return None
