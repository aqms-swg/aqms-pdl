CREATE TABLE ASSOCEVENTS
 (      EVID BIGINT NOT NULL ,
        EVIDASSOC BIGINT NOT NULL ,
        COMMID BIGINT,
        LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
        CONSTRAINT ASSOCEVENTS_PK PRIMARY KEY (EVID, EVIDASSOC),
        CONSTRAINT Evid1 FOREIGN KEY (EVID) references Event(evid),
        CONSTRAINT Evid2 FOREIGN KEY (EVIDASSOC) references Event(evid),
        CONSTRAINT ASSOCEVENTS_01 CHECK (EVID != EVIDASSOC)
 );
 CREATE UNIQUE INDEX unique_combinations ON ASSOCEVENTS (least(evid, evidassoc), greatest(evid, evidassoc));
