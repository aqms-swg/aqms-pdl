# -*- coding: utf-8 -*-
"""
Read in quakeml file and insert into AQMS db

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import argparse
import io
import logging
import numpy as np
import os
import sys
from pathlib import Path
import time

from obspy.core.event import read_events
from obspy.geodetics.base import gps2dist_azimuth

from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg, optional_arg
from aqms_pdl.libs.libs_amps import scan_xml_amps, unassoc_amplitude_to_aqms
from aqms_pdl.libs.libs_aqms import (obspy_event_to_aqms, obspy_origin_to_aqms, obspy_magnitude_to_aqms,
                                     obspy_arrival_to_aqms, obspy_amplitude_to_aqms,
                                     obspy_amplitude_to_assocamm, obspy_amplitude_to_assocamo,
                                     obspy_arrival_to_assocaro, obspy_focal_mechanism_to_aqms,
                                     aqmsprefor, aqmsprefmag, aqmsprefmec, summarize)
from aqms_pdl.libs.libs_db import test_connection, call_procedure, get_gtype
#from aqms_pdl.libs.libs_db import pg_dbase_is_read_only
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config, read_config
from aqms_pdl.libs.libs_obs import scan_quakeml
from aqms_pdl.libs.libs_util import archive_quakeml
from aqms_pdl.libs.schema_aqms_PI import Eventprefmag, Eventprefor, Eventprefmec, Origin, Event

KM_PER_DEG = 111.19

logger = None


fname = 'qml_to_aqms'
def main():
    """
    Insert quakeml into aqms db tables
    """
    global logger

    r = required_arg(arg=['qmlfile'], type=str, description='quakeml filename')
    optional_args = []
    optional_args.append(optional_arg(arg=['--use-existing-ids'], type=bool, description='Use existing ids in quakeml: Warning: These ids cant already exist in db!'))
    optional_args.append(optional_arg(arg=['-x', '--hypodd'], type=bool, description='Use existing event ids in quakeml but no others (not orid, arid, etc)'))

    args, parser = parse_cmd_line(required_args=[r], optional_args=optional_args)
    config, log_msgs = read_config(requireConfigFile=True, configfile=args.configfile, debug=False)
    update_args_with_config(args, config, fname)
    configure_logger(**vars(args))
    logger = logging.getLogger()
    logger.info(sys.argv)

    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        logger.error("Can't continue without valid config to specify DB connection.  Exitting!")
        print()
        parser.print_help()
        exit(2)

    quakemlfile = args.qmlfile
    if not os.path.exists(quakemlfile):
        logger.error("quakemlfile=%s does not exist --> Exitting" % quakemlfile)
        print()
        parser.print_help()
        exit(2)

    #for required_field in ['auth', 'RSN_source', 'subsource']:
    for required_field in ['RSN_source']:
        if required_field not in config:
            logger.error("You must set field:%s in config.yml --> Exitting!" % required_field)
            print("You must set field:%s in config.yml --> Exitting!" % required_field)
            exit(2)

    RSN_source = config['RSN_source']
    if 'auth' in config:
        auth = config['auth']
    else:
        auth = config['RSN_source']
    if 'subsource' in config:
        subsource = config['subsource']
    else:
        subsource = 'QML'

    if len(subsource) > 8:
        logger.error("config.yml subsource=[%s] has > 8 chars! --> Exitting" % config.subsource)
        exit(2)

    # Last ditch effort to prevent accidental injection into primary db
    protected_dbs = ['texas', 'oklahoma', 'archdb', 'archdb1',]

    use_db = test_connection(config)
    use_db = True
    if use_db:
        logger.info("%s: We have a database connection!" % fname)
        dbname1 = os.path.basename(config['sqlalchemy.url'])
        dbname2 = config['conn_string'].split("dbname=")[1].split()[0]

        confirm_db = None
        if dbname1 in protected_dbs:
            confirm_db = dbname1
        elif dbname2 in protected_dbs:
            confirm_db = dbname2

        if confirm_db:
            if not confirm_write_to_primary(confirm_db):
                parser.print_help()
                exit(2)

        createSession(config)
    else:
        logger.warning("%s: Running without database connection!" % fname)

    found_PDL_orig = None

# 1. Read in the PDL quakeml file:
    if 0:
        locevid = sfile_to_locevid(quakemlfile)
    else:
        locevid = quakemlfile.split(".")[0]

    locevid = ''
    #print("Read quakeml:%s --> locevid:%s" % (quakemlfile, locevid))
    start = time.time()
    #logger.info("Read quakeml:%s --> locevid:%s" % (quakemlfile, locevid))
    logger.info("Read quakeml:%s" % (quakemlfile))
    cat = read_quakemlfile(quakemlfile)
    logger.info("Read quakeml:%s took %.2f sec" % (quakemlfile, (time.time()-start)))

    session = Session()

    #if pg_dbase_is_read_only(session):
        #logger.warning("Database is read-only!! Stopping here")
        #exit(2)
    #else:
        #print("No worries, db is writeable")


    use_existing_ids = True if args.use_existing_ids else False

    start = time.time()
    logger.info("Begin loop over cat.events")

    for ievt, event in enumerate(cat.events):

        min_dist = None

        for origin in event.origins:
            fix_leap_seconds(origin, event, session)


        logger.debug("Process Event:%d: n_origins:%d n_picks:%d n_amps:%d n_mags:%d" %
                    (ievt, len(event.origins), len(event.picks),
                     len(event.amplitudes), len(event.magnitudes)))

        if use_existing_ids or args.hypodd:
            evid = getid(event)
        else:
            evid = getSequence('event', 1)[0]
            logger.debug("%s: Request new evid from sequence returned: [evid=%s]" % (fname, evid))

        event_subsource = subsource
        if event.comments:
            for comment in event.comments:
                if comment.text and 'subsource:' in comment.text:
                    event_subsource = comment.text.replace('subsource:', '')

        aqms_event= obspy_event_to_aqms(event, evid=evid, auth=auth, subsource=event_subsource)
        session.add(aqms_event)

        for iorg, origin in enumerate(event.origins):
            logger.debug("Origin:%d: %s [method:%s] [narr:%d]" %
                        (iorg, origin.time, origin.method_id, len(origin.arrivals)))

            if use_existing_ids:
                orid = getid(origin)
            else:
                orid = getSequence('origin', 1)[0]

            logger.debug("%s: Request new orid from sequence returned: [orid=%s]" % (fname, orid))

            if origin.resource_id == event.preferred_origin().resource_id:
                logger.debug("%s:  Make orid=%d preferred" % (fname, orid))
                aqms_event.prefor = orid
                session.add(aqmsprefor(evid, orid))

            if origin.arrivals:
                arrs = [x for x in origin.arrivals if x.distance is not None]
                sorted_arrivals = sorted(arrs, key=lambda x: x.distance, reverse=False)
                #sorted_arrivals = sorted(origin.arrivals, key=lambda x: x.distance, reverse=False)
                #MTH: obspy distance is in deg and all AQMS lengths are km:
                if sorted_arrivals:
                    min_dist = sorted_arrivals[0].distance * KM_PER_DEG
                else:
                    logger.warning("Sorted_arrivals is empty --> Can't set min_dist")

            origin_subsource = subsource
            if origin.comments:
                for comment in origin.comments:
                    if 'subsource:' in comment.text:
                        origin_subsource = comment.text.replace('subsource:', '')

            aqms_orig = obspy_origin_to_aqms(event, orid=orid, evid=evid,
                                             auth=auth, subsource=origin_subsource,
                                             min_dist=min_dist, obspy_origin=origin)

            #gtype = set_gtype(config, RSN_source, aqms_orig.lat, aqms_orig.lon)
            gtype = get_gtype(config, RSN_source, aqms_orig.lat, aqms_orig.lon)

            if gtype is not None:
                aqms_orig.gtype = gtype
            else:
                logger.debug("set_gtype() returned None --> origin.gtype not set")

            #if len(event.origins) > 1:
            if 0:
                logger.error("Can't set origin.locevid=%s --> There are [%d] origins in this event and they'll *all* get same locevid! --> Exitting" %
                             (locevid, len(event.origins)))
                exit(2)
            aqms_orig.locevid = locevid

            session.add(aqms_orig)
            #for obj in session.new:
                #print(obj)
            #print("session.dirty:%s" % session.dirty)
            #print("session.deleted:%s" % session.deleted)
            session.flush()

            if origin.arrivals:
                aqms_arrivals = []
                assocaro_adds = []

                if not use_existing_ids:
                    arids = getSequence('arrival', len(origin.arrivals))

                for arrival in origin.arrivals:
                    if use_existing_ids:
                        arid = getid(arrival)
                    else:
                        arid = arids.pop(0)

                    # agency_id: 'pr'  ?? use this ??
                    auth_arr = auth
                    if getattr(arrival, 'creation_info', None) and \
                       getattr(arrival.creation_info, 'agency_id', None):
                        auth_arr = arrival.creation_info.agency_id.upper()
                    else:
                        logger.debug("Arrival doesn't have creation_info --> use origin auth=[%s] for auth_arr" % (auth))

                    logger.debug("Convert arrival: arid=[%s] auth=[%s]" % (arid, auth))
                    aqms_arrivals.append( obspy_arrival_to_aqms(arrival, arid=arid, auth=auth, subsource=subsource) )
                    """
                    print("Last arrival:")
                    print(aqms_arrivals[-1])
                    print(aqms_arrivals[-1].sta)
                    print("seedchan=[%s]" % aqms_arrivals[-1].seedchan)
                    print(type(aqms_arrivals[-1].seedchan))
                    print("location=[%s] len=%d" % (aqms_arrivals[-1].location, len(aqms_arrivals[-1].location)))
                    print(type(aqms_arrivals[-1].location))
                    exit()
                    """
                    # assocaro - Data associating arrivals with origins
                    assocaro_adds.append( obspy_arrival_to_assocaro(arrival, arid=arid, orid=orid, auth=auth, subsource=subsource) )

                logger.debug("Insert %d arrivals arid:%d -to- %d" % (len(aqms_arrivals), aqms_arrivals[0].arid, aqms_arrivals[-1].arid))

                session.bulk_save_objects(aqms_arrivals)
                session.bulk_save_objects(assocaro_adds)
                session.flush()


        for imag, magnitude in enumerate(event.magnitudes):
            logger.debug("Magnitude:%d: type:%s method:%s val:%.2f" %
                        (imag, magnitude.magnitude_type, magnitude.method_id, magnitude.mag))
            if use_existing_ids:
                magid = getid(magnitude)
            else:
                magid = getSequence('magnitude', 1)[0]
                logger.debug("%s: Request magid returned:%s" % (fname, magid))

            if magnitude.resource_id == event.preferred_magnitude().resource_id:
                logger.debug("%s: Make magid:%d preferred" % (fname, magid))
                aqms_event.prefmag = magid

            magtype = magnitude.magnitude_type.lower()[-1] # ML --> 'l'  Mww --> 'w'
            # MTH: We want Mww --> 'w' but we also want Mlr --> 'lr':
            magtype = 'w' if magnitude.magnitude_type.lower() == 'mww' else \
                             magnitude.magnitude_type.lower()[1:]

            #aqms_mag  = obspy_magnitude_to_aqms(magnitude, orid=orid, evid=evid, magid=magid, auth=auth,
            aqms_mag  = obspy_magnitude_to_aqms(magnitude, orid=aqms_event.prefor, evid=evid,
                                                magid=magid, auth=auth, magtype=magtype,
                                                subsource=subsource, min_dist=min_dist)
            session.add(aqms_mag)
            session.flush()
            #session.add(aqmsprefmag(evid, magid, magtype=aqms_mag.magtype))


        if event.amplitudes:
            amps = []
            for amp in event.amplitudes:
                if amp.pick_id is not None:
                    amps.append(amp)
                else: # Search for missing pick for this amplitude
                    logger.debug("amp is missing pick --> search for it")
                    found = False
                    w = amp.waveform_id
                    seed_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                    for pk in event.picks:
                        w = pk.waveform_id
                        pk_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                        if pk_id == seed_id:
                            amp.pick_id = pk.resource_id
                            found = True
                            break

                    # MTH: I have no idea what this is for ?? Seems to be a catch for mismatch horizontal channel code ??
                    if not found:
                        w = amp.waveform_id
                        comp = "E" if w.channel_code[2] == "N" else "N"   # Swap N, E components
                        w.channel_code = "%s%s" % (w.channel_code[0:2], comp)

                        seed_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                        for pk in event.picks:
                            w = pk.waveform_id
                            pk_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                            if w.station_code == amp.waveform_id.station_code:
                                amp.pick_id = pk.resource_id
                                found = True

                    if found:
                        amps.append(amp)
                    else:
                        w = amp.waveform_id
                        seed_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                        logger.error("No pick found for event: evid:%d id:%s OT:%s amp:%s id:%s" %
                                     (aqms_event.evid, event.resource_id.id,
                                      event.preferred_origin().time, seed_id, amp.resource_id.id))
            event.amplitudes = amps

            if not use_existing_ids:
                ampids = getSequence('amplitude', len(event.amplitudes))
                logger.debug("Got %d ampids" % len(ampids))

            if event.amplitudes:
                aqms_amplitudes = []
                assocamm_adds = []
                assocamo_adds = []
                # These should be checked!
                orid = aqms_event.prefor
                magid = aqms_event.prefmag
                logger.debug("Insert amps with orid:%s and magid:%s" % (orid, magid))
                # MTH: ATODO: Need to test if these amplitudes are in fact coda measurements and fill coda table accordingly
                for amplitude in event.amplitudes:
                    if use_existing_ids:
                        ampid = getid(amplitude)
                        print("Got ampid:%s" % ampid)
                    else:
                        ampid = ampids.pop(0)

                    auth_amp = auth
                    #if getattr(amplitude.pick_id.get_referred_object(), 'creation_info', None) and \
                    #getattr(amplitude.pick_id.get_referred_object().creation_info, 'agency_id', None):
                    if getattr(amplitude, 'creation_info', None) and \
                    getattr(amplitude.creation_info, 'agency_id', None):
                        auth_amp = amplitude.creation_info.agency_id.upper()
                    else:
                        logger.debug("Amplitude doesn't have creation_info --> use origin auth=[%s] for auth_amp" % (auth))

                    logger.debug("Convert amplitude: ampid=[%s] auth=[%s]" % (ampid, auth))

                    aqms_amplitudes.append(obspy_amplitude_to_aqms(amplitude, ampid=ampid, auth=auth, subsource=subsource))

                    st_mag = None
                    #for station_mag in event.station_magnitudes:
                    for station_mag in event.station_magnitudes:
                        if amplitude.resource_id == station_mag.amplitude_id:
                            st_mag = station_mag
                            break
                    # assocamm - associates amps with mag
                    if magid:
                        assocamm_adds.append(obspy_amplitude_to_assocamm(ampid=ampid, magid=magid,
                                                                        auth=auth, station_mag=st_mag,
                                                                        subsource=subsource))
                    # assocamo - associates amps with origin
                    # Note: This will not set fields: delta & seaz in assocamo
                    #       since they are not present in obspy amplitudes
                    #       The fix would be to relate ampid scnl to arrival scnl and
                    #       pull delta/seaz from the corresponding arrival
                    assocamo_adds.append( obspy_amplitude_to_assocamo(ampid=ampid, orid=orid,
                                                                      auth=auth, subsource=subsource) )
                session.bulk_save_objects(aqms_amplitudes)
                session.bulk_save_objects(assocamm_adds)
                session.bulk_save_objects(assocamo_adds)
                logger.debug("Insert %d amps ampid:%d -to- %d" % (len(aqms_amplitudes), aqms_amplitudes[0].ampid, aqms_amplitudes[-1].ampid))

            else:
                logger.warning("No suitable Amps (with picks!) found to insert!")

    logger.info("End loop over cat.events took:%.2f sec" % (time.time()-start))

# 5. Commit the session
    try:
        start = time.time()
        logger.debug("commit the session")
        session.commit()
        logger.debug("commit the session took %.2f sec" % ((time.time()-start)))
    except:
        session.rollback()
        raise
    finally:
        session.close()

        # MTH: probably should check that this isn't a new event ...

    # Use stored proc to set preferred magnitude
    #if insert_magnitude:
    if event.preferred_magnitude():
        logger.debug("Set the preferred magnitude to  magid=%d" % magid)
        retval = call_procedure('epref.setprefmag_magtype', [evid, magid], config)
        if retval == 2:
            logger.debug("Stored procedure setprefmag returned val=%d" % retval)
        elif retval == 1:
            logger.warning("Stored procedure setprefmag returned val=%d" % retval)
        else:
            logger.error("Stored procedure setprefmag returned val=%d" % retval)


    '''
    # Use stored proc to set preferred magnitude
    if insert_magnitude:
        # MTH: Use stored proc to determine if this mag is preferred
        #      If so, the procedure will update eventprefmag, event.prefmagid and origin.prefmagid
        logger.info("Set the preferred magnitude to  magid=%d" % magid)
        try:
            engine = engine_from_config(config)
            connection = engine.raw_connection()
            cursor = connection.cursor()
            query = "select * from epref.setprefmag_magtype(%s, %s)" % (evid, magid)
            logger.debug("Stored proc query=%s" % query)
            cursor.execute(query)
            retval = cursor.fetchone()[0]
            if retval == 2:
                logger.info("Stored procedure setprefmag returned val=%d" % retval)
            elif retval == 1:
                logger.warning("Stored procedure setprefmag returned val=%d" % retval)
            else:
                logger.error("Stored procedure setprefmag returned val=%d" % retval)
            cursor.close()
            connection.commit()
        finally:
            connection.close()

    # Look for any local origins in db that associate with this event
    if associate_with_local_origins:
        PDL_orid = orid
        local_orig = associate_local_origin(PDL_orid, RSN_source, assoc_thresh)
        if local_orig:
            logger.info("Local db orid:%d OT:%f <%.2f, %.2f> h=%.1f km "
                        "is the single match to this PDL origin (orid:%d)" %
                        (local_orig.orid, local_orig.datetime, local_orig.lat, local_orig.lon,
                         local_orig.depth, PDL_orid))

    '''
    logger.info("==== End of pdl_to_aqms  ============================================")

    return


from sqlalchemy import Sequence
from sqlalchemy import create_engine, engine_from_config
from sqlalchemy.orm import sessionmaker

Session = None

def createSession(config):
    global Session

    if Session:
        logger.warning("Session is already created")
        return

    try:
        #engine = engine_from_config(config, echo=True)
        engine = engine_from_config(config)
        Session = sessionmaker(bind=engine)
    except :
        raise

    #return Session

def getSequence(name, count):
    sequences = {'event':'evseq', 'origin':'orseq', 'arrival':'arseq',
                 'amplitude':'ampseq', 'magnitude':'magseq',
                 'mechanism':'mecseq',
                 'unassocamp':'unassocseq'}

    session = Session()

    nextids = []

    if name in sequences:
        seq = Sequence(sequences[name])
        for i in range(count):
            nextids.append(session.execute(seq))
    else:
        logger.error("getSequence: Error - Unrecognized sequence name=[%s]" % name)

    session.close()
    return nextids

def query_for_RSN_event_in_aqms(evid):
    '''
        Look up event evid in the origin table
        If found, return the origin
    '''

    session = Session()
    found = session.query(Origin).filter(Origin.evid==evid).all()
    session.close()

    if len(found) >= 1:
        orig = found[-1]
        return orig
    else:
        return None

# ATODO: What if there is > 1 origin with this locevid ?
#        Should look at the associated event and return
#        the preferred origin if set (?)
def query_for_pdl_event_in_aqms(pdl_id):
    '''
        Query origin table for origin.locevid == pdl_id
        If found return origin
    '''

    # See if this PDL event already exists in db
    # Query the database for origin.locevid == dataid
    # If it does, then get the evid and use this to
    #  update the event.version, create a new origin from the PDL one, and make
    #  this the preferred origin, preferred mag, etc

    session = Session()
    found = session.query(Origin).filter(Origin.locevid==pdl_id).all()
    session.close()

    if len(found) >= 1:
        orig = found[-1]
        return orig
    else:
        return None

def fix_unassoc_amp_leap_seconds(amp_dict):
    epoch = amp_dict['wstart'].timestamp
    orig_utc = amp_dict['wstart']
    session = Session()
    epoch_base=list(session.execute("select truetime.nominal2true(%d)" % int(epoch)))
    session.close()
    fixed_epoch = epoch_base[0][0]
    leap_secs = fixed_epoch - int(epoch)

    logger.debug("fix_unassoc_amp wstart=[%s] --> epoch=[%s] + leap_secs=[%d] = [%s]" % \
                (amp_dict['wstart'], epoch, leap_secs, (amp_dict['wstart'].timestamp + leap_secs)))

    amp_dict['wstart'] = amp_dict['wstart'].timestamp + leap_secs
    amp_dict['datetime'] = amp_dict['datetime'].timestamp + leap_secs

def fix_leap_seconds(origin, event, session):
    epoch = origin.time.timestamp
    epoch_base=list(session.execute("select truetime.nominal2true(%d)" % int(epoch)))
    fixed_epoch = epoch_base[0][0]
    leap_secs = fixed_epoch - int(epoch)
    otime = origin.time
    origin.time += leap_secs
    logger.debug("fix_leap: origin.time=[%s] + leap_secs=[%d] = new origin.time=[%s]" % \
                (otime, leap_secs, origin.time))
    for pick in event.picks:
        pick.time += leap_secs

    return

def read_quakemlfile(quakemlfile):

    try:
        with open(quakemlfile, 'r') as file:
            xml = file.read()
        anssInternal = False
        if 'anssevent:internalEvent' in xml:
            anssInternal = True
            logger.debug("This is an anssevent:internalEvent quakeml")
            xml = xml.replace('anssevent:internalEvent', 'event')
            #xml = file.read().replace('anssevent:internalEvent', 'event')
        cat = read_events(io.BytesIO(bytes(xml, encoding='utf-8')), format="quakeml")
    except:
        logger.error("Problem reading quakemlfile=[%s]" % quakemlfile)
        raise

    return cat

def compare_origins(quakeml_origin=None, aqms_origin=None):
    '''
        Compare incoming quakeml_origin to db aqms_origin for same PDL event
    '''

    matches = False
    origin = quakeml_origin
    found_orig = aqms_origin
    logger.debug("quakeml_origin.latitude=%f aqms_origin.lat=%f\n" % 
                (quakeml_origin.latitude, float(aqms_origin.lat)))
    # Determine if origin is the same as the db origin (=found_orig)
    #timestamp in seconds, depth in m
    x = [origin.time.timestamp, origin.latitude, origin.longitude, origin.depth]
    y = [float(found_orig.datetime), float(found_orig.lat), float(found_orig.lon), float(found_orig.depth) * 1e3]
    close = np.isclose(x, y, rtol=1e-09, atol=0.0)
    if np.all(close):
        logger.debug("%s: found_orig:%s is perfect match, do nothing" % (fname, found_orig.orid))
        matches = True

    return matches

def associate_local_origin(PDL_orid, RSN_source, assoc_thresh):

    local_origin = None

    session = Session()
    rows = session.query(Origin).filter(Origin.orid==PDL_orid).all()
    if len(rows) > 1:
        logger.warning("Query for PDL orid=%d returned >1 origins!", PDL_orid)
    elif len(rows) == 0:
        logger.warning("Query for PDL orid=%d returned no origin", PDL_orid)
    PDL_orig = rows[-1]

    matching_origins = query_origins_for_matching_origin(PDL_orig, assoc_thresh)
    # At this point we have 0-many matching_origins with orid != this PDL_orig
    #   We can further filter out any origins with auth == the local RSN
    #   and if there is just 1, then we have our local association.
    local_origins = []
    if matching_origins:
        for orig in matching_origins:
            if orig.auth == RSN_source:
                local_origins.append(orig)
                logger.debug("Local db orid:%d OT:%f <%.2f, %.2f> h=%.1f km matches PDL origin" %
                            (orig.orid, orig.datetime, orig.lat, orig.lon, orig.depth))
        if len(local_origins) == 1:
            local_origin = local_origins[0]
        #elif nfound > 1:
        else:
            logger.warning("More than 1 local origin associates with this PDL origin!")
            for orig in local_origins:
                logger.debug("local orig: orid:%d evid:%d datetime:%s <%.3f, %.3f> h=%.2f" % 
                            (orig.orid, orig.evid, orig.datetime, orig.lat, orig.lon, orig.depth))

    else:
            logger.debug("Query for PDL orid=%d found no matching local origins in db" % PDL_orid)

    return local_origin



class thresh:
    def __init__(self, time, km, depth):
        self.time = time
        self.km = km
        self.depth = depth

def query_origins_for_matching_origin(target_origin, thresh):
    '''
        return list of origins in local db within thresh
            of target_origin
    '''
    session = Session()
    t1 = float(target_origin.datetime) - thresh.time
    t2 = float(target_origin.datetime) + thresh.time
    rows = session.query(Origin).filter(Origin.datetime>=t1).\
                                 filter(Origin.datetime<=t2).all()
    session.close()

    matching_origins = []
    if rows:
        for origin in rows:
            if origin.orid != target_origin.orid \
              and compare_aqms_origins(target_origin, origin, thresh):
                matching_origins.append(origin)

    return matching_origins

def compare_aqms_origins(orig1, orig2, thresh):
    '''
        return True if orig1 and orig2 are within thresh.{time, km, depth}
    '''
    match = False
    tol = 1e-6

    lat1 = float(orig1.lat)
    lon1 = float(orig1.lon)
    lat2 = float(orig2.lat)
    lon2 = float(orig2.lon)

    t1 = float(orig1.datetime)
    t2 = float(orig2.datetime)

    h1 = float(orig1.depth)
    h2 = float(orig2.depth)

    if (np.fabs(lat1 - lat2) < tol) \
       and (np.fabs(lon1 - lon2) < tol):
        dist = 0.
    else:
        dist, az, baz = gps2dist_azimuth(lat1, lon1, lat2, lon2)

    if (np.fabs(t1 - t2) < thresh.time) \
       and (np.fabs(h1 - h2) < thresh.depth) \
       and (dist/1e3 < thresh.km):
        match = True

    return match


def check_for_RSN_evid(args, RSN_source=None):
    '''
        See if args.eventids has an id that begins with the
          2-char RSN code and if so, pull the rest of the evid
          to see if it corresponds to evids in the local (RSN) db.
    '''
    if len(args.eventids) > 1:
        logger.debug("%s: Attention pdl_type:%s code:%s preferred_eventid:%s has >1 eventids:%s" %
                    (fname, args.type, args.code, args.preferred_eventid, args.eventids))
        found_eventid = None
        for evid in args.eventids:
            if evid[0:2] == RSN_source:
                found_eventid = evid
                break
        if found_eventid:
            preferred = False
            if found_eventid == args.preferred_eventid:
                preferred = True
            RSN_evid = found_eventid[2:]
            logger.debug("%s: RSN_source=[%s] RSN_evid=[%s] preferred:[%s]" %
                        (fname, RSN_source, RSN_evid, preferred))
            return RSN_evid

    return None

def handle_unassociated_amp(quakemlfile, use_db=0):

    xml_amps_dir = quakemlfile
    amps = scan_xml_amps(xml_amps_dir)
    logger.debug("%s: pdl_type:%s scan for amps in xml_dir=[%s] returned:[%d] unassoc amps" % \
                (fname, pdl_type, xml_amps_dir, len(amps)))
    if use_db:
        session = Session()
        ampids = getSequence('unassocamp', len(amps))
        AQMS_amps = []
        for amp in amps:
            ampid = ampids.pop(0)
            fix_unassoc_amp_leap_seconds(amp)
            AQMS_amps.append(unassoc_amplitude_to_aqms(amp, ampid=ampid, auth=auth, subsource=subsource))
        session.bulk_save_objects(AQMS_amps)
        session.commit()
        session.close()
        logger.debug("%s: Done inserting unassocamps --> Exit" % fname)

    return

def set_gtype(config, source, lat, lon):

    '''
    returns:
        l - if lat,lon within authoritative region for this source (RSN)
        r - if lat,lon not within authoritative region for this source (RSN)
        None - if it can't be determined (e.g., if source not set)
    '''

    gtype = None

    logger.debug("set_gtype called with source=%s lat=%f lon=%f" % (source, lat, lon))

    try:
        engine = engine_from_config(config)
        connection = engine.raw_connection()
        cursor = connection.cursor()
        query = "select geo_region.inside_border('%s', %f, %f)" % \
                (source, lat, lon);
        cursor.execute(query)
        retval = cursor.fetchone()[0]
        logger.debug("query:[%s] returned val=%d" % (query, retval))
        if retval == 1:
            gtype = 'l'
        elif retval == 0:
            gtype = 'r'
        else:
            logger.warning("gtype was not successfully set by SP!")
        cursor.close()
        connection.commit()
    finally:
        connection.close()

    logger.debug("set_gtype return gtype=%s" % gtype)
    return gtype


def sfile_to_locevid(sfile):
    #print("Got sfile:%s" % sfile)
    evid = os.path.basename(sfile)
    #print("with evid:%s" % evid)
    yyyy = int(evid[13:17])
    mm = int(evid[17:19])
    dd = int(evid[0:2])
    hh = int(evid[3:5])
    mi = int(evid[5:7])
    ss = int(evid[8:10])
    locevid = "%02d%02d%02d%02d%02d%02d" % (yyyy-2000, mm, dd, hh, mi, ss)
    return locevid


def getEvid(event):
    '''
    <event publicID="quakeml:OK.isti.com/Event/OK/70037753" ns0:datasource="ok" ns0:dataid="ok70037753" ns0:eventsource
="ok" ns0:eventid="70037753">

AttribDict({'datasource': AttribDict({'value': 'ok', 'namespace': 'http://anss.org/xmlns/catalog/0.1', 'type': 'attribute'}), 'dataid': AttribDict({'value': 'ok70036948', 'namespace': 'http>
    event.resource_id.id

    '''
    #evid = None

    if 'eventid' in event.extra:
        evid = int(event.extra.eventid.value)
        return evid
    #if evid is None:
    else:
        print("Error scanning evid from event!  Exitting!")
        logger.error("Error scanning evid from event!  Exitting!")
        exit(2)

def getid(obspy_object):
    #return int(os.path.basename(obspy_object.resource_id.id))
    try:
        _id = int(os.path.basename(obspy_object.resource_id.id))
    except ValueError as e:
    # MTH: Hack for CISN weird ids like 
    #  <event publicID="quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?eventid=39645386" catalog:datasource="ci" ...
        _id = int(os.path.basename(obspy_object.resource_id.id.split("=")[-1]))
    return _id

def pg_dbase_is_read_only(session):
    #with Session.begin() as session:
    if 1:
        readonly = list(session.execute(text("select pg_is_in_recovery()")))
        session.expunge_all()
    return readonly[0][0]

def strtobool(word):
    yes = {'y', 'yes', 'ye'}
    no = {'no', 'n', ''}
    if word in yes:
        return True
    elif word in no:
        return False
    else:
        raise ValueError("Please respond with 'y' or ['n']")

def confirm_write_to_primary(dbname):

    print("You are requesting to inject into primary db:%s" % dbname)
    print("Are you sure you want to continue ? [y/n]")
    while True:
        try:
            return strtobool(input().lower())
        except ValueError as e:
            print(e)


if __name__ == "__main__":
    main()
