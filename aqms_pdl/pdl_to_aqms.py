# -*- coding: utf-8 -*-
"""
Read in quakeml file + flags and insert into AQMS db
Triggered by PDL

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import logging
import os
import sys

from aqms_pdl import createSession
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg, optional_arg
from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.libs_db import pg_dbase_is_read_only
from aqms_pdl.libs.libs_log import configure_logger, read_config, update_args_with_config
from aqms_pdl.libs.run_pdl_to_aqms import run_pdl_to_aqms


def main():
    """
        pdl-to-aqms: app to insert PDL messages into AQMS db

        >pdl-to-aqms -h  # To get help/see usage
    -OR-
        >pdl2aqms -h     # To get help/see usage
    """

    global logger

    fname = 'pdl_to_aqms'

    args, parser = parse_cmd_line(required_args=get_required_args(), optional_args=get_optional_args(), width=49)
    config, log_msgs = read_config(requireConfigFile=True, configfile=args.configfile, debug=False)
    update_args_with_config(args, config, fname)
    configure_logger(**vars(args))

    logger = logging.getLogger()
    logger.info(sys.argv)

    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['info']:
            print("[  INFO ] %s" % msg)
        for msg in log_msgs['error']:
            print("[ ERROR ] %s" % msg)
            logger.error(msg)
        logger.error("Can't continue without valid config to specify DB connection.  Exitting!")
        print()
        parser.print_help()
        exit(2)

    quakemlfile = process_args(args)

    # Configure the Session for the whole module in __init__.py:
    createSession(config)
    # import the configured Session into the libs_db pkg for use
    set_Session()

    #if pg_dbase_is_read_only():
        #logger.warning("Database is read-only! ==> Exitting")
        #exit(2)

    evid = run_pdl_to_aqms(config, quakemlfile, args)

    logger = logging.getLogger()
    logger.info("%s: inserted evid:%d" % (fname, evid))

    # Need to test how to return both evid and retval to calling shell
    #return evid
    return


from obspy import UTCDateTime

PDL_STATUS = ['DELETE', 'UPDATE',]
PDL_TYPES = ['origin', 'phase-data', 'amplitude', 'unassociated-amplitude',
             'moment-tensor', 'internal-moment-tensor',]

def get_required_args():

     r = []

     r.append(required_arg(arg=["--status"], type=str, description='STATUS',
                          help='STATUS in {' + ','.join(PDL_STATUS) + '}'))
     r.append(required_arg(arg=["--code"], type=str, description='pdl_id',
                          help='--code hv71386392'))
     r.append(required_arg(arg=["--directory"], type=str, description='DIR',
                          help='path to dir containing quakeml.xml'))
     r.append(required_arg(arg=["--type"], type=str, description='TYPE', choices=PDL_TYPES,
                          help='PDL TYPE in {' + ','.join(PDL_TYPES) + '}'))
     r.append(required_arg(arg=["--source"], type=str, description='XX', help='--source=pr or --source CI'))

     return r


def get_optional_args():

    o = []

# could have --type=origin --status=DELETE --delete --action=PRODUCT_ADDED
    o.append(optional_arg(arg=['--PDL_flags'], description='', type=str,
                          help='** Preferred tags + action + eventids set by PDL'))
    o.append(optional_arg(arg=['--action'], type=str, description='',
                          help='EVENT_UPDATED, EVENT_DELETED, PRODUCT_ADDED, etc.'))
    o.append(optional_arg(arg=['--eventids'], type=list_str, help='list of eventids'))
    o.append(optional_arg(arg=['--preferred-eventid'], type=str, description='pdl_id',
                          help='--preferred-eventid=hv71386392'))
    o.append(optional_arg(arg=['--preferred-eventsource'], description='source', type=str,
                          help='--preferred-eventsource=hv'))
    o.append(optional_arg(arg=['--preferred-eventsourcecode'], description='code', type=str,
                          help='--preferred-eventsourcecode=71386392'))
    o.append(optional_arg(arg=['--preferred-eventtime'], description='UTCDateTime', type=UTCDateTime,
                          help='--preferred-eventtime 2022-02-17T18:43:36.007'))


    # MTH: internal-moment-tensor sets this to 'null' !
    #optional.add_argument("--preferred-magnitude", type=float)
    o.append(optional_arg(arg=["--preferred-magnitude"], description='mag', type=str,
                          help='--preferred-magnitude 6.2'))
    o.append(optional_arg(arg=["--preferred-latitude"], description='lat', type=float,
                          help='--preferred-latitude 45.27'))
    o.append(optional_arg(arg=["--preferred-longitude"], description='lon', type=float,
                          help='--preferred-longitude -123.45'))
    o.append(optional_arg(arg=["--preferred-depth"], description='dep', type=float,
                          help='--preferred-depth 10.8'))

    o.append(optional_arg(arg=["--property-xxxxxxxxxx"], description='VALUE', type=str,
                          help='** Property tags set by PDL and only used if --preferred tag was not set'))
    o.append(optional_arg(arg=["--property-origin-source"], description='val', type=str))
    o.append(optional_arg(arg=["--property-magnitude"], description='val', type=float))
    o.append(optional_arg(arg=["--property-magnitude-type"], description='val', type=str))
    o.append(optional_arg(arg=["--property-derived-magnitude"], description='val', type=float))
    o.append(optional_arg(arg=["--property-derived-magnitude-type"], description='val', type=str))
    o.append(optional_arg(arg=["--property-magnitude-num-stations-used"], description='val', type=int))
    o.append(optional_arg(arg=["--property-magnitude-source"], description='val', type=str))
    o.append(optional_arg(arg=["--property-standard-error"], description='val', type=float))
    o.append(optional_arg(arg=["--property-horizontal-error"], description='val', type=float))
    o.append(optional_arg(arg=["--property-error-ellipse-azimuth"], description='val', type=float))
    o.append(optional_arg(arg=["--property-minimum-distance"], description='val', type=float))
    o.append(optional_arg(arg=["--property-azimuthal-gap"], description='val', type=float))
    o.append(optional_arg(arg=["--property-event-type"], description='val', type=str))
    o.append(optional_arg(arg=["--property-version"], description='val', type=int))
    o.append(optional_arg(arg=["--property-num-stations-used"], description='val', type=int))
    o.append(optional_arg(arg=["--property-num-phases-used"], description='val', type=int))
    o.append(optional_arg(arg=["--property-review-status"], description='val', type=str))

    o.append(optional_arg(arg=["--ignore-latitude-above"], type=float, description='lat_max',
                          help='Ignore latitude > lat_max'))
    o.append(optional_arg(arg=["--ignore-latitude-below"], type=float, description='lat_min',
                          help='Ignore latitude < lat_min'))
    o.append(optional_arg(arg=["--ignore-longitude-above"], type=float, description='lon_max',
                          help='Ignore longitude > lon_max'))
    o.append(optional_arg(arg=["--ignore-longitude-below"], type=float, description='lon_min',
                          help='Ignore longitude < lon_max'))
    o.append(optional_arg(arg=["--ignore-depth-above"], type=float, description='dep_max',
                          help='Ignore depth > dep_max'))
    o.append(optional_arg(arg=["--ignore-depth-below"], type=float, description='dep_min',
                          help='Ignore depth < dep_min'))
    o.append(optional_arg(arg=["--ignore-magnitude-above"], type=float, description='mag_max',
                          help='Ignore magnitude > mag_max'))
    o.append(optional_arg(arg=["--ignore-magnitude-below"], type=float, description='mag_min',
                          help='Ignore magnitude < mag_min'))
    o.append(optional_arg(arg=["--ignore-sources"], type=list_str_sources, description='XX,YY,..',
                          help='Ignore msgs from this list of sources, e.g., --ignore-sources=HV,US'))
    o.append(optional_arg(arg=["--only-listen-to-sources"], type=list_str_sources, description='XX,YY,..',
                          help='Only listen to msgs from this list of sources'))
    o.append(optional_arg(arg=["--ignore-arrivals"], type=bool, help='Dont insert arrivals'))
    o.append(optional_arg(arg=["--ignore-amplitudes"], type=bool, help='Dont insert amplitudes'))
    o.append(optional_arg(arg=["--ignore-event-after"], type=str, description='val',
                          help='Ignore if event age > {10mins, 3hrs, 5days, 2weeks, 3months, etc}'))
    o.append(optional_arg(arg=["--debug"], type=str, description='action',
                          help='--debug summarize-only ==> summarize quakeml contents and exit'))

    o.append(optional_arg(arg=["--archive-quakeml"], type=bool,
                          help='Archive quakeml from PDL data/.. dir to archive-quakeml-dir'))
    o.append(optional_arg(arg=['--archive-quakeml-dir'], type=str, description='DIR',
                          help='Archive quakeml to this dir'))

    o.append(optional_arg(arg=["--subsource"], type=str, description='source',
                          help='e.g., --subsource=PDL or --subsource=SEISAN'))
    o.append(optional_arg(arg=["--set_selectflag_zero"], type=bool, help='Insert events with event.selectflag=0'))

    return o


def process_args(args):

    logger.info("==== Process PDL msg %s" % (("=" * 70)))
    logger.info("     code:[%s]    source:[%s]    status:[%s]   type:[%s]" % \
                (args.code, args.source, args.status, args.type))
    if args.preferred_eventid:
        logger.info("     preferred_eventid:[%s]  eventids:%s" % (args.preferred_eventid, args.eventids))

    if args.status.upper() == 'DELETE':
        logger.info("     status=DELETE ==> Stop processing")
        exit(0)

    # Prevent event summary from args.preferred flags:
    if args.preferred_eventtime:
        lat = None
        lon = None
        if args.preferred_latitude:
            lat = "%.2f" % float(args.preferred_latitude)
        if args.preferred_longitude:
            lon = "%.2f" % float(args.preferred_longitude)

        if not args.preferred_magnitude or args.preferred_magnitude == 'null':
            logger.warning("--preferred-magnitude is not set!")
            if args.property_magnitude:
                logger.warning("--property-magnitude *is* set - use it as preferred")
                args.preferred_magnitude = args.property_magnitude
                args.preferred_magnitude_type = args.property_magnitude_type
            elif args.property_derived_magnitude:
                logger.warning("--property-derived-magnitude *is* set - use it as preferred")
                args.preferred_magnitude = args.property_derived_magnitude
                args.preferred_magnitude_type = args.property_derived_magnitude_type
            else:
                logger.error("No --preferred/--property magnitude set --> Exit")
                exit(2)
        else:
            args.preferred_magnitude = float(args.preferred_magnitude)

        logger.info("     preferred_eventtime:[%s]  lat:%s lon:%s dep:%s mag:%s" %
                    (args.preferred_eventtime, lat, lon,
                     args.preferred_depth, args.preferred_magnitude))
    elif args.type != 'unassociated_amplitude' and args.status.upper() != 'DELETE':
        logger.warning("--preferred-eventtime is not set!")
        if args.property_eventtime:
            logger.warning("--property-eventtime *is* set - use it as preferred")
            args.preferred_eventtime = args.property_eventtime
        else:
            logger.error("--preferred-eventtime not set --> Exit!")
            exit(2)


    if args.archive_quakeml and not args.archive_quakeml_dir:
        logger.error("If you set archive_quakeml you *must* also set archive_quakeml_dir --> Exitting!")
        exit(2)

    # Check if msg type is in pdl allowable-types:
    #if args.pdl-types:
    pdl_types = getattr(args, 'pdl-types', None)
    if pdl_types is None:
        pdl_types = getattr(args, 'pdl_types', None)

    if pdl_types:
        allowable_types = pdl_types
    else:
        logger.error("Must set pdl-types in config.yml to configure allowable PDL msg types to process --> Exitting!")
        exit(2)

    if args.type not in allowable_types:
        logger.warning("Type=[%s] not in allowable_types --> Exitting!" % args.type)
        exit(0)

    # Check if event is stale:
    if args.preferred_eventtime:
        now = datetime.datetime.now(datetime.timezone.utc)
        now_epoch = now.timestamp()
        args.event_age = now_epoch - args.preferred_eventtime.timestamp

        logger.info("        current_UTC_time:[%s]  age_of_event:[%.1f seconds]" %
                     (UTCDateTime(now_epoch), args.event_age))
        if args.ignore_event_after:
            args.max_event_age = parse_time_string(args.ignore_event_after)

    # Check args against filters:
    ignore = check_filters(args)

    #if not ignore and 'polygon' in config:
    polygon = getattr(args, 'polygon', None)
    if not ignore and polygon:

        points = [tuple(x) for x in polygon]
        #print(points)
        try:
            from geojson import Point, Polygon, Feature
            from turfpy.measurement import boolean_point_in_polygon
        except ImportError as e:
            logger.warning("'polygon' is specified in config but geojson/turfpy are not installed!")
            logger.warning("falling back on --ignore-lat/lon flags if specified")
        else:
            polygon = Polygon([points])
            point = Point((args.preferred_longitude, args.preferred_latitude))
            if not boolean_point_in_polygon(point, polygon):
                logger.info("Epicenter: %.2f, %.2f is outside specified POLYGON" % (args.preferred_latitude, args.preferred_longitude))
                ignore = True
            else:
                logger.info("Epicenter: %.2f, %.2f locates within specified POLYGON" % (args.preferred_latitude, args.preferred_longitude))

    if ignore:
        logger.info("Didn't pass filter:%s" % (ignore))
        exit(0)

# could have --type=origin --status=DELETE --delete --action=PRODUCT_ADDED

    if args.debug and args.debug == 'summarize-only':
        logger.info("PDL: [code=%s] [type=%s] [status=%s] [action=%s]" % \
                    (args.code, args.type, args.status, args.action))

        logger.info("PDL: [source=%s] preferred-eventsource=[%s] [preferred_eventid=%s] eventids=%s" % \
                    (args.source, args.preferred_eventsource, args.preferred_eventid, args.eventids))

        if args.preferred_magnitude:
            logger.info("PDL: [ origin: %s (%.2f, %.2f) h=%.2f M:%.2f]" % \
                        (args.preferred_eventtime, args.preferred_latitude, args.preferred_longitude,
                        args.preferred_depth, args.preferred_magnitude))

        logger.info("PDL: [dir=%s]" % (args.directory))
        #exit(0)


    # MTH: Fix this: I think only unassociated-amplitudes don't pass quakeml.xml ??
    if args.type in {'phase-data', 'origin', 'focal-mechanism', 'moment-tensor',
                     'internal-origin', 'internal-moment-tensor'}:

        quakemlfile = os.path.join(args.directory, 'quakeml.xml')
        #return quakemlfile, args
    else:
        quakemlfile = args.directory

    return quakemlfile


def check_source(args):
    if args.ignore_sources and args.source.upper() in args.ignore_sources or \
       args.only_listen_to_sources and args.source.upper() not in args.only_listen_to_sources:
        return 'source=%s did not pass filter' % args.source.upper()

    return False

def check_filters(args):

    if args.ignore_sources and args.source.upper() in args.ignore_sources or \
       args.only_listen_to_sources and args.source.upper() not in args.only_listen_to_sources:
        return 'source=%s did not pass filter' % args.source.upper()
    if args.ignore_latitude_below and args.preferred_latitude < args.ignore_latitude_below:
        return 'latitude=%.2f < min latitude=%.2f' % (args.preferred_latitude, args.ignore_latitude_below)
    elif args.ignore_latitude_above and args.preferred_latitude > args.ignore_latitude_above:
        return 'latitude=%.2f > max latitude=%.2f' % (args.preferred_latitude, args.ignore_latitude_above)
    elif args.ignore_longitude_below and args.preferred_longitude < args.ignore_longitude_below:
        return 'longitude=%.2f < min longitude=%.2f' % (args.preferred_longitude, args.ignore_longitude_below)
    elif args.ignore_longitude_above and args.preferred_longitude > args.ignore_longitude_above:
        return 'longitude=%.2f > max longitude=%.2f' % (args.preferred_longitude, args.ignore_longitude_above)
    elif args.ignore_depth_below and args.preferred_depth < args.ignore_depth_below:
        return 'depth=%.2f < min depth=%.2f' % (args.preferred_depth, args.ignore_depth_below)
    elif args.ignore_depth_above and args.preferred_depth > args.ignore_depth_above:
        return 'depth=%.2f > max depth=%.2f' % (args.preferred_depth, args.ignore_depth_above)
    elif args.ignore_magnitude_below and args.preferred_magnitude < args.ignore_magnitude_below:
        return 'mag=%.2f < min mag=%.2f' % (args.preferred_magnitude, args.ignore_magnitude_below)
    elif args.ignore_magnitude_above and args.preferred_magnitude > args.ignore_magnitude_above:
        return 'mag=%.2f > max mag=%.2f' % (args.preferred_magnitude, args.ignore_magnitude_above)

    elif args.ignore_event_after and args.preferred_eventtime \
        and args.event_age > args.max_event_age:
        return 'age=%.2f > max age=%.2f' % (args.event_age, args.max_event_age)

    return False


def list_str(values):
    return values.split(',')

def list_str_sources(values):
    return values.upper().split(',')


import re
import datetime
def parse_time_string(time_string):
    '''
    parse a time string like: "50 days" or "2 MINS" 
        into seconds
    '''

    seconds = None

    time_units = {"SECONDS": 1, "SECS": 1, "S": 1,
                  "MINUTES": 60, "MINS": 60, "M": 60,
                  "HOURS": 3600, "HRS": 3600, "H": 3600,
                  "DAYS": 86400, "D": 86400,
                  "WEEKS": 604800, "W": 604800,
                  "MONTHS": 2628002.88, "M": 2628002.88,
                  }

    m = re.match(r"^(?P<number>\d+)(?P<unit>\w+)$", time_string)
    if m is None:
        m = re.match(r"^(?P<number>\d+) (?P<unit>\w+)$", time_string)

    if m is not None:
        d = m.groupdict()
        if d['unit'].upper() in time_units:
            seconds = float(d['number']) * time_units[d['unit'].upper()]
        else:
            logger.error("--ignore-event-after=%s' --> units=[%s] is not a valid time unit" %
                  (time_string, d['unit']))
            exit(2)
    else:
        logger.error('Unable to parse --ignore-event-after=%s into an integer + unit, ' \
                     'e.g., 5mins, "5 minutes", "10 days", 2months, etc.' %
                     time_string)
        exit(2)

    return seconds


if __name__ == '__main__':
    main()
