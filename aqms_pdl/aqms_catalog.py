import argparse
import copy
import json
import logging
import os
import sys

from socket import gethostname

from aqms_pdl import createSession
from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg, optional_arg
from aqms_pdl.libs.libs_aqms_to_obspy import aqms_arrival_to_obspy, aqms_amp_to_obspy
from aqms_pdl.libs.libs_aqms_to_obspy import aqms_event_to_obspy, aqms_origin_to_obspy
from aqms_pdl.libs.libs_aqms_to_obspy import aqms_mec_to_obspy, aqms_netmag_to_obspy
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config, read_config
from aqms_pdl.libs.schema_aqms_PI import Event, Origin, Netmag, Origin_Error
from aqms_pdl.libs.schema_aqms_PI import Mec, Mecdata, Mecchannel
from aqms_pdl.libs.schema_aqms_PI import Assocaro, Arrival
from aqms_pdl.libs.schema_aqms_PI import Assocamo, Amp, Assocamm
from aqms_pdl.libs.leapseconds import get_leapseconds_from_timestamp

from aqms_pdl.libs.catalog_formats import write_text, write_quakeml, write_csv
from aqms_pdl.libs.catalog_formats import write_ncedc, write_detail, write_json
from aqms_pdl.libs.catalog_formats import write_fpfit, write_geojson, write_kml
from aqms_pdl.libs.catalog_formats import missing_netmag, write_phase

from obspy.core.utcdatetime import UTCDateTime

from sqlalchemy import or_, func

from time import time
import numpy as np

version = '0.0.2'

output_formats = ['text', 'xml', 'csv', 'fpfit', 'geojson', 'json', 'phase', 'kml']

text_formats = ['neat', 'detail', 'ncedc']

# These are used for quakeml output:
resource_id_base="quakeml:%s"%gethostname().replace('net','com')
ns_catalog = 'http://anss.org/xmlns/catalog/0.1'
# earth_model_id = 'quakeml:OK.isti.com/velmodel/OK/mod'
# Both obspy/qml origin + arrival have earth_model_id param

DEG2KM = 111.19

logger = None

catalog_name = None


def main():
    """
        Return AQMS catalog - To be used (eventually) as back-end of FDSN event service

        Usage: >python event.py -h

        db connection can come from [3. trumps 2. trumps 1.]:
            1. configfile or lookup in ~/.pgpass
            2. cmd line --DB_USER --DB_NAME etc
            3. cmd line --catalog hypodd
    """

    fname = 'aqms-catalog'

    global logger
    global catalog_name

    args, parser = parse_cmd_line(required_args=[], optional_args=get_optional_args(), width=46,
                                  version=version)
    config, log_msgs = read_config(debug=False, requireConfigFile=False,
                                   configfile=args.configfile, look_for_pgpass=True,
                                   cmd_line_args=args)
    update_args_with_config(args, config, fname)

    configure_logger(**vars(args))
    logger = logging.getLogger()

    logger.info(sys.argv)
    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    if args.catalog:
        update_config_with_db(config, args.catalog)

    # Configure the Session for the whole module in __init__.py:
    try:
        createSession(config)
    except Exception as e:
        lines = [line for line in str(e).split('\n') if len(line.strip()) and "Background" not in line]
        _msg = '\n'.join(lines)
        logger.error(_msg)
        sys.stderr.write(_msg + '\n')
        exit(2)

    # import the configured Session into the libs_db pkg for use
    set_Session()
    from aqms_pdl import Session

    valid = validate_args(args)

    if not valid:
        msg = "Sorry, your args are invalid ==> Exitting"
        sys.stderr.write(msg)
        exit(2)

    catalog_name = config['DB_NAME']

    # xml + phase are the only formats (?) that includes arrivals, amplitudes & all origins in the output catalog
    # Leave includemechanisms as fpfit needs event.mecs[]
    if args.fmt not in ['xml', 'phase']:
        for xml_flag in ['includearrivals', 'includeamplitudes', 'includeallorigins']:
            if getattr(args, xml_flag, None) and getattr(args, xml_flag, None) == True:
                logger.warning(f"You selected format={args.fmt} != 'xml' ==> ignoring flag --{xml_flag}")
            setattr(args,xml_flag,None)

    if args.fmt == 'fpfit':
        setattr(args, 'includemechanisms', True) # Need this to collect mechanisms for fpfit output

    if args.fmt == 'phase':
        setattr(args, 'includearrivals', True) # Need this to collect all arrivals for events

    start = time()

    events, mecids, mecids_fp = queryEvents(Session, args)

    try:
        # We may have mecs but fpfit only wants mechtype=FP
        if args.fmt == 'fpfit':
            ncount = len(mecids_fp)
        else:
            ncount = len(events)
    except ValueError as e:
        raise

    if args.return_count or not args.stdout:
        sys.stdout.write(str(ncount))

    if args.return_count:
        exit(0)

    """
    if args.return_count:
        # We may have mecs but fpfit only wants mechtype=FP
        if args.fmt == 'fpfit':
            sys.stdout.write(str(len(mecids_fp)))
        else:
            sys.stdout.write(str(len(events)))
        exit(0)
    """

    if len(events) == 0:
        msg = "aqms-catalog: No events matched your search params"
        sys.stderr.write(msg)
        exit(3)

    origins = [origin for event in events for origin in event.origins]

    debug = 0
    if debug:
        netmags = [netmag for event in events for netmag in event.netmags]
        logger.debug("rows:    %d" % (len(rows)))
        logger.debug("events:  %d %d" % (len(events), len(set(events))))
        logger.debug("origins: %d %d" % (len(origins), len(set(origins))))
        logger.debug("netmags: %d %d" % (len(netmags), len(set(netmags))))
        exit()

    logger.debug("[%8.4e] Done sort Event/Origin" % (time() - start))

    mecids = sorted(mecids)

    if args.fmt == 'fpfit' and len(mecids) == 0:
        msg = "aqms-catalog: No mechanisms matched your search params"
        sys.stderr.write(msg)
        exit(3)

    if args.includemechanisms and len(mecids):
        queryMecs(Session, mecids, events)

    logger.debug("[%8.4e] Done query/assemble FocalMechanisms" % (time() - start))

    origins = sorted(origins, key=lambda x: x.orid, reverse=False)
    orids   = [origin.orid for origin in origins]

    for event in events:
        for origin in event.origins:
            origin.leapsecs = get_leapseconds_from_timestamp(origin.datetime)

    logger.debug("[%8.4e] Query Arrivals" % (time() - start))
    if args.includearrivals:
        queryArrivals(Session, orids, events)
    logger.debug("[%8.4e] Done query/assemble Arrivals" % (time() - start))

    if args.includeamplitudes:
        queryAmplitudes(Session, orids, events)

    logger.debug("[%8.4e] Done query/assemble Amplitudes" % (time() - start))

    for event in events:
        if getattr(event, 'preferred_magnitude', None) is None:
            logger.warning("evid:%d has no preferred_magnitude!" % event.evid)

    # Sort events
    if args.orderby.value == 'time':
        events = sorted(events, key=lambda x: x.preferred_origin.datetime, reverse=True)
    elif args.orderby.value == 'time-asc':
        events = sorted(events, key=lambda x: x.preferred_origin.datetime)

    elif args.orderby.value in ['magnitude', 'magnitude-asc']:
        # We don't have a way to sort events with missing mag so sort by OT and append to end of
        #  (magnitude) sorted list
        #missing_mags = [event for event in events if event.preferred_magnitude.magnitude == "N/A"]
        missing_mags = [event for event in events if isinstance(event.preferred_magnitude, missing_netmag)]

        missing_mags = sorted(missing_mags, key=lambda x: x.preferred_origin.datetime)
        # Note: may want to copy full events list before overwriting it here:
        #events = [event for event in events if event.preferred_magnitude.magnitude != "N/A"]
        events = [event for event in events if not isinstance(event.preferred_magnitude, missing_netmag)]

        reverse = True if args.orderby.value == 'magnitude' else False
        events = sorted(events, key=lambda x: x.preferred_magnitude.magnitude, reverse=reverse)
        events.extend(missing_mags)

    # [Optionally] Limit events
    if args.limit:
        events = events[args.offset: args.offset+args.limit]

    cat_or_text = get_catalog(events, args.fmt, args.text_format)

    if not args.outfile:
        outfile = "quakeml.xml" if args.fmt == "xml" else "catalog.%s"%args.fmt
    else:
        outfile = args.outfile

    # Return catalog as file or stdout
    if args.fmt == 'xml':
        with_arrs = "with arrivals" if args.includearrivals else ""
        logger.info("Assemble quakeml for [%d] events %s" % (len(events), with_arrs))
        cat_or_text.write(outfile, format='QUAKEML', nsmap={'catalog':ns_catalog})
        logger.debug("[%8.4e] Done cat.write" % (time() - start))
    else:
        if args.stdout:
            if args.fmt in ['geojson', 'json']:
                #json.dump(cat_or_text, sys.stdout) #no line breaks
                json.dump(cat_or_text, sys.stdout, indent=4) #pretty
                sys.stdout.write('\n')
            else:
                sys.stdout.write(cat_or_text + '\n')
        else:
            if args.fmt in ['geojson', 'json']:
                with open(outfile, 'w') as f:
                    #json.dump(cat_or_text, f)
                    json.dump(cat_or_text, f, indent=4)
            else:
                with open(outfile, 'w') as f:
                    f.write(cat_or_text)

    return



def queryEvents(Session, args):

    filter_by_dates     = True if args.starttime or args.endtime else False
    filter_by_magnitude = True if args.minmag or args.maxmag else False
    filter_by_location  = True if args.minlat or args.maxlat \
                               or args.minlon or args.maxlon \
                               or args.mindep or args.maxdep else False

    # DEFAULTS
    earliest = args.starttime.timestamp if args.starttime else UTCDateTime('1970-01-01').timestamp
    latest = args.endtime.timestamp if args.endtime else UTCDateTime.utcnow().timestamp
    minlat = args.minlat if args.minlat else -90
    maxlat = args.maxlat if args.maxlat else 90
    minlon = args.minlon if args.minlon else -180
    maxlon = args.maxlon if args.maxlon else 180
    mindep = args.mindep if args.mindep else -10
    maxdep = args.maxdep if args.maxdep else 1e5
    minmag = args.minmag if args.minmag else -2
    maxmag = args.maxmag if args.maxmag else 10.

    logger.debug("Earliest: %s   Latest: %s" % (UTCDateTime(earliest), UTCDateTime(latest)))
    logger.debug("Lat: %.1f - %.1f   Lon: %.1f - %.1f" % (minlat, maxlat, minlon, maxlon))
    logger.debug("Dep: %.1f - %.1f   Mag: %.1f - %.1f" % (mindep, maxdep, minmag, maxmag))


    with Session.begin() as session:

        if args.eventid:
            query = session.query(Event, Origin, Netmag, Mec, Origin_Error).\
                        filter(Event.evid.in_(args.eventid)).\
                        filter(Event.evid==Origin.evid).\
                        filter(Event.selectflag==1).\
                        filter(Event.prefor.isnot(None)).\
                        filter(Origin.rflag.notin_(['c','C'])).\
                        outerjoin(Netmag, Netmag.orid==Origin.orid).\
                        outerjoin(Origin_Error, Origin_Error.orid==Origin.orid).\
                        outerjoin(Mec, Mec.oridin==Origin.orid)
                        #filter(Event.prefmag.isnot(None)).\
                        #filter(or_(Netmag.orid==Origin.orid, Netmag.magid==Event.prefmag, Event.prefmag.is_(None))).\

        else:
            query = session.query(Event, Origin, Netmag, Mec, Origin_Error).\
                        filter(Event.evid==Origin.evid).\
                        filter(Event.selectflag==1).\
                        filter(Event.prefor.isnot(None)).\
                        filter(Origin.rflag.notin_(['c','C'])).\
                        outerjoin(Netmag, Netmag.orid==Origin.orid).\
                        outerjoin(Origin_Error, Origin_Error.orid==Origin.orid).\
                        outerjoin(Mec, Mec.oridin==Origin.orid)
                        #filter(Netmag.orid==Origin.orid).\
                        #filter(Event.prefmag.isnot(None)).\
                        #filter(or_(Netmag.orid==Origin.orid, Netmag.magid==Event.prefmag, Event.prefmag.is_(None))).\


            if args.etype:
                query = query.filter(Event.etype==args.etype)

            if args.contributor:
                query = query.filter(Event.auth==args.contributor)


            # --magnitudetype: Specify a magnitude type to use for testing the minimum and maximum limits.
            # This is really meant to work on eventprefmag table where 1 evid has >= 1 preferred magtypes
            # But often eventprefmag is only sparsely populated so we'll run on event.prefmag instead
            if filter_by_magnitude:
                q = session.query(Event.evid).\
                            join(Netmag, Netmag.magid==Event.prefmag).\
                            filter(Netmag.magnitude >= minmag).\
                            filter(Netmag.magnitude <= maxmag).\
                            filter(Event.selectflag==1)
                if args.magtype:
                    q = q.filter(Netmag.magtype == args.magtype)

                rows = q.all()
                evids = [row[0] for row in rows]
                query = query.filter(Event.evid.in_(evids))


            if filter_by_location:
                rows = session.query(Event.evid).\
                           join(Origin, Origin.orid==Event.prefor).\
                           filter(Origin.lat >= minlat).\
                           filter(Origin.lat <= maxlat).\
                           filter(Origin.lon >= minlon).\
                           filter(Origin.lon <= maxlon).\
                           filter(Origin.depth >= mindep).\
                           filter(Origin.depth <= maxdep).\
                           filter(Event.selectflag==1).all()
                evids = [row[0] for row in rows]
                query = query.filter(Event.evid.in_(evids))

            if args.maxdist:
                rows = session.query(Event.evid).\
                           join(Origin, Origin.orid==Event.prefor).\
                           filter(func.wheres.separation_km(Origin.lat,Origin.lon,args.lat,args.lon) <= args.maxdist).\
                           filter(func.wheres.separation_km(Origin.lat,Origin.lon,args.lat,args.lon) >= args.mindist).\
                           filter(Event.selectflag==1).all()
                evids = [row[0] for row in rows]
                query = query.filter(Event.evid.in_(evids))

            if filter_by_dates:
                rows = session.query(Event.evid).\
                           join(Origin, Origin.orid==Event.prefor).\
                           filter(Origin.datetime >= earliest).\
                           filter(Origin.datetime <= latest).\
                           filter(Event.selectflag==1).all()
                evids = [row[0] for row in rows]
                query = query.filter(Event.evid.in_(evids))

            if args.updatedafter:
                rows = session.query(Event.evid).\
                           join(Origin, Origin.orid==Event.prefor).\
                           filter(Origin.lddate >= args.updatedafter.datetime).\
                           filter(Event.selectflag==1).all()
                evids = [row[0] for row in rows]
                query = query.filter(Event.evid.in_(evids))

        #if args.eventid:
            #query = query.filter(Event.evid.in_(args.eventid))

        rows = query.order_by(Event.evid.asc(), Origin.orid.asc(), Netmag.magid.asc()).all()

        session.expunge_all()

    #logger.debug("[%8.4e] Done query Event/Origin" % (time() - start))


    event_dict = {}

    events = []
    mecids = []
    mecids_fp = []

    # event_dict[evid] is only used for bookkeeping here
    # what we're really getting is the organized events list

    #print("len(rows)=%d" % len(rows))

    for event, origin, netmag, mec, origin_error in rows:
        evid = event.evid
        orid = origin.orid

        if evid not in event_dict:
            event_dict[evid] = {}
            event_dict[evid]['event'] = event
            event.origins = []
            event.netmags = []
            event.mecs = []
            events.append(event)

        if origin not in event.origins:
            origin.arrivals = []
            origin.origin_error = None

        include_origin = False
        if orid == event.prefor or args.includeallorigins:
            include_origin = True

        if mec and args.includemechanisms:
            if (event.prefmec and mec.mecid==event.prefmec) \
            or (mec.oridin == event.prefor) \
            and mec not in event.mecs:   # or includeallorigins ?
                mecids.append(mec.mecid)
                event.mecs.append(mec)
                include_origin = True
                if mec.mechtype == 'FP':
                    mecids_fp.append(mec.mecid)

        if netmag and (netmag.magid==event.prefmag or args.includeallmagnitudes) \
           and netmag not in event.netmags:
            event.netmags.append(netmag)
            if netmag.magid==event.prefmag:
                event.preferred_magnitude=netmag

        if origin_error:
            origin.origin_error = origin_error

        if include_origin and origin not in event.origins:
            event.origins.append(origin)
            if orid==event.prefor:
                event.preferred_origin=origin

    # Some events can have event.prefmag (prefor) set but the corresponding netmag.magid (origin) row is missing
    clean_events = []
    for event in events:
            #logger.warning("evid:%d has no preferred magnitude!! --> Dropping" % event.evid)
        if getattr(event, 'preferred_origin', None) is None:
            logger.warning("evid:%d has no preferred origin!! --> Dropping event" % event.evid)
            continue
        if getattr(event, 'preferred_magnitude', None) is None:
            event.preferred_magnitude = missing_netmag(auth=event.preferred_origin.auth)

        clean_events.append(event)

    events = clean_events

    return events, mecids, mecids_fp


def queryArrivals(Session, orids, events):

    with Session.begin() as session:
        arr_rows = session.query(Assocaro, Arrival).\
                          filter(Assocaro.arid==Arrival.arid).\
                          filter(Assocaro.orid.in_(orids)).\
                          order_by(Assocaro.orid).all()
                          #order_by(Arrival.sta, Arrival.seedchan).all()
        session.expunge_all()

    arr_dict={}
    old = -9
    new = -9
    #logger.debug("[%8.4e] [Done] Query [%d] Arrivals" % (time() - start, len(arr_rows)))
    for row in arr_rows:
        orid = row[0].orid
        if orid == old:
            #print("Hasn't changed")
            arrivals.append(row)
        else:
            #print("New orid=%d" % orid)
            arrivals=[]
            arr_dict[orid]=arrivals
            arrivals.append(row)
            old = orid

    #logger.debug("[%8.4e] [Done] Sort arr_rows" % (time() - start))

    for event in events:
        event.picks = []
        for origin in event.origins:
            #origin.leapsecs = get_leapseconds_from_timestamp(origin.datetime)
            origin.arrivals = []
            rows = arr_dict[origin.orid] if origin.orid in arr_dict else []

            for ass, arr in rows:
                arr.datetime -= origin.leapsecs
                arrival, pick = aqms_arrival_to_obspy(ass, arr, prefix=os.path.join(resource_id_base, "Arrival/%s/%s" % (arr.auth, str(arr.arid))))
                origin.arrivals.append(arrival)
                event.picks.append(pick)


def queryAmplitudes(Session, orids, events):

    with Session.begin() as session:
        amp_rows = session.query(Assocamm, Amp).\
                          join(Assocamo, Assocamo.ampid==Amp.ampid).\
                          filter(Assocamm.ampid==Amp.ampid).\
                          filter(Assocamo.orid.in_(orids)).\
                          order_by(Assocamm.magid).all()
                          #order_by(Amp.ampid).all()
        session.expunge_all()

    amp_dict={}
    old = -9
    new = -9
    for row in amp_rows:
        magid = row[0].magid
        if magid == old:
            amplitudes.append(row)
        else:
            amplitudes=[]
            amp_dict[magid]=amplitudes
            amplitudes.append(row)
            old = magid

    for event in events:
        event.amplitudes = []
        event.station_magnitudes = []

        for netmag in event.netmags:
            station_mag_contributions = []

            #rows = [row for row in amp_rows if row[0].magid==netmag.magid]
            rows = amp_dict[netmag.magid] if netmag.magid in amp_dict else []
            #print("  Netmag magid:%d --> Loop over %d amp rows" % (netmag.magid, len(rows)))

            for ass, amp in rows:
                #amp.datetime -= event.origins[0].leapsecs
                leapsecs = get_leapseconds_from_timestamp(amp.datetime)
                amp.datetime -= leapsecs
                if amp.wstart > 0:
                    amp.wstart -= leapsecs
                amplitude, station_mag, station_mag_contribution = aqms_amp_to_obspy(ass, amp,
                          prefix=os.path.join(resource_id_base, "Amp/%s/%s" % (amp.auth, str(amp.ampid))),
                          orid=netmag.orid)
                event.amplitudes.append(amplitude)
                event.station_magnitudes.append(station_mag)
                station_mag_contributions.append(station_mag_contribution)

            netmag.station_magnitude_contributions = station_mag_contributions

def queryMecs(Session, mecids, events):

    with Session.begin() as session:
        rows = session.query(Mecdata, Mecchannel).\
                          outerjoin(Mecchannel, Mecchannel.mecdataid==Mecdata.mecdataid).\
                          filter(Mecdata.mecid.in_(mecids)).\
                          order_by(Mecdata.mecdataid).all()
        session.expunge_all()

    extras = {}
    for mecdata, mecchannel in rows:
        mecid = mecdata.mecid
        mecdataid = mecdata.mecdataid
        if mecid not in extras:
            extras[mecid] = {}
        extras[mecid][mecdataid] = {}
        extras[mecid][mecdataid]['net'] = mecchannel.net
        extras[mecid][mecdataid]['sta'] = mecchannel.sta
        extras[mecid][mecdataid]['cha'] = mecchannel.seedchan
        extras[mecid][mecdataid]['loc'] = mecchannel.location
        extras[mecid][mecdataid]['polarity'] = mecdata.polarity
        #extras[mecid][mecdataid]['quality'] = mecdata.quality
        #extras[mecid][mecdataid]['time'] = mecdata.time1

    #for mecid in sorted(extras.keys()):
        #print("mecid:%s" %mecid)

    for event in events:
        event.focal_mechanisms = []
        for mec in event.mecs:
            if mec.mecid not in extras:
                logger.warning(f"Unable to locate mecid:{mec.mecid} in extras --> Skipping!")
                continue
            mec.extras = extras[mec.mecid]
            prefix=os.path.join(resource_id_base, "Mec/%s/%s" % (mec.auth, str(mec.mecid)))
            focalmech = aqms_mec_to_obspy(mec, prefix=prefix)
            focalmech.triggering_origin_id = os.path.join(resource_id_base, "Origin/%s/%s" % (mec.auth, str(mec.oridin)))
            event.focal_mechanisms.append(focalmech)

def update_config_with_db(config, dbname):
#'sqlalchemy.url': 'postgresql://rtem:rtem_pass@localhost:5432/myarch'}
    conn_string = config['sqlalchemy.url']
    config['sqlalchemy.url'] = conn_string.replace(os.path.basename(conn_string), dbname)
    config['DB_NAME'] = dbname


def get_catalog(events, fmt, text_format):
    debug = False

    match fmt:
        case 'text':
            match text_format:
                case 'detail':
                    return write_detail(events, debug=debug)
                case 'ncedc':
                    return write_ncedc(events, debug=debug)
                case _:
                    return write_text(events, catalog=catalog_name)
        case 'json':
            return write_json(events)
        case 'geojson':
            return write_geojson(events)
        case 'fpfit':
            return write_fpfit(events)
            #return write_fpfit(events, mechtype='MT')
        case 'csv':
            return write_csv(events)
        case 'xml':
            return write_quakeml(events, resource_id_base, ns_catalog)
        case 'phase':
            return write_phase(events)
        case 'kml':
            return write_kml(events)
    return None



from enum import Enum

class orderBy(Enum):
    time = 'time'
    timeasc = 'time-asc'
    magnitude = 'magnitude'
    magnitudeasc = 'magnitude-asc'

    def __str__(self):
        return self.value



def validate_args(args):

    minmax_lat_lon_keys = ['minlat','maxlat','minlon','maxlon']

    radius_required_keys = ['lat','lon','maxradius']
    dist_required_keys   = ['lat','lon','maxdist']

    use_radius = False
    use_dist = False
    if getattr(args, 'maxradius', None):
        if getattr(args, 'maxdist', None):
            logger.error("Select *either* --maxradius *or* --maxdist but NOT both!")
            return False
        use_radius = True
    elif getattr(args, 'maxdist', None):
        use_dist = True

    if use_radius or use_dist:
        nfound = 0
        for key in ['lat', 'lon']:
            if getattr(args, key, None) is not None:
                nfound += 1
        if nfound < 2:
            _param = "--maxradius" if use_radius else "--maxdist"
            logger.error("%s requires --lat and --lon to be set as well!" % _param)
            return False

        for k in minmax_lat_lon_keys:
            if getattr(args, k, None) is not None:
                #logger.error("You must specify *either* minlat,maxlat,etc *or* lat,lon,minradius,maxradius ==> You cannot specify *both*!")
                logger.error("You must specify *either* minlat,maxlat,etc *or* lat,lon,minradius,maxradius ==> You cannot specify *both*!")
                return False

    # We'll do all the filtering using SP that calcs dist in km
    if args.maxradius:
        args.mindist = args.minradius * DEG2KM
        args.maxdist = args.maxradius * DEG2KM

    nfound = 0
    start_keys = ['starttime', 'updatedafter']
    for key in start_keys:
        if getattr(args, key, None) is not None:
            nfound += 1
    if nfound == len(start_keys):
        logger.error("You must specify *either* --starttime *or* --updatedafter but not BOTH!")
        return False

    # updated after seems to filter on: origin.lddate >= updated_after 

    return True


def get_optional_args():

    # Optional args - follow flag names from fprun
    o = []

    o.append(optional_arg(arg=['--minlat','--minlatitude'],  type=float,
                          help='--minlat 30.5 [Default -90]'))
    o.append(optional_arg(arg=['--maxlat', '--maxlatitude'],  type=float,
                          help='--maxlat 47.5 [Default +90]'))

    o.append(optional_arg(arg=['--minlon','--minlongitude'], type=float,
                          help='--minlon -120.5 [Default -180]'))
    o.append(optional_arg(arg=['--maxlon','--maxlongitude'], type=float,
                          help='--maxlon -118 [Default +180]'))

    o.append(optional_arg(arg=['--lat','--latitude'],  type=float,
                          help='--lat 30.5 (include events within dist:minrad-maxrad of <lat,lon>)'))
    o.append(optional_arg(arg=['--lon','--longitude'],  type=float,
                          help='--lon -118'))

    o.append(optional_arg(arg=['--minradius'],  type=float, description='DEG', default=0.,
                          help='--minradius 1.5  [deg] [Default: 0]'))
    o.append(optional_arg(arg=['--maxradius'],  type=float, description='DEG',
                          help='--maxradius 17[deg]'))

    o.append(optional_arg(arg=['--mindist'],  type=float, description='KM', default=0.,
                          help='--mindist 1.5  [km] [Default: 0]'))
    o.append(optional_arg(arg=['--maxdist'],  type=float, description='KM',
                          help='--maxdist 26.5[km]'))


    o.append(optional_arg(arg=['--mindep','--mindepth'],  type=float,
                          help='--mindep 10.0 [Default -10 (km)]'))

    o.append(optional_arg(arg=['--maxdep','--maxdepth'],  type=float,
                          help='--maxdep 30.0 [Default 1e5]'))

    o.append(optional_arg(arg=['--minmag','--minmagnitude'],  type=float,
                          help='--minmag 3.0 [Default=-2]'))

    o.append(optional_arg(arg=['--maxmag','--maxmagnitude'],  type=float,
                          help='--maxmag 6. [Default=10]'))

    o.append(optional_arg(arg=['--magtype', '--magnitudetype'], type=str, metavar='MAGTYPE',
                          help='Apply --min/maxmag limits to this magtype (e.g,. --magtype w)'))

    o.append(optional_arg(arg=['--starttime', '--start'], type=UTCDateTime, description='starttime',
                          help='--starttime 2022-01-01T04'))
    o.append(optional_arg(arg=['--endtime', '--end'],   type=UTCDateTime, description='endtime',
                          help='--endtime 2022-01-02T10:47:30'))
    o.append(optional_arg(arg=['--updatedafter'], type=UTCDateTime, description='UTC',
                          help='--updatedafter 2022-01-01T04'))

    o.append(optional_arg(arg=['--limit',],  type=int, description='N',
                          help='--limit 100 (only return 100 events starting at offset)'))
    o.append(optional_arg(arg=['--offset',],  type=int, description='ioff', default=0,
                          help='--offset 10 (return first N events starting at 10)'))

    o.append(optional_arg(arg=['--includeallorigins'], type=bool,
                          help="Include all origins for event [Default=event.prefor only]"))
    o.append(optional_arg(arg=['--includeallmagnitudes'], type=bool,
                          help="Include all netmags for event [Default=event.premag only]"))
    o.append(optional_arg(arg=['--includemechanisms', '--includemecs', '--mecs'], type=bool,
                          help="Include any mechanisms for event"))
    o.append(optional_arg(arg=['--includearrivals'], type=bool,
                          help="Include origin arrivals"))
    o.append(optional_arg(arg=['--includeamplitudes'], type=bool,
                          help="Include pick amplitudes"))

    o.append(optional_arg(arg=['-E', '--eventid', '--evid',], type=list_int, description='id1,..',
                          help='--eventid 6002347,5082341,...'))


    o.append(optional_arg(arg=['--etype', '--eventtype'], type=str, default='eq',
                          help='--eventtype eq [Default=eq]'))

    o.append(optional_arg(arg=['--orderby'], type=orderBy, default='time',
                          help='--orderby time-asc  OPTIONS={time,time-asc,magnitude,magnitude-asc} [Default=time]'))

    o.append(optional_arg(arg=['--catalog', '--db'], type=str, description='DBNAME', choices=['hypodd','archdb1', 'myarch'],
                          help='--catalog hypodd'))

    o.append(optional_arg(arg=['-F', '--fmt', '--format'], type=str, default='xml', choices=output_formats,
                          description='FMT',
                          help='catalog output format Options:{%s} [Default=xml]'%','.join(output_formats)))

    o.append(optional_arg(arg=['-T', '--text-format'], type=str, default='neat', choices=text_formats,
                          description='fmt',
                          help='text-format Options:{%s} [Default=neat]'%','.join(text_formats)))

    o.append(optional_arg(arg=['--return_count'], type=bool,
                          help="Return count of matching events"))

    o.append(optional_arg(arg=['-o', '--outfile'], type=str, #default='quakeml.xml',
                          help='Name of catalog output file'))

    o.append(optional_arg(arg=['-s','--stdout'], type=bool, default=False,
                          help="output results to stdout (ignored if format=xml)"))

    # Use this option to limit events to those from this contributor
    o.append(optional_arg(arg=['--contributor'], type=str,
                          help="limit returned events to those with event.auth=contributor"))

    return o

def list_int(values):
    return [int(val) for val in values.split(',')]


if __name__ == "__main__":
    main()
