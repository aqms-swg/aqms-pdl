# -*- coding: utf-8 -*-
"""
Read in quakeml file + flags and insert into AQMS db
Triggered by PDL

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import numpy as np
import os

from obspy.core.event.base import Comment
from obspy.core.event.base import ConfidenceEllipsoid
from obspy.core.event.base import CreationInfo
from obspy.core.event.base import QuantityError
from obspy.core.event.base import WaveformStreamID
from obspy.core.event.event import Event
from obspy.core.event.magnitude import Amplitude
from obspy.core.event.magnitude import Amplitude
from obspy.core.event.magnitude import Magnitude
from obspy.core.event.magnitude import StationMagnitude
from obspy.core.event.magnitude import StationMagnitudeContribution
from obspy.core.event.origin import Arrival
from obspy.core.event.origin import Origin
from obspy.core.event.origin import OriginQuality
from obspy.core.event.origin import OriginUncertainty
from obspy.core.event.origin import Pick
from obspy.core.utcdatetime import UTCDateTime

import logging
logger = logging.getLogger()

###  Converters from AQMS rows --> obspy/quakeml objects

def aqms_event_to_obspy(aqms_event, prefix):

    event = Event(force_resource_id=False)
    event.resource_id = prefix
    if aqms_event.etype.lower() == 'eq':
        event.event_type = "earthquake"

    if aqms_event.remark:
        #Comment(force_resource_id=True, text=aqms_event.remark.remark, creation_info)
        #event.comments = [Comment(force_resource_id=True, text=aqms_event.remark.remark)]
        event.comments = [Comment(force_resource_id=True, text=aqms_event.remark)]

    event.creation_info = CreationInfo(agency_id=aqms_event.auth, creation_time=aqms_event.lddate,
                                       version=aqms_event.version)

    return event

def get_evaluation_mode_and_status(rflag):
    evaluation_mode = 'automatic'
    evaluation_status = 'preliminary'
    if rflag.lower() in ['h', 'f', 'i']:
        evaluation_mode = 'manual'
        if rflag.lower() == 'f':
            evaluation_status = "final"
        else:
            evaluation_status = "reviewed"

    return evaluation_mode, evaluation_status


KM_TO_M = 1000.
def aqms_origin_to_obspy(aqms_origin, prefix):

    origin = Origin(force_resource_id=False)
    origin.resource_id = prefix

    origin.time = UTCDateTime(aqms_origin.datetime)
    origin.latitude  = float(aqms_origin.lat)
    origin.longitude = float(aqms_origin.lon)
    origin.depth     = float(aqms_origin.depth) * KM_TO_M
    origin.origin_type = "hypocenter"

    if aqms_origin.algorithm:
        origin.method_id = "smi:OK.isti.com/origin/%s" % aqms_origin.algorithm

    origin.evaluation_mode, origin.evaluation_status = get_evaluation_mode_and_status(aqms_origin.rflag)
    origin.creation_info = CreationInfo(agency_id=aqms_origin.auth, creation_time=aqms_origin.lddate)

    origin.time_errors  = QuantityError(uncertainty=aqms_origin.stime)
    # The ECS origins don't have sdep set for some reason!
    if aqms_origin.sdep:
        origin.depth_errors = QuantityError(uncertainty=aqms_origin.sdep * KM_TO_M)
    # MTH: What units are erlat/erlon in ??
    if aqms_origin.erlat:
        origin.latitude_errors = QuantityError(uncertainty=aqms_origin.erlat)
    if aqms_origin.erlon:
        origin.longitude_errors = QuantityError(uncertainty=aqms_origin.erlon)
    if aqms_origin.fdepth == 'y':
        origin.depth_type = "operator assigned"
    if aqms_origin.ftime == 'y':
        origin.time_fixed = True
    if aqms_origin.fepi == 'y':
        origin.epicenter_fixed = True

    orig_quality = OriginQuality()
    orig_quality.standard_error = aqms_origin.wrms
    orig_quality.azimuthal_gap = aqms_origin.gap
    origin.quality = orig_quality
    #orig_quality.used_phase_count = x
    #orig_quality.used_station_count = x


    arrivals = [arr for arr in aqms_origin.arrivals if arr.distance is not None]
    aqms_origin.arrivals = arrivals
    if aqms_origin.arrivals:
        sorted_arrivals = sorted(aqms_origin.arrivals, key=lambda x: x.distance, reverse=False)
        orig_quality.associated_phase_count = len(sorted_arrivals)
        orig_quality.minimum_distance = sorted_arrivals[0].distance
        orig_quality.maximum_distance = sorted_arrivals[-1].distance
        orig_quality.median_distance = np.median([arr.distance for arr in sorted_arrivals])
        #print("orid:%d has %d arrivals. min_dist:%f max_dist:%f mean_dist:%f" %
            #(aqms_origin.orid, len(sorted_arrivals), orig_quality.minimum_distance, orig_quality.maximum_distance, orig_quality.median_distance))
        stns = [arr.pick_id.get_referred_object().waveform_id.station_code for arr in aqms_origin.arrivals]
        #print("Before: len(stns)=%d" % len(stns))
        stns = set(stns)
        #print(" After: len(stns)=%d" % len(stns))
        orig_quality.associated_station_count = len(stns)


    if aqms_origin.origin_error:
        # MTH: The next 2 come from qml which says they are in Hypoinv manual Appendix 3
        #      eg, to get 95% confidence, need to multiply error vals by 2.4 and also by 1000 (to convert km to m)
        confidenceLevel = 95
        scale = 2.4 * KM_TO_M

        origin_uncertainty = OriginUncertainty()
        origin.origin_uncertainty = origin_uncertainty
        conf_ellipsoid = ConfidenceEllipsoid()

        if aqms_origin.origin_error.magsmall:
            conf_ellipsoid.semi_minor_axis_length = float(aqms_origin.origin_error.magsmall) * scale
        if aqms_origin.origin_error.maginter:
            conf_ellipsoid.semi_intermediate_axis_length = float(aqms_origin.origin_error.maginter) * scale
        if aqms_origin.origin_error.maglarge:
            conf_ellipsoid.semi_major_axis_length = float(aqms_origin.origin_error.maglarge) * scale

        if aqms_origin.origin_error.azilarge:
            conf_ellipsoid.major_axis_azimuth = aqms_origin.origin_error.azilarge
        if aqms_origin.origin_error.diplarge:
            conf_ellipsoid.major_axis_plunge = aqms_origin.origin_error.diplarge

        origin_uncertainty.confidence_ellipsoid = conf_ellipsoid
        origin_uncertainty.preferred_description = "confidence ellipsoid"
        origin_uncertainty.confidence_level = 95.
        # MTH: is erhor in KM ??
        if aqms_origin.erhor > 0:
            origin_uncertainty.horizontal_uncertainty = aqms_origin.erhor * KM_TO_M

    elif aqms_origin.erhor:
        origin_uncertainty = OriginUncertainty()
        origin_uncertainty.preferred_description = "horizontal uncertainty"
        origin_uncertainty.horizontal_uncertainty = aqms_origin.erhor * KM_TO_M
        origin.origin_uncertainty = origin_uncertainty


    return origin


def aqms_netmag_to_obspy(aqms_netmag, prefix):

    mag = Magnitude(force_resource_id=False)
    mag.resource_id = prefix
    mag.mag = aqms_netmag.magnitude
    mag.magnitude_type = 'ML' if aqms_netmag.magtype.lower() == 'l' else aqms_netmag.magtype

    mag.station_count = aqms_netmag.nsta
    mag.azimuthal_gap = aqms_netmag.gap
    mag.evaluation_mode, mag.evaluation_status = get_evaluation_mode_and_status(aqms_netmag.rflag)
    mag.creation_info = CreationInfo(agency_id=aqms_netmag.auth, creation_time=aqms_netmag.lddate)
    if aqms_netmag.magalgo:
        mag.method_id = "smi:OK.isti.com/netmag/%s" % aqms_netmag.magalgo
    if aqms_netmag.uncertainty and aqms_netmag.uncertainty > 0:
        mag.mag_errors = QuantityError(uncertainty=aqms_netmag.uncertainty)

    return mag


from obspy.core.event.source import FocalMechanism, NodalPlane, NodalPlanes
from obspy.core.event.base import Comment

def aqms_mec_to_obspy(aqms_mec, prefix):

    p1 = NodalPlane(strike=aqms_mec.strike1, dip=aqms_mec.dip1, rake=aqms_mec.rake1,
                    strike_errors=QuantityError(uncertainty=aqms_mec.unstrike1),
                    dip_errors=QuantityError(uncertainty=aqms_mec.undip1),
                    rake_errors=QuantityError(uncertainty=aqms_mec.unrake1),
                    )
    p2 = NodalPlane(strike=aqms_mec.strike2, dip=aqms_mec.dip2, rake=aqms_mec.rake2,
                    strike_errors=QuantityError(uncertainty=aqms_mec.unstrike2),
                    dip_errors=QuantityError(uncertainty=aqms_mec.undip2),
                    rake_errors=QuantityError(uncertainty=aqms_mec.unrake2),
                    )

    comments = []
    comments.append("mechtype:%s mecalgo:%s subsource:%s pvr:%s quality:%s" %
                    (aqms_mec.mechtype, aqms_mec.mecalgo, aqms_mec.subsource,
                    aqms_mec.pvr, aqms_mec.quality))
    comments = [Comment(text=comment) for comment in comments]

    fc = FocalMechanism(nodal_planes = NodalPlanes(nodal_plane_1=p1, nodal_plane_2=p2),
                        #azimuthal_gap = out['azim_gap'],
                        station_polarity_count = aqms_mec.nsta,
                        #station_distribution_ratio = out['stdr'],
                        #misfit = out['misfit'],
                        comments = comments,
                        #method_id=ResourceIdentifier('HASH_v1.2')
                       )
    fc.resource_id = prefix
    fc.evaluation_mode, fc.evaluation_status = get_evaluation_mode_and_status(aqms_mec.rflag)
    fc.creation_info = CreationInfo(agency_id=aqms_mec.auth, creation_time=aqms_mec.lddate)

    extras = getattr(aqms_mec, 'extras', None)
    if extras:
        fc.waveform_id = []
        station_polarity_count = 0
        for mecdataid, d in extras.items():
            fc.waveform_id.append(
                WaveformStreamID(network_code=d['net'], station_code=d['sta'],
                                 location_code=d['loc'], channel_code=d['cha'])
            )
            station_polarity_count += 1
        fc.station_polarity_count = station_polarity_count

    #if aqms_mec.mecalgo:
        #fc.method_id = ResourceIdentifier(aqms_mec.mecalgo)

    return fc



def aqms_arrival_to_obspy(aqms_assocaro, aqms_arrival, prefix):
    """
     Convert aqms assocaro + arrival --> obspy Arrival + Pick
    """

    fname = 'aqms_arrival_to_obspy'

    arr = Arrival(force_resource_id=False)
    arr.resource_id = prefix
    arr.creation_info = CreationInfo(agency_id=aqms_arrival.auth, creation_time=aqms_arrival.lddate)

    # MTH 2025-01-28: Add quality in for aqms-catalog --format phase output
    arr.comments = [Comment(text=f"arid:{aqms_arrival.arid} quality:{aqms_arrival.quality} snr:{aqms_arrival.snr}")]

    arr.phase = aqms_assocaro.iphase
    if getattr(aqms_assocaro, 'seaz', None):
        arr.azimuth = float(aqms_assocaro.seaz)
    if getattr(aqms_assocaro, 'ema', None):
        arr.takeoff_angle = float(aqms_assocaro.ema)
    else:
        logger.debug("aqms_assocaro.arid=%d --> ema is None !!" % aqms_assocaro.arid)

    if getattr(aqms_assocaro, 'delta', None):
        #arr.distance      = float(aqms_assocaro.delta) / 111.19    # Convert aqms km to obspy deg
        # Or keep distance in km ?
        arr.distance      = float(aqms_assocaro.delta)  # distance in km
    else:
        logger.debug("aqms_assocaro.arid=%d --> delta is None !!" % aqms_assocaro.arid)

    if aqms_assocaro.timeres:
        arr.time_residual = float(aqms_assocaro.timeres)
    if aqms_assocaro.wgt:
        arr.time_weight   = float(aqms_assocaro.wgt)

    pk = Pick(force_resource_id=False)
    pk.resource_id = prefix.replace("Arrival", "Pick")

    arr.pick_id = pk.resource_id

    pk.time = aqms_arrival.datetime    # AQMS datetime should already have leapsecs removed!
    pk.waveform_id = WaveformStreamID(aqms_arrival.net, aqms_arrival.sta,
                                      aqms_arrival.location, aqms_arrival.seedchan)
    pk.phase_hint     = aqms_assocaro.iphase

 # pick.polarity
    if aqms_arrival.fm:
        if 'c' in aqms_arrival.fm or 'u' in aqms_arrival.fm:
            pk.polarity = 'positive'
        elif 'd' in aqms_arrival.fm or 'r' in aqms_arrival.fm:
            pk.polarity = 'negative'


    if aqms_arrival.delinc:
        arr.takeoff_angle_errors = QuantityError(uncertainty=aqms_arrival.delinc)

 # pick.time_errors
    if aqms_arrival.deltim and aqms_arrival.deltim > 0:
        pk.time_errors = QuantityError(uncertainty=aqms_arrival.deltim)
    elif aqms_arrival.quality:
        pick_uncertainty = None
        if aqms_arrival.quality == 1.0:
            pick_uncertainty = 0.06
        elif aqms_arrival.quality == 0.75:
            pick_uncertainty = 0.12
        elif aqms_arrival.quality == 0.5:
            pick_uncertainty = 0.30
        elif aqms_arrival.quality == 0.25:
            pick_uncertainty = 0.60

        if pick_uncertainty:
            pk.time_errors = QuantityError(uncertainty=pick_uncertainty)

 # pick.onset
    if aqms_arrival.qual:
        if aqms_arrival.qual == 'e':
            pk.onset = 'emergent'
        elif aqms_arrival.qual == 'i':
            pk.onset = 'impulsive'

 # pick.evaluation_mode
    pk.evaluation_mode, pk.evaluation_status = get_evaluation_mode_and_status(aqms_arrival.rflag)

    return arr, pk

def aqms_amp_to_obspy(aqms_assocamm, aqms_amp, prefix, orid=None):
    """
     Convert aqms assocaro + arrival --> obspy Arrival + Pick
    """

    fname = 'aqms_amp_to_obspy'

    waveform_id = WaveformStreamID(aqms_amp.net, aqms_amp.sta,
                                   aqms_amp.location, aqms_amp.seedchan)

    amp = Amplitude(force_resource_id=False)
    amp.resource_id = prefix

    amp.type = 'AML' if aqms_amp.amptype == 'WASF' else aqms_amp.amptype
    amp.magnitude_hint = 'ML' if amp.type == 'AML' else None

    obspy_units_to_aqms_units = {
       'm':'m', 's':'s',
       'm/s':'ms',
       'm/(s*s)':'mss',
    }
    #if aqms_amp.units[0:1] == 'm':
    if aqms_amp.units in obspy_units_to_aqms_units.keys():
        amp.unit = aqms_amp.units
        amp.generic_amplitude = aqms_amp.amplitude
    elif aqms_amp.units[0:2] == 'cm':
        amp.unit = aqms_amp.units.replace('cm', 'm')
        amp.generic_amplitude = aqms_amp.amplitude/100.
    elif aqms_amp.units[0:2] == 'mm':
        amp.unit = aqms_amp.units.replace('mm', 'm')
        amp.generic_amplitude = aqms_amp.amplitude/1000.

    amp.period      = aqms_amp.per
    amp.snr         = aqms_amp.snr
    amp.scaling_time= aqms_amp.wstart  # ??
    amp.time_window = aqms_amp.duration  # ??
    amp.waveform_id = waveform_id

    #pk = Pick()
    #pk.time = aqms_amp.datetime
    #pk.waveform_id = waveform_id
    #pk.phase_hint     = aqms_amp.iphase
    #amp.pick_id = pk.resource_id

 # pick.evaluation_mode
    """
    if aqms_amp.rflag.lower() in ['h', 'f']:
        pk.evaluation_mode = 'manual'
        if aqms_amp.rflag.lower() == 'f':
            pk.evaulation_status = "final"
        else:
            pk.evaulation_status = "reviewed"
    else:
        pk.evaluation_mode = 'automatic'
        pk.evaluation_status = "preliminary"
    """

    station_mag = StationMagnitude(force_resource_id=False)
    station_mag.resource_id = prefix.replace("Amp", "Assocamm")
    if orid:
        station_mag.origin_id = os.path.join(os.path.dirname(prefix.replace("Amp", "Origin")), str(orid))

    station_mag.mag = aqms_assocamm.mag
    station_mag.mag_errors = QuantityError(uncertainty=aqms_assocamm.magres)
    #station_mag.station_magnitude_type = ??
    station_mag.amplitude_id = amp.resource_id
    station_mag.waveform_id  = waveform_id
    station_mag.station_magnitude_type = 'ML' if amp.type == 'AML' else None

    station_mag_cont = StationMagnitudeContribution(force_resource_id=False,
                                                station_magnitude_id=station_mag.resource_id,
                                                residual=aqms_assocamm.magres, weight=aqms_assocamm.weight)
    #StationMagnitudeContribution - doesn't have resource_id
    #station_mag_cont.resource_id = prefix.replace("Amp", "Assocamm")

    return amp, station_mag, station_mag_cont

