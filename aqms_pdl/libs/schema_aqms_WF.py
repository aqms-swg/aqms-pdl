# -*- coding: utf-8 -*-
"""
    Classes that describe AQMS WF tables

    CISN Waveform Schema

    http://ncedc.org/db/Documents/NewSchemas/WF/v1.2.7/WF.1.2.7/index.htm

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime

import logging
logger = logging.getLogger()

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text
from sqlalchemy import Column, DateTime, Integer, Numeric, String, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import CheckConstraint, UniqueConstraint

# create the base class of all ORM classes
#Base = declarative_base()
from aqms_pdl.libs.schema_aqms_utils import Base, fix_location
from aqms_pdl.libs.schema_aqms_utils import truncate_strings

def get_tables():
    Base.TBLNAME_TO_CLASS = {}

    for mapper in Base.registry.mappers:
        cls = mapper.class_
        classname = cls.__name__
        if not classname.startswith('_'):
            tblname = cls.__tablename__
            #print(tblname, cls)
            Base.TBLNAME_TO_CLASS[tblname] = cls
    return Base.TBLNAME_TO_CLASS

'''
Defaults aren't applied until Insert or Update in sqlalchemy
This is a work around so you can use/compare them before that
'''
def init(self, **kwargs):
    tables = get_tables()
    #for t in sorted(tables.keys()):
        #print(t)
    cls_tbl = tables[self.__tablename__]
    truncate_strings(cls_tbl, kwargs)
    table_cols = [x.name for x in cls_tbl.__table__.c]

    has_offdate = 'offdate' in table_cols
    has_location = 'location' in table_cols
    #print("table:%20s has_location:%s has_offdate:%s" %
          #(self.__tablename__, has_location, has_offdate))

    if has_offdate and 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    if has_location:
        fix_location(kwargs)
    cls = getattr(sys.modules[__name__], self.__class__.__name__)
    return super(cls, self).__init__(**kwargs)

'''
Data associating waveforms with events
'''
class AssocWaE(Base):
    __tablename__ = "assocwae"
    wfid = Column(ForeignKey('waveform.wfid'), primary_key=True, nullable=False)
    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False)
    datetime_on  = Column(Numeric, info='Time of first datum', nullable=False)
    datetime_off = Column(Numeric, info='Time of last datum', nullable=False)
    lddate = Column(DateTime, server_default=text('NOW()'))

'''
Summary information on a waveform filenames
'''
class Filename(Base):
    __tablename__ = "filename"

    fileid = Column(Integer, Sequence('fiseq'), primary_key=True, nullable=False)
    dfile  = Column(String(32), nullable=False,
                    info='Data file. This is the file name of a disk-based waveform file')
    datetime_on  = Column(Numeric, nullable=True, info='Time of first datum')
    datetime_off = Column(Numeric, nullable=True, info='Time of last datum')
    nbytes = Column(Numeric, nullable=True,
                    info='Number of bytes. This quantity is the number of bytes in a waveform segment or a file')
    subdirid = Column(ForeignKey('subdir.subdirid'),
                      info='Directory identifier. The key field is a unique identifier for a directory name on a disk')
    lddate = Column(DateTime, server_default=text('NOW()'))
    CheckConstraint('nbytes >= 0', name='fn01')

'''
Summary information on a directory name
'''
class Subdir(Base):
    __tablename__ = "subdir"

    subdirid = Column(Integer, Sequence('subseq'), primary_key=True, nullable=False)
    subdirname  = Column(String(64), nullable=False,
                    info='Path name. This is the name of a disk-based directory')
    lddate = Column(DateTime, server_default=text('NOW()'))

'''
Summary information on a waveform
'''
class Waveform(Base):
    __tablename__ = "waveform"

    wfid = Column(Integer, Sequence('waseq'), primary_key=True, nullable=False,
                  info='Waveform identifier. The key field is a unique identifier '\
                       'for a segment of digital waveform data')
    net  = Column(String(6), nullable=False)
    sta  = Column(String(6), nullable=False)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8), info='A second identifier to specify a unique source within the domain specified by auth')
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used '\
                                        'for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), nullable=False,
                      info='SEED channel name. The first character denotes the band code, the second for the '\
                           'instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the '\
                                      'same network operator ')
    archive = Column(String(8), nullable=False,
                     info='This field specifies the place where the seismogram is archived')
    datetime_on  = Column(Numeric(asdecimal=False), nullable=False, info='Time of first datum')
    datetime_off = Column(Numeric(asdecimal=False), nullable=False, info='Time of last datum')
    samprate = Column(Numeric(asdecimal=False), nullable=False,
                       info='Sampling rate. This attribute is the sample rate in samples/second')
    wavetype = Column(String(1), info='This attribute denotes the type of waveform')

    fileid = Column(ForeignKey('filename.fileid'), nullable=False,
                      info='Filename identifier. The key field is a unique identifier for a physical file on a disk')

    foff = Column(Integer, info='File offset. This is the byte offset of a waveform segment within '\
                                'a data file. If the segment does not contain header information, '\
                                'this value is equal to traceoff')
    nbytes = Column(Integer, info='Number of bytes. This quantity is the number of bytes in a '\
                                      'waveform segment or a file. If the segment does not contain '\
                                      'header information, this value is equal to tracelen; otherwise '\
                                      'nbytes = headerlen + tracelen')
    traceoff = Column(Integer, info='Trace offset. This is the byte offset of the entire trace ')
    tracelen = Column(Integer, info='Number of bytes. This quantity represents the length of the entire trace')
    status = Column(String(1), nullable=False, info='This attribute denotes the status of the waveform in the file')
    wave_fmt = Column(Integer, info='Waveform type')
    format_id = Column(Integer, info='This attribute denotes a code indicating the encoding format. This number is assigned by the FDSN Data Exchange Working Group')
    wordorder = Column(Integer, info='Word order')
    recordsize = Column(Integer,
                        info='Record size for data. For MiniSEED, the value is the MiniSEED record size, '\
                             'eg. 512, 4096, etc')
    locevid = Column(String(12),
                     info='Local event identifier. This attribute describes the original '\
                          'event identifier of the event. In combination with auth and subsource, '\
                          'it can be used to uniquely identify the original event identifier from '\
                          'that source. For example, waveforms from the original source may be '\
                          'associated with this identifier')
    qc_level = Column(String(1),
                      info='This flag specifies the Data Header/Quality indicator flag for the specified waveform. '\
                           'Reference: SEED manual in Fixed Section of Data Header.' \
                           'For non-MiniSEED data, this would be the MiniSEED flag to be used '\
                           'if the data would be converted to MiniSEED')

    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('fileid > 0', name='wf01')
    CheckConstraint('foff >= 0', name='wf02')
    CheckConstraint('format_id >= 1', name='wf03')
    CheckConstraint('recordsize >= 0', name='wf04')
    CheckConstraint('samprate > 0', name='wf05')
    CheckConstraint('tracelen >= 0', name='wf06')
    CheckConstraint('traceoff >= 0', name='wf07')
    CheckConstraint("wavetype in ('C','T')", name='wf08')
    CheckConstraint('wave_fmt >= 1', name='wf09')
    CheckConstraint('wfid > 0', name='wf10')
    CheckConstraint('wordorder >= 0', name='wf11')
    CheckConstraint('nbytes >= 0', name='wf12')
    CheckConstraint("qc_level in ('R','D','Q','M')", name='wf13')
    CheckConstraint("status in ('E','T','A')", name='wf14')

    def __init__(self, **kwargs):
        return init(self, **kwargs)


'''
Summary information on possible locations of waveforms based on common columns it shares with the WAVEFORM table
'''
class Waveroots(Base):
    __tablename__ = "waveroots"

    archive  = Column(ForeignKey('waveform.archive'), primary_key=True, nullable=False)
    wavetype = Column(ForeignKey('waveform.wavetype'), primary_key=True, nullable=False)
    status   = Column(ForeignKey('waveform.status'), primary_key=True, nullable=False)
    net      = Column(ForeignKey('waveform.net'), primary_key=True, nullable=False)
    wave_fmt = Column(ForeignKey('waveform.wave_fmt'), primary_key=True, nullable=False)
    datetime_on  = Column(ForeignKey('waveform.datetime_on'), primary_key=True, nullable=False)
    datetime_off = Column(ForeignKey('waveform.datetime_off'), primary_key=True, nullable=False)
    wcopy = Column(Integer, primary_key=True, nullable=False)
    fileroot = Column(String(256), nullable=False,
                      info='The top file directory of the waveform file. '\
                      'Subdirectories are allowed (i.e. /top_directory/subdirectory1/subdirectory2) '\
                      'but the top level must be included and it must be consistent with entries in SUBDIR')


    CheckConstraint("wcopy > 0", name='wfroot01')
    CheckConstraint("wavetype in ['T','C']", name='wfroot02')
    CheckConstraint("status in ['T','A']", name='wfroot03')
    CheckConstraint("wave_fmt in [1,2,3,4]", name='wfroot04')
