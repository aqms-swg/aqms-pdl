# -*- coding: utf-8 -*-
"""
Functions to read/process aqms Amps

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import glob
import logging
import os
from obspy.core.utcdatetime import UTCDateTime
import xml.etree.ElementTree as ET

from aqms_pdl.libs.schema_aqms_PI import Unassocamp

logger = logging.getLogger()

EMPTY_LOCATION_CODE = "  "

def unassoc_amplitude_to_aqms(amp_dict, ampid=None, auth=None, subsource=None):
    '''
    Convert an amp_dict read from PDL unassociated amp XML
        to AQMS amp format for insertion into unassoc table
    '''

    fname = 'unassoc_amplitude_to_aqms'

    AQMSAmplitude = Unassocamp(ampid=ampid, auth=auth, subsource=subsource)
    # MTH: wstart is not nullable. AQMS docs say:
    #    * In the case where we know only the time of the amplitude but not the window, we set:
    #      datetime = [AMPLITUDE TIME]
    #      wstart = [AMPLITUDE TIME]
    #      duration = 0
    AQMSAmplitude.datetime = amp_dict['datetime']
    AQMSAmplitude.wstart = amp_dict['wstart']
    AQMSAmplitude.duration = 0.

    AQMSAmplitude.sta      = amp_dict['sta']
    AQMSAmplitude.net      = amp_dict['net']
    AQMSAmplitude.channel  = amp_dict['seedchan']
    AQMSAmplitude.seedchan = amp_dict['seedchan']
    empty_locations = ['', '--']
    if 'location' in amp_dict and amp_dict['location'] not in empty_locations:
        AQMSAmplitude.location = amp_dict['location']
    else:
        AQMSAmplitude.location = EMPTY_LOCATION_CODE

    AQMSAmplitude.amplitude= amp_dict['amplitude']
    AQMSAmplitude.units    = amp_dict['units']
    AQMSAmplitude.amptype  = amp_dict['amptype']
    if 'per' in amp_dict:
        AQMSAmplitude.per      = amp_dict['per']

    return AQMSAmplitude

def scan_xml_amps(xmldir):
    '''
    Scan PDL XML unassoc msg
    '''
    files = glob.glob(xmldir + '/' + '*.xml')
    amps = []
    for xmlfile in files:
        file = os.path.basename(xmlfile)
        if file not in ['product.xml']:
            logger.info("scan_xml_amps: xmlfile=[%s]" % xmlfile)
            amps.extend(scan_xml_amp(xmlfile))
            #for amp in amps:
                #print(amp)
    return amps

def scan_xml_amp(xmlfile):
    '''
    Scan xmlfile (unassociated amplitudes from PDL) into list of AQMS amp dicts
    '''
    tree = ET.parse(xmlfile)
    root = tree.getroot()

    if root.attrib['agency'] == "NEIC":
        amps = scan_xml_amp_NEIC(root)
    else:
        amps = scan_xml_amp_NCSN(root)

    return amps

def scan_xml_amp_NEIC(root):
    '''
    NEIC format: 1 <station> per file contains all <component> elems

<?xml version="1.0" encoding="US-ASCII" standalone="yes"?>
<amplitudes agency="NEIC">
  <record>
    <timing>
      <reference zone="GMT" quality="0.5">
        <year value="2020"/><month value="3"/><day value="6"/><hour value="14"/><minute value="59"/><second value="39"/>
      </reference>
      <trigger value="0"/>
    </timing>
    <station code="HOPS" lat="38.99349" lon="-123.07234" name="Hopland Research and Extension Center, Hopland, CA" netid="BK">
      <component name="BHZ">
        <acc value="0.007363" units="cm/s/s"/>
        <vel value="0.000284" units="cm/s"/>
        <sa period="0.3" value="0.010063" units="cm/s/s"/>
        <sa period="1.0" value="0.005748" units="cm/s/s"/>
        <sa period="3.0" value="0.000654" units="cm/s/s"/>
      </component>
    '''

    flatten_units = {'cm' : 'cm',
                     'cm/s' : 'cms',
                     'cm/s/s' : 'cmss',
                     }

    amps = []

    for record in root.findall('record'):

        timing = record.find('timing')
        stations = record.findall('station')
        quality = timing.find('reference').attrib['quality']
        datetime = []
        for child in timing.find('reference'):
            datetime.append(int(child.attrib['value']))
        datetime = UTCDateTime(*datetime)
        #print(datetime)

        for station in stations:
            components = station.findall('component')
            for component in components:
                for elem in component:      # ['acc','vel', (3) 'sa']
                    amp_dict = {}
                    amp_dict['sta'] = station.attrib['code']
                    amp_dict['net'] = station.attrib['netid']
                    amp_dict['seedchan'] = component.attrib['name']
                    amp_dict['amplitude'] = float(elem.attrib['value'])
                    amp_dict['units'] = flatten_units[elem.attrib['units']]

                    amp_dict['datetime'] = datetime
                    amp_dict['wstart'] = datetime

                    if elem.tag == 'acc':
                        amp_dict['amptype'] = 'PGA'
                    elif elem.tag == 'vel':
                        amp_dict['amptype'] = 'PGV'
                    elif elem.tag == 'sa':
                        if 'period' in elem.attrib:
                            if elem.attrib['period'] == '0.3':
                                amp_dict['amptype'] = 'SP.3'
                            elif elem.attrib['period'] == '1.0':
                                amp_dict['amptype'] = 'SP1.0'
                            elif elem.attrib['period'] == '3.0':
                                amp_dict['amptype'] = 'SP3.0'
                            else:
                                print("ERROR: Unknown period:%s" % elem.attrib['period'])

                            amp_dict['per'] = float(elem.attrib['period'])

                    amps.append(amp_dict)

    return amps

def scan_xml_amp_NCSN(root):
    '''
    File contains several component measures (acc, vel, sa, etc) for a *single* station,
         all <component> elements are contained in one <station> outer element
         No location code attrib on <component>
         No datetime attrib on <acc/vel/sa>

<amplitudes agency="NC">
 <record>
  <timing>
   <reference zone="GMT" quality="0.5">
    <PGMTime>2020-03-05T17:36:07.800Z</PGMTime>
   </reference>
   <trigger value="0" />
  </timing>
  <station code="Q0046" net="CI" lat="34.14510" lon="-117.65051" name="Monterrey Street La Verne">
   <component name="HNZ" loc="01" qual="4">
    <pga value="26.054129" units="cm/s/s" datetime="2020-03-05T17:36:47.920Z" />
    <pgv value="0.182617" units="cm/s" datetime="2020-03-05T17:36:47.900Z" />
    <pgd value="0.007777" units="cm" datetime="2020-03-05T17:36:49.735Z" />
    <sa period="0.3" value="3.848229" units="cm/s/s" datetime="2020-03-05T17:36:48.015Z" />
    <sa period="1.0" value="0.580610" units="cm/s/s" datetime="2020-03-05T17:36:47.910Z" />
    <sa period="3.0" value="0.058211" units="cm/s/s" datetime="2020-03-05T17:36:47.910Z" />
   </component>
  </station>
 </record>
</amplitudes>
    '''

    flatten_units = {'cm' : 'cm',
                     'cm/s' : 'cms',
                     'cm/s/s' : 'cmss',
                     }

    amps = []

    for record in root.findall('record'):

        timing = record.find('timing')
        stations = record.findall('station')
        quality = timing.find('reference').attrib['quality']
        pgmtime = timing.find('reference').find('PGMTime')

        for station in stations:
            # 1 component per station in these xml files
            component = station.find('component')
            for elem in component:      # ['pga', 'pgv', 'pgd', (3) 'sa']
                amp_dict = {}
                amp_dict['sta'] = station.attrib['code']
                amp_dict['net'] = station.attrib['net']
                amp_dict['seedchan'] = component.attrib['name']
                amp_dict['location'] = component.attrib['loc']
                # MTH: qual 0-4 needs to be mapped to 0<=quality<=1.0
                #amp_dict['quality'] = component.attrib['qual']
                amp_dict['amplitude'] = float(elem.attrib['value'])
                amp_dict['units'] = flatten_units[elem.attrib['units']]
                amp_dict['wstart'] = elem.attrib['datetime']
                amp_dict['datetime'] = elem.attrib['datetime']

                amp_dict['amptype'] = elem.tag.upper()
                if 'period' in elem.attrib:
                    if elem.attrib['period'] == '0.3':
                        amp_dict['amptype'] = 'SP.3'
                    elif elem.attrib['period'] == '1.0':
                        amp_dict['amptype'] = 'SP1.0'
                    elif elem.attrib['period'] == '3.0':
                        amp_dict['amptype'] = 'SP3.0'
                    else:
                        print("ERROR: Unknown period:%s" % elem.attrib['period'])

                    amp_dict['per'] = float(elem.attrib['period'])

                amps.append(amp_dict)

    return amps

if __name__ == "__main__":
    main()
