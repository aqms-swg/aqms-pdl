# -*- coding: utf-8 -*-
"""
Functions for converting obspy objects (Event, Origin, Magnitude, etc)
to AQMS rows (event, origin, netmag, etc)

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

from aqms_pdl.libs.libs_db import query_origins_around_origin_time, query_origin_by_evid
from aqms_pdl.libs.libs_util import get_leapsecs_from_file
from aqms_pdl.libs.schema_aqms_PI import Event, Origin, Arrival, Amp, Netmag, Mec
from aqms_pdl.libs.schema_aqms_PI import Assocamm, Assocamo, Assocaro, Assocevents
from aqms_pdl.libs.schema_aqms_PI import Eventprefmag, Eventprefor, Eventprefmec
from aqms_pdl.libs.schema_aqms_PI import Remark, Remark_Origin
#from aqms_pdl.libs.schema_aqms_utils import fix_int_fields
from aqms_pdl.libs.schema_aqms_PI import fix_int_fields

from aqms_pdl.libs.util_cls import diff, thresh

import os
import numpy as np
import obspy
from obspy.core.utcdatetime import UTCDateTime
from obspy.geodetics.base import gps2dist_azimuth

import logging
logger = logging.getLogger()

KM_PER_DEG = 111.19

def get_assoc_thresh(config):

    # Threshold for brute association via dt,dx,dz
    assoc_thresh = thresh(time=2., km=10., depth=20.)

    if 'assoc_thresh' in config and 'time' in config['assoc_thresh'] and \
       'km_dist' in config['assoc_thresh'] and \
       'km_depth' in config['assoc_thresh']:
        assoc_thresh = thresh(time=config['assoc_thresh']['time'],
                              km=config['assoc_thresh']['km_dist'],
                              depth=config['assoc_thresh']['km_depth'])

    return assoc_thresh

def get_rflag(obspy_obj):

    '''
    The following AQMS tables contain rflag:
        Amp, Arrival, Coda, Origin, Mec, Netmag,
        Assocamm, Assocamo, Assocaro, Assoccom, Assoccoo

    The following obspy objects have attributes: evalutation_mode + evaluation_status:
        origin, pick, magnitude, focalmechanism
    The rest only have creation_info.author to check

    MTH: It looks like rflag is not consistently used/checked in AQMS
    AQMS Amp, Arrival, Netmag check for rflag.upper() in {'A','H','F'}
    AQMS Origin checks for rflag.upper() in {'A','H','F', 'I', 'C'}
    AQMS Mec - doesn't even check it

    Compromise: We'll only return one of: A, H, F
    '''

    #print("MTH: get_rflag")
    from sqlalchemy.inspection import inspect
    mapper = inspect(globals()['Mec'])
    #print("got mappper=%s" % mapper)


    rflag = 'A'  # overall default

    obspy_classes_with_eval_mode = {
        "Origin", "Magnitude", "FocalMechanism", "Pick"
    }

    # Either of these will work
    # class_name = type(obspy_obj).__name__
    class_name = obspy_obj.__class__.__name__

    # evaluation_mode = {'manual', 'automatic'}
    # evaluation_status = {'preliminary', 'confirmed', 'reviewed', 'final', 'rejected', 'reported'}
    if class_name in obspy_classes_with_eval_mode:
        #print("MTH: class_name=%s" % class_name)
        mode = obspy_obj.evaluation_mode
        status = obspy_obj.evaluation_status
        logger.debug("get_rflag: class_name=%s mode=%s status=%s" % (class_name, mode, status))

        if mode == 'manual':
            # Make this the default for every manual solution except final
            # Note that only Origin allows for "I", so that could also be checked/set here
            rflag = 'H'
            if status == 'final':
                rflag = 'F'
    else:
        if hasattr(obspy_obj.creation_info, 'author') \
            and obspy_obj.creation_info.author is not None:
                # NEIC (?) seems to set obspy_arrival.creation_info.author = 'manual'
                rflag = 'H'

    logger.debug("get_rflag: obspy_obj class=%s --> rflag=%s" % (class_name, rflag))

    return rflag


def getid(obspy_object):
    #return int(os.path.basename(obspy_object.resource_id.id))
    if isinstance(obspy_object, obspy.core.event.resourceid.ResourceIdentifier):
        _id = obspy_object.id
    else:
        _id = os.path.basename(obspy_object.resource_id.id)
    # MTH: Hack for CISN weird ids like.
    #  <event publicID="quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?eventid=39645386" catalog:datasource="ci" ...
    if "=" in _id:
        _id = _id.split("=")[-1]
    #print(type(obspy_object), _id)
    return int(_id)
    #return int(os.path.basename(obspy_object.resource_id.id.split("=")[-1]))

def obspy_event_origin_mag_to_aqms(obspy_event):
    """
    MTH: Created to convert, e.g., NCSN qmls, pulled from fdsnws
         to aqms db Event, Origin, Netmag rows
    try:
        orid = int(os.path.basename(obspy_origin.resource_id.id))
        magid = int(os.path.basename(obspy_mag.resource_id.id))
    except ValueError as e:
        print(e)
        exit()
    """

    #evid = int(os.path.basename(obspy_event.resource_id.id))
    evid = getid(obspy_event)

    event_auth = obspy_event.creation_info.agency_id
    #db_event  = obspy_event_to_aqms(obspy_event, evid=evid, orid=orid, magid=magid, auth=event_auth)
    db_event  = obspy_event_to_aqms(obspy_event, evid=evid, auth=event_auth)

    db_event.origins = []
    db_event.netmags = []
    for obspy_origin in obspy_event.origins:
        #orid = int(os.path.basename(obspy_origin.resource_id.id))
        orid = getid(obspy_origin)
        origin_auth = obspy_origin.creation_info.agency_id

        # Note: we're coming in from obspy/qml, there is no way to know which magnitude
        #       is preferred for a particular origin (only that event.preferred_origin
        #       and event.preferred_magnitude probably go together)
        #db_origin = obspy_origin_to_aqms(obspy_event, evid=evid, orid=orid, magid=magid,
        db_origin = obspy_origin_to_aqms(obspy_event, evid=evid, orid=orid,
                                         obspy_origin=obspy_origin, auth=origin_auth)
        #db_origin.leapsecs = 0
        db_origin.leapsecs = get_leapsecs_from_file(obspy_origin.time)

        # If this incoming origin were in the aqms db it would have leapsecs added in
        # We want to return an equivalent aqms db Origin row
        db_origin.datetime += db_origin.leapsecs

        db_event.origins.append(db_origin)
        if obspy_origin.resource_id.id == obspy_event.preferred_origin().resource_id.id:
            db_event.preferred_origin = db_origin
            db_event.prefor = orid

    for obspy_mag in obspy_event.magnitudes:
        #magid = int(os.path.basename(obspy_mag.resource_id.id))
        magid = getid(obspy_mag)
        mag_auth = obspy_mag.creation_info.agency_id
        #mag_orid = int(os.path.basename(obspy_mag.origin_id.id))
        mag_orid = getid(obspy_mag.origin_id)

        db_netmag = obspy_magnitude_to_aqms(obspy_mag, evid=evid, orid=mag_orid, magid=magid, auth=mag_auth)
        db_event.netmags.append(db_netmag)
        if obspy_mag.resource_id.id == obspy_event.preferred_magnitude().resource_id.id:
            db_event.preferred_magnitude = db_netmag
            db_event.prefmag = magid


    return db_event


def obspy_event_to_aqms(obspy_event, evid=None, orid=None, magid=None, mecid=None,
                        auth=None, subsource=None, selectflag=1):

    if obspy_event.event_type != 'earthquake':
        logger.warning("Warning: evid=%s event_type=[%s] is being loaded as etype='eq'" % (evid, obspy_event.event_type))

    AQMSEvent = Event(evid=evid, prefor=orid, prefmag=magid, prefmec=mecid, auth=auth, version=0, etype='eq', subsource=subsource)
    # MTH: This needs to be set for jiggle to work (?)
    AQMSEvent.selectflag = selectflag

    return AQMSEvent

def obspy_origin_to_aqms(obspy_event, evid=None, orid=None, magid=None, mecid=None, auth=None,
                         pdl_id=None, subsource=None, min_dist=None, obspy_origin=None):

    if not obspy_origin:
        obspy_origin = obspy_event.preferred_origin() or obspy_event.origins[0]

    AQMSOrigin = Origin(orid=orid, evid=evid, prefmag=magid, prefmec=mecid, auth=auth, subsource=subsource)
    #AQMSOrigin.prefmec
    #AQMSOrigin.commid
    AQMSOrigin.bogusflag = 0
    AQMSOrigin.datetime = obspy_origin.time.timestamp
    AQMSOrigin.lat      = obspy_origin.latitude
    AQMSOrigin.lon      = obspy_origin.longitude
    # obspy depth is in meters, aqmss in km:
    AQMSOrigin.depth    = obspy_origin.depth/1000.
    #AQMSOrigin.mdepth
    #AQMSOrigin.type
    if obspy_origin.method_id:
        #id="smi:OK.isti.com/origin/HYP2000"
        AQMSOrigin.algorithm = obspy_origin.method_id.id.split("/")[-1]
        #print("Set algorithm=[%s]" % AQMSOrigin.algorithm)
    #AQMSOrigin.algo_assoc
    #AQMSOrigin.datumhor
    #AQMSOrigin.datumver
    if getattr(obspy_origin, 'quality', None):
        if obspy_origin.quality.azimuthal_gap:
            AQMSOrigin.gap      = obspy_origin.quality.azimuthal_gap
        #MTH: obspy minimum_distance is in deg and all AQMS lengths are km:
        if obspy_origin.quality.minimum_distance:
            AQMSOrigin.distance = obspy_origin.quality.minimum_distance * KM_PER_DEG
        if obspy_origin.quality.standard_error:
            #print("Set wrms = %s" % obspy_origin.quality.standard_error)
            AQMSOrigin.wrms     = obspy_origin.quality.standard_error
        if obspy_origin.quality.used_phase_count:
            AQMSOrigin.ndef     = obspy_origin.quality.used_phase_count

    AQMSOrigin.stime    = obspy_origin.time_errors.uncertainty
    # Obspy lengths in m --> AQMS lengths in km:
    if getattr(obspy_origin, 'origin_uncertainty', None):
        if obspy_origin.origin_uncertainty.horizontal_uncertainty:
            AQMSOrigin.erhor    = obspy_origin.origin_uncertainty.horizontal_uncertainty/1.e3
        elif obspy_origin.origin_uncertainty.max_horizontal_uncertainty:
            AQMSOrigin.erhor    = obspy_origin.origin_uncertainty.max_horizontal_uncertainty/1.e3
    # MTH: presumably obspy depth_errors are in meters!

    if getattr(obspy_origin, 'depth_errors', None) and \
       getattr(obspy_origin.depth_errors, 'uncertainty', None) and \
       obspy_origin.depth_errors.uncertainty >= 0.:
        AQMSOrigin.sdep     = obspy_origin.depth_errors.uncertainty/1.e3
    if getattr(obspy_origin, 'latitude_errors', None) and \
       getattr(obspy_origin.latitude_errors, 'uncertainty', None) and \
       obspy_origin.latitude_errors.uncertainty >= 0.:
        AQMSOrigin.erlat    = obspy_origin.latitude_errors.uncertainty
    if getattr(obspy_origin, 'longitude_errors', None) and \
       getattr(obspy_origin.longitude_errors, 'uncertainty', None) and \
       obspy_origin.longitude_errors.uncertainty >= 0.:
        AQMSOrigin.erlon    = obspy_origin.longitude_errors.uncertainty

    AQMSOrigin.totalarr = len(obspy_origin.arrivals)
    AQMSOrigin.totalamp = len(obspy_event.amplitudes)

    if min_dist:
        AQMSOrigin.distance = min_dist

    #AQMSOrigin.nbs   #nbs = Column(Integer, info='Number of S phase readings with non-zero output weights')
    #AQMSOrigin.nbfm  #nbfm = Column(Integer, info='Number of P first motions with non-zero input weights')
    if pdl_id:
        AQMSOrigin.locevid = pdl_id
    #quality = Column(Numeric, info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad')
    # These don't match (vmodelid = String(2)):
    #AQMSOrigin.vmodelid = obspy_origin.earth_model_id

    AQMSOrigin.fdepth = 'n'
    if obspy_origin.depth_type == "operator assigned":
        AQMSOrigin.fdepth = 'y'

    AQMSOrigin.ftime = 'n'
    if obspy_origin.time_fixed:
        AQMSOrigin.ftime = 'y'
    AQMSOrigin.fepi = 'n'
    if obspy_origin.epicenter_fixed:
        AQMSOrigin.fepi = 'y'

    AQMSOrigin.rflag = get_rflag(obspy_origin)
    if getattr(AQMSOrigin, 'algorithm', None) and 'HypoDD' in AQMSOrigin.algorithm:
        AQMSOrigin.rflag = "F"

    return AQMSOrigin

def obspy_magnitude_to_aqms(obspy_mag, evid=None, orid=None, magid=None, auth=None,
                            magtype=None, subsource=None, min_dist=None):

    # MTH: Need better mapping between expected PDL magtype and allowable AQMS magtype
    # aqms magtype in ('p','a','b','e','l','l1','l2','lg','c','s','w','z','B', 'un','d','h','n','dl')
    #magtype = obspy_mag.magnitude_type.lower()[-1] # ML --> 'l'  Mww --> 'w'

    # MTH: We want Mww --> 'w' but we also want Mlr --> 'lr':
    magtype = 'w' if obspy_mag.magnitude_type.lower() == 'mww' else \
                     obspy_mag.magnitude_type.lower()[1:]

    # Mwr, etc are not valid types
    #if magnitude.magnitude_type[0:2].lower() == 'Mw' and magnitude.magnitude_type[2] != 'w':
    if obspy_mag.magnitude_type[0:2].lower() == 'mw':
        logger.info("magnitude type=[%s] --> inserting as aqms netmag type='w' " % obspy_mag.magnitude_type)
        magtype = 'w'

    logger.debug("Incoming magnitude_type=[%s] --> Set magtype=%s magalgo=%s" %
                 (obspy_mag.magnitude_type, magtype, obspy_mag.magnitude_type))

    netmag = Netmag(magid=magid, orid=orid, auth=auth, subsource=subsource, magtype=magtype)
    netmag.magnitude = obspy_mag.mag
    netmag.magtype = magtype
    # This should distinguish between Mww, Mwr, Mwb, etc when they all have magtype = 'w'
    netmag.magalgo = obspy_mag.magnitude_type
    netmag.nsta    = obspy_mag.station_count
    netmag.nobs    = obspy_mag.station_count
    netmag.gap     = obspy_mag.azimuthal_gap
    netmag.uncertainty = obspy_mag.mag_errors.uncertainty

    if min_dist:
        netmag.distance = min_dist

    # MTH: check constraint: quality >= 0.0 and quality <= 1.0
    #      but how to assign this from obspy mag ?
    netmag.quality = 0.0
    netmag.rflag = get_rflag(obspy_mag)

    return netmag


def waveform_id_to_kwargs(waveform_id):
    kwargs = {}
    kwargs['sta']      = waveform_id.station_code
    kwargs['net']      = waveform_id.network_code
    kwargs['channel']  = waveform_id.channel_code
    kwargs['seedchan'] = waveform_id.channel_code[:3]
    kwargs['location'] = waveform_id.location_code
    return kwargs

def obspy_arrival_to_aqms(obspy_arrival, arid=None, auth=None, subsource=None):

    fname = 'obspy_arrival_to_aqms'

    pick = obspy_arrival.pick_id.get_referred_object()

    if pick is None:
        logger.error("%s: Unable to find pick corresponding to arrival for arid=%s" % (fname, arid))
        return None
    if not isinstance(pick, obspy.core.event.origin.Pick):
        logger.error("%s: arrival.pick_id for arid=%s has type=%s. Expected type=obspy_pick"
                     % (fname, arid, type(pick)))
        return None

    #AQMSArrival = Arrival(arid=arid, auth=auth, subsource=subsource)
    #kwargs = waveform_id_to_kwargs(pick.waveform_id)
    AQMSArrival = Arrival(arid=arid, auth=auth, subsource=subsource, **waveform_id_to_kwargs(pick.waveform_id))
    #print(AQMSArrival)
    #exit()

    AQMSArrival.datetime = pick.time.timestamp
    # Don't set these here, contain empty location code
    #AQMSArrival.sta      = pick.waveform_id.station_code
    #AQMSArrival.net      = pick.waveform_id.network_code
    ##AQMSArrival.channel  = pick.waveform_id.channel_code
    #AQMSArrival.seedchan = pick.waveform_id.channel_code[:3]
    #AQMSArrival.location = pick.waveform_id.location_code

    AQMSArrival.iphase   = obspy_arrival.phase
    # MTH: This field left empty in Jiggle - see assocaro.ema instead
    #AQMSArrival.ema      = obspy_arrival.takeoff_angle
    if getattr(obspy_arrival, 'takeoff_angle_errors', None):
        AQMSArrival.delinc   = obspy_arrival.takeoff_angle_errors.uncertainty
    AQMSArrival.azimuth  = obspy_arrival.azimuth
    # MTH: 02/2020 : the events I see coming through PDL don't have these fields set:
    AQMSArrival.deltim   = pick.time_errors.uncertainty

    # AQMSArrival.  = pick.onset
    # AQMSArrival.fm      = pick.polarity
    # AQMSArrival.  = pick.evaluation_status

    # Not sure if USGS even sets these:
    if pick.onset == "emergent":
        AQMSArrival.qual  = 'e'
    elif pick.onset == "impulsive":
        AQMSArrival.qual  = 'i'
    elif pick.onset == "questionable":
        pass
    else:
        pass
        #print("Unknown pick.onset=[%s] --> How to map to amqs arrival.qual ?" % pick.onset)

    AQMSArrival.rflag = get_rflag(obspy_arrival)

    #quality = Column(Numeric, info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad.')
    # MTH: I think you'd have to pull snr from the obspy amplitude corresponding to the pick (?)
    #snr = Column(Numeric, info='Signal-to-noise ratio.')
    # obspy pick - has time_errors.uncertainty and has onset {emergent, impulsive, questionable}
    # obspy amplitude - has snr
    # quality:  1.0    0.75 0.5  0.25 0.
    # PwtCode:    0    1    2    3   4
    # deltas = [.06, .12, .30, .60, 2.0]
    quality = 0.1 # Default so jiggle sees it

    use_pick_uncertainty = True
    if pick.comments:
        for comment in pick.comments:
            if 'eqc_pick_snr:' in comment.text:
                use_pick_uncertainty = False
                snr = float(comment.text.replace('eqc_pick_snr:', ''))
                AQMSArrival.snr = snr
                if snr >= 30:
                    quality = 1.0
                elif snr >= 20:
                    quality = 0.9
                elif snr >= 15:
                    quality = 0.8
                elif snr >= 10:
                    quality = 0.7
                elif snr >= 5:
                    quality = 0.6
                elif snr >= 4:
                    quality = 0.5
                elif snr >= 3:
                    quality = 0.4

                break

    if use_pick_uncertainty:
        if pick.time_errors and getattr(pick.time_errors, 'uncertainty', None):
            pick_uncertainty = pick.time_errors.uncertainty
            # quality:  1.0    0.75 0.5  0.25 0.
            # PwtCode:    0    1    2    3   4
            # deltas = [.06, .12, .30, .60, 2.0]
            if pick_uncertainty <= 0.06:
                quality = 1.0
            elif pick_uncertainty <= 0.12:
                quality = 0.75
            elif pick_uncertainty <= 0.30:
                quality = 0.5
            elif pick_uncertainty <= 0.60:
                quality = 0.25
            else:
                quality = 0.1    # Default for visibility in Jiggle
            logger.debug("pick_uncertainty=%s --> arrival.quality=%f" % (pick_uncertainty, quality))

    AQMSArrival.quality = quality

    return AQMSArrival

def obspy_amplitude_to_aqms(obspy_amplitude, ampid=None, auth=None, subsource=None):

    fname = 'obspy_amplitude_to_aqms'

    pick = None

    # MTH: referenced pick is used as backup for any info not directly contained in amplitude object
    if obspy_amplitude.pick_id:
        try:
            pick = obspy_amplitude.pick_id.get_referred_object()
        except:
            logger.warn("%s: ampid=%s has pick_id but get_referred_object() didn't work" % (fname, ampid))
            raise

    if pick and not isinstance(pick, obspy.core.event.origin.Pick):
        logger.error("%s: amplitude.pick_id for ampid=%s has type=%s. Expected type=obspy_pick"
                     % (fname, ampid, type(pick)))
        return None

    if getattr(obspy_amplitude, 'scaling_time', None):
        amp_time = obspy_amplitude.scaling_time.timestamp
        logger.debug("%s: Use amplitude.scaling_time" % fname)
    elif pick:
        amp_time = pick.time.timestamp
        logger.debug("%s: Use pick.time" % fname)
    else:
        logger.error("%s: Unable to locate a scaling_time or pick.time for this amplitude --> Can't insert!" % fname)
        return None

    #AQMSAmplitude = Amp(ampid=ampid, auth=auth, subsource=subsource)
    AQMSAmplitude = Amp(ampid=ampid, auth=auth, subsource=subsource, **waveform_id_to_kwargs(pick.waveform_id))
    AQMSAmplitude.datetime = amp_time

    AQMSAmplitude.rflag = get_rflag(obspy_amplitude)

    if getattr(obspy_amplitude, 'waveform_id', None):
        waveform_id = obspy_amplitude.waveform_id
        logger.debug("%s: Got waveform_id from amplitude itself" % fname)
    elif pick:
        waveform_id = pick.waveform_id
        logger.debug("%s: Got waveform_id from pick" % fname)
    else:
        logger.error("%s: Unable to locate waveform_id for this amplitude --> Can't insert!" % fname)
        return None

    # MTH: wstart is not nullable. AQMS docs say:
    #    * In the case where we know only the time of the amplitude but not the window, we set:
    #      datetime = [AMPLITUDE TIME]
    #      wstart = [AMPLITUDE TIME]
    #      duration = 0
    AQMSAmplitude.wstart   = amp_time
    AQMSAmplitude.duration = 0.

    #AQMSAmplitude.sta      = waveform_id.station_code
    #AQMSAmplitude.net      = waveform_id.network_code
    #AQMSAmplitude.channel  = waveform_id.channel_code
    #AQMSAmplitude.seedchan = waveform_id.channel_code[:3]
    #AQMSAmplitude.location = waveform_id.location_code
    if pick:
        AQMSAmplitude.iphase   = pick.phase_hint
    AQMSAmplitude.amplitude= obspy_amplitude.generic_amplitude
    # MTH: I'm assuming aqms unit "ms" is "m/s" and not "m-s" ?
    # AQMS units: ('c','s','mm','cm','m','ms','mss','cms','cmss',\
    # 'mms','mmss','mc','nm','e','cmcms','none','dycm')", name='amp10')
    # obspy units: {'m', 's', 'm/s', 'm/(s*s)', 'm*s', 'dimensionless', 'other'}
    obspy_units_to_aqms_units = {
        'm':'m', 's':'s',
        'm/s':'ms',
        'm/(s*s)':'mss',
        }
    if obspy_amplitude.unit in obspy_units_to_aqms_units:
        AQMSAmplitude.units = obspy_units_to_aqms_units[obspy_amplitude.unit]
    # MTH: temp hack to pass SEISAN S-file 'nm' units through quakeml (which supports 'other' but not 'nm'!)
    elif obspy_amplitude.unit == 'other':
        logger.warning("obspy_amplitude.unit == 'other' --> Hard-coded to aqms amp.units='nm' !")
        AQMSAmplitude.units = 'nm'
    else:
        logger.error("Bad obspy_amplitude.unit=%s ==> Won't pass aqms check constraint!" % obspy_amplitude.unit)

    if getattr(obspy_amplitude, 'type', None) and obspy_amplitude.type:
        if obspy_amplitude.type in ['C','WA','WAS','WASF','PGA','PGV','PGD','WAC',
                                    'WAU','IV2','SP.3','SP1.0','SP3.0','ML100','ME100',
                                    'EGY','M0']:
            AQMSAmplitude.amptype = obspy_amplitude.type
        elif obspy_amplitude.type in ['AML']:
            # MTH: this is producing too much output
            #logger.warning("obspy_amplitude type=AML ==> mapping to aqms amp.type=WAS!")
            AQMSAmplitude.amptype = 'WAS'

    if getattr(obspy_amplitude, 'period', None) and obspy_amplitude.period > 0.:
        AQMSAmplitude.per = obspy_amplitude.period

    AQMSAmplitude.eramp = obspy_amplitude.generic_amplitude_errors.uncertainty
    # MTH: add these if you want to insert coda amps:
    #per = Column(Numeric, info='Signal period. This attribute is the period of the signal described by the amplitude record ')
    #snr = Column(Numeric, info='Signal-to-noise ratio. This is an estimate of the signal relative to that of the noise immediately precedi>
    #tau = Column(Numeric, info='Coda duration (F-P time)')

    return AQMSAmplitude

def obspy_arrival_to_assocaro(obspy_arrival, arid=None, orid=None, auth=None, subsource=None):

    assocaro = Assocaro(orid=orid, arid=arid, auth=auth, subsource=subsource)
    assocaro.iphase = obspy_arrival.phase
    if obspy_arrival.distance:
        assocaro.delta  = obspy_arrival.distance * KM_PER_DEG
    assocaro.seaz   = obspy_arrival.azimuth
    assocaro.wgt    = obspy_arrival.time_weight
    assocaro.timeres = obspy_arrival.time_residual
    assocaro.ema    = obspy_arrival.takeoff_angle
    assocaro.slow   = obspy_arrival.horizontal_slowness_residual

    assocaro.rflag = get_rflag(obspy_arrival)

    return assocaro

def obspy_amplitude_to_assocamm(ampid=None, magid=None, auth=None, station_mag=None, subsource=None):
    assocamm = Assocamm(magid=magid, ampid=ampid, auth=auth, subsource=subsource)
    if station_mag:
        assocamm.mag = station_mag.mag
        #weight 	 	NUMBER(4, 3) 	YES 	Magnitude weight
        #in_wgt 	 	NUMBER(4, 3) 	YES 	Input weight

    return assocamm

def obspy_amplitude_to_assocamo(ampid=None, orid=None,  auth=None, subsource=None):
    assocamo = Assocamo(orid=orid, ampid=ampid, auth=auth, subsource=subsource)
    # MTH: To set these 2 field I'd need to locate the corresponding obspy arrival
    #      and get them from that
    #delta = Column(Numeric, info='Source-receiver distance. This attribute is the arc length, over the earth�s surface, of the path the s>
    #seaz = Column(Numeric, info='Station to event azimuth. It is measured clockwise from North ')

    return assocamo

def aqmsprefor(evid, orid):
    # MTH: Not sure what this type should be: type in ('h','H','c','C','a','A','d','D','u','U') 
    return Eventprefor(evid=evid, orid=orid, _type='h')

def aqmsprefmag(evid, magid, magtype):
    return Eventprefmag(evid=evid, magid=magid, magtype=magtype)

def aqmsprefmec(evid, mecid, mechtype):
    return Eventprefmec(evid=evid, mecid=mecid, mechtype=mechtype)

def summarize(event, origin, mag):

    if 1:
        event_datasource = None
        event_dataid = None
        event_eventid = None
        event_agencyid = None
        origin_datasource = None
        origin_dataid = None
        origin_eventid = None
        origin_agencyid = None
        mag_datasource = None
        mag_dataid = None
        mag_eventid = None
        mag_agencyid = None

        if getattr(event, 'creation_info', None) and getattr(event.creation_info, 'agency_id', None):
            event_agencyid = event.creation_info.agency_id
        if getattr(origin, 'creation_info', None) and getattr(origin.creation_info, 'agency_id', None):
            origin_agencyid = origin.creation_info.agency_id
        if getattr(mag, 'creation_info', None) and getattr(mag.creation_info, 'agency_id', None):
            mag_agencyid = mag.creation_info.agency_id

        if 'extra' in event and 'datasource' in event['extra']:
            event_datasource = event['extra']['datasource'].value
        if 'extra' in event and 'dataid' in event['extra']:
            event_dataid = event['extra']['dataid'].value
        if 'extra' in event and 'eventid' in event['extra']:
            event_eventid = event['extra']['eventid'].value

        if 'extra' in origin and 'datasource' in origin['extra']:
            origin_datasource = origin['extra']['datasource'].value
        if 'extra' in origin and 'dataid' in origin['extra']:
            origin_dataid = origin['extra']['dataid'].value
        if 'extra' in origin and 'eventid' in origin['extra']:
            origin_eventid = origin['extra']['eventid'].value

        if 'extra' in mag and 'datasource' in mag['extra']:
            mag_datasource = mag['extra']['datasource'].value
        if 'extra' in mag and 'dataid' in mag['extra']:
            mag_dataid = mag['extra']['dataid'].value
        if 'extra' in mag and 'eventid' in mag['extra']:
            mag_eventid = mag['extra']['eventid'].value

        logger.info("quakeml:  -event datasource=[%s] dataid=[%s] eventid=[%s] agencyid=[%s]" % \
                    (event_datasource, event_dataid, event_eventid, event_agencyid))

        logger.info("quakeml: -origin datasource=[%s] dataid=[%s] eventid=[%s] agencyid=[%s]" % \
                    (origin_datasource, origin_dataid, origin_eventid, origin_agencyid))

        logger.info("quakeml:    -mag datasource=[%s] dataid=[%s] eventid=[%s] agencyid=[%s]" % \
                    (mag_datasource, mag_dataid, mag_eventid, mag_agencyid))

    return


def obspy_focal_mechanism_to_aqms(focal_mechanism, obspy_event, mecid=None, orid=None, 
                                  magid=None, auth=None, pdl_id=None, subsource=None):

    obspy_origin = obspy_event.preferred_origin() or obspy_event.origins[0]

    # Do we have mecid yet ?
    # Should ordin != oridout ?

    #AQMSMec = Mec(mecid=mecid, oridin=orid, oridout=orid, auth=auth, subsource=subsource)
    #AQMSMec = Mec(mecid=mecid, oridin=orid, oridout=orid, magid=magid, auth=auth, subsource=subsource)
    AQMSMec = Mec(mecid=mecid, oridin=orid, magid=magid, auth=auth, subsource=subsource)

    AQMSMec.datetime = obspy_origin.time.timestamp

    AQMSMec.rflag = get_rflag(focal_mechanism)

    if focal_mechanism.principal_axes:
        AQMSMec.eigenn  = focal_mechanism.principal_axes.n_axis.length
        AQMSMec.striken = focal_mechanism.principal_axes.n_axis.azimuth
        AQMSMec.plungen = focal_mechanism.principal_axes.n_axis.plunge
        AQMSMec.eigenp  = focal_mechanism.principal_axes.p_axis.length
        AQMSMec.strikep = focal_mechanism.principal_axes.p_axis.azimuth
        AQMSMec.plungep = focal_mechanism.principal_axes.p_axis.plunge
        AQMSMec.eigent  = focal_mechanism.principal_axes.t_axis.length
        AQMSMec.striket = focal_mechanism.principal_axes.t_axis.azimuth
        AQMSMec.plunget = focal_mechanism.principal_axes.t_axis.plunge

    if focal_mechanism.nodal_planes:
        AQMSMec.mechtype = 'FP'
        AQMSMec.strike1 = focal_mechanism.nodal_planes.nodal_plane_1.strike
        AQMSMec.dip1    = focal_mechanism.nodal_planes.nodal_plane_1.dip
        AQMSMec.rake1   = focal_mechanism.nodal_planes.nodal_plane_1.rake
        AQMSMec.unstrike1 = focal_mechanism.nodal_planes.nodal_plane_1.strike_errors.uncertainty
        AQMSMec.undip1    = focal_mechanism.nodal_planes.nodal_plane_1.dip_errors.uncertainty
        AQMSMec.unrake1   = focal_mechanism.nodal_planes.nodal_plane_1.rake_errors.uncertainty

        AQMSMec.strike2 = focal_mechanism.nodal_planes.nodal_plane_2.strike
        AQMSMec.dip2    = focal_mechanism.nodal_planes.nodal_plane_2.dip
        AQMSMec.rake2   = focal_mechanism.nodal_planes.nodal_plane_2.rake
        AQMSMec.unstrike2 = focal_mechanism.nodal_planes.nodal_plane_2.strike_errors.uncertainty
        AQMSMec.undip2    = focal_mechanism.nodal_planes.nodal_plane_2.dip_errors.uncertainty
        AQMSMec.unrake2   = focal_mechanism.nodal_planes.nodal_plane_2.rake_errors.uncertainty

        if focal_mechanism.misfit:
            #AQMSMec.pvr   = focal_mechanism.misfit * 100.
            AQMSMec.pvr   = (1.0 - focal_mechanism.misfit) * 100.

    '''
pvr = Goodness of fit. For TDMT this is percent variance reduction; for FPFIT this is (1-misfit)*100
quality = This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad 
    '''

    if focal_mechanism.method_id:
        #known_methods = {'TMTS', 'TDMT', 'FPFIT', 'SMTINV'}
        known_methods = {'TMTS', 'TDMT', 'FPFIT', 'HASH', 'SMTINV'}
        # ResourceIdentifier(id="smi:nc.anss.org/momentTensor/TMTS")
        for method in known_methods:
            if method in focal_mechanism.method_id.id:
                AQMSMec.mecalgo = method
                break

    if focal_mechanism.station_polarity_count:
        AQMSMec.nsta = focal_mechanism.station_polarity_count
    elif focal_mechanism.waveform_id and len(focal_mechanism.waveform_id) > 0:
        AQMSMec.nsta = len(focal_mechanism.waveform_id)

    if focal_mechanism.moment_tensor:

        AQMSMec.mechtype = 'MT'
        mt = focal_mechanism.moment_tensor

        if mt.method_id:
            known_methods = {'TMTS', 'TDMT', 'FPFIT', 'SMTINV'}
            # ResourceIdentifier(id="smi:nc.anss.org/momentTensor/TMTS")
            for method in known_methods:
                if method in mt.method_id.id:
                    AQMSMec.mecalgo = method
                    break

        AQMSMec.pvr   = mt.variance_reduction

        # This doesn't appear to be used correctly in PDL msgs:
        #if mt.data_used:
            #print("data_used=%d" % len(mt.data_used))
        #print("mt_method=%s" % mt.method_id)

        AQMSMec.scalar = mt.scalar_moment
        AQMSMec.erscalar = mt.scalar_moment_errors.uncertainty

        if mt.double_couple:
            AQMSMec.pdc   = 100. * mt.double_couple
        if mt.clvd:
            AQMSMec.pclvd = 100. * mt.clvd
        if mt.iso:
            AQMSMec.piso  = 100. * mt.iso

        # Obspy doc is unclear on what convention they use.
        # https://docs.obspy.org/packages/autogen/obspy.core.event.source.Tensor.html#obspy.core.event.source.Tensor
        #   The Tensor class represents the six moment-tensor elements 
        #   Mrr, Mtt, Mpp, Mrt, Mrp, Mtp in the spherical coordinate system 
        #   defined by local upward vertical (r), North-South (t), and West-East (p) directions.
        # I suspect they are using Harvard/Global CMT convention
        # So these have to be converted to A&R convention (NED).
        # AQMS:  Aki Richards uses NED:       +x=N  +y=E +z=Down
        # ObsPy: Harvard/Global CMT uses USE: +t=S  +p=E +r=Up
        # Mrr=Mzz, Mtt=Mxx, Mpp=Myy, Mrt=Mxz, Mrp=-Myz, Mtp=-Mxy.
        AQMSMec.mzz  = mt.tensor.m_rr
        AQMSMec.smzz = mt.tensor.m_rr_errors.uncertainty
        AQMSMec.mxx  = mt.tensor.m_tt
        AQMSMec.smxx = mt.tensor.m_tt_errors.uncertainty
        AQMSMec.myy  = mt.tensor.m_pp
        AQMSMec.smyy = mt.tensor.m_pp_errors.uncertainty
        AQMSMec.mxz  = mt.tensor.m_rt
        AQMSMec.smxz = mt.tensor.m_rt_errors.uncertainty
        AQMSMec.myz  = -mt.tensor.m_rp
        AQMSMec.smyz = mt.tensor.m_rp_errors.uncertainty
        AQMSMec.mxy  = -mt.tensor.m_tp 
        AQMSMec.smxy = mt.tensor.m_tp_errors.uncertainty

        if mt.source_time_function:
            AQMSMec.tft = mt.source_time_function.type # Enum(["box car", "triangle", "trapezoid", "unknown"])
            AQMSMec.tfd = mt.source_time_function.duration         # Either this line or next is likely wrong
            AQMSMec.srcduration = mt.source_time_function.duration # Is this a half-duration ??

    # rows in memory don't have NUMERIC(n,0) or INTEGER applied until they are committed,
    # so to compare correctly to db rows we need to fix that:
    """
    print("MTH: call fix_int_fields AQMSMec=%s" % type(AQMSMec))
    print(type(globals()['Mec']))
    print(globals()['Mec'])
    from sqlalchemy.inspection import inspect
    #mapper = inspect(globals()['Mec'])
    import aqms_pdl
    from aqms_pdl.libs.schema_aqms_PI import Mec as MMec
    #mapper = inspect(aqms_pdl.libs.schema_aqms_PI.Mec)
    mapper = inspect(MMec)
    for col in mapper.c:
        print("col.key=%s" % col.key)
    """
    return fix_int_fields(AQMSMec)



def compare_quakeml_origin_to_aqms_origin(quakeml_origin=None, aqms_origin=None):
    '''
        Compare incoming quakeml_origin to db aqms_origin for same PDL event
    '''

    fname = 'compare_quakeml_origin_to_aqms'

    origin = quakeml_origin
    found_orig = aqms_origin
    #logger.info("quakeml_origin.latitude=%f aqms_origin.lat=%f\n" % 
                #(quakeml_origin.latitude, float(aqms_origin.lat)))
    # Determine if origin is the same as the db origin (=found_orig)
    #timestamp in seconds, depth in m

    x = [origin.time.timestamp, origin.latitude, origin.longitude, origin.depth]
    y = [float(found_orig.datetime), float(found_orig.lat), float(found_orig.lon), float(found_orig.depth) * 1e3]
    close = np.isclose(x, y, rtol=1e-09, atol=0.0)
    #print("x=%s" % x)
    #print("y=%s" % y)
    #print("close=%s" % close)
    if np.all(close):
        logger.info("%s: found_orig:%s is perfect match, do nothing" % (fname, found_orig.orid))
        return True

    return False


def calc_diff_between_aqms_origins(orig1, orig2):
    '''
    returns diff dt=sec dx=km dz=km
            between orig1 and orig2
    '''

    tol = 1e-6

    lat1 = float(orig1.lat)
    lon1 = float(orig1.lon)
    lat2 = float(orig2.lat)
    lon2 = float(orig2.lon)

    t1 = float(orig1.datetime)
    t2 = float(orig2.datetime)

    h1 = float(orig1.depth)
    h2 = float(orig2.depth)

    if (np.fabs(lat1 - lat2) < tol) \
       and (np.fabs(lon1 - lon2) < tol):
        dist = 0.
    else:
        # Returns dist in meters
        dist, az, baz = gps2dist_azimuth(lat1, lon1, lat2, lon2)

    dt = np.fabs(t1 - t2)
    dz = np.fabs(h1 - h2)
    dx = dist/1e3

    return diff(dt, dx, dz)

def compare_aqms_origins(orig1, orig2, thresh):
    '''
        return True if orig1 and orig2 are within thresh.{time, km, depth}
    '''

    #od = calc_origin_diff(orig1, orig2)
    od = calc_diff_between_aqms_origins(orig1, orig2)
    logger.debug("compare_aqms_origins: dt=%.1f dx=%.1f dz=%.1f" % (od.dt, od.dx, od.dz))

    if (od.dt < thresh.time) \
       and (od.dz < thresh.depth) \
       and (od.dx < thresh.km):
        return True
    else:
        return False


def associate_PDL_origin_with_RSN_origins(PDL_orig, RSN_source, assoc_thresh):
    '''
    Get a list of RSN origins that match within thresh
        of this PDL origin
    '''

    fname = 'associate_PDL_origin_with_RSN_origins'

    local_origins = query_origins_around_origin_time(PDL_orig.datetime, assoc_thresh.time)
    logger.debug("%s: query_origins around_time returned [%d] origins" % (fname, len(local_origins)))
    for orig in local_origins:
        logger.debug("%s: check orig orid:%d auth:%s subsource:%s" % (fname, orig.orid, orig.auth, orig.subsource))

    local_origins = [origin for origin in local_origins if origin.subsource != 'PDL' and origin.auth == RSN_source]
    logger.debug("%s: Found [%d] local_origins where origin.subsource != PDL" % (fname, len(local_origins)))

    assoc_origins = []

    if local_origins:
        for orig in local_origins:
            if orig.evid == PDL_orig.evid:
                logger.warning("%s candidate orig=%d and PDL_orig=%d have SAME evid! --> Skipping" %
                      (fname, orig.orid, PDL_orig.orid))
                continue

            logger.info("PDL evid:%d orid:%d  ==> Compare to candidate RSN evid:%d orid:%d" %
                          (PDL_orig.evid, PDL_orig.orid, orig.evid, orig.orid))
            if compare_aqms_origins(PDL_orig, orig, assoc_thresh):
                logger.info("PDL evid:%d orid:%d OT:%s <%.2f, %.2f> h=%.1f [auth:%s] (locevid:%s)==> Assocs to:" %
                            (PDL_orig.evid, PDL_orig.orid, UTCDateTime(PDL_orig.datetime),
                             PDL_orig.lat, PDL_orig.lon, PDL_orig.depth, PDL_orig.auth, PDL_orig.locevid))
                logger.info("RSN evid:%d orid:%d OT:%s <%.2f, %.2f> h=%.1f [auth:%s] (locevid:%s)" %
                            (orig.evid, orig.orid, UTCDateTime(orig.datetime),
                             orig.lat, orig.lon, orig.depth, orig.auth, orig.locevid))
                assoc_origins.append(orig)
    else:
        logger.info("%s: PDL evid=%d orid=%d did not associate with any RSN origins in db" %
                    (fname, PDL_orig.evid, PDL_orig.orid))

    return assoc_origins


def associate_RSN_origin_with_PDL_origins(RSN_orig, assoc_thresh):
    '''
    Get a list of PDL origins that match within thresh
        of this RSN origin
    '''

    fname = 'associate_RSN_origin_with_PDL_origins'

    local_origins = query_origins_around_origin_time(RSN_orig.datetime, assoc_thresh.time)
    for orig in local_origins:
        if orig.subsource == 'PDL':
            logger.debug("check orig orid:%d auth:%s subsource:%s [Use this PDL!]" % (orig.orid, orig.auth, orig.subsource))
        else:
            logger.debug("check orig orid:%d auth:%s subsource:%s" % (orig.orid, orig.auth, orig.subsource))
    PDL_origins = [origin for origin in local_origins if origin.subsource == 'PDL']

    assoc_origins = []

    if PDL_origins:
        for orig in PDL_origins:

            logger.info("RSN evid:%d orid:%d  ==> Compare to candidate PDL evid:%d orid:%d" %
                          (RSN_orig.evid, RSN_orig.orid, orig.evid, orig.orid))

            if compare_aqms_origins(RSN_orig, orig, assoc_thresh):

                logger.info("RSN evid:%d orid:%d OT:%s <%.2f, %.2f> h=%.1f [auth:%s] (locevid:%s)==> Assocs to:" %
                            (RSN_orig.evid, RSN_orig.orid, UTCDateTime(RSN_orig.datetime),
                             RSN_orig.lat, RSN_orig.lon, RSN_orig.depth, RSN_orig.auth, RSN_orig.locevid))

                logger.info("PDL evid:%d orid:%d OT:%s <%.2f, %.2f> h=%.1f [auth:%s] (locevid:%s):" %
                            (orig.evid, orig.orid, UTCDateTime(orig.datetime),
                             orig.lat, orig.lon, orig.depth, orig.auth, orig.locevid))

                od = calc_diff_between_aqms_origins(RSN_orig, orig)
                comment = 'orids:%d and %d are close: dt:%.3f s dx:%.2f km dz:%.2f km' % \
                            (RSN_orig.orid, orig.orid, od.dt, od.dx, od.dz)

                assoc_origins.append((orig, comment))
    else:
        logger.info("%s: RSN evid=%d orid=%d did not associate with any PDL origins in db" %
                    (fname, RSN_orig.evid, RSN_orig.orid))

    return assoc_origins


if __name__ == "__main__":
    main()
