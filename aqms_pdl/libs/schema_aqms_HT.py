# -*- coding: utf-8 -*-
"""
    Classes that describe AQMS IR tables

    CISN Instrument Response Schema

    http://ncedc.org/db/Documents/NewSchemas/IR/v1.5.4/IR.1.5.4/index.htm

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime
import sys

import logging
logger = logging.getLogger()

from sqlalchemy import text
from sqlalchemy import Column, DateTime, Integer, Numeric, String, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import CheckConstraint, UniqueConstraint

from aqms_pdl.libs.schema_aqms_utils import MyMixin, DEFAULT_ENDDATE, Base, check_matching, fix_location
from aqms_pdl.libs.schema_aqms_utils import truncate_strings

import sqlalchemy.types

def get_tables():
    Base.TBLNAME_TO_CLASS = {}

    for mapper in Base.registry.mappers:
        cls = mapper.class_
        classname = cls.__name__
        if not classname.startswith('_'):
            tblname = cls.__tablename__
            #print(tblname, cls)
            Base.TBLNAME_TO_CLASS[tblname] = cls
    return Base.TBLNAME_TO_CLASS

'''
Defaults aren't applied until Insert or Update in sqlalchemy
This is a work around so you can use/compare them before that
'''
def init(self, **kwargs):
    tables = get_tables()
    #for t in sorted(tables.keys()):
        #print(t)
    cls_tbl = tables[self.__tablename__]
    truncate_strings(cls_tbl, kwargs)

    table_cols = [x.name for x in cls_tbl.__table__.c]
    has_offdate = 'offdate' in table_cols
    has_location = 'location' in table_cols
    #print("table:%20s has_location:%s has_offdate:%s" %
          #(self.__tablename__, has_location, has_offdate))

    if has_offdate and 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    if has_location:
        fix_location(kwargs)
    cls = getattr(sys.modules[__name__], self.__class__.__name__)

# MTH: 2023-11-29 Set matching_fields here for classes that don't have them
#                 as they are needed in libs_epochs.insert_update_or_remove
    if getattr(self, 'matching_fields', None) is None:
        exclude_fields = getattr(self, 'exclude_fields_from_comparison', None)
        if exclude_fields:
            matching_fields = [key for key in self.__table__.columns.keys() if key not in exclude_fields]
            #print("** init cls:%s exclude_fields:%s ==> matching_fields:%s" % (cls, exclude_fields, matching_fields))
            self.matching_fields = matching_fields

# MTH: 2024-03-07 Set table.channel = table.seedchan & table.channelsrc = "SEED" **If not set in kwargs**
    # print("Inside init tablename:%s seedchan:%s" % (self.__tablename__, kwargs.get('seedchan')))
    if 'seedchan' in kwargs:
        if 'channel' not in kwargs:
            kwargs['channel'] = kwargs['seedchan']
        if 'channelsrc' not in kwargs:
            kwargs['channelsrc'] = "SEED"

    return super(cls, self).__init__(**kwargs)



class Datalogger(Base, MyMixin):
    __tablename__ = "datalogger"

    data_id = Column(Integer, info='Datalogger identifier', primary_key=True, nullable=False)
    data_type = Column(String(80),   info='Datalogger type')
    serial_nb = Column(String(80),   info='Datalogger serial number')
    firmware_nb = Column(String(80),   info='Datalogger firmware version number')
    software = Column(String(80),   info='Datalogger software (mshear, ushear, ..)')
    software_nb = Column(String(80),   info='Datalogger software version number')
    ondate  = Column(DateTime, info='timestamp without time zone', nullable=False)
    offdate = Column(DateTime, info='timestamp without time zone', default=DEFAULT_ENDDATE)
    nb_board = Column(Integer, info='Number of boards in datalogger')
    word_32 = Column(Integer, info='The swap order in which 32-bit quantities are specified in data headers', nullable=False, default=3210)
    word_16 = Column(Integer, info='The swap order in which 16-bit quantities are specified in data headers', nullable=False, default=10)
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))

    '''
    Defaults aren't applied until Insert or Update in sqlalchemy
    This is a work around so you can use/compare them before that
    '''
    def __init__(self, **kwargs):
        if 'word_32' not in kwargs:
             kwargs['word_32'] = self.__table__.c.word_32.default.arg
        if 'word_16' not in kwargs:
             kwargs['word_16'] = self.__table__.c.word_16.default.arg
        if 'offdate' not in kwargs:
             kwargs['offdate'] = self.__table__.c.offdate.default.arg

        return init(self, **kwargs)


class Station_Datalogger(Base, MyMixin):
    __tablename__ = "station_datalogger"

    net = Column(String(8),   info='Network name', primary_key=True, nullable=False)
    sta = Column(String(6),   info='Station name', primary_key=True, nullable=False)
    data_nb = Column(Integer, info='Datalogger number', primary_key=True, nullable=False)
    ondate = Column(DateTime, info='timestamp without time zone', primary_key=True, nullable=False)
    data_id = Column(Integer, info='Datalogger identifier FK --> datalogger.data_id', nullable=False)
    nb_pchannel = Column(Integer, info='Number of physical channels in datalogger', nullable=False)
    offdate = Column(DateTime, info='timestamp without time zone', default=DEFAULT_ENDDATE)
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))


    #CheckConstraint('lat >= -90.0 and lat <= 90', name='std02')
    #CheckConstraint('lon >= -180.0 and lon <= 180', name='std03')

    exclude_fields_from_comparison = ['lddate']

    """
    def __repr__(self):
        s = "%s net:%s sta:%s ondate:%s offdate:%s lat:%.3f lon:%.3f elev:%.1f" %\
            (self.__tablename__, self.net, self.sta, self.ondate, self.offdate, self.lat, self.lon, self.elev)
        #s = "%s net:%s sta:%s (%s) ondate:%s lat:%.3f lon:%.3f elev:%.1f w32:%s w16:%s" %\
            #(self.__tablename__, self.net, self.sta, self.staname, self.ondate, self.lat, self.lon, self.elev,
             #self.word_32, self.word_16)
        #s+= "\n         word_32:%d word_16:%d" % (self.word_32, self.word_16)
        return s
    """


def main():
    print("congrats - you're in schema_aqms_IR!")
#        1         2         3         4         5         6         7         8
    name = """
12345678901234567890123456789012345678901234567890123456789012345678901234567890abcd
    """
    pz = PZ(name=name.strip())
    print(pz)
    print(pz.name)

if __name__ == '__main__':
    main()
