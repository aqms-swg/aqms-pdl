# -*- coding: utf-8 -*-
"""
Read in quakeml file + flags and insert into AQMS db
Triggered by PDL

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import os
import re
import shutil
import sys

from obspy.core.utcdatetime import UTCDateTime

import logging
logger = logging.getLogger()

# Either will work
# import xml.etree.ElementTree as ET
from lxml import etree as ET

def archive_quakeml(args):

    ns = "{http://quakeml.org/xmlns/bed/1.2}"

    if 'archive_quakeml_dir' not in args:
        logger.error("archive_quakeml_dir must be set in config.yml!")
        return
    if not os.path.isdir(args.archive_quakeml_dir):
        logger.error("archive_quakeml_dir:%s is not a dir!" % args.archive_quakeml_dir)
        return
    if not isWritable(args.archive_quakeml_dir):
        logger.error("archive_quakeml_dir:%s is not writable!" % args.archive_quakeml_dir)
        return

    quakemlfile = os.path.join(args.directory, 'quakeml.xml')

    # Get the eventParameters/creationInfo/creationTime from the quakemlfile
    #     and convert to timestamp_str
    with open(quakemlfile,"r") as fp:
        element = ET.parse(fp)
        e = element.findall('{0}eventParameters/{0}creationInfo/{0}creationTime'.format(ns))
    if e:
        UTC = UTCDateTime(e[0].text)
        # Convert timestamp to millisecs and then to string
        timestamp = float(UTC.timestamp) * 1000
        timestamp_str = "%d" % timestamp
    else:
        logger.error("The quakeml file:%s does not contain an eventParameters/creationInfo/creationTime!"
                     % quakemlfile)
        exit(2)

    # Desired archive on disk:
    # archive_dir   code     timestamp
    # archive_dir/us70008j5/1582562692040/run.sh
    # archive_dir/us70008j5/1582562692040/quakeml.xml
    # archive_dir/us70008j5/1582562696040/run.sh
    # archive_dir/us70008j5/1582562696040/quakeml.xml

    archive_dir = args.archive_quakeml_dir
    code_dir = os.path.join(archive_dir, args.code)

    if not os.path.isdir(code_dir):
        try:
            os.mkdir(code_dir)
        except OSError as exc:
            logger.error("Unable to create code_dir:%s returned:%s" % (code_dir, exc))
            return
    else:
        logger.debug("code_dir:%s already exists" % code_dir)

    timestamp_dir = os.path.join(code_dir, timestamp_str)
    if not os.path.isdir(timestamp_dir):
        try:
            os.mkdir(timestamp_dir)
        except OSError as exc:
            logger.error("Unable to create dir:%s returned:%s" % (timestamp_dir, exc))
            return

    if args.directory == timestamp_dir:
        #logger.info("This seems to be running from an archive script: args.directory==archive_quakeml_dir:%s --> don't rearchive" % args.directory)
        logger.info("This seems to be running from an archive script --> Don't rearchive")
        return


    # Copy the PDL quakeml.xml file into the timestamp_dir:
    dest = shutil.copy(quakemlfile, timestamp_dir)

    # Create the run string and save to run.sh exectuable in timestamp_dir:
    args = []
    # Remove --archive_quakeml flags and redirect --directory to archive_dir
    for arg in sys.argv:
        if '--directory' in arg:
            args.append('--directory=%s' % timestamp_dir)
        elif '--archive-quakeml' in arg:
            pass
        else:
            args.append(arg)

    run_string = "#!/bin/sh\n"
    run_string += "%s   \\" % args[0]
    i=1
    while i < len(args) - 1:
        run_string += "\n    %s    \\" % args[i]
        i += 1
    run_string += "\n    %s" % args[i]

    script_file = os.path.join(timestamp_dir, 'run.sh')
    with open(script_file, 'w') as fp:
        fp.write(run_string)

    os.chmod(script_file, 0o775)

    script_file = os.path.join(timestamp_dir, 'run.sh')
    logger.info(" PDL quakeml archived at:%s" % os.path.join(timestamp_dir, 'quakeml.xml'))
    logger.info("To rerun PDL message see:%s" % script_file)

    return


def isWritable(directory):
    try:
        tmp_prefix = "write_tester";
        count = 0
        filename = os.path.join(directory, tmp_prefix)
        while(os.path.exists(filename)):
            filename = "{}.{}".format(os.path.join(directory, tmp_prefix),count)
            count = count + 1
        f = open(filename,"w")
        f.close()
        os.remove(filename)
        return True
    except Exception as e:
        #print "{}".format(e)
        return False


import sqlite3

sqlite3_db = None
def init_sqlitedb(fname):
    global sqlite3_db
    if sqlite3_db is None:
        sqlite3_db = fname
    conn = sqlite3.connect(fname)
    conn.execute("""CREATE TABLE IF NOT EXISTS processed_msgs (
                    hash TEXT,
                    hash_string TEXT
                )""")
    conn.close()

import hashlib
def get_hash(obspy_object, code):

    conn = sqlite3.connect(sqlite3_db)
    name = type(obspy_object).__name__
    hash_string = "%s.%s" % (code, name)

    if obspy_object.creation_info:
        info = obspy_object.creation_info
        if info.creation_time:
            hash_string += ".%s" % info.creation_time
        if info.version:
            hash_string += ".%s" % info.version

    hash_object = hashlib.md5(hash_string.encode())
    #print(hash_object.hexdigest())

    values = (hash_object.hexdigest(), hash_string)
    #print(values)
    cur = conn.execute("""SELECT * FROM processed_msgs where hash='%s'""" % hash_object.hexdigest())
    rows = cur.fetchall()
    insert = True
    if rows:
        #print("Found match --> IGNORE")
        insert = False
    #else:
        #print("No match --> INSERT")

    cur.close()
    conn.close()

    return insert

def put_hash(obspy_object, code):
    conn = sqlite3.connect(sqlite3_db)

    name = type(obspy_object).__name__
    hash_string = "%s.%s" % (code, name)

    if obspy_object.creation_info:
        info = obspy_object.creation_info
        if info.creation_time:
            hash_string += ".%s" % info.creation_time
        if info.version:
            hash_string += ".%s" % info.version

    hash_object = hashlib.md5(hash_string.encode())
    values = (hash_object.hexdigest(), hash_string)
    conn.execute("""INSERT INTO processed_msgs VALUES (?, ?)""", values)
    conn.commit()
    conn.close()

    return values


def main():
    _date = UTCDateTime('2017/01/12T00:00:00')
    leapsecs = get_leapsecs_from_file(_date)
    print(_date, leapsecs)

def get_leapsecs_from_file(lookupUTC, leapsec_file=None):

    if leapsec_file is None:
        from aqms_pdl import resource_dir
        leapsec_file = os.path.join(resource_dir(), 'leapseconds')

    epochs = read_leapseconds(leapsec_file)
    leapsecs = 0
    for i, _date in enumerate(epochs):
        if lookupUTC <= _date:
            break
        leapsecs += 1
    return leapsecs


month = {
        'Jan': 1,
        'Feb': 2,
        'Mar': 3,
        'Apr': 4,
        'May': 5,
        'Jun': 6,
        'Jul': 7,
        'Aug': 8,
        'Sep': 9,
        'Oct': 10,
        'Nov': 11,
        'Dec': 12,
}

def read_leapseconds(leapseconds_file):

#Leap  YEAR    MON     DAY     23:59:60        +       S
#Leap    1972    Jun     30      23:59:60        +       S
#Leap    1972    Dec     31      23:59:60        +       S

    with open(leapseconds_file, 'r') as f:
        lines = f.readlines()

    lines = [l.strip() for l in lines]

    epochs = []
    for line in lines:
        match = re.search("^Leap", line)
        if match:
            _leap, year, mon, day, time, sign, code = line.split()
            hh, mm, ss = time.split(':')
            if ss == '60':
                ss = '59'
            time = "%02d:%02d:%02d" % (int(hh), int(mm), int(ss))
            date_str = "%4d/%02d/%02dT%s" % (int(year), month[mon], int(day), time)
            _date = UTCDateTime(date_str)
            epochs.append(_date)
    return epochs

if __name__ == "__main__":
    main()
