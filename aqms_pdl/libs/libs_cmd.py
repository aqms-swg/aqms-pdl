import argparse
import os
import sys

import logging
logger = logging.getLogger()

#from aqms_pdl import installation_dir



"""
**************   DEPRECATED 2025-01-13 ==> Moved to the common libs/cmd_line functions ***********
"""

from obspy.core.utcdatetime import UTCDateTime

from aqms_pdl.libs.libs_log import update_args_with_config

PDL_STATUS = ['DELETE', 'UPDATE',]
PDL_TYPES = ['origin', 'phase-data', 'amplitude', 'unassociated-amplitude',
             'moment-tensor', 'internal-moment-tensor',]
LOGLEVELS = ['DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL']

def process_cmd_line(fname, config):
    args, parser = parse_cmd_line(fname)
    update_args_with_config(args, config, fname)
    quakemlfile = process_args(args)
    return quakemlfile, args

def parse_cmd_line(fname):

    #parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(prog=fname,
                      formatter_class=lambda prog: argparse.HelpFormatter(prog,max_help_position=40))
    optional = parser._action_groups.pop()
    required = parser.add_argument_group("required arguments")

    parser._action_groups.append(optional) # 

    required.add_argument("--status", type=str, metavar='STATUS', required=True,
                          help='STATUS in {' + ','.join(PDL_STATUS) + '}')

    required.add_argument("--code", type=str, metavar='pdl_id', required=True,
                          help='--code hv71386392')

    required.add_argument("--directory", type=str, metavar='DIR', required=True,
                          help='path to dir containing quakeml.xml')

    required.add_argument("--type", type=str, metavar='TYPE', choices=PDL_TYPES, required=True,
                          help='PDL TYPE in {' + ','.join(PDL_TYPES) + '}')

    required.add_argument("--source", type=str, metavar='XX', help='--source=pr or --source CI', required=True)

    #optional.add_argument("--optional_arg")

# could have --type=origin --status=DELETE --delete --action=PRODUCT_ADDED
    optional.add_argument("--PDL_flags", metavar='', type=str,
                          help='** Preferred tags + action + eventids set by PDL')
    optional.add_argument("--action", type=str, metavar='', help='EVENT_UPDATED, EVENT_DELETED, PRODUCT_ADDED, etc.')
    optional.add_argument("--eventids", type=list_str, help='list of eventids')
    optional.add_argument("--preferred-eventid", type=str, metavar='pdl_id', help='--preferred-eventid=hv71386392')
    optional.add_argument("--preferred-eventsource", metavar='source', type=str, help='--preferred-eventsource=hv')
    optional.add_argument("--preferred-eventsourcecode", metavar='code', type=str, help='--preferred-eventsourcecode=71386392')
    optional.add_argument("--preferred-eventtime", metavar='UTCDateTime', type=UTCDateTime,
                          help='--preferred-eventtime 2022-02-17T18:43:36.007')
    # MTH: internal-moment-tensor sets this to 'null' !
    #optional.add_argument("--preferred-magnitude", type=float)
    optional.add_argument("--preferred-magnitude", metavar='mag', type=str, help='--preferred-magnitude 6.2')
    optional.add_argument("--preferred-latitude", metavar='lat', type=float, help='--preferred-latitude 45.27')
    optional.add_argument("--preferred-longitude", metavar='lon', type=float, help='--preferred-longitude -123.45')
    optional.add_argument("--preferred-depth", metavar='dep', type=float, help='--preferred-depth 10.8')

    optional.add_argument("--property-xxxxxxxxxx", metavar='VALUE', type=str,
                          help='** Property tags set by PDL and only used if --preferred tag was not set')
    optional.add_argument("--property-origin-source", metavar='val', type=str)
    optional.add_argument("--property-magnitude", metavar='val', type=float)
    optional.add_argument("--property-magnitude-type", metavar='val', type=str)
    optional.add_argument("--property-derived-magnitude", metavar='val', type=float)
    optional.add_argument("--property-derived-magnitude-type", metavar='val', type=str)
    optional.add_argument("--property-magnitude-num-stations-used", metavar='val', type=int)
    optional.add_argument("--property-magnitude-source", metavar='val', type=str)
    optional.add_argument("--property-standard-error", metavar='val', type=float)
    optional.add_argument("--property-horizontal-error", metavar='val', type=float)
    optional.add_argument("--property-error-ellipse-azimuth", metavar='val', type=float)
    optional.add_argument("--property-minimum-distance", metavar='val', type=float)
    optional.add_argument("--property-azimuthal-gap", metavar='val', type=float)
    optional.add_argument("--property-event-type", metavar='val', type=str)
    optional.add_argument("--property-version", metavar='val', type=int)
    optional.add_argument("--property-num-stations-used", metavar='val', type=int)
    optional.add_argument("--property-num-phases-used", metavar='val', type=int)
    optional.add_argument("--property-review-status", metavar='val', type=str)

    optional.add_argument("--ignore-latitude-above", type=float, metavar='lat_max', help='Ignore latitude > lat_max')
    optional.add_argument("--ignore-latitude-below", type=float, metavar='lat_min', help='Ignore latitude < lat_min')
    optional.add_argument("--ignore-longitude-above", type=float, metavar='lon_max', help='Ignore longitude > lon_max')
    optional.add_argument("--ignore-longitude-below", type=float, metavar='lon_min', help='Ignore longitude < lon_max')
    optional.add_argument("--ignore-depth-above", type=float, metavar='dep_max', help='Ignore depth > dep_max')
    optional.add_argument("--ignore-depth-below", type=float, metavar='dep_min', help='Ignore depth < dep_min')
    optional.add_argument("--ignore-magnitude-above", type=float, metavar='mag_max', help='Ignore magnitude > mag_max')
    optional.add_argument("--ignore-magnitude-below", type=float, metavar='mag_min', help='Ignore magnitude < mag_min')
    optional.add_argument("--ignore-sources", type=list_str_sources, metavar='XX,YY,..', help='Ignore msgs from this list of sources, e.g., --ignore-sources=HV,US')
    optional.add_argument("--only-listen-to-sources", type=list_str_sources, metavar='XX,YY,..', help='Only listen to msgs from this list of sources')
    optional.add_argument("--ignore-arrivals", action='store_true', help='Dont insert arrivals')
    optional.add_argument("--ignore-amplitudes", action='store_true', help='Dont insert amplitudes')
    optional.add_argument("--ignore-event-after", type=str, metavar='val', help='Ignore if event age > {10mins, 3hrs, 5days, 2weeks, 3months, etc}')
    optional.add_argument("--debug", type=str, metavar='action', help='--debug summarize-only ==> summarize quakeml contents and exit')

    default_logfile = "%s.log" % fname
    default_logdir = "./log"
    config_info = "(Can also be set in configFile)"

    optional.add_argument('--loglevel', type=str, metavar='LEVEL', choices=LOGLEVELS,
                          help='LEVEL in {' + ','.join(LOGLEVELS) + '}' + config_info )

    # MTH: If we set the default logfile/logdir here, then it will always look like --logdir is passed to args
    #      and this will always override whatever is in the config.yml setting LOG_DIR!
    optional.add_argument('--logfile', type=str, metavar='fname',
                          )
                          #help='Default logfile=%s %s' % (default_logfile, config_info), default=default_logfile)
    optional.add_argument('--logdir', type=str, metavar='DIR',
                          )
                          #help='Default logdir=%s %s' % (default_logdir, config_info), default=default_logdir)

    optional.add_argument("--logconsole", action='store_true', help='Also log to console %s' % config_info)
    optional.add_argument("--logutc", "--log-utc", action="store_true", help='Log msgs with UTC timestamps [Default=local]')

    optional.add_argument('--configFile', type=str, metavar='fname', help='--configFile=config.yml', default=None)

    optional.add_argument("--archive-quakeml", action='store_true', help='Archive quakeml from PDL data/.. dir to archive-quakeml-dir')
    optional.add_argument('--archive-quakeml-dir', type=str, metavar='DIR', help='Archive quakeml to this dir')

    optional.add_argument("--subsource", type=str, metavar='source', help='e.g., --subsource=PDL or --subsource=SEISAN')
    optional.add_argument("--set_selectflag_zero", action='store_true', help='Insert events with event.selectflag=0')

    args, unknown = parser.parse_known_args()
    return args, parser


def process_args(args):

    logger.info("==== Process PDL msg %s" % (("=" * 70)))
    logger.info("     code:[%s]    source:[%s]    status:[%s]   type:[%s]" % \
                (args.code, args.source, args.status, args.type))
    if args.preferred_eventid:
        logger.info("     preferred_eventid:[%s]  eventids:%s" % (args.preferred_eventid, args.eventids))

    if args.status.upper() == 'DELETE':
        logger.info("     status=DELETE ==> Stop processing")
        exit(0)

    # Prevent event summary from args.preferred flags:
    if args.preferred_eventtime:
        lat = None
        lon = None
        if args.preferred_latitude:
            lat = "%.2f" % float(args.preferred_latitude)
        if args.preferred_longitude:
            lon = "%.2f" % float(args.preferred_longitude)

        if not args.preferred_magnitude or args.preferred_magnitude == 'null':
            logger.warning("--preferred-magnitude is not set!")
            if args.property_magnitude:
                logger.warning("--property-magnitude *is* set - use it as preferred")
                args.preferred_magnitude = args.property_magnitude
                args.preferred_magnitude_type = args.property_magnitude_type
            elif args.property_derived_magnitude:
                logger.warning("--property-derived-magnitude *is* set - use it as preferred")
                args.preferred_magnitude = args.property_derived_magnitude
                args.preferred_magnitude_type = args.property_derived_magnitude_type
            else:
                logger.error("No --preferred/--property magnitude set --> Exit")
                exit(2)
        else:
            args.preferred_magnitude = float(args.preferred_magnitude)

        logger.info("     preferred_eventtime:[%s]  lat:%s lon:%s dep:%s mag:%s" %
                    (args.preferred_eventtime, lat, lon,
                     args.preferred_depth, args.preferred_magnitude))
    elif args.type != 'unassociated_amplitude' and args.status.upper() != 'DELETE':
        logger.warning("--preferred-eventtime is not set!")
        if args.property_eventtime:
            logger.warning("--property-eventtime *is* set - use it as preferred")
            args.preferred_eventtime = args.property_eventtime
        else:
            logger.error("--preferred-eventtime not set --> Exit!")
            exit(2)


    if args.archive_quakeml and not args.archive_quakeml_dir:
        logger.error("If you set archive_quakeml you *must* also set archive_quakeml_dir --> Exitting!")
        exit(2)

    # Check if msg type is in pdl allowable-types:
    #if args.pdl-types:
    pdl_types = getattr(args, 'pdl-types', None)
    if pdl_types is None:
        pdl_types = getattr(args, 'pdl_types', None)

    if pdl_types:
        allowable_types = pdl_types
    else:
        logger.error("Must set pdl-types in config.yml to configure allowable PDL msg types to process --> Exitting!")
        exit(2)

    if args.type not in allowable_types:
        logger.warning("Type=[%s] not in allowable_types --> Exitting!" % args.type)
        exit(0)

    # Check if event is stale:
    if args.preferred_eventtime:
        now = datetime.datetime.now(datetime.timezone.utc)
        now_epoch = now.timestamp()
        args.event_age = now_epoch - args.preferred_eventtime.timestamp

        logger.info("        current_UTC_time:[%s]  age_of_event:[%.1f seconds]" %
                     (UTCDateTime(now_epoch), args.event_age))
        if args.ignore_event_after:
            args.max_event_age = parse_time_string(args.ignore_event_after)

    # Check args against filters:
    ignore = check_filters(args)

    #if not ignore and 'polygon' in config:
    polygon = getattr(args, 'polygon', None)
    if not ignore and polygon:

        points = [tuple(x) for x in polygon]
        #print(points)
        try:
            from geojson import Point, Polygon, Feature
            from turfpy.measurement import boolean_point_in_polygon
        except ImportError as e:
            logger.warning("'polygon' is specified in config but geojson/turfpy are not installed!")
            logger.warning("falling back on --ignore-lat/lon flags if specified")
        else:
            polygon = Polygon([points])
            point = Point((args.preferred_longitude, args.preferred_latitude))
            if not boolean_point_in_polygon(point, polygon):
                logger.info("Epicenter: %.2f, %.2f is outside specified POLYGON" % (args.preferred_latitude, args.preferred_longitude))
                ignore = True
            else:
                logger.info("Epicenter: %.2f, %.2f locates within specified POLYGON" % (args.preferred_latitude, args.preferred_longitude))

    if ignore:
        logger.info("Didn't pass filter:%s" % (ignore))
        exit(0)

# could have --type=origin --status=DELETE --delete --action=PRODUCT_ADDED

    if args.debug and args.debug == 'summarize-only':
        logger.info("PDL: [code=%s] [type=%s] [status=%s] [action=%s]" % \
                    (args.code, args.type, args.status, args.action))

        logger.info("PDL: [source=%s] preferred-eventsource=[%s] [preferred_eventid=%s] eventids=%s" % \
                    (args.source, args.preferred_eventsource, args.preferred_eventid, args.eventids))

        if args.preferred_magnitude:
            logger.info("PDL: [ origin: %s (%.2f, %.2f) h=%.2f M:%.2f]" % \
                        (args.preferred_eventtime, args.preferred_latitude, args.preferred_longitude,
                        args.preferred_depth, args.preferred_magnitude))

        logger.info("PDL: [dir=%s]" % (args.directory))
        #exit(0)


    # MTH: Fix this: I think only unassociated-amplitudes don't pass quakeml.xml ??
    if args.type in {'phase-data', 'origin', 'focal-mechanism', 'moment-tensor',
                     'internal-origin', 'internal-moment-tensor'}:

        quakemlfile = os.path.join(args.directory, 'quakeml.xml')
        #return quakemlfile, args
    else:
        quakemlfile = args.directory

    return quakemlfile


def check_source(args):
    if args.ignore_sources and args.source.upper() in args.ignore_sources or \
       args.only_listen_to_sources and args.source.upper() not in args.only_listen_to_sources:
        return 'source=%s did not pass filter' % args.source.upper()

    return False

def check_filters(args):

    if args.ignore_sources and args.source.upper() in args.ignore_sources or \
       args.only_listen_to_sources and args.source.upper() not in args.only_listen_to_sources:
        return 'source=%s did not pass filter' % args.source.upper()
    if args.ignore_latitude_below and args.preferred_latitude < args.ignore_latitude_below:
        return 'latitude=%.2f < min latitude=%.2f' % (args.preferred_latitude, args.ignore_latitude_below)
    elif args.ignore_latitude_above and args.preferred_latitude > args.ignore_latitude_above:
        return 'latitude=%.2f > max latitude=%.2f' % (args.preferred_latitude, args.ignore_latitude_above)
    elif args.ignore_longitude_below and args.preferred_longitude < args.ignore_longitude_below:
        return 'longitude=%.2f < min longitude=%.2f' % (args.preferred_longitude, args.ignore_longitude_below)
    elif args.ignore_longitude_above and args.preferred_longitude > args.ignore_longitude_above:
        return 'longitude=%.2f > max longitude=%.2f' % (args.preferred_longitude, args.ignore_longitude_above)
    elif args.ignore_depth_below and args.preferred_depth < args.ignore_depth_below:
        return 'depth=%.2f < min depth=%.2f' % (args.preferred_depth, args.ignore_depth_below)
    elif args.ignore_depth_above and args.preferred_depth > args.ignore_depth_above:
        return 'depth=%.2f > max depth=%.2f' % (args.preferred_depth, args.ignore_depth_above)
    elif args.ignore_magnitude_below and args.preferred_magnitude < args.ignore_magnitude_below:
        return 'mag=%.2f < min mag=%.2f' % (args.preferred_magnitude, args.ignore_magnitude_below)
    elif args.ignore_magnitude_above and args.preferred_magnitude > args.ignore_magnitude_above:
        return 'mag=%.2f > max mag=%.2f' % (args.preferred_magnitude, args.ignore_magnitude_above)

    elif args.ignore_event_after and args.preferred_eventtime \
        and args.event_age > args.max_event_age:
        return 'age=%.2f > max age=%.2f' % (args.event_age, args.max_event_age)

    return False


def list_str(values):
    return values.split(',')

def list_str_sources(values):
    return values.upper().split(',')


import re
import datetime
def parse_time_string(time_string):
    '''
    parse a time string like: "50 days" or "2 MINS" 
        into seconds
    '''

    seconds = None

    time_units = {"SECONDS": 1, "SECS": 1, "S": 1,
                  "MINUTES": 60, "MINS": 60, "M": 60,
                  "HOURS": 3600, "HRS": 3600, "H": 3600,
                  "DAYS": 86400, "D": 86400,
                  "WEEKS": 604800, "W": 604800,
                  "MONTHS": 2628002.88, "M": 2628002.88,
                  }

    m = re.match(r"^(?P<number>\d+)(?P<unit>\w+)$", time_string)
    if m is None:
        m = re.match(r"^(?P<number>\d+) (?P<unit>\w+)$", time_string)

    if m is not None:
        d = m.groupdict()
        if d['unit'].upper() in time_units:
            seconds = float(d['number']) * time_units[d['unit'].upper()]
        else:
            logger.error("--ignore-event-after=%s' --> units=[%s] is not a valid time unit" %
                  (time_string, d['unit']))
            exit(2)
    else:
        logger.error('Unable to parse --ignore-event-after=%s into an integer + unit, ' \
                     'e.g., 5mins, "5 minutes", "10 days", 2months, etc.' %
                     time_string)
        exit(2)

    return seconds

