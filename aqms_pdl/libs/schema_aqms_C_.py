# -*- coding: utf-8 -*-
"""
    Classes that describe AQMS C_ tables

    Tables in support of strong-motion mkv0 headers

    See COSMOS Format:
        https://www.strongmotioncenter.org/vdc/cosmos_format_1_20.pdf

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime

import logging
logger = logging.getLogger()
import sys

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text
from sqlalchemy import Column, DateTime, Integer, Numeric, String, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import CheckConstraint, UniqueConstraint

#from aqms_pdl.libs.schema_aqms_IR import Base, DEFAULT_ENDDATE, MyMixin
from aqms_pdl.libs.schema_aqms_utils import Base, DEFAULT_ENDDATE, MyMixin, fix_location
from aqms_pdl.libs.schema_aqms_utils import truncate_strings

# create the base class of all ORM classes
#Base = declarative_base()

# MTH: With the exception of C_Channeldata, these C_ tables should
#      be read-only - e.g., only used for lookups to set id fields
#      on C_Channeldata.  They should be populated/modified *outside*
#      of python/sqlalchemy

def get_tables():
    Base.TBLNAME_TO_CLASS = {}

    for mapper in Base.registry.mappers:
        cls = mapper.class_
        classname = cls.__name__
        if not classname.startswith('_'):
            tblname = cls.__tablename__
            #print(tblname, cls)
            Base.TBLNAME_TO_CLASS[tblname] = cls
    return Base.TBLNAME_TO_CLASS

'''
Defaults aren't applied until Insert or Update in sqlalchemy
This is a work around so you can use/compare them before that
'''
def init(self, **kwargs):
    tables = get_tables()
    #for t in sorted(tables.keys()):
        #print(t)
    cls_tbl = tables[self.__tablename__]
    truncate_strings(cls_tbl, kwargs)
    table_cols = [x.name for x in cls_tbl.__table__.c]

    has_offdate = 'offdate' in table_cols
    has_location = 'location' in table_cols
    #print("table:%20s has_location:%s has_offdate:%s" %
          #(self.__tablename__, has_location, has_offdate))

    if has_offdate and 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    if has_location:
        fix_location(kwargs)
    cls = getattr(sys.modules[__name__], self.__class__.__name__)
    return super(cls, self).__init__(**kwargs)


class C_Auth(Base):
    __tablename__ = "c_auth"

    authid = Column(Integer, info='Authority lookup id', primary_key=True, nullable=False)
    authdesc = Column(String(32), info='Authority description', nullable=False)

class C_Loggerdata(Base):
    __tablename__ = "c_loggerdata"

    lgrname = Column(String(48), info='', primary_key=True, nullable=False)
    lgrid = Column(Integer, info='',nullable=False)
    lgrbits = Column(Integer, info='')
    lgrfsv = Column(Numeric(asdecimal=False), info='')

class C_Sensordata(Base):
    __tablename__ = "c_sensordata"

    snsname = Column(String(48), info='', primary_key=True, nullable=False)
    snsid = Column(Integer, info='',nullable=False)
    maxg = Column(Numeric(asdecimal=False), info='')
    vperg = Column(Numeric(asdecimal=False), info='')


class C_Net(Base):
    __tablename__ = "c_net"

    netid = Column(Integer, info='Network lookup id', primary_key=True, nullable=False)
    netabbr = Column(String(8), info='', nullable=False)
    net = Column(String(2), info='Network code')
    netagent = Column(String(128), info='Network agent')


class C_Phystyp(Base):
    __tablename__ = "c_phystyp"

    physid = Column(Integer, info='Physical type lookup id', primary_key=True, nullable=False)
    physdesc = Column(String(32), info='Physical type description', nullable=False)


class C_Site(Base):
    __tablename__ = "c_site"

    siteid = Column(Integer, info='', primary_key=True, nullable=False)
    sitedesc = Column(String(132), info='', nullable=False)


class C_Ornt(Base):
    __tablename__ = "c_ornt"

    orntid = Column(Integer, info='', primary_key=True, nullable=False)
    orntdesc = Column(String(128), info='', nullable=False)


class C_Units(Base):
    __tablename__ = "c_units"

    unitsid = Column(Integer, info='Units lookup id', primary_key=True, nullable=False)
    unitsdesc = Column(String(24), info='Units description', nullable=False)


class C_Channeldata(Base, MyMixin):
    __tablename__ = "c_channeldata"

    net = Column(String(8), info='Network name', primary_key=True, nullable=False)
    sta = Column(String(6), info='Station name', primary_key=True, nullable=False)
    seedchan = Column(String(3), info='Channel name', primary_key=True, nullable=False)
    location = Column(String(2), info='Location code', primary_key=True, nullable=False)
    ondate = Column(DateTime, info='timestamp without time zone', primary_key=True, nullable=False)
    offdate = Column(DateTime, info='timestamp without time zone', primary_key=False, nullable=False,
                     default=DEFAULT_ENDDATE)
    netid = Column(Integer, info='Network lookup id')
    lgrname = Column(String(48), info='')
    lgrid = Column(Integer, info='Recorder type code (Table 9)', nullable=False)
    lgrbits = Column(Integer, info='Recorder sample word length')
    lgrfsv = Column(Numeric(asdecimal=False), info='Recorder full-scale input [Volts]')
    snsname = Column(String(48), info='')
    snsid = Column(Integer, info='Sensor type code (Table 10)', nullable=False)
    maxg = Column(Numeric(asdecimal=False), info='Full scale sensing capability of sensor [in g]')
    orntid = Column(Integer, info='Azimuth of sensor wrt True North')
    vperg = Column(Numeric(asdecimal=False), info='Sensor sensitivity (Volts per g for accelerometer)')
    siteid = Column(Integer, info='Station lookup type (Table 6: free-field, small bldg, bridge, etc)')
    timeid = Column(Integer, info='Time source (Table 8)')
    datumid = Column(Integer, info='Reference datum for station coords')
    stavs30 = Column(Numeric(asdecimal=False), info='Site geology V30 [km/s]')
    stageoid = Column(Integer, info='Code for site geology')
    chan_number = Column(Integer, info='Station channel number')
    description = Column(String(60), info='')
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))
    # MTH:this is numeric in table but gets stored in the mkv0 int_hdr[21] --> Probably should be int ?!
    ref_azi = Column(Numeric(asdecimal=False), info='Structure/array reference orientation')
    # MTH: offdate says non-nullable in table but it is NOT part of primary_key!
    description = Column(String(60), info='Description')

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        s = "c_chan sncl:%s.%s.%s.%s" % (self.sta, self.net, self.seedchan, self.location)
        s += " on:%s off:%s\n" % (self.ondate, self.offdate)
        s += "  lgrname:%s lgrid:%s lgrbits:%s lgrfsv:%s" %\
                (self.lgrname, self.lgrid, self.lgrbits, self.lgrfsv)
        #s += "  netid:%d snsname:%s snsid:%s\n" %\
        s += "  netid:%s snsname:%s snsid:%s\n" %\
                (self.netid, self.snsname, self.snsid)
        s += "  maxg:%s orntid:%s vperg:%s\n" %\
                (self.maxg, self.orntid, self.vperg)
        s += "  siteid:%s timeid:%s datumid:%s\n" %\
                (self.siteid, self.timeid, self.datumid)
        s += "  stavs30:%s stageoid:%s chan_number:%s\n" %\
                (self.stavs30, self.stageoid, self.chan_number)
        return s

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'netid', 'lgrname', 'lgrid', 'lgrbits',
                       'lgrfsv', 'snsname', 'snsid', 'maxg',
                       'orntid', 'vperg', 'siteid', 'timeid',
                       'datumid', 'stavs30', 'stageoid',
                       'chan_number',
                       'ref_azi', 'ondate', 'offdate'
                      ]

