# -*- coding: utf-8 -*-
"""
    Classes that describe AQMS AP tables

    CISN/ANSS Application Schema

    http://ncedc.org/db/Documents/NewSchemas/AP/v1.0.2/AP.1.0.2/index.htm

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""
import datetime

import logging
logger = logging.getLogger()
import sys

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text
from sqlalchemy import Column, DateTime, Integer, Numeric, String, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import CheckConstraint, UniqueConstraint

from aqms_pdl.libs.schema_aqms_utils import Base, MyMixin, fix_location
from aqms_pdl.libs.schema_aqms_utils import truncate_strings

# create the base class of all ORM classes
#Base = declarative_base()

def get_tables():
    Base.TBLNAME_TO_CLASS = {}

    for mapper in Base.registry.mappers:
        cls = mapper.class_
        classname = cls.__name__
        if not classname.startswith('_'):
            tblname = cls.__tablename__
            #print(tblname, cls)
            Base.TBLNAME_TO_CLASS[tblname] = cls
    return Base.TBLNAME_TO_CLASS

'''
Defaults aren't applied until Insert or Update in sqlalchemy
This is a work around so you can use/compare them before that
'''
def init(self, **kwargs):
    tables = get_tables()
    #for t in sorted(tables.keys()):
        #print(t)
    cls_tbl = tables[self.__tablename__]
    truncate_strings(cls_tbl, kwargs)
    table_cols = [x.name for x in cls_tbl.__table__.c]

    has_offdate = 'offdate' in table_cols
    has_location = 'location' in table_cols
    #print("table:%20s has_location:%s has_offdate:%s" %
          #(self.__tablename__, has_location, has_offdate))

    if has_offdate and 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    if has_location:
        fix_location(kwargs)
    cls = getattr(sys.modules[__name__], self.__class__.__name__)
    return super(cls, self).__init__(**kwargs)

class Appchannels(Base):
    __tablename__ = "appchannels"

    #progid = Column(Integer, Sequence('evseq'), primary_key=True, nullable=False,
    progid = Column(Integer, primary_key=True, nullable=False, info='Application identifier.')
    net = Column(String(8), primary_key=True, nullable=False)
    sta = Column(String(6), primary_key=True, nullable=False)
    seedchan = Column(String(3), primary_key=True, nullable=False)
    location = Column(String(2), primary_key=True, nullable=False)
    config = Column(String(64))
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=datetime.datetime(3000,1,1))
    lddate = Column(DateTime, server_default=text('NOW()'))

    def __init__(self, **kwargs):
        return init(self, **kwargs)


class Applications(Base):
    __tablename__ = "applications"

    #progid = Column(Integer, Sequence('evseq'), primary_key=True, nullable=False,
    progid = Column(Integer, primary_key=True, nullable=False,
                  info='Application identifier.')
    name = Column(String(16), nullable=False)
    lddate = Column(DateTime, server_default=text('NOW()'))


''' Channel clipping count values (used by magnitude algorithms)
'''
class Channelmap_Ampparms(Base, MyMixin):
    __tablename__ = "channelmap_ampparms"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=datetime.datetime(3000,1,1))
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    clip = Column('clip', Numeric(asdecimal=False))
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "Channelmap_Ampparms: net={}, sta={}, seedchan={}, location={}, ondate={}, \
                offdate={}, clip={}".\
                format(self.net, self.sta, self.seedchan, self.location, self.ondate, \
                self.offdate, self.clip)

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'ondate', 'offdate', 'clip']


'''Channel response dependent data (used by coda magnitude algorithms)
'''
class Channelmap_Codaparms(Base, MyMixin):
    __tablename__ = "channelmap_codaparms"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=datetime.datetime(3000,1,1))
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    cutoff = Column('cutoff', Numeric(asdecimal=False))
    gain_corr = Column('gain_corr', Numeric(asdecimal=False))
    summary_wt = Column('summary_wt', Numeric(asdecimal=False))
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "Channelmap_Codaparms: net={}, sta={}, seedchan={}, location={}, ondate={}, \
                offdate={}, cutoff={}, gain_corr={}, summary_wt={}".\
                format(self.net, self.sta, self.seedchan, self.location, self.ondate, \
                self.offdate, self.cutoff, self.gain_corr, self.summary_wt)

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'ondate', 'offdate', 'cutoff',
                       'gain_corr', 'summary_wt'
                      ]


class Epochtimebase(Base):
    __tablename__ = "epochtimebase"

    base = Column(String(1), primary_key=True, nullable=False, info='T or N')
    ondate = Column(DateTime, primary_key=True, nullable=False)
    offdate = Column(DateTime)

    CheckConstraint("base in ('T', 'N')", name='timebasechk')


class StaCorrections(Base, MyMixin):
    __tablename__ = "stacorrections"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=datetime.datetime(3000,1,1))
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    auth = Column('auth', String(15), default="UW")
    corr = Column('corr', Numeric(asdecimal=False), nullable=False)
    corr_flag = Column('corr_flag', String(1))
    corr_type = Column('corr_type', String(3), primary_key=True, nullable=False)
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "StaCorrections: net={}, sta={}, seedchan={}, location={}, ondate={}, \
                offdate={}, corr={}, corr_type={}".\
                format(self.net, self.sta, self.seedchan, self.location, self.ondate, \
                self.offdate, self.corr, self.corr_type)

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'ondate', 'offdate'
                       'auth', 'corr', 'corr_flag', 'corr_type'
                      ]


class Stamapping(Base):
    __tablename__ = "stamapping"

    net = Column(String(8), info='Network name', primary_key=True, nullable=False)
    sta = Column(String(6), info='Station name', primary_key=True, nullable=False)
    stanumber = Column(Integer, info='Station lookup id')
    locdescr = Column(String(40), info='')
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))


class Credit(Base):
    __tablename__ = "credit"
    _id = Column('id', Integer, info='Primary key id to tname, like mecid, magid, etc', primary_key=True, nullable=False)
    tname = Column(String(30), info='Name of table in which id is a key', primary_key=True, nullable=False)
    refer = Column(String(80), info='Credit reference (e.g,. keyid of credit_alias table)', nullable=False)

    def __repr__(self):
        return "Credit: refer={} tname={} id={}".\
                format(self.refer, self.tname, self._id)



class Credit_alias(Base):
    __tablename__ = "credit_alias"
    alias = Column(String(32), info='Host, application or schema username', primary_key=True, nullable=False)
    keyid = Column(String(32), info='String mapped to alias, e.g., credit.refer', nullable=False)
