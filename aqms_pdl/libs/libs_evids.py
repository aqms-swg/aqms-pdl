# -*- coding: utf-8 -*-
"""
Functions for sorting lists of evids that are flags
  on incoming PDL messages

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import logging
logger = logging.getLogger()


def check_eventids(args, RSN_source, keep_them_separated=False):
    '''
        MTH: I see several cases coming through PDL:
            1. --source=us --code=us6000dxnd --eventids = [us6000dxnd]
                >pdl_id=6000dxnd + auth=us
            2. --source=nn --code=nn00803678 --preferred-eventid=nn00803678 --eventids = [nn00803678, us6000dxnd]
                >separate into pdl_id=00803678 + auth=nn + other_ids=[6000dxnd]
            3. --source=us --code=us6000dxna --preferred-eventid=nc73541781 --eventids = [nc73541781, us6000dxna]
                >separate into pdl_id=6000dxna + auth=us + RSN_pdl_id=73541781
            4. --source=us --code=us6000dykc --preferred-eventid=nn00804169 --eventids = [nn00804169, us6000dykc]
                >separate into pdl_id=6000dykc + auth=us + other_ids=[00804169]

    Sort the eventids + codes into:
      pdl_id, RSN_pdl_id, other_pdlids

      other_pdlids - from eventids with args.code removed and prefix=pdl_prefix if keep_them_separated

      Examples for:RSN_source=NC + keep_them_separated=True:

   code          source                eventids                            RSN_pdl_id   other_ids
===============   ====   =============================================     ==========   ============
us_6000etc0_mww]   us    ['ew1625784601', 'ew1625784613', 'pt21189000',     73584926   ['us6000etc0']
                          'nc73584926', 'us6000etc0']
nn00813500         nn    ['nn00813500', 'ew1626234374',                     73589811   []
                          'nc73589811', 'us7000elsa']
ci39727887_mt1     ci    ['ci39727887', 'us7000eifl']                       None       ['39727887']
us_7000eijb_mwr_nc us    ['nc73582701', 'us7000eijb']                       73582701   ['7000eijb']
es_7000e9mg_mww    us    ['at00qu637v', 'pt21155000', 'usauto7000e9mg']     None       ['7000e9mg']

    '''

    pdl_prefix, pdl_id = parse_eventid(args.code) #prefix should be same as args.source for this

    RSN_pdl_id = None
    other_pdlids = []

    eventids = args.eventids.copy()
    if args.code in eventids:
        eventids.remove(args.code)
    else:
        logger.warning("eventids list does NOT CONTAIN args.code=%s" % args.code)

    eventids = [eid for eid in args.eventids if eid != args.code]

    for i, eventid in enumerate(eventids):
        prefix, eid = parse_eventid(eventid)

        if prefix.upper() == RSN_source.upper():

            RSN_pdl_id = eid
            logger.info("check_eventids: eventids[%d]:%s ==> prefix:%s matches RSN_source:%s RSN_pdl_id=%s" %
                        (i, eventid, prefix, RSN_source, RSN_pdl_id))
        else:
            # If keep_them_separated ==> Only add eids with same first 2-char code to other_pdlids
            #    so that "us" and "usauto" remain on same evid
            #        and "ci" and "cidev" remain on same evid
            #        but "nn" and "us" separate (for same event)
            if keep_them_separated and prefix[0:2] != pdl_prefix[0:2]:
                    continue


            other_pdlids.append(eid)
            logger.info("check_eventids: Add to other_pdlids: eventids[%d]:%s ==> prefix:%s pdl_id:%s" %
                        (i, eventid, prefix, eid))


    # MTH: EQC
    #args.code=[eqc_det.2422]
    if 'eqc_det' in args.code:
        pdl_id = args.code

    return pdl_id, RSN_pdl_id, other_pdlids

import re
def parse_eventid(eventid):
    """ Parse eventid string into prefix + eid

    :param eventid: alphanumeric eventid e.g., us400053yy
    :type eventid: str

    :returns: prefix, eid
    :rtype: tuple(str, str)

    Example inputs:
         input              output
        eventid         prefix      eid
      =====================================
      us400063yy        us        400063yy
      usauto400063yy    usauto    400063yy
      us_400063yy_mww   us        400063yy
      us_a000es01_mww   us        a000es01
      nc73554385_fm1    nc        73554385
    """

    p1 = re.compile(r'[a-z]+_+')
    p2 = re.compile(r'[a-z]+')

    match = p1.match(eventid)
    if match is None:
        match = p2.match(eventid)

    if match:
        prefix = match.group()
        prefix = prefix.replace('_','')
        eid = eventid[match.end():]
        indx = eid.find('_')
        if indx > 0:
            eid = eid[:indx]
    else:
        logger.error("parse_eventid eventid=%s is unexpected format that is NOT handled!" % eventid)
        exit(2)

    return prefix, eid


