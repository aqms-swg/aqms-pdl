# -*- coding: utf-8 -*-
"""
    Classes that describe AQMS PI tables

    CISN/ANSS Parametric Information Schema

    http://ncedc.org/db/Documents/NewSchemas/PI/v1.6.2/PI.1.6.2/index.htm

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime
import sys

import logging
logger = logging.getLogger()

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text
from sqlalchemy import Column, DateTime, Integer, Numeric, String, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import CheckConstraint, UniqueConstraint
from sqlalchemy.dialects.postgresql import BYTEA

from sqlalchemy.orm import relationship

# create the base class of all ORM classes
#Base = declarative_base()

from aqms_pdl.libs.schema_aqms_utils import MyMixin, Base, fix_location
from aqms_pdl.libs.schema_aqms_utils import truncate_strings

from sqlalchemy.inspection import inspect
def fix_int_fields(aqms_obj):
      """
      Check for table cols that are INTEGER or NUMERIC(n, 0)
        incoming fields (from quakeml/obspy) may not be ints
        We want to fix that here so that in memory object fields
        compare correctly to db rows for __eq__

      MTH: I moved this from schema_aqms_utils to here since running
           helpers/focalmech.py which calls libs_aqms.obspy_focal_mechanism_to_aqms
           was erring in here when globals didn't contain 'Mec' key for some reason
      """
      #mapper = inspect(Mec)
      mapper = inspect(globals()[aqms_obj.__class__.__name__])
      #mapper = Mec
      #col = mapper.c.rake1
      for col in mapper.c:
          #print("col.key=%s" % col.key)
          val = getattr(aqms_obj, col.key, None)
          if getattr(aqms_obj, col.key, None) and isinstance(col.type, Numeric):
              precision = col.type.precision
              scale = col.type.scale
              if scale is not None:
                  setattr(aqms_obj, col.key, round(val, scale))
                  #print("col_key=%s --> val=%s [precision=%s, scale=%s]-->[%s]" %
                          #(col.key, val, precision, scale, getattr(aqms_mec, col.key, None)))
          elif getattr(aqms_obj, col.key, None) and isinstance(col.type, Integer):
              setattr(aqms_obj, col.key, int(round(val, 0)))

      return aqms_obj

def get_tables():
    Base.TBLNAME_TO_CLASS = {}

    for mapper in Base.registry.mappers:
        cls = mapper.class_
        classname = cls.__name__
        if not classname.startswith('_'):
            tblname = cls.__tablename__
            #print(tblname, cls)
            Base.TBLNAME_TO_CLASS[tblname] = cls
    return Base.TBLNAME_TO_CLASS

'''
  Defaults aren't applied until Insert or Update in sqlalchemy
  This is a work around so you can use/compare them before that
'''
def init(self, **kwargs):
    tables = get_tables()
    #for t in sorted(tables.keys()):
        #print(t)
    cls_tbl = tables[self.__tablename__]
    truncate_strings(cls_tbl, kwargs)
    table_cols = [x.name for x in cls_tbl.__table__.c]

    has_offdate = 'offdate' in table_cols
    has_location = 'location' in table_cols
    #print("table:%20s has_location:%s has_offdate:%s" %
          #(self.__tablename__, has_location, has_offdate))

    if has_offdate and 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    if has_location:
        fix_location(kwargs)
    cls = getattr(sys.modules[__name__], self.__class__.__name__)
    #print("Inside schema_aqms_PI.init cls=%s" % cls)
    return super(cls, self).__init__(**kwargs)


class SpectralAmp(Base, MyMixin):
    __tablename__ = "spectralamp"

    ampid = Column(Integer, Sequence('spectralampseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    sta = Column(String(6), nullable=False)
    net = Column(String(6))
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), info='SEED channel name. The first character denotes the band code, the second for the instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the same network operator ')

    starttime = Column(Numeric(asdecimal=False), nullable=False, info='Start of time window of amplitude measurement')
    duration = Column(Numeric(asdecimal=False), info='Duration of amplitude reading')
    spectype = Column(String(64), info='Searchable name of specamp, e.g., Sa, Sv, Sd, Fourier Amplitude')
    description = Column(String(128), info='Description of spectype, e.g., spectral amplitudes')
    units_x = Column(String(8), nullable=False, info='Units of amplitude')
    units_y = Column(String(8), nullable=False, info='Units of amplitude')
    damping = Column(Numeric(asdecimal=False), info='Fractional damping, e.g., 0.2')

    xydata = Column(BYTEA, info='2d numpy array x,y')

    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'))

    # table level CHECK constraint.  'name' is optional.
    CheckConstraint('ampid > 0', name='amp01')
    #CheckConstraint("units in ('c','s','mm','cm','m','ms','mss','cms','cmss',\
                    #'mms','mmss','mc','nm','e','cmcms','none','dycm')", name='amp10')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='amp12')

    exclude_fields_from_comparison = ['ampid', 'commid', 'lddate']

    def __init__(self, **kwargs):
        return init(self, **kwargs)


class Amp(Base, MyMixin):
    __tablename__ = "amp"

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    ampid = Column(Integer, Sequence('ampseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    datetime = Column(Numeric(asdecimal=False))
    sta = Column(String(6), nullable=False)
    net = Column(String(6))
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), info='SEED channel name. The first character denotes the band code, the second for the instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the same network operator ')
    iphase = Column(String(8), info='Reported phase. This eight-character field holds the name initially given to a seismic phase.')
    amplitude = Column(Numeric(asdecimal=False), nullable=False, info='Amplitude in appropriate units for type ')
    amptype = Column(String(8), info='Amplitude type')
    units = Column(String(4), nullable=False, info='Units of amplitude')
    ampmeas = Column(String(1), info='Amplitude measure')
    eramp = Column(Numeric(asdecimal=False), info='Uncertainty in amplitude measurement')
    flagamp = Column(String(4), info='This attribute is a flag to indicate whether amplitude is over P packet, S packet, entire waveform, etc')
    per = Column(Numeric(asdecimal=False), info='Signal period. This attribute is the period of the signal described by the amplitude record ')
    snr = Column(Numeric(asdecimal=False), info='Signal-to-noise ratio. This is an estimate of the signal relative to that of the noise immediately preceding it')
    tau = Column(Numeric(asdecimal=False), info='Coda duration (F-P time)')
    quality = Column(Numeric(asdecimal=False), info='This attribute describes the completeness of the time window of data. A complete time window is 1.0, an incomplete time window is 0.0.')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    cflag = Column(String(2), info='This flag indicates whether the amplitude value is below noise, on scale or clipped')
    wstart = Column(Numeric(asdecimal=False), nullable=False, info='Start of time window of amplitude measurement')
    duration = Column(Numeric(asdecimal=False), info='Duration of amplitude reading')
    lddate = Column(DateTime, server_default=text('NOW()'))

    # table level CHECK constraint.  'name' is optional.
    CheckConstraint('ampid > 0', name='amp01')
    CheckConstraint('amplitude > 0', name='amp02')
    CheckConstraint("ampmeas in ('0','1')", name='amp03')
    CheckConstraint("amptype in ('C','WA','WAS','WASF','PGA','PGV','PGD','WAC',\
                                 'WAU','IV2','SP.3','SP1.0','SP3.0','ML100','ME100',\
                                 'EGY','M0')", name='amp04')
    CheckConstraint('eramp>=0.0', name='amp06')
    CheckConstraint("flagamp in ('P','S','R','PP','ALL','SUR')", name='amp07')
    CheckConstraint('per>0.0', name='amp08')
    CheckConstraint('tau>0.0', name='amp09')
    CheckConstraint("units in ('c','s','mm','cm','m','ms','mss','cms','cmss',\
                    'mms','mmss','mc','nm','e','cmcms','none','dycm')", name='amp10')
    CheckConstraint('quality>=0.0 and quality<=1.0', name='amp11')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='amp12')
    CheckConstraint("cflag in cflag in ('bn', 'os','cl','BN','OS','CL')", name='amp13')

    exclude_fields_from_comparison = ['ampid', 'commid', 'lddate']

# This doesn't work -it will replace the location=Column with a str
    """
    @property
    def location(self):
        return self._location
    @location.setter
    def location(self, value):
        self._location = set_location_code(value)
    """

class Arrival(Base, MyMixin):
    __tablename__ = "arrival"

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    arid = Column(Integer, Sequence('arseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    datetime = Column(Numeric(asdecimal=False), nullable=False)
    sta = Column(String(6), nullable=False)
    net = Column(String(6), nullable=True)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8))
    channelsrc = Column(String(8))
    seedchan = Column(String(3))
    location = Column(String(2))
    iphase = Column(String(8), info='Reported phase. This eight-character field holds the name initially given to a seismic phase.')
    qual = Column(String(1), info='Onset quality. This single-character flag is used to denote the sharpness of the onset of a seismic phase.')
    clockqual = Column(String(1), info='This field describes whether the clock quality associated with a particular arrival is known, and if so, is good or bad')
    #clockcorr = Column(Numeric, info='This field indicates the amount of time to add to the arrival time if clock correction is known')
    #ccset = Column(String(1), info='This field describes whether the clock correction has been added or applied to the arrivals in computing a hypocenter solution')
    clockcorr = Column(Integer, info='This field indicates the amount of time to add to the arrival time if clock correction is known')
    ccset = Column(Integer, info='This field describes whether the clock correction has been added or applied to the arrivals in computing a hypocenter solution')
    fm = Column(String(2), info='First motion. This is a two-character indication of first motion.')
    ema = Column(Numeric(asdecimal=False), info='Emergence angle. This attribute is the emergence angle of an arrival, as observed at a three-component station or array. The value increases from the vertical direction towards the horizontal')
    azimuth = Column(Numeric(asdecimal=False), info='Station to event azimuth measured clockwise from true north. Identified from waveform analysis')
    slow = Column(Numeric(asdecimal=False), info='Observed slowness. This is the observed slowness of a wave as it sweeps across an array')
    deltim = Column(Numeric(asdecimal=False), info='Arrival time uncertainty.')
    delinc = Column(Numeric(asdecimal=False), info='Uncertainty in the angle. This attribute gives the standard deviation of the angle of emergence')
    delaz = Column(Numeric(asdecimal=False), info='Uncertainty in the azimuth.')
    delslo = Column(Numeric(asdecimal=False), info='Uncertainty in slowness.')
    quality = Column(Numeric(asdecimal=False), info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad.')
    snr = Column(Numeric(asdecimal=False), info='Signal-to-noise ratio.')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'), info='Load date. Date and time that the record was created, in Oracle date datatype')

    CheckConstraint('arid > 0', name='arrival01')
    CheckConstraint('azmiuth >= 0.0 and azimuth <= 360.0', name='arrival02')
    CheckConstraint('delaz > 0.0', name='arrival03')
    CheckConstraint('delinc >= 0.0', name='arrival04')
    CheckConstraint('delslo > 0.0', name='arrival05')
    CheckConstraint('deltim >= 0.0', name='arrival06')
    CheckConstraint('ema >= 0.0 and ema <= 90.0', name='arrival07')
    CheckConstraint("fm in ('cu','cr','c.','du','dr','d.','.u','.r','..','+u','+r',\
                            '+.','-u','-r','-.')", name='arrival08')
    CheckConstraint("qual in ('i','e','w','I','E','W')", name='arrival09')
    CheckConstraint('slow >= 0.0', name='arrival10')
    CheckConstraint('snr > 0.0', name='arrival11')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='arrival12')
    CheckConstraint('ccset < 1', name='arrival13')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='arrival14')

    exclude_fields_from_comparison = ['arid', 'commid', 'lddate']
    #matching_fields = [key for key in Arrival.__table__.keys() if key not in exclude_keys_from_comparison]

    def __repr__(self):
        s = "%s arid:%s net:%s sta:%s cha:%s location:[%s] iphase:%s" % \
            (self.__tablename__.capitalize(), self.arid, self.net, self.sta,
             self.seedchan, self.location, self.iphase)
        return s

class Coda(Base, MyMixin):
    __tablename__ = "coda"

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    coid = Column(Integer, Sequence('coseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    datetime = Column(Numeric)
    sta = Column(String(6), nullable=False)
    net = Column(String(6))
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), info='SEED channel name. The first character denotes the band code, the second for the instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the same network operator ')
    iphase = Column(String(8), info='Reported phase. This eight-character field holds the name initially given to a seismic phase.')
    codatype = Column(String(3), info='Coda type')
    afix = Column(Numeric, info='Nominal coda amplitude')
    afree = Column(Numeric, info='Free amplitude')
    qfix = Column(Numeric, info='Fixed coda decay constant')
    qfree = Column(Numeric, info='Free decay')
    tau = Column(Numeric, info='Coda duration (F-P time)')
    nsample = Column(Integer, info='Number of coda sample windows')
    rms = Column(Numeric, info='Square root of the sum of the squares of the coda residuals, divided by the number of observations')
    durtype = Column(String(3), info='Duration type')
    eramp = Column(Numeric, info='Uncertainty in amplitude measurement')
    units = Column(String(4), nullable=False, info='Units of amplitude')
    time1 = Column(Numeric, info='Time for pairs')
    amp1 = Column(Numeric, info='Amplitude for pairs')
    time2 = Column(Numeric, info='Time for pairs')
    amp2 = Column(Numeric, info='Amplitude for pairs')
    time3 = Column(Numeric, info='Time for pairs')
    amp3 = Column(Numeric, info='Amplitude for pairs')
    time4 = Column(Numeric, info='Time for pairs')
    amp4 = Column(Numeric, info='Amplitude for pairs')
    time5 = Column(Numeric, info='Time for pairs')
    amp5 = Column(Numeric, info='Amplitude for pairs')
    time6 = Column(Numeric, info='Time for pairs')
    amp6 = Column(Numeric, info='Amplitude for pairs')
    quality = Column(Numeric, info='This attribute describes the completeness of the time window of data. A complete time window is 1.0, an incomplete time window is 0.0.')
    algorithm = Column(String(15), info='Algorithm used. This is a brief textual description of the algorithm used to derive coda data')
    winsize = Column(Numeric, info='Window over which waveform absolute amplitudes averaged')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('afix >= 0.0', name='coda01')
    CheckConstraint('amp1 > 0.0', name='coda03')
    CheckConstraint('amp2 > 0.0', name='coda04')
    CheckConstraint('amp3 > 0.0', name='coda05')
    CheckConstraint('amp4 > 0.0', name='coda06')
    CheckConstraint('amp5 > 0.0', name='coda07')
    CheckConstraint('amp6 > 0.0', name='coda08')
    CheckConstraint("codatype in ('P','S')", name='coda09')
    CheckConstraint('coid > 0', name='coda10')
    CheckConstraint('nsample >= 0', name='coda11')
    CheckConstraint('rms >= 0', name='coda12')
    CheckConstraint('time1 > 0', name='coda13')
    CheckConstraint('time2 > 0', name='coda14')
    CheckConstraint('time3 > 0', name='coda15')
    CheckConstraint('time4 > 0', name='coda16')
    CheckConstraint('time5 > 0', name='coda17')
    CheckConstraint('time6 > 0', name='coda18')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='coda19')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='coda20')
    CheckConstraint("durtype in ('a','d','h')", name='coda21')

    exclude_fields_from_comparison = ['coid', 'commid', 'lddate']


class Event(Base):
    __tablename__ = "event"

    evid = Column(Integer, Sequence('evseq'), primary_key=True, nullable=False,
                  info='Event identifier. Each event is assigned a unique integer which identifies it in a database. It is possible for several records in the Origin relation to have the same evid.')
    prefor = Column(ForeignKey('origin.orid'))
    prefmag = Column(ForeignKey('netmag.magid'))
    prefmec = Column(ForeignKey('mec.mecid'))
    commid = Column(Integer, info='Comment identification. This is a key used to point to free-form comments entered in the Remark relation.')
    auth = Column(String(15), nullable=False)
    subsource = Column(String(28), info='A second identifier to specify the system or process that derived the data.')
    etype = Column(ForeignKey('eventtype.etype'), nullable=False, info='This attribute is used to identify the type of seismic event, when known')
    selectflag = Column(Integer, info='This flag describes the definitive definition of an event. Helps to differentiate which event to use from two different real-time systems')
    lddate = Column(DateTime, server_default=text('NOW()'))
    version = Column(Integer, nullable=False, info='Event version number. This attribute is incremented each time the preferred origin, magnitude or mechanism of the event is updated')

    CheckConstraint('evid > 0', name='event02')

    def __repr__(self):
        s = "%s evid:%d prefor:%s prefmag:%s prefmec:%s auth:%s subsource:%s" % \
                (self.__tablename__.capitalize(), self.evid, self.prefor, self.prefmag,
                 self.prefmec, self.auth, self.subsource)
        return s


class Assocevents(Base):
    __tablename__ = "assocevents"

    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False, info='This should refer to event generated by local RSN source')
    evidassoc = Column(ForeignKey('event.evid'), primary_key=True, nullable=False, info='ID of other event, eg from PDL')
    commid = Column(Integer, info='Comment identification. This is a key used to point to free-form comments entered in the Remark relation.')
    lddate = Column(DateTime, server_default=text('NOW()'))
    #__table_args__ = (UniqueConstraint('evid', 'evidassoc', name='_evid_evidassoc_uc'),)
    __table_args__ = (UniqueConstraint('evid', 'evidassoc', name='_evid_evidassoc_uc'), CheckConstraint('evid <> evidassoc', name='_evid_evidassoc_uc1'))

    CheckConstraint('evid != evidassoc', name='assocevents01')

class Eventprefmag(Base):
    __tablename__ = "eventprefmag"

    magtype = Column(String(6), primary_key=True, nullable=False, info='This character string is used to specify the type of the magnitude measure')
    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False)
    magid = Column(ForeignKey('netmag.magid'), nullable=False)
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint("magtype in ('p','a','b','e','l','l1','l2','l3','lg','c','s',\
                                 'w','z','B', 'un','d','h','n','dl','lr')", name='EventPrefMag_CHKTYP')

class Eventprefmec(Base):
    __tablename__ = "eventprefmec"

    mechtype = Column(String(2), primary_key=True, nullable=False, info='Mechanism type')
    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False)
    mecid = Column(ForeignKey('mec.mecid'))
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('evid > 0', name='EvPMec01')
    CheckConstraint('mecid > 0', name='EvPMec02')
    CheckConstraint("mechtype IN ('FP','MT')", name='EvPMec03')

    def __repr__(self):
        s = "%s evid:%d mecid:%d mechtype:%s" % \
                (self.__tablename__.capitalize(), self.evid, self.mecid, self.mechtype)
        return s


class Eventprefor(Base):
    __tablename__ = "eventprefor"

    _type = Column('type', String(2), primary_key=True, nullable=False, info='Type of location')
    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False)
    orid = Column(ForeignKey('origin.orid'), nullable=False)
    lddate = Column(DateTime, server_default=text('NOW()'))
    CheckConstraint("type in ('h','H','c','C','a','A','d','D','u','U')", name='EventPrefOr_CHKTYP')
    def __repr__(self):
        s = "%s evid:%d orid:%d type:%s" % \
                (self.__tablename__.capitalize(), self.evid, self.orid, self._type)
        return s

class Eventtype(Base):
    __tablename__ = "eventtype"

    etype = Column(String(2), primary_key=True, nullable=False,
                   info ='This attribute is used to identify the type of seismic event')
    name = Column(String(16), nullable=False, info='Name associated with an event type')
    description = Column(String(80), info='Event type description')

    CheckConstraint("etype in ('eq','lp','se','to','tr','vt','nt','qb','ce','ex',\
                               'sh','sn','th','av','co','df','ls','rb','rs','ve',\
                               'bc','mi','pc','ot','st','uk','lf','su','px')", name='EVENTTYPE02')

class Mec(Base, MyMixin):
    __tablename__ = "mec"

    mecid = Column(Integer, Sequence('mecseq'), primary_key=True, nullable=False)
    oridin = Column(ForeignKey('origin.orid'))
    oridout = Column(ForeignKey('origin.orid'))
    magid = Column(ForeignKey('netmag.magid'))
    commid = Column(Integer)
    mechtype = Column(String(2), info='Mechanism type')
    mecalgo = Column(String(15), info='This attribute represents the algorithm used to compute the mechanism (TDMT,TMTS=Dregers code; FPFIT=fpfit; SMTINV=Pasyanos code)')
    scalar = Column(Numeric(asdecimal=False), info='Seismic scalar moment (no value stored for fpfit')
    erscalar = Column(Numeric(asdecimal=False), info='Error in scalar moment. This is the standard deviation of the scalar moment')
    tft = Column(String(2), info='Type of source time function. Used to specify if time function and source half-duration are assumed, derived, or determined')
    tfd = Column(Numeric(asdecimal=False), info='Duration of the source time function')
    mxx = Column(Numeric(asdecimal=False), info='Moment tensor elements. Given in Aki convention')
    myy = Column(Numeric(asdecimal=False), info='Moment tensor elements. Given in Aki convention')
    mzz = Column(Numeric(asdecimal=False), info='Moment tensor elements. Given in Aki convention')
    mxy = Column(Numeric(asdecimal=False), info='Moment tensor elements. Given in Aki convention')
    mxz = Column(Numeric(asdecimal=False), info='Moment tensor elements. Given in Aki convention')
    myz = Column(Numeric(asdecimal=False), info='Moment tensor elements. Given in Aki convention')
    smxx = Column(Numeric(asdecimal=False), info='Uncertainties in moment tensor elements')
    smyy = Column(Numeric(asdecimal=False), info='Uncertainties in moment tensor elements')
    smzz = Column(Numeric(asdecimal=False), info='Uncertainties in moment tensor elements')
    smxy = Column(Numeric(asdecimal=False), info='Uncertainties in moment tensor elements')
    smxz = Column(Numeric(asdecimal=False), info='Uncertainties in moment tensor elements')
    smyz = Column(Numeric(asdecimal=False), info='Uncertainties in moment tensor elements')
    srcduration = Column(Numeric(asdecimal=False), info='Source half-duration')

    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    #strike1 = Column(Numeric(precision=3, scale=0, asdecimal=False), info='Strike of fault plane 1 from the best double couple')
    strike1 = Column(Integer, info='Strike of fault plane 1 from the best double couple')
    dip1 = Column(Integer, info='Dip of fault plane 1 from the best double couple')
    rake1 = Column(Integer, info='Rake of fault plane 1 from the best double couple')
    strike2 = Column(Integer, info='Strike of fault plane 2 from the best double couple')
    dip2 = Column(Integer, info='Dip of fault plane 2 from the best double couple')
    rake2 = Column(Integer, info='Rake of fault plane 2 from the best double couple')
    unstrike1 = Column(Numeric(asdecimal=False), info='Uncertainty in strike 1')
    undip1 = Column(Numeric(asdecimal=False), info='Uncertainty in dip 1')
    unrake1 = Column(Numeric(asdecimal=False), info='Uncertainty in rake 1')
    unstrike2 = Column(Numeric(asdecimal=False), info='Uncertainty in strike 2')
    undip2 = Column(Numeric(asdecimal=False), info='Uncertainty in dip 2')
    unrake2 = Column(Numeric(asdecimal=False), info='Uncertainty in rake 2')
    eigenp = Column(Numeric(asdecimal=False), info='Eigen value of compressional axis in the principal axes representation of the best double couple')
    plungep = Column(Numeric(precision=2, scale=0, asdecimal=False), info='Plunge of compressional axis in the principal axes representation of the best double couple')
    strikep = Column(Numeric(precision=3, scale=0, asdecimal=False), info='Strike of compressional axis in the principal axes representation of the best double couple')
    eigenn = Column(Numeric(asdecimal=False), info='Eigen value of null axis axis in the principal axes representation of the best double couple')
    plungen = Column(Numeric(precision=2, scale=0, asdecimal=False), info='Plunge of null axis axis in the principal axes representation of the best double couple')
    striken = Column(Numeric(precision=3, scale=0, asdecimal=False), info='Strike of null axis axis in the principal axes representation of the best double couple')
    eigent = Column(Numeric(asdecimal=False), info='Eigen value of tension axis axis in the principal axes representation of the best double couple')
    plunget = Column(Numeric(precision=2, scale=0, asdecimal=False), info='Plunge of tension axis axis in the principal axes representation of the best double couple')
    striket = Column(Numeric(precision=3, scale=0, asdecimal=False), info='Strike of tension axis axis in the principal axes representation of the best double couple')
    nsta = Column(Integer, info='This quantity is the number of observations with non-zero input weights used to compute the mechanism. For CW=nsta, for FPFIT=nPobs, for HASH=nobs')
    #pvr = Column(Numeric(precision=5, scale=0, asdecimal=False), info='Goodness of fit. For TDMT this is percent variance reduction; for FPFIT this is (1-misfit)*100')
    pvr = Column(Integer, info='Goodness of fit. For TDMT this is percent variance reduction; for FPFIT this is (1-misfit)*100')
    quality = Column(Numeric(asdecimal=False), info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad')
    #pdc = Column(Numeric(precision=3, scale=0, asdecimal=False), info='Percent double couple of the seismic moment tensor')
    pdc = Column(Integer, info='Percent double couple of the seismic moment tensor')
    pclvd = Column(Integer, info='Percent compensated linear vector dipole of the seismic moment tensor')
    piso = Column(Integer, info='Percent isotropic of the seismic moment tensor. If the solution is constrained to have zero trace (no "ISO"), then "ISO" should be NULL, as opposed to a case where the full moment tensor was used, and the ISO component ended up being very small ("0")')
    datetime = Column(Numeric, nullable=False, info='The date of associated with information in the record, in true epoch format')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('plungen >= 0 and plungen <= 90', name='mec13')
    CheckConstraint('plungep >= 0 and plungep <= 90', name='mec14')
    CheckConstraint('plunget >= 0 and plunget <= 90', name='mec15')
    CheckConstraint('pclvd >= 0 and pclvd <= 100', name='mec16')
    CheckConstraint('pdc >= 0 and pdc <= 100', name='mec17')
    CheckConstraint('piso >= 0 and piso <= 100', name='mec18')
    CheckConstraint('pvr >= 0 and pvr <= 100', name='mec19')
    CheckConstraint('rake1 >= -180 and rake1 <= 180', name='mec20')
    CheckConstraint('rake2 >= -180 and rake1 <= 180', name='mec21')
    CheckConstraint('srcduration >= 0.0 and srcduration <= 100', name='mec23')  #MTH: seems odd to have max srcduration!
    CheckConstraint('striken >= 0 and striken <= 360', name='mec24')
    CheckConstraint('strikep >= 0 and strikep <= 360', name='mec25')
    CheckConstraint('striket >= 0 and striket <= 360', name='mec26')
    CheckConstraint('strike1 >= 0 and strike1 <= 360', name='mec27')
    CheckConstraint('strike2 >= 0 and strike2 <= 360', name='mec28')
    CheckConstraint('tfd > 0', name='mec29')
    CheckConstraint('undip1 >= -180 and undip1 <= 180', name='mec30')
    CheckConstraint('undip2 >= -180 and undip2 <= 180', name='mec31')
    CheckConstraint('unrake1 >= -180 and unrake1 <= 180', name='mec38')
    CheckConstraint('unrake2 >= -180 and unrake2 <= 180', name='mec39')
    CheckConstraint('unstrike1 >= -180 and unstrike1 <= 180', name='mec40')
    CheckConstraint('unstrike2 >= -180 and unstrike2 <= 180', name='mec41')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='mec42')

    CheckConstraint('dip1 >= -90 and dip1 <= 90', name='mec01')
    CheckConstraint('dip2 >= -90 and dip2 <= 90', name='mec02')
    CheckConstraint('erscalar >= 0.0', name='mec03')
    CheckConstraint('mecid > 0', name='mec05')
    CheckConstraint("mechtype in ('FP', 'MT')", name='mec06')

    exclude_fields_from_comparison = ['mecid', 'commid', 'magid', 'lddate']

    def __repr__(self):
        s = "%s auth:%s subsource:%s mecid:%s magid:%s oridin:%s mechtype:%s mecalgo:%s nsta:%s quality:%s" % \
            (self.__tablename__.capitalize(), self.auth, self.subsource, self.mecid, self.magid, self.oridin,
             self.mechtype, self.mecalgo, self.nsta, self.quality)
        return s

class Mecfreq(Base, MyMixin):
    __tablename__ = "mecfreq"

    mecfreqid = Column(Integer, Sequence('mecfreqseq'), primary_key=True, nullable=False)
    mecalgo = Column(String(15), info='This attribute represents the algorithm used to compute the mechanism')
    lddate = Column(DateTime, server_default=text('NOW()'))
    CheckConstraint('mecfreqid > 0', name='MecF01')

    """
    mecdatas = relationship(
      "Mecdata",
      order_by = Mecdata.mecfreqid,
      back_populates = "mecfreq",
      cascade = "all, delete, delete-orphan"
   )
    """

    def __repr__(self):
        s = "%s mecfreqid:%s mecalgo:%s" % \
            (self.__tablename__.capitalize(), self.mecfreqid, self.mecalgo)
        return s


class Mecdata(Base, MyMixin):
    __tablename__ = "mecdata"

    mecdataid = Column(Integer, Sequence('mecdataseq'), primary_key=True, nullable=False)
    mecid = Column(Integer, ForeignKey(Mec.mecid))
    # MTH: Is aqms using mecfreq table ??
    mecfreqid = Column(Integer, ForeignKey(Mecfreq.mecfreqid))
    #mecfreqid = Column(Integer, ForeignKey('mecfreq.mecfreqid', ondelete='CASCADE'))
    #mecfreqid = Column(ForeignKey('mecfreq.mecfreqid'))
    #mecfreqid = Column(Integer)

    #mecfreq = relationship(
      #"Mecfreq",
      #back_populates = "mecdatas",
   #)

    polarity = Column(String(2), info="Polarity of pick. Format like arrival.fm={'cu','cr',...,'.u',... ")
    discrepancy = Column(String(1), info="Mainly used for fpfit. Does the polarity of the data match the quadrant for the mechanism")
    orientation = Column(String(1), info="How are the compponents in the mecchannel table used (rotation, transformation). If channel has a polarity reversal, this should be indicated by a 'I' for inverted in this field.")
    quality = Column(Numeric(asdecimal=False), info="Quality. Fpfit uses quality 0-9")
    amplitude = Column(Numeric(asdecimal=False), info="SMTINV: spectral amplitude a the frequency given by mecfreqid")
    time1 = Column(Numeric(asdecimal=False), info="Start time of: TDMT(waveform window)/SMTINV(spectrum window)/FPFIT(pick time)")
    time2 = Column(Numeric(asdecimal=False), info="End of window, if window used")
    model = Column(String(10), info="Model used for greens functions; velocity model for other methods")
    zcor = Column(Numeric(asdecimal=False), info="TDMT: value of cross correlation shift in samples for best solution")
    corlen = Column(Numeric(asdecimal=False), info="TDMT: length of cross correlation window used in samples")
    dt = Column(Numeric(asdecimal=False), info="Sampling interval o data used in inversion")
    varred = Column(Numeric(asdecimal=False), info="Fit for this station")
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint("discrepancy in ('T', 'F')", name='MecData01')
    CheckConstraint("orientation in ('C', 'R', 'T', 'H', '3', 'L', 'E', 'N', 'Z', 'A', 'I', ' ')", name='MecData02')
    CheckConstraint('quality >= 0 and quality <= 1', name='MecData03')
    CheckConstraint("model in ('mend1_', 'socal', 'gil7_')", name='MecData04')
    CheckConstraint('mecid > 0', name='MecData05')
    CheckConstraint('mecdataid > 0', name='MecData06')
    CheckConstraint('mecfreqid > 0', name='MecData07')

    # MTH: runfp seems to be setting: mecid, mecdataid, polarity (U/D), quality, time1, discrepancy 
    #                        but not: mecfreqid or orientation
    #exclude_fields_from_comparison = ['mecid', 'commid', 'magid', 'lddate']
    # ???
    exclude_fields_from_comparison = ['mecdataid', 'mecid', 'lddate']

    def __repr__(self):
        s = "%s mecdataid:%s mecid:%s mecfreqid:%s orientation:%s time1:%s time2:%s zcor:%s corlen:%s varred:%s" % \
            (self.__tablename__.capitalize(), self.mecdataid, self.mecid, self.mecfreqid, self.orientation,
             self.time1, self.time2, self.zcor, self.corlen, self.varred)
        return s

class Mecchannel(Base, MyMixin):
    __tablename__ = "mecchannel"

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    mecdataid = Column(Integer, ForeignKey(Mecdata.mecdataid), primary_key=True, nullable=False)
    #mecdataid = Column(Integer, ForeignKey('mecdata.mecdataid'), primary_key=True, nullable=False)
    net = Column(String(8), primary_key=True, info="Network code", nullable=False)
    sta = Column(String(6), primary_key=True, info="Station code", nullable=False)
    seedchan = Column(String(3), primary_key=True, info="Channel code", nullable=False)
    location = Column(String(2), primary_key=True, info="Location code", nullable=False)

    CheckConstraint('mecdataid > 0', name='MecD01')

    def __repr__(self):
        s = "%s mecdataid:%s net:%s sta:%s cha:%s loc:%s" % \
            (self.__tablename__.capitalize(), self.mecdataid, self.net, self.sta, self.seedchan, self.location)
        return s

class Mecfreqdata(Base, MyMixin):
    __tablename__ = "mecfreqdata"
    mecfreqid = Column(Integer, Sequence('mecfreqseq'), primary_key=True, nullable=False)
    _type = Column('type', String(15), primary_key=True, nullable=False, info='frequency for fit')
    freq = Column(Integer, info='Frequency value')

    CheckConstraint('mecfreqid > 0', name='MecFreqData02')
    CheckConstraint("type in ('LP','HP','SF')", name='MecFreqData01')

    def __repr__(self):
        s = "%s mecfreqid:%s type:%s freq:%s" % \
            (self.__tablename__.capitalize(), self.mecfreqid, self._type, self.freq)
        return s

class Mecobject(Base, MyMixin):
    __tablename__ = "mecobject"

    mecid = Column(Integer, primary_key=True, nullable=False)
    dataid = Column(Integer, primary_key=True, nullable=False)
    mimetype = Column(String(20))
    meta = Column(String(128))
    data = Column(BYTEA, info='mec aux file object')


class Netmag(Base, MyMixin):
    __tablename__ = "netmag"

    magid = Column(Integer, Sequence('magseq'), primary_key=True, nullable=False)
    orid = Column(ForeignKey('origin.orid'), nullable=False)
    commid = Column(Integer)
    magnitude = Column(Numeric(asdecimal=False), nullable=False, info='This gives the magnitude value of the type indicated by magtype')
    magtype = Column(String(6), nullable=False, info='This character string is used to specify the type of the magnitude measure')
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    magalgo = Column(String(15), info='This attribute represents the algorithm used to compute the magnitude')
    #nsta = Column(Numeric(asdecimal=False), info='This quantity is the number of stations with non-zero input weights used to compute the magnitude')
    #nobs = Column(Numeric(asdecimal=False), info='This quantity is the number of observations with non-zero input weights used to compute the magnitude')
    nsta = Column(Integer, info='This quantity is the number of stations with non-zero input weights used to compute the magnitude')
    nobs = Column(Integer, info='This quantity is the number of observations with non-zero input weights used to compute the magnitude')
    uncertainty = Column(Numeric(asdecimal=False), info='Magnitude uncertainty. Median of the absolute values of the channel magnitude residual deviations from the channel magnitudes median, the summary magnitude')
    gap = Column(Numeric(asdecimal=False), info='Gap in azimuthal coverage')
    distance = Column(Numeric(asdecimal=False), info='Distance from the origin to the nearest station')
    quality = Column(Numeric(asdecimal=False), info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('magnitude >= -10.0 and magnitude <= 10.0', name='netmag01')
    CheckConstraint("magtype in ('p', 'a', 'b', 'e', 'l', 'l1', 'l2', 'l3', 'lg', \
                                 'c', 's', 'w', 'z', 'B', 'un', \
                                 'd', 'h', 'n', 'dl', 'lr')", name='netmag02')
    CheckConstraint('nsta >= 0', name='netmag03')
    CheckConstraint('uncertainty >= 0.0', name='netmag04')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='netmag05')
    CheckConstraint('magid > 0', name='netmag06')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='netmag07')
    CheckConstraint('nobs >= 0', name='netmag08')

    exclude_fields_from_comparison = ['magid', 'orid', 'commid', 'lddate']

    #def __init__(self, **kwargs):
        #return init(self, **kwargs)

    def __repr__(self):
        s = "%s auth:%s subsource:%s magid:%d orid:%s magnitude:%.2f magtype:%s nsta:%s nobs:%s" % \
            (self.__tablename__, self.auth, self.subsource, self.magid, self.orid, self.magnitude,
             self.magtype, self.nsta, self.nobs)
        return s

    '''
    def __repr__(self):
        s = "%s net:%s sta:%s seedchan:%s loc:%s ondate:%s offdate:%s lat:%.3f lon:%.3f elev:%.1f azim:%s" %\
            (self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, self.offdate,
             self.lat, self.lon, self.elev, self.azimuth)
        return s
    '''



class Origin(Base, MyMixin):
    __tablename__ = "origin"

    orid = Column(Integer, Sequence('orseq'), primary_key=True, nullable=False)
    evid = Column(ForeignKey('event.evid'), nullable=False)
    prefmag = Column(ForeignKey('netmag.magid'))
    prefmec = Column(ForeignKey('mec.mecid'))
    commid = Column(Integer)
    bogusflag = Column(Integer, nullable=False, info='This field describes whether an origin is bogus or not')
    datetime = Column(Numeric(asdecimal=False), nullable=False, info='The date of associated with information in the record, in true epoch format')
    lat = Column(Numeric(asdecimal=False), nullable=False, info='Locations north of the equator have positive latitudes')
    lon = Column(Numeric(asdecimal=False), nullable=False, info='Longitudes are measured positive east of the Greenwich meridian')
    depth = Column(Numeric(asdecimal=False), info='Distance in KM of the earthquake source below the geoid.')
    mdepth = Column(Numeric(asdecimal=False), info='Distance in KM of the earthquake source below the model surface.')
    _type = Column('type', String(2), info='Type of location')
    algorithm = Column(String(15), info='Algorithm used to compute a seismic origin')
    algo_assoc = Column(String(80), info='Association algorithm used')
    auth = Column(String(15), nullable=False, info='The auth field specifies the source of the information')
    subsource = Column(String(8), info='A second identifier to specify the system or process that derived the data')
    datumhor = Column(String(8), info='Datum type for horizontal (latitude and longitude) coordinates')
    datumver = Column(String(8), info='Datum type for depth or elevation')
    gap = Column(Numeric(asdecimal=False), info='Gap in azimuthal coverage')
    distance = Column(Numeric(asdecimal=False), info='Distance from the origin to the nearest station')
    wrms = Column(Numeric(asdecimal=False), info='This attribute denotes the weighted RMS')
    stime = Column(Numeric(asdecimal=False), info='Origin time error. This attribute denotes the standard deviation of the origin time')
    erhor = Column(Numeric(asdecimal=False), info='Horizontal error. This attribute denotes the location uncertainty that accompanies the location')
    sdep = Column(Numeric(asdecimal=False), info='Depth error. This is the standard deviation of the depth estimate')
    erlat = Column(Numeric(asdecimal=False), info='Error in latitude. This is the standard deviation of the latitude estimate')
    erlon = Column(Numeric(asdecimal=False), info='Error in longitude. This is the standard deviation of the longitude estimate')
    totalarr = Column(Integer, info='Total number of phases in the database for the specified orid. This field signifies the number of P and S arrivals with non-zero input weights')
    totalamp = Column(Integer, info='Total number of amplitudes in the database for the specified orid. Not all amplitudes have to associated with all magnitudes or origins')
    ndef = Column(Integer, info='This attribute denotes the number of phases with non-zero output weights used in hypocenter calculation')
    nbs = Column(Integer, info='Number of S phase readings with non-zero output weights')
    nbfm = Column(Integer, info='Number of P first motions with non-zero input weights')
    locevid = Column(String(12), info='Local event identifier. This attribute describes the original event identifier of the event')
    quality = Column(Numeric(asdecimal=False), info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad')
    fdepth = Column(String(1), info='This attribute indicates if the depth was fixed or not to compute the solution')
    fepi = Column(String(1), info='This attribute indicates if the epicenter was fixed or not to compute the solution')
    ftime = Column(String(1), info='This attribute indicates if the origin time was fixed or not to compute the solution')
    vmodelid = Column(String(2), info='Velocity model identification. This string identifies a version (instance) of the velocity model for the specified domain and algorithm')
    cmodelid = Column(String(2), info='Velocity model domain. This string identifies the organization responsible for the velocity model')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    crust_type = Column(String(1), info='The type of crust model used for travel time calculations.')
    crust_model = Column(String(3), info='The code for the regional (geographic) crust model that is dominant in the travel time calculations.')
    gtype = Column(String(1), info='Geographic type (local, regional, teleseism)')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint("datumhor in ('NAD27','WGS84')", name='origin02')
    CheckConstraint("datumver in ('NAD27','WGS84','AVERAGE')", name='origin03')
    CheckConstraint('depth >= -10.0 and depth <= 1000.0', name='origin04')
    CheckConstraint('distance >= 0.0', name='origin05')
    CheckConstraint('erhor >= 0.0', name='origin06')
    CheckConstraint('erlat >= 0.0', name='origin07')
    CheckConstraint('erlon >= 0.0', name='origin08')
    CheckConstraint("fdepth in ('y','n')", name='origin09')
    CheckConstraint("fepi in ('y','n')", name='origin10')
    CheckConstraint("ftime in ('y','n')", name='origin11')
    CheckConstraint('gap >= 0.0 and gap <= 360.0', name='origin12')
    CheckConstraint('nbfm >= 0', name='origin15')
    CheckConstraint('nbs >= 0', name='origin16')
    CheckConstraint('ndef >= 0', name='origin17')
    CheckConstraint('orid > 0', name='origin18')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='origin19')
    CheckConstraint("type in ('H','h','C','c','A','a','D','d','u','U')", name='origin20')
    CheckConstraint('stime >= 0.0', name='origin21')
    CheckConstraint('wrms >= 0.0', name='origin23')
    CheckConstraint('sdep >=0.0', name='origin24')
    CheckConstraint('totalarr >=0', name='origin25')
    CheckConstraint('totalamp >= 0', name='origin26')
    CheckConstraint("rflag in ('a','h','f','A','H','F','i','I','c','C')", name='origin28')
    CheckConstraint("crust_type in ('H','T','E','L','V')", name='origin30')
    CheckConstraint("gtype in ('l','r','t')", name='origin31')
    #CheckConstraint("gtype in ('L','R','T')", name='origin31')

    def __repr__(self):
        s = "%s orid:%s auth:%s datetime:%.1f lat:%.2f lon:%.2f depth:%.1f subsource:%s locevid:%s" % \
            (self.__tablename__, self.orid, self.auth, self.datetime, self.lat, self.lon, self.depth,
             self.subsource, self.locevid)
        return s

    exclude_fields_from_comparison = ['orid', 'evid', 'lddate']


class Origin_Error(Base):
    __tablename__ = "origin_error"

    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False)
    sxx = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    syy = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    szz = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    stt = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    sxy = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    sxz = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    syz = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    stx = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    sty = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    stz = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    azismall = Column(Numeric, info='Azimuth of smallest principal error')
    dipsmall = Column(Numeric, info='Dip of smallest principal error')
    magsmall = Column(Numeric, info='Magnitude of smallest principal error')
    aziinter = Column(Numeric, info='Azimuth of intermediate principal error')
    dipinter = Column(Numeric, info='Dip of intermediate principal error')
    maginter = Column(Numeric, info='Magnitude of intermediate principal error')
    azilarge = Column(Numeric, info='Azimuth of largest principal error')
    diplarge = Column(Numeric, info='Dip of largest principal error')
    maglarge = Column(Numeric, info='Magnitude of largest principal error')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('aziinter >= 0.0 and aziinter <= 360.0', name='origin_error01')
    CheckConstraint('azilarge >= 0.0 and azilarge <= 360.0', name='origin_error02')
    CheckConstraint('azismall >= 0.0 and azismall <= 360.0', name='origin_error03')
    CheckConstraint('dipinter >= -90.0 and dipinter <= 90.0', name='origin_error04')
    CheckConstraint('diplarge >= -90.0 and diplarge <= 90.0', name='origin_error05')
    CheckConstraint('dipsmall >= -90.0 and dipsmall <= 90.0', name='origin_error06')
    CheckConstraint('maginter >= 0.0', name='origin_error07')
    CheckConstraint('maglarge >= 0.0', name='origin_error08')
    CheckConstraint('magsmall >= 0.0', name='origin_error09')
    CheckConstraint('stx > 0.0', name='origin_error10')
    CheckConstraint('sty > 0.0', name='origin_error11')
    CheckConstraint('stz > 0.0', name='origin_error12')
    CheckConstraint('sxx > 0.0', name='origin_error13')
    CheckConstraint('sxy > 0.0', name='origin_error14')
    CheckConstraint('sxz > 0.0', name='origin_error15')
    CheckConstraint('syy > 0.0', name='origin_error16')
    CheckConstraint('syz > 0.0', name='origin_error17')
    CheckConstraint('stt > 0.0', name='origin_error18')
    CheckConstraint('szz > 0.0', name='origin_error19')

class Remark(Base):
    __tablename__ = "remark"

    commid = Column(Integer, Sequence('commseq'), primary_key=True, nullable=False)
    lineno = Column(Integer, primary_key=True, nullable=False, info='Comment line number. This integer attribute is assigned as a sequence number for multiple line comments. The combination of commid and lineno is unique')
    remark = Column(String(80), info='Descriptive text. This single line of text is an arbitrary comment about a record in the database. The comment is linked to its parent relation only by forward reference from commid in the tuple of the relation of interest')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('commid > 0', name='remark01')
    CheckConstraint('lineno > 0', name='remark02')

class Remark_Origin(Base):
    __tablename__ = "remark origin"

    lineno = Column(ForeignKey('remark.lineno'), primary_key=True, nullable=False, info='Comment line number. This integer attribute is assigned as a sequence number for multiple line comments. The combination of commid and lineno is unique')
    commid = Column(ForeignKey('remark.commid'), primary_key=True, nullable=False, info='Comment Identifier')
    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False, info='Origin identification. Each origin is assigned a unique positive integer which identifies it in the database. The orid is used to identify one of the many hypotheses of the actual location of the event')
    lddate = Column(DateTime, server_default=text('NOW()'))

class Unassocamp(Base):
    __tablename__ = "unassocamp"

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    ampid = Column(Integer, Sequence('unassocseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    datetime = Column(Numeric)
    sta = Column(String(6), nullable=False)
    net = Column(String(6))
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), info='SEED channel name. The first character denotes the band code, the second for the instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the same network operator ')
    iphase = Column(String(8), info='Reported phase. This eight-character field holds the name initially given to a seismic phase.')
    amplitude = Column(Numeric, nullable=False, info='Amplitude in appropriate units for type ')
    amptype = Column(String(8), info='Amplitude type')
    units = Column(String(4), nullable=False, info='Units of amplitude')
    ampmeas = Column(String(1), info='Amplitude measure')
    eramp = Column(Numeric, info='Uncertainty in amplitude measurement')
    flagamp = Column(String(4), info='This attribute is a flag to indicate whether amplitude is over P packet, S packet, entire waveform, etc')
    per = Column(Numeric, info='Signal period. This attribute is the period of the signal described by the amplitude record ')
    snr = Column(Numeric, info='Signal-to-noise ratio. This is an estimate of the signal relative to that of the noise immediately preceding it')
    tau = Column(Numeric, info='Coda duration (F-P time)')
    quality = Column(Numeric, info='This attribute describes the completeness of the time window of data. A complete time window is 1.0, an incomplete time window is 0.0.')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    cflag = Column(String(2), info='This flag indicates whether the amplitude value is below noise, on scale or clipped')
    wstart = Column(Numeric, nullable=False, info='Start of time window of amplitude measurement')
    duration = Column(Numeric, info='Duration of amplitude reading')
    lddate = Column(DateTime, server_default=text('NOW()'))
    fileid = Column(Integer, info='The value is assigned to all new rows parsed from a single GMP channel packet. Rows having the same fileid value are associated with the same event origin')

    # table level CHECK constraint.  'name' is optional.
    CheckConstraint('ampid > 0', name='amp01')
    CheckConstraint('amplitude > 0', name='amp02')
    CheckConstraint("ampmeas in ('0','1')", name='amp03')
    CheckConstraint("amptype in ('C','WA','WAS','WASF','PGA','PGV','PGD','WAC',\
                                 'WAU','IV2','SP.3','SP1.0','SP3.0','ML100','ME100',\
                                 'EGY','M0')", name='amp04')
    CheckConstraint('eramp>=0.0', name='amp06')
    CheckConstraint("flagamp in ('P','S','R','PP','ALL','SUR')", name='amp07')
    CheckConstraint('per>0.0', name='amp08')
    CheckConstraint('tau>0.0', name='amp09')
    CheckConstraint("units in ('c','s','mm','cm','m','ms','mss','cms','cmss',\
                    'mms','mmss','mc','nm','e','cmcms','none','dycm')", name='amp10')
    CheckConstraint('quality>=0.0 and quality<=1.0', name='amp11')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='amp12')
    CheckConstraint("cflag in cflag in ('bn', 'os','cl','BN','OS','CL')", name='amp13')

'''
Data associating amplitudes with magnitudes
'''
class Assocamm(Base):
    __tablename__ = "assocamm"

    magid = Column(ForeignKey('netmag.magid'), primary_key=True, nullable=False)
    ampid = Column(ForeignKey('amp.ampid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    weight = Column(Numeric, info='Magnitude weight')
    in_wgt = Column(Numeric, info='Input weight: 0.0-1.0')
    mag = Column(Numeric, info='Magnitude for this station reading')
    magres = Column(Numeric, info='Magnitude residual')
    magcorr = Column(Numeric, info='Magnitude correction for this channel')
    importance = Column(Numeric, info='This attribute denotes the importance of a phase/amplitude reading towards an origin/magnitude. 0.0 means no importance, 1.0 implies extremely important')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('mag >= -10.0 and mag <= 10.0', name='assocamm01')
    CheckConstraint('magcorr >= -10.0 and magcorr <= 10.0', name='assocamm02')
    CheckConstraint('magid > 0', name='assocamm03')
    CheckConstraint('weight >= 0.0 and weight <= 1.0', name='assocamm05')
    CheckConstraint('in_wgt >= 0.0 and in_wgt <= 1.0', name='assocamm06')
    CheckConstraint('importance > 0.0 and importance <= 1.0', name='assocamm07')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assocamm08')

'''
Data associating amplitudes with origins
'''
class Assocamo(Base):
    __tablename__ = "assocamo"

    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False)
    ampid = Column(ForeignKey('amp.ampid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    delta = Column(Numeric, info='Source-receiver distance. This attribute is the arc length, over the earth�s surface, of the path the seismic phase follows from source to receiver.')
    seaz = Column(Numeric, info='Station to event azimuth. It is measured clockwise from North ')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('seaz >= 0.0 and seaz <= 360.0', name='assocamo01')
    CheckConstraint('delta >= 0.0', name='assocamo02')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assocamo03')

'''
Data associating arrivals with origins
'''
class Assocaro(Base):
    __tablename__ = "assocaro"

    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False)
    arid = Column(ForeignKey('arrival.arid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    iphase = Column(String(8))
    importance = Column(Numeric, info='This attribute denotes the importance of a phase/amplitude reading towards an origin/magnitude. 0.0 means no importance, 1.0 implies extremely important')
    delta = Column(Numeric, info='Source-receiver distance. This attribute is the arc length, over the earth�s surface, of the path the seismic phase follows from source to receiver.')
    seaz = Column(Numeric, info='Station to event azimuth. It is measured clockwise from North ')
    in_wgt = Column(Numeric, info='Input weight. 0.0-1.0')
    wgt = Column(Numeric, info='Location weight. This attribute gives the final weight assigned to the allied arrival by the location program.')
    timeres = Column(Numeric, info='Time residual. This attribute is a travel time residual, measured in seconds. The residual is found by taking the observed arrival time (saved in the Arrival relation) of a seismic phase and substracting the expected arrival time.')
    ema = Column(Numeric, info='Emergence angle residual. This attribute is the difference between an observed emergence angle and the theorical prediction for the same phase, assuming an event location as specified by the accompanying orid')
    slow = Column(Numeric, info='Slowness residual. This attribute gives the difference between an observed slowness (saved in the Arrival relation) and a theorical prediction')
    vmodelid = Column(Integer, info='Velocity model identification')
    scorr = Column(Numeric, info='Station correction')
    sdelay = Column(Numeric, info='Station delay')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    ccset = Column(Integer, info='This field describes whether the clock correction has been added or applied to the arrivals in computing a hypocenter solution')
    #ccset = Column(String(1), info='This field describes whether the clock correction has been added or applied to the arrivals in computing a hypocenter solution')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('delta >= 0.0', name='assocaro02')
    CheckConstraint('ema >= 0 and ema <= 180', name='assocaro03')
    CheckConstraint('importance >= 0.0 and importance < 1.0', name='assocaro04')
    CheckConstraint('seaz >= 0.0 and seaz <= 360.0', name='assocaro05')
    CheckConstraint('slow >= 0.0', name='assocaro06')
    CheckConstraint('timeres >= 0.0', name='assocaro07')
    CheckConstraint('ccset < 1', name='assocaro09')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assocaro10')

'''
Data associating codas with magnitudes
'''
class Assoccom(Base):
    __tablename__ = "assoccom"
    magid = Column(ForeignKey('netmag.magid'), primary_key=True, nullable=False)
    coid = Column(ForeignKey('coda.coid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    weight = Column(Numeric, info='Magnitude weight')
    in_wgt = Column(Numeric, info='Input weight')
    mag = Column(Numeric, info='Magnitude for this station reading')
    magres = Column(Numeric, info='Magnitude residual')
    magcorr = Column(Numeric, info='Magnitude correction for this channel')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('weight >= 0.0 and weight <= 1.0', name='assoccomkey04')
    CheckConstraint('in_wgt >= 0.0 and in_wgt <= 1.0', name='assoccomkey05')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assoccomkey06')


'''
Data associating codas with origins
'''
class Assoccoo(Base):
    __tablename__ = "assoccoo"
    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False)
    coid = Column(ForeignKey('coda.coid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    delta = Column(Numeric, info='Source-receiver distance. This attribute is the arc length, over the earth�s surface, of the path the seismic phase follows from source to receiver.')
    seaz = Column(Numeric, info='Station to event azimuth. It is measured clockwise from North ')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assoccookey04')


"""
                                        Table "trinetdb.magprefpriority"
     Column     |            Type             | Collation | Nullable |                 Default
----------------+-----------------------------+-----------+----------+------------------------------------------
 region_name    | character varying(24)       |           | not null |
 datetime_on    | double precision            |           | not null |
 datetime_off   | double precision            |           | not null |
 priority       | double precision            |           | not null |
 magtype        | character varying(6)        |           |          |
 auth           | character varying(15)       |           |          |
 subsource      | character varying(8)        |           |          |
 magalgo        | character varying(15)       |           |          |
 minmag         | double precision            |           |          |
 maxmag         | double precision            |           |          |
 minreadings    | double precision            |           |          |
 maxuncertainty | double precision            |           |          |
 quality        | double precision            |           |          |
 lddate         | timestamp without time zone |           |          | timezone('UTC'::text, CURRENT_TIMESTAMP)
Indexes:
    "magpref_pk" PRIMARY KEY, btree (region_name, datetime_on, priority)
Check constraints:
    "magpref_magtype_ck" CHECK (magtype::text = ANY (ARRAY['p'::character varying, 'a'::character varying, 'b'::character varying, 'e'::character varying, 'l'::character varying, 'l1'::character varying, 'l2'::character varying, 'l3'::character varying, 'lg'::character varying, 'c'::character varying, 's'::character varying, 'w'::character varying, 'z'::character varying, 'B'::character varying, 'un'::character varying, 'd'::character varying, 'h'::character varying, 'n'::character varying, 'dl'::character varying, 'lr'::character varying]::text[]))
    "magpref_maxmag_ck" CHECK (maxmag >= '-10.0'::numeric::double precision AND maxmag <= 10.0::double precision)
    "magpref_minmag_ck" CHECK (minmag >= '-10.0'::numeric::double precision AND minmag <= 10.0::double precision)
"""

class Magprefpriority(Base):
    __tablename__ = "magprefpriority"

    region_name = Column(String(24), primary_key=True, nullable=False)
    datetime_on  = Column(Numeric, info='Time of first datum', primary_key=True, nullable=False)
    datetime_off = Column(Numeric, info='Time of last datum', nullable=False)
    priority  = Column(Numeric, info='magnitude pref priority', primary_key=True, nullable=False)
    magtype = Column(String(6), info='This character string is used to specify the type of the magnitude measure')
    auth = Column(String(15))
    subsource = Column(String(8))
    magalgo = Column(String(15), info='This attribute represents the algorithm used to compute the magnitude')
    minmag  = Column(Numeric, info='min mag for this type')
    maxmag  = Column(Numeric, info='min mag for this type')
    minreadings = Column(Numeric, info='min mag readings')
    maxuncertainty  = Column(Numeric)
    quality  = Column(Numeric)
    lddate = Column(DateTime, server_default=text('NOW()'))

    # table level CHECK constraint.  'name' is optional.
    CheckConstraint('ampid > 0', name='amp01')

    CheckConstraint("magtype in ('p', 'a', 'b', 'e', 'l', 'l1', 'l2', 'l3', 'lg', \
                                 'c', 's', 'w', 'z', 'B', 'un', \
                                 'd', 'h', 'n', 'dl', 'lr')", name='magpref_magtype_ck')
    CheckConstraint('maxmag >= -10.0 and maxmag <= 10.0', name='magpref_maxmag_ck')
    CheckConstraint('minmag >= -10.0 and maxmag <= 10.0', name='magpref_minmag_ck')

class Ampset(Base):
    __tablename__ = "ampset"
    ampsetid = Column(Integer, Sequence('ampsetseq'), primary_key=True, nullable=False,
                  info='Amplitude set identifier')
    ampid = Column(ForeignKey('amp.ampid'), primary_key=True, nullable=False, info='Amplitude identification')

class Assocevampset(Base):
    __tablename__ = "assocevampset"

    ampsetid = Column(ForeignKey('ampset.ampsetid'), primary_key=True, nullable=False, info='Amplitude set identifier')
    ampsettype = Column(ForeignKey('ampsettypes.ampsettype'), primary_key=True, nullable=False, info='Amplitude set type, eg, "sm"')
    evid = Column(ForeignKey('event.evid'), nullable=False, info='event evid')
    subsource = Column(String(8), info='Name tag of the application creating the set, e.g., ampgen')
    isvalid = Column(Integer, info='1 if preferred set, else 0')
    lddate = Column(DateTime, server_default=text('NOW()'))

class Ampsettypes(Base):
    __tablename__ = "ampsettypes"
    ampsettype = Column(String(20), primary_key=True, nullable=False, info='Amplitude set type, eg, "sm"')
    description = Column(String(80), info='The long descriptoin of an amplitude set type')

