# -*- coding: utf-8 -*-
"""
functions for querying AQMS db IR tables

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime
import logging
import time

logger = logging.getLogger(__name__)

from aqms_pdl.libs.libs_db_PI import getSequence
from aqms_pdl.libs.schema_aqms_AP import Epochtimebase, Applications, Appchannels
from aqms_pdl.libs.schema_aqms_C_ import C_Channeldata, C_Auth, C_Net, C_Units, C_Phystyp
from aqms_pdl.libs.schema_aqms_C_ import C_Loggerdata, C_Sensordata
from aqms_pdl.libs.schema_aqms_IR import Station_Data, Channel_Data, SimpleResponse, D_Abbreviation, D_Unit
from aqms_pdl.libs.schema_aqms_IR import D_Format, Poles_Zeros, PZ, PZ_Data
from aqms_pdl.libs.schema_aqms_IR import Polynomial, PN, PN_Data
from aqms_pdl.libs.schema_aqms_IR import Coefficients, DC, DC_Data, Decimation, DM
from aqms_pdl.libs.schema_aqms_IR import Sensitivity, Equipment
from aqms_pdl.libs.schema_aqms_IR import get_tables
from aqms_pdl.libs.schema_aqms_IR import Channel_Comment, Station_Comment, D_Comment
from aqms_pdl.libs.schema_aqms_PI import Amp, Ampset, Assocevampset, SpectralAmp

from sqlalchemy import text


def set_Session_IR(SessionIn):
    global Session
    Session = SessionIn

def load_lookup_tables(tables):

    lookup_tables = {}

    base = get_tables()

    with Session.begin() as session:
        #for tbl in tables:
        for tablename in tables:
            #print(tbl.__tablename__)
            #lookup_tables[tbl.__tablename__] = session.query(tbl).all()
            cls = base[tablename]
            lookup_tables[tablename] = session.query(cls).all()
        session.expunge_all()

    return lookup_tables

def get_or_insert_id_from_abbrev(description):

    fname = 'get_or_insert_id_from_abbrev'
    #print("%s: got description=[%s]" % (fname, description))

    if len(description) == 0:
        logger.warning("%s: len(description)=0 --> Don't insert or return id!" % fname)
        return None

    _id = get_id_from_abbrev(description)

    DESCRIPTION_LEN = 70

    if _id:
        #print("get_or_insert: Found id=%d for description=[%s]" % (_id, description))
        return _id
    else:
        with Session.begin() as session:
            if len(description) > DESCRIPTION_LEN:
                logger.warning("%s: description len=%d will be truncated to:%d" %
                      (fname, len(description), DESCRIPTION_LEN))
                description=description[:DESCRIPTION_LEN]
            logger.warning("%s: description=[%s] not found --> Add it" % (fname, description))
            session.add(D_Abbreviation(description=description))

    return get_id_from_abbrev(description)

def get_abbrev_description_from_id(id):
    with Session.begin() as session:
        row = session.query(D_Abbreviation).filter_by(id=id).one_or_none()
        session.expunge_all()
    return row.description


def get_d_unit_from_id(id):
    '''
    '''
    fname = 'get_d_unit_from_id'

    with Session.begin() as session:
        row = session.query(D_Unit).filter(D_Unit.id==id).one_or_none()
        session.expunge_all()

    #if row:
        #return row.name

    return row

def get_id_from_abbrev(description):
    '''
    '''
    fname = 'get_id_from_abbrev'

    start = time.time()
    with Session.begin() as session:
        row = session.query(D_Abbreviation).filter(D_Abbreviation.description==description).first()
        session.expunge_all()
    """
    with Session.begin() as session:
        rows = session.query(D_Abbreviation).all()
        session.expunge_all()

    print("%s: time to query all D_Abbreviation:%s nrows:%d" % (fname, time.time()-start, len(rows)))

    start = time.time()
    found=False
    for row in rows:
        if row.description==description:
            found=True
            break
    print("%s: time to find description:%s found:%s" % (fname, time.time()-start, found))
    exit()
    """

    if row:
        return row.id

    return None

def get_or_insert_unit(name, description):

    fname = 'get_or_insert_unit'
    #print("%s: got name=[%s] description=[%s]" % (fname, name, description))
    if len(name) == 0:
        logger.warning("%s: len(name)=0 --> Don't insert, don't return id!" % fname)
        return None

    _id = get_id_from_unit(name, description)

    if _id:
        return _id
    else:
        with Session.begin() as session:
            session.add(D_Unit(name=name, description=description))

    return get_id_from_unit(name, description)

def get_id_from_unit(name, description):
    '''
    '''
    fname = 'get_id_from_unit'
    #start = time.time()
    with Session.begin() as session:
        result = session.query(D_Unit).filter_by(name=name, description=description).first()
        session.expunge_all()

    #print("%s: name:%s description:%s time spent:%s" % (fname, name, description, time.time()-start))
    if result:
        return result.id
    return None

def get_or_insert_format(name):

    fname = 'get_or_insert_format'
    if len(name) == 0:
        logger.warning("%s: len(name)=0 --> Don't insert, don't return id!" % fname)
        return None

    _id = get_id_from_format(name)

    if _id:
        return _id
    else:
        with Session.begin() as session:
            session.add(D_Format(name=name))

    return get_id_from_format(name)

def get_id_from_format(name):
    '''
    '''
    with Session.begin() as session:
        row = session.query(D_Format).filter(name==name).first()
        session.expunge_all()

    if row:
        return row.id

    return None


'''
1633           SELECT count(distinct recsn) as nrecsta
1634           FROM c_channeldata cd
1635           WHERE cd.net = :net AND cd.sta = :sta
1636           AND cd.offdate > sysdate});
'''
# MTH: Example above (from mkv0.pl) would seem to undercount
#      cases where we have same seedchan with 2 different locations!
def get_c_channel_count(net, sta):
    '''
    Count number of distinct seedchans in c_channeldata
       that are still active (= have offdate > now)
    '''
    fname = 'get_channel_count'

    now = datetime.datetime.utcnow()

    with Session.begin() as session:
        nchan = session.query(C_Channeldata).filter(C_Channeldata.net==net).\
                                             filter(C_Channeldata.sta==sta).\
                                             filter(C_Channeldata.offdate > now).\
                                             distinct(C_Channeldata.seedchan, C_Channeldata.location).count()
                                             #distinct(C_Channeldata.seedchan).count()
    return nchan

def get_channel_count(net, sta):
    '''
    Count number of distinct seedchans in channel_data
       that are still active (= have offdate > now)
    '''
    fname = 'get_channel_count'

    now = datetime.datetime.utcnow()
    with Session.begin() as session:
        nchan = session.query(Channel_Data).filter(Channel_Data.net==net).\
                                            filter(Channel_Data.sta==sta).\
                                            filter(Channel_Data.offdate > now).count()

    logger.debug("%s: net=%s sta=%s nchan=%d" % (fname, net, sta, nchan))

    return nchan

def get_total_stations(net, active_only=False):
    """
    Return the total number of stations affiliated with this net code in the aqms db

    if active_only: only count stations currently active
    """
    fname = 'get_total_stations'

    now = datetime.datetime.utcnow()
    with Session.begin() as session:
        stations = session.query(Station_Data).filter(Station_Data.net==net).all()
        session.expunge_all()

    nstations = len(stations)

    if active_only:
        noff = 0
        if nstations:
            for station in stations:
                if station.offdate and station.offdate < now:
                    noff += 1
        nactive = nstations - noff
        return nactive
    else:
        return nstations

def get_total_channels(net, sta, active_only=False):
    """
    Return the total number of channels affiliated with this net.sta in the aqms db

    if active_only: only count channels currently active
    """
    fname = 'get_total_channels'

    now = datetime.datetime.utcnow()
    with Session.begin() as session:
        channels = session.query(Channel_Data).filter(Channel_Data.net==net).\
                                               filter(Channel_Data.sta==sta).all()
        session.expunge_all()

    # nchannels = # of channel epochs --> vs. number of distinct (cha, loc) 
    nchannels = len(channels)

    if active_only and nchannels:
        noff = 0
        active_channels = []
        for channel in channels:
            if channel.offdate and channel.offdate < now:
                noff += 1
            else:
                active_channels.append(channel)
        nactive = nchannels - noff

        channels = active_channels

    chalocs = [(c.seedchan, c.location) for c in channels]
    total_channels = len(set(chalocs))
    #print("active_only=%s nchannels=%d total_channels=%d" % (active_only, nchannels, total_channels))

    return total_channels


def get_chanlocs_in_channel_data(net, sta, search_date):
    """First try to get list of (seedchan, location) ordered
       by: HNZ, '  '
           HNE, '  '
           HNN, '  '
           HNZ, '10'
           HNE, '10'
           HNN, '10'

       Better solution is to use c_channeldata.chan_number to order (below)
    """
    fname = 'get_chanlocs'

    with Session.begin() as session:
        channels = session.query(Channel_Data).filter(Channel_Data.net==net).\
                                            filter(Channel_Data.sta==sta).\
                                            filter(Channel_Data.ondate<=search_date).\
                                            filter(Channel_Data.offdate>=search_date).all()
        session.expunge_all()

    locations = list(set([chan.location for chan in channels]))
    locations.sort()
    chanlocs = []
    for loc in locations:
        chans = [chan.seedchan for chan in channels if chan.location == loc]
        chans.sort(reverse=True)
        for chan in chans:
            chanlocs.append((chan, loc))

    #chanlocs = list(set([(chan.seedchan, chan.location) for chan in channels]))
    #chanlocs.sort(key=lambda x: (x[1], x[0]), reverse=True)

    return chanlocs

def get_chanlocs_from_c_channeldata(net, sta, search_date):
    """Return list of (cha, loc) ordered by chan_numbers at station, to match v0 order
    """
    fname = 'get_chanlocs'

    table = C_Channeldata
    with Session.begin() as session:
        channels = session.query(table).filter(table.net==net).\
                                        filter(table.sta==sta).\
                                        filter(table.ondate<=search_date).\
                                        filter(table.offdate>=search_date).\
                                        order_by(table.location, table.chan_number).all()
                                        #order_by(table.chan_number).all()
        session.expunge_all()

    return [(chan.seedchan, chan.location) for chan in channels]

def get_c_net_by_code(net_code):
    with Session.begin() as session:
        net = session.query(C_Net).filter_by(net=net_code).first()
        if net is None:
            net = session.query(C_Net).filter_by(netabbr="UNK").first()
        session.expunge_all()

    return net

def get_c_net_by_id(netid):
    with Session.begin() as session:
        net = session.query(C_Net).filter_by(netid=netid).one()
        session.expunge_all()
    return net


def get_unitsId(unitsdesc):
    '''
        Get unitsid from units description
    '''
    fname = 'get_units'

    #search_units = unitsdesc.replace("sec", "s").replace("s2", "s/s")
    search_units = unitsdesc.replace("sec2", "sec/sec")

    print("get_unitsId: unitsdesc=[%s] --> search_units=[%s]" %
          (unitsdesc, search_units))

    with Session.begin() as session:
        # The name/description in d_units is not unique!
        #units = session.query(C_Units).filter(C_Units.unitsdesc==unitsdesc).one()
        try:
            units = session.query(C_Units).filter(C_Units.unitsdesc==search_units).one()
        except NoResultFound as e:
            print("%s: unit_desc=%s not found in C_Units table!" %
                  (fname, unitsdesc))
            return None
        session.expunge_all()

    return units.unitsid

def get_loggerdata(lgrname):

    #print("get_loggerdata lgrname=[%s]" % lgrname)

    loggerdata = None
    with Session.begin() as session:
        loggerdata = session.query(C_Loggerdata).filter(C_Loggerdata.lgrname==lgrname).one_or_none()
        session.expunge_all()

    if 0:
        if loggerdata:
            print("get_loggerdata: lgrname=[%s] found!" % lgrname)
        else:
            print("get_loggerdata: lgrname=[%s] NOT found!" % lgrname)

    return loggerdata

def get_sensordata(snsname):
    #print("get_sensordata snsname=[%s]" % snsname)

    sensordata = None
    with Session.begin() as session:
        sensordata = session.query(C_Sensordata).filter(C_Sensordata.snsname==snsname).one_or_none()
        session.expunge_all()

    if 0:
        if sensordata:
            print("get_sensordata: snsname=[%s] found!" % snsname)
        else:
            print("get_sensordata: snsname=[%s] NOT found!" % snsname)

    return sensordata


def get_physId(seedchan):
    '''
        Get physType from SEED channel code
    '''
    fname = 'get_physId'
    code = seedchan[1]
    physdesc = None
    if code in ['L', 'N']:
        physdesc = 'Acceleration'
    elif code in ['H']:
        physdesc = 'Velocity'
    else:
        logger.error("Unknown c_channeldata.seedchan %s code:%s --> can't convert!" %
                     (seedchan, seedchan[1]))
        return None

    with Session.begin() as session:
        phys_typ = session.query(C_Phystyp).filter(C_Phystyp.physdesc==physdesc).one()
        session.expunge_all()

    return phys_typ.physid

def get_net_code(netid):
    '''
    '''
    fname = 'get_net_code'
    with Session.begin() as session:
        netcode = session.query(C_Net).filter(C_Net.netid==netid).one()
        session.expunge_all()
    return netcode

def get_station_data(net, sta, search_date, return_all=False):
    '''
    '''
    fname = 'get_station_data'

    with Session.begin() as session:
        station_data = session.query(Station_Data).\
                         filter(Station_Data.net==net).\
                         filter(Station_Data.sta==sta).\
                         filter(Station_Data.ondate<=search_date).\
                         filter(Station_Data.offdate>=search_date).all()
        session.expunge_all()

    logger.debug("%s: query for net=%s sta=%s timestamp=%s returned %d results" %
                (fname, net, sta, search_date, len(station_data)))

    if not return_all:
        if len(station_data) == 1:
            return station_data[0]
        else:
            logger.warning("%s: query for net=%s sta=%s timestamp=%s returned %d results != 1!!" % 
                    (fname, net, sta, search_date, len(station_data)))
            return None
    else:
        return list(station_data)


def get_all_station_and_channel_comments_for_sta(sta):
    """
    """

    db_comments = []
    with Session.begin() as session:
        for cls in [Station_Comment, Channel_Comment]:
            rows = session.query(cls).filter(cls.sta==sta).all()
            for row in rows:
                parent = session.query(D_Comment).filter(D_Comment.id==row.comment_id).one_or_none()
                row.parent = parent
                db_comments.append(row)

        session.expunge_all()

    return list(set(db_comments))

def get_all_station_and_channel_comments_for_net(net):
    """
    """

    db_comments = []
    with Session.begin() as session:
        for cls in [Station_Comment, Channel_Comment]:
            rows = session.query(cls).filter(cls.net==net).all()
            for row in rows:
                parent = session.query(D_Comment).filter(D_Comment.id==row.comment_id).one_or_none()
                row.parent = parent
                db_comments.append(row)

        session.expunge_all()

    return list(set(db_comments))


def get_aqms_comments(station_or_channel_data=None):
    """
    """

    cls_in = station_or_channel_data.__class__
    cls = None

    with Session.begin() as session:

        if cls_in == Station_Data:

            cls = Station_Comment
            station_data = station_or_channel_data
            #rows = session.query(cls, D_Comment)\

            rows = session.query(D_Comment).join(cls, cls.comment_id==D_Comment.id)\
                                             .filter(cls.net==station_data.net)\
                                             .filter(cls.sta==station_data.sta)\
                                             .filter(cls.ondate<=station_data.ondate)\
                                             .filter(cls.offdate>=station_data.offdate)\
                                             .order_by(cls.comment_level)\
                                             .all()
                                             #.join(D_Comment, D_Comment.id==cls.comment_id)\
                                             #.filter(D_Comment.id==cls.comment_id)\
                                             #.filter(D_Comment.unit==cls.comment_level)\
        elif cls_in == Channel_Data:

            cls = Channel_Comment
            channel_data = station_or_channel_data
            #location = channel_data.location.strip()
            rows = session.query(D_Comment).join(cls, cls.comment_id==D_Comment.id)\
                                             .filter(cls.net==channel_data.net)\
                                             .filter(cls.sta==channel_data.sta)\
                                             .filter(cls.seedchan==channel_data.seedchan)\
                                             .filter(cls.location==channel_data.location)\
                                             .filter(cls.ondate<=channel_data.ondate)\
                                             .filter(cls.offdate>=channel_data.offdate)\
                                             .order_by(cls.comment_level)\
                                             .all()

            #print("%s.%s.%s.[%s] on:%s off:%s returned %d rows" %
                  #(channel_data.net, channel_data.sta, channel_data.seedchan, channel_data.location,
                   #channel_data.ondate, channel_data.offdate, len(rows)))
        else:
            print("Error: Unrecognized class:%s" % cls)
            exit(2)

        session.expunge_all()

    db_comments = []

    if rows:
        db_comments = [row for row in rows]

    return db_comments


def get_station_or_channel_data_all(cls, net=None, sta=None, search_date=None, return_all=True):
    '''
    cls = Station_Data or Channel_Data
    '''
    fname = 'get_station_or_channel_data'

    with Session.begin() as session:
        query = session.query(cls)
        if net:
            if '*' in net or '?' in net:
                # psql: '%' matches everything after pattern, '_' matches 1 char
                query = query.filter(cls.net.like(net.replace('*', '%').replace('?', '_')))
            else:
                query = query.filter(cls.net==net)
        if sta:
            if '*' in sta or '?' in sta:
                query = query.filter(cls.sta.like(sta.replace('*', '%').replace('?', '_')))
            else:
                query = query.filter(cls.sta==sta)
        if search_date:
            query = query.filter(cls.ondate<=search_date).filter(cls.offdate>=search_date)

        rows = query.order_by(cls.net, cls.sta, cls.ondate).all()

        session.expunge_all()

    logger.debug("%s: query for net=%s sta=%s timestamp=%s returned %d results" %
                (fname, net, sta, search_date, len(rows)))

    return list(rows)


def get_row_from_table_with_name(table, name):
    with Session.begin() as session:
        row = session.query(table).filter_by(name=name).one_or_none()
        session.expunge_all()
    return row


def get_row_from_table_with_key(table, key):
    with Session.begin() as session:
        row = session.query(table).filter_by(key=key).one_or_none()
        session.expunge_all()
    return row

def get_rows_from_table_with_key(table, key):
    with Session.begin() as session:
        rows = session.query(table).filter_by(key=key).all()
        session.expunge_all()
    return rows


def get_foreign_key(table):
    fk = None
    for col in table.__table__.columns:
        if col.foreign_keys:
            fk = next(iter(col.foreign_keys))
            #print(fk.parent.name)
            break

    return fk

def get_child_cls(parent_key, tablename):

    # If we let channel_comment through it will find
    #   station_comment and mistakenly think it's a child
    if tablename in ['channel_comment', 'station_comment']:
        return None

    base = get_tables()
    for tblname in base:
        if tblname == tablename:
            continue
        key = get_foreign_key(base[tblname])
        if key and key.column==parent_key.column:
            return base[tblname]

    return None


def blast_epochs(tablename=None, net=None, sta=None, cha=None, loc=None, ondate=None, offdate=None, seq_no=None):
    """
    blast all epochs for this table matching optional nscl + ondate + offdate + seq_no
      and all associated child/parent table rows
    """

    fname = 'blast_epochs'

    #print("blast_epochs: tablename:%s net:%s sta:%s cha:%s loc:%s on:%s off:%s seq_no:%s" %
          #(tablename, net, sta, cha, loc, ondate, offdate, seq_no))

    base = get_tables()
    cls = base[tablename]
    table = cls
    table_cols = [x.name for x in table.__table__.c]

    parent_cls = None
    parent_key = None
    child_cls = None
    child_key = None
    epoch_key_name = None

    fkey = get_foreign_key(cls)
    if fkey:
        #parent_cls = fkey.column.table
        parent_cls = base[fkey.column.table.name]
        parent_key = getattr(parent_cls, fkey.column.name, None)
        #print(parent_cls.__tablename__)
        epoch_key_name  = fkey.parent.name
        child_cls = get_child_cls(fkey, tablename)
        child_key = getattr(child_cls, fkey.column.name, None)

        #print("%s: tablename=%s parent_cls=%s parent_key=%s epoch_key_name=%s child_key=%s" %
              #(fname, tablename, parent_cls, parent_key, epoch_key_name, child_key))


    with Session.begin() as session:
        start = time.time()

        if net and not sta:
            query = session.query(cls).filter_by(net=net)
        elif sta and not net:
            query = session.query(cls).filter_by(sta=sta)
        elif sta and net:
            query = session.query(cls).filter_by(net=net).filter_by(sta=sta)
        else:
            logger.error("%s: Either sta or net must be specified for query!" % fname)

        if 'seedchan' in table_cols and 'location' in table_cols:
            if cha:
                query = query.filter(cls.seedchan==cha)
            if loc:
                query = query.filter(cls.location==loc)

        if ondate:
            query = query.filter(cls.ondate==ondate)
        if offdate:
            query = query.filter(cls.offdate==offdate)
        if seq_no is not None:
            query = query.filter(cls.stage_seq==seq_no)

        #rows = query.all()
        rows = query.order_by(table.ondate).all()

        logger.debug("%s: query epochs for tablename:%s net:%s sta:%s cha:%s loc:%s ondate:%s offdate:%s seq_no:%s returned [%d] rows" %
                     (fname, tablename, net, sta, cha, loc, ondate, offdate, seq_no, len(rows)))
        #print("%s: query epochs for tablename:%s returned %d rows" % (fname, tablename, len(rows)))

        if epoch_key_name:
            ids = [getattr(row, epoch_key_name) for row in rows]

        ndelete = query.delete()
        #print("%s: deleted [%d] rows from table:%s" % (fname, ndelete, tablename))

        #for row in rows:
            #session.delete(row)

        if child_key:
            start = time.time()
            #child_rows = session.query(child_cls).filter(child_key.in_(ids)).all()
            child_rows = session.query(child_cls).filter(child_key.in_(ids)).delete()
            #print("Got [%d] dc_data rows in time:%s" % (len(rows), time.time()-start))

        if parent_key:
            start = time.time()
            #parent_rows = session.query(parent_cls).filter(parent_key.in_(ids)).all()
            parent_rows = session.query(parent_cls).filter(parent_key.in_(ids)).delete()
            #print("Got [%d] dc rows in time:%s" % (len(rows), time.time()-start))

        return

def get_epochs(tablename=None, net=None, sta=None, cha=None, loc=None, ondate=None, offdate=None, seq_no=None):
    """
    Get all epochs for tablename for net + sta + cha + loc + on + off
    For each epoch row, attach any corresponding parent and children rows
    """

    fname = 'get_epochs'
    #print("%s: tablename:%s net:%s sta:%s" % (fname, tablename, net, sta))

    base = get_tables()
    cls = base[tablename]
    table = cls
    table_cols = [x.name for x in table.__table__.c]

    parent_cls = None
    parent_key = None
    child_cls = None
    child_key = None
    epoch_key_name = None

    t0 = time.time()

    fkey = get_foreign_key(cls)
    if fkey:
        #parent_cls = fkey.column.table
        parent_cls = base[fkey.column.table.name]
        parent_key = getattr(parent_cls, fkey.column.name, None) # e.g., PZ.key
        #print(parent_cls.__tablename__)
        epoch_key_name  = fkey.parent.name
        child_cls = get_child_cls(fkey, tablename)
        child_key = getattr(child_cls, fkey.column.name, None)
        row_key = getattr(child_cls, "row_key", None)

        logger.debug("get_all_epochs: tablename=%s parent_cls=%s parent_key=%s epoch_key_name=%s child_key=%s" %
              (tablename, parent_cls, parent_key, epoch_key_name, child_key))

    with Session.begin() as session:
        start = time.time()
        if net and not sta:
            query = session.query(cls).filter_by(net=net)
        elif sta and not net:
            query = session.query(cls).filter_by(sta=sta)
        elif sta and net:
            query = session.query(cls).filter_by(net=net).filter_by(sta=sta)
        else:
            logger.error("%s: Either sta or net must be specified for query!" % fname)
            return None

        # station_data doesn't have seedchan,location cols so exclude it from this:
        #if tablename != 'station_data':
        if 'seedchan' in table_cols and 'location' in table_cols:
            if cha:
                query = query.filter(cls.seedchan==cha)
            if loc:
                query = query.filter(cls.location==loc)

        if ondate:
            query = query.filter(cls.ondate==ondate)
        if offdate:
            query = query.filter(cls.offdate==offdate)
        if seq_no is not None:
            query = query.filter(cls.stage_seq==seq_no)

        #logger.info("%s: query epochs for tablename:%s" % (fname, tablename))
        query = query.order_by(table.ondate)
        rows = query.all()
        logger.info("%s: query epochs for tablename:%s returned %d rows" % (fname, tablename, len(rows)))
        #print("%s: query epochs for tablename:%s returned %d rows in time:%s" %
              #(fname, tablename, len(rows), time.time()-start))

        # Convenience so we can treat station_data rows the same as channel_data, simple_response, etc rows:
        if 'seedchan' not in table_cols:
            for row in rows:
                row.seedchan = 'N/A'
                row.location = 'N/A'

        if parent_key or child_key:
            start=time.time()

            #epoch_key = fkey.parent # e.g., Coefficients.dc_key
            #query = session.query(cls).filter(table.net==net).\
                                       #filter(table.sta==sta).order_by(epoch_key)
            #rows = query.all()

            _ids = [getattr(row, epoch_key_name) for row in rows]

            parent = []
            if parent_key:
                parents = session.query(parent_cls).filter(parent_key.in_(_ids)).order_by(parent_key).all()
                pp = {}
                # PZ.key ==> "pz", "key"
                pcls, pkey = str(parent_key).lower().split(".")
                # MTH:
                pcls = pcls.lower()

                for row in parents:
                    key = getattr(row, pkey, None)
                    pp[key] = row

            children = []
            if child_key:
                if row_key:
                    children = session.query(child_cls).filter(child_key.in_(_ids)).order_by(child_key, row_key).all()
                else:
                    children = session.query(child_cls).filter(child_key.in_(_ids)).order_by(child_key).all()
                dd = {_id:[] for _id in _ids}
                # PZ_Data.key ==> "pz_data", "key"
                ccls, ckey = str(child_key).split(".")
                # MTH:
                ccls = ccls.lower()
                for row in children:
                    key = getattr(row, ckey, None)
                    dd[key].append(row)

            for row in rows:
                key = getattr(row, epoch_key_name, None)
                if parent_key:
                    try:
                        row.parent   = pp[key]
                    except KeyError as e:
                        logger.warning("%s: table:%s net:%s sta:%s cha:%s loc:%s on:%s off:%s seq:%s epoch_key_name:%s ==> pp[key=%s] NOT FOUND !!" %
                                      (fname, tablename, net, sta, cha, loc, ondate, offdate, seq_no, epoch_key_name, key))
                        raise
                    # Hack so that poles_zeros.pz can be found, etc
                    setattr(row, pcls, row.parent) # row.pz --> row.parent

                    logger.debug("table:%s set row.%s = parent:%s" % (tablename, pcls, row.parent))

                if child_key:
                    row.children = dd[key]
                    # Hack so that poles_zeros.pz_data can be found, etc
                    setattr(row, ccls, row.children) # row.pz_data --> row.children
                    logger.debug("table:%s set row.%s = children:%s" % (tablename, ccls, row.children))

            logger.info("%s: table:%s nrows:%d nparents:%d nchildren:%s" %
                       (fname, tablename, len(rows), len(parents), len(children)))
            #print("%s: table:%s nrows:%d nparents:%d nchildren:%s" %
                       #(fname, tablename, len(rows), len(parents), len(children)))

        session.expunge_all()

    #print("get_epochs: table:%s nrows:%d total_time:%s" % (tablename, len(rows), time.time()-t0))

    return rows


def db_insert_update_delete(insert_rows, update_rows, delete_rows):

    fname = "db_insert_update_delete"

    keys = {'poles_zeros':'pz_key', 'coefficients':'dc_key', 'decimation':'dm_key',
            'polynomial':'pn_key',
            'channel_comment':'comment_id', 'station_comment':'comment_id'}

    t0 = time.time()
    with Session.begin() as session:

        logger.info("delete %d rows" % (len(delete_rows)))
        for row in delete_rows:
            #logger.debug("Delete epoch row=[%s]" % (row))
            logger.info("Delete epoch row=[%s]" % (row))
            parent = getattr(row, 'parent', None)
            if parent:
                logger.debug("  Delete parent=[%s]" % (parent))
                logger.info("  Delete parent=[%s]" % (parent))
                session.delete(parent)
                """
                # MTH: I've seen cases where channel_comment exists but d_comment parent was already deleted
                try:
                    session.delete(parent)
                except InvalidRequestError as e:
                    logger.warning("%s: Unable to delete parent:%s\nIt may be that this item was already manually deleted from db" %
                                   (fname, parent))
                    logger.warning("%s" % e)
                    print("%s: Unable to delete parent:%s\nIt may be that this item was already manually deleted from db" %
                                   (fname, parent))
                    print("%s" % e)
                """

            children = getattr(row, 'children', None)
            if children:
                for child in children:
                    logger.debug("  Delete child_row=[%s]" % (child))
                    logger.info("  Delete child_row=[%s]" % (child))
                    session.delete(child)

            session.delete(row)
            session.flush()
            key = None  # done with it

        logger.info("update %d rows" % (len(update_rows)))

        for row in update_rows:
            if row in delete_rows:
                print("**** update row already in delete row!")
                print(row)
            # update_rows are already db rows with parent key
            logger.debug("Update epoch row=[%s]" % (row))
            session.add(row)
            parent = getattr(row, 'parent', None)
            if parent:
                logger.debug("Update parent=[%s]" % (parent))
                session.add(parent)
            children = getattr(row, 'children', None)
            if children:
                for child in children:
                    logger.debug("Update child_row=[%s]" % (child))
                    session.add(child)

        logger.info("insert %d rows" % (len(insert_rows)))

        all_children = []

        if insert_rows:
            parent = getattr(insert_rows[0], 'parent', None)
            if parent:
                parent_ids = getSequence(parent.__tablename__, len(insert_rows))
                logger.debug("Got [%d] parent_ids for tablename:%s range:%d-%d" %
                            (len(parent_ids), parent.__tablename__, parent_ids[0], parent_ids[-1]))

        start = time.time()
        for i, row in enumerate(insert_rows):
            start = time.time()
            #print("db_insert: sta:%s cha:%s location:[%s]" %
                #(row.sta, row.seedchan, row.location))
            # insert_rows + any parent/child do not yet have parent key
            #logger.debug("Insert epoch row=[%s]" % (row))
            logger.info("Insert epoch row=[%s]" % (row))
            parent = getattr(row, 'parent', None)
            #parent_key = None
            if parent:
                parent_key = parent_ids.pop(0)
                # Insert the parent and get the parent key
                session.add(parent)
                #session.flush()
                logger.debug("  Insert parent=[%s]" % (parent))
                #parent.name parent.key
                #parent.description,.class,.unit +  parent.id   // for d_comment
                key_name = 'key'
                if parent.__class__ == D_Comment:
                    key_name = 'id'

                setattr(parent, key_name, parent_key)
                #parent_key = getattr(parent, key_name, None)
                #logger.debug("Got db_parent key=%s" % parent_key)
                #print("Got db_parent key=%s" % parent_key)

                logger.debug("%s: Set epoch_row.%s=%s" % (fname, keys[row.__tablename__], parent_key))
                setattr(row, keys[row.__tablename__], parent_key)

            children = getattr(row, 'children', None)
            if children:
                #print("insert %d children" % len(children))
                for child in children:
                    logger.debug("Insert child_row=[%s]" % (child))
                    child.key = parent_key
                    #session.add(child)
                #session.bulk_save_objects(children)
                all_children.extend(children)

            #logger.debug("Insert epoch_row=[%s]" % (row))
            session.add(row)
            #print("[%2d] time to insert row + children:%s" % (i, time.time()-start))

        #print("db_insert: loop over [%d] insert_rows time:%s" % (len(insert_rows), time.time()-start))
        session.bulk_save_objects(all_children)
    #print("Exit session.begin() time:%s" % (time.time()-t0))

    """
    child_mappings = []
    for child in all_children:
        dd = {"key":child.key,
              "row_key":child.row_key,
              "type":child.dctype,
              "coefficient":child.coefficient,
              "error":child.error,
              }
        child_mappings.append(dd)

    start = time.time()
    with Session.begin() as session:
        session.bulk_save_objects(all_children)
        #session.bulk_insert_mappings(DC_Data, child_mappings)
        #session.add_all(all_children)
    print("db_insert: time to bulk_save [%d] children:%s" % (len(all_children), time.time()-start))
    """

    logger.debug("Return from db_insert")
    return 0


def get_db_epochs(table, net, sta, cha, loc, search_date, seq_no=None, return_all=False, ignore_dates=False):
    '''
    '''
    fname = 'get_db_epochs'

    logger.debug("%s table:%s net:%s sta:%s cha:%s loc:[%s] search_date:%s return_all=%s ignore_dates=%s" %
                (fname, table.__tablename__, net, sta, cha, loc, search_date, return_all, ignore_dates))

    table_cols = [x.name for x in table.__table__.c]

    parent_cls = None
    parent_key = None
    child_cls = None
    child_key = None
    epoch_key_name = None

    tablename = table.__tablename__

    base = get_tables()
    fkey = get_foreign_key(table)

    if fkey:
        parent_cls = base[fkey.column.table.name]
        parent_key = getattr(parent_cls, fkey.column.name, None)
        #print(parent_cls.__tablename__)
        epoch_key_name  = fkey.parent.name
        child_cls = get_child_cls(fkey, tablename)
        child_key = getattr(child_cls, fkey.column.name, None)

    #print("** %s table=%s fkey=%s parent_cls=%s parent_key=%s epoch_key_name=%s child_cls=%s child_key=%s" %
          #(fname, table.__tablename__, fkey, parent_cls, parent_key, epoch_key_name, child_cls, child_key))


    with Session.begin() as session:

        query = session.query(table).filter(table.net==net).filter(table.sta==sta)

        if not ignore_dates:
            query = query.filter(table.ondate<=search_date).filter(table.offdate>=search_date)

        if seq_no:
            # ***********     MTH: should this be stage_seq == seq_no ?!
            #query = query.filter(table.stage_seq>=seq_no)
            #print("Add filter stage_seq==%s" % seq_no)
            query = query.filter(table.stage_seq==seq_no)

        if 'seedchan' in table_cols and 'location' in table_cols:
            #logger.debug("get_db_epochs: search table:%s on net:%s sta:%s seedchan:%s loc:%s ondate<=%s offdate>=%s seq_no:%s" %
                         #(table.__tablename__, net, sta, cha, loc, search_date, search_date, seq_no))
            query = query.filter(table.seedchan==cha).filter(table.location==loc)

        rows = query.order_by(table.ondate).all()

        for row in rows:
            # Convenience so we can treat station_data rows the same as channel_data, simple_response, etc rows:
            if 'seedchan' not in table_cols:
                row.seedchan = 'N/A'
                row.location = 'N/A'

            if parent_key:
                for row in rows:
                    key = getattr(row, epoch_key_name, None)
                    row.parent = session.query(parent_cls).filter(parent_key==key).one_or_none()
                    logger.debug("%s: row.parent=%s" % (fname, row.parent))
                    #print("%s: row.parent=%s" % (fname, row.parent))

                    #logger.debug("%s: search child_cls=%s on key=%s" % (fname, child_cls, key))

                    if child_key:
                        row.children = session.query(child_cls).filter(child_key==key).all()

                        logger.debug("%s: Got [%d] children using child_key=%s == parent_key=%s" %
                                    (fname, len(row.children), child_key, parent_key))
                        for r in row.children:
                            logger.debug("%s: row.child=%s" % (fname, r))


        session.expunge_all()

    logger.debug("%s: len(rows)=%d" % (fname, len(rows)))

    if not return_all:
        if len(rows) == 1:
            return rows[0]
        else:
            logger.warning("%s: table:%s query for net=%s sta=%s cha=%s loc=%s timestamp=%s returned %d results != 1!!" % 
                    (fname, table.__tablename__, net, sta, cha, loc, search_date, len(rows)))
            return None, None
    else:
        return list(rows)


def get_channel_data_and_simpleresponse(net, sta, cha, loc, search_date, return_all=False):
    '''
    '''
    fname = 'get_channel_data_and_simpleresponse'

    '''
    FROM channel_data cd, simple_response sr
              WHERE cd.net = :net AND cd.sta = :sta AND cd.seedchan = :chan
              AND cd.location = :loc
              AND TO_DATE(:dtime) BETWEEN cd.ondate AND cd.offdate
              AND cd.net = sr.net AND cd.sta = sr.sta AND cd.seedchan = sr.seedchan
              AND cd.location = sr.location
              AND TO_DATE(:dtime) BETWEEN sr.ondate AND sr.offdate
    '''
    logger.info("%s net=[%s] sta=[%s] cha=[%s] loc=[%s] len=%d" %
                (fname, net, sta, cha, loc, len(loc)))

    with Session.begin() as session:
        rows = session.query(Channel_Data, SimpleResponse).\
                         filter(Channel_Data.net==net).\
                         filter(Channel_Data.sta==sta).\
                         filter(Channel_Data.seedchan==cha).\
                         filter(Channel_Data.location==loc).\
                         filter(Channel_Data.ondate<=search_date).\
                         filter(Channel_Data.offdate>=search_date).\
                         filter(SimpleResponse.ondate<=search_date).\
                         filter(SimpleResponse.offdate>=search_date).\
                         filter(Channel_Data.net==SimpleResponse.net).\
                         filter(Channel_Data.sta==SimpleResponse.sta).\
                         filter(Channel_Data.seedchan==SimpleResponse.seedchan).\
                         filter(Channel_Data.location==SimpleResponse.location).all()
        session.expunge_all()

    logger.debug("%s: query for net=%s sta=%s timestamp=%s returned %d results" % 
                (fname, net, sta, search_date, len(rows)))

    if not return_all:
        if len(rows) == 1:
            return rows[0]
        else:
            logger.warning("%s: query for net=%s sta=%s cha=%s loc=%s timestamp=%s returned %d results != 1!!" % 
                    (fname, net, sta, cha, loc, search_date, len(rows)))
            return None, None
    else:
        return list(rows)


def get_equipment(net, sta, cha, loc, search_date, _type=None, return_all=False, ignore_dates=False):
    '''
    '''
    fname = 'get_equipment'

    with Session.begin() as session:
        query = session.query(Equipment).\
                            filter(Equipment.net==net).\
                            filter(Equipment.sta==sta).\
                            filter(Equipment.seedchan==cha).\
                            filter(Equipment.location==loc)

        if not ignore_dates:
            query = query.filter(Equipment.ondate<=search_date).\
                          filter(Equipment.offdate>=search_date)
        if _type:
            query = query.filter(Equipment._type==_type)

        rows = query.order_by(Equipment.ondate).all()
        session.expunge_all()

    logger.debug("%s: query Equipment for net=%s sta=%s cha=[%s] loc=[%s] search_date=%s returned %d results" % 
                (fname, net, sta, cha, loc, search_date, len(rows)))

    if not return_all:
        if len(rows) == 1:
            return rows[0]
        else:
            logger.warning("%s: query Equipment for net=%s sta=%s cha=%s loc=%s timestamp=%s returned %d results != 1!!" % 
                    (fname, net, sta, cha, loc, search_date, len(rows)))
            return None

    return list(rows)


def get_c_channeldata(net, sta, cha, loc, search_date, return_all=False, ignore_dates=False):
    '''
    '''
    fname = 'get_c_channeldata'

    '''
    FROM channel_data cd, simple_response sr
              WHERE cd.net = :net AND cd.sta = :sta AND cd.seedchan = :chan
              AND cd.location = :loc
              AND TO_DATE(:dtime) BETWEEN cd.ondate AND cd.offdate
              AND cd.net = sr.net AND cd.sta = sr.sta AND cd.seedchan = sr.seedchan
              AND cd.location = sr.location
              AND TO_DATE(:dtime) BETWEEN sr.ondate AND sr.offdate

  		      SELECT ccd.chan_number, ccd.description, ccd.lgrname,
      				      ccd.snsname, ccd.siteid, ccd.orntid, ccd.timeid, ccd.ref_azi,
      				      ccd.datumid, ccd.maxg, ccd.snsid, ccd.vperg,
      				      ccd.lgrbits, ccd.lgrfsv, ccd.lgrid, ccd.recsn
      		      FROM c_channeldata ccd
      		      WHERE ccd.net = :net AND ccd.sta = :sta AND ccd.seedchan = :chan
      		      AND ccd.location = :loc
      		      AND TO_DATE(:dtime) BETWEEN ccd.ondate AND ccd.offdate
    '''

    with Session.begin() as session:
        query = session.query(C_Channeldata).\
                            filter(C_Channeldata.net==net).\
                            filter(C_Channeldata.sta==sta).\
                            filter(C_Channeldata.seedchan==cha).\
                            filter(C_Channeldata.location==loc)

        if not ignore_dates:
            query = query.filter(C_Channeldata.ondate<=search_date).\
                          filter(C_Channeldata.offdate>=search_date)

        rows = query.order_by(C_Channeldata.ondate).all()
        session.expunge_all()

    logger.debug("%s: query for net=%s sta=%s cha=[%s] loc=[%s] search_date=%s returned %d results" % 
                (fname, net, sta, cha, loc, search_date, len(rows)))

    if not return_all:
        if len(rows) == 1:
            return rows[0]
        else:
            logger.warning("%s: query for net=%s sta=%s cha=%s loc=%s timestamp=%s returned %d results != 1!!" % 
                    (fname, net, sta, cha, loc, search_date, len(rows)))
            return None

    return list(rows)

def get_simpleresponse(net, sta, cha, loc, search_date, return_all=False, ignore_dates=False):
    '''
    '''
    fname = 'get_simpleresponse'

    with Session.begin() as session:
        query = session.query(SimpleResponse).\
                            filter(SimpleResponse.net==net).\
                            filter(SimpleResponse.sta==sta).\
                            filter(SimpleResponse.seedchan==cha).\
                            filter(SimpleResponse.location==loc)

    if not ignore_dates:
        query = query.filter(SimpleResponse.ondate<=search_date).\
                      filter(SimpleResponse.offdate>=search_date)

    rows = query.order_by(SimpleResponse.ondate).all()
    session.expunge_all()


    if not return_all:
        if len(rows) == 1:
            return rows[0]
        else:
            logger.warning("%s: query for net=%s sta=%s cha=%s loc=%s timestamp=%s returned %d results != 1!!" % 
                    (fname, net, sta, cha, loc, search_date, len(rows)))
            return None, None

    return list(rows)


def _remove_poles_zeros(session, network_code, station_code):
    """
        Removes any rows in poles_zeros for this station. Will also remove
        the PZ and PZ_Data entries if there are no other poles_zeros rows that
        refer to them, to limit the number of obsolete PZ,PZ_Data rows in the
        database.
    """

    pz_keys = set()
    status = -1
    logger.debug("In _remove_poles_zeros, for station {}.{}".format(network_code,station_code))

    try:
        with Session.begin() as session:
            all_in_list = session.query(Poles_Zeros.pz_key).filter_by(net=network_code,sta=station_code).all()
            for key in all_in_list:
                pz_keys.add(key)
            logger.debug("Retrieved {} unique pole zero keys for {}.{}\n".format(len(pz_keys),network_code,station_code))
            status = session.query(Poles_Zeros).filter_by(net=network_code,sta=station_code).delete()
            logger.debug("Deleting poles_zeros entries: {}".format(status))
            session.expunge_all()

    except Exception as e:
         logger.error(e)

    with Session.begin() as session:
        for key in pz_keys:
        # do other poles_zeros entries using this key? yes, keep, no, remove.
            rows_returned = session.query(Poles_Zeros.pz_key).\
                            filter(pz_key==key, net!=network_code, sta!=station_code).all()
            logger.debug("PZ KEY: {}. Number of other poles_zeros that use this set of poles and zeros: {}".format(key, len(rows_returned)))
            if len(rows_returned) > 0:
                logger.debug("PZ and PZ_Data in use, not removing")
            else:
            # remove as well.
                status = status + session.query(PZ).filter_by(key=key).delete()
                status1 = session.query(PZ_Data).filter_by(key=key).delete()
                logger.debug("Removed {} PZ and PZ_data entries".format(status))
        session.expunge_all()

    return status


def load_c_channeldata_csv(csvfile):
    import pandas as pd

    #csvfile = "/Users/mth/mth/python_pkgs/mkv0/from_Stephane/c_channeldata.csv"
    '''
            A=net    B=sta      C=seedchan  D=location
            E=ondate F=offdate  G=netid     H=lgrname
            I=lgrid  J=lgrbits  K=lgrfsv    L=snsname
            M=snsid  N=maxg     O=vperg     P=siteid
            Q=orntid R=timeid   S=datumid   T=stavs30                 //T,U,V are empty
            U=stageoid V=chan_number   W=description   X=lddate       //W is missing
            Y=ref_azi                                                 //Y is empty
    '''

    columns = ['net', 'sta', 'seedchan', 'location',
               'ondate', 'offdate', 'netid',
               'lgrname', 'lgrid', 'lgrbits', 'lgrfsv',
               'snsname', 'snsid', 'maxg', 'vperg',
               'siteid', 'orntid', 'timeid', 'datumid',
               'stavs30', 'stageoid', 'chan_number', 
               'description', 'lddate', 'ref_azi',
               ]


    float_converter = lambda x: (float(x) if x.strip() else sqlalchemy.null())
    int_converter = lambda x: (int(x) if x.strip() else sqlalchemy.null())
    str_converter = lambda x: (str(x) if x.strip() else sqlalchemy.null())

    # MTH: We can't use the pd.datetime converter directly because the POS can only
    #      calculate dates out to ~458 years due to some nanosecond/storage limitation
    #      and the metadata offdates are typically set to year 3000.
    #date_converter = lambda x: pd.to_datetime(x, unit='s', format='%Y/%m/%d %H:%M:%S')

    #date_converter = lambda x: datetime.datetime.strptime(x, '%Y/%m/%d  %H:%M:%S').replace(tzinfo=timezone.utc)
    # MTH: setting timezone caused the datetimes to be converted to local timezone as they were injected
    #      into the db.  Solution was to remove tz awareness and use "naive" unaware datetime objects:
    date_converter = lambda x: datetime.datetime.strptime(x, '%Y/%m/%d  %H:%M:%S')

    converters = {'net': str.strip,
                  'sta': str.strip,
                  'seedchan': str.strip,
                  'location': str.strip,
                  'lgrname': str.strip,
                  'snsname': str.strip,
                  'lgrfsv': float_converter,
                  'maxg': float_converter,
                  'vperg': float_converter,
                  'ref_azi': float_converter,
                  'siteid': int_converter,
                  'chan_number': int_converter,
                  'description': str_converter,
                  'stavs30': float_converter,
                  'ref_azi': float_converter,
                  'stageoid': int_converter,
                  'ondate': date_converter,
                  'offdate': date_converter,
                  #'ondate': pd.to_datetime,
                  #'offdate': pd.to_datetime,
                  }

    df = pd.read_csv(csvfile, names=columns, converters=converters, delimiter='|')
    #df = pandas.read_csv(csvfile, names=columns, converters=converters,
                         #dtype={'ondate': datetime.datetime, 'offdate': datetime.datetime})
    #print(df)
    #print(df['net'], df['sta'], df['seedchan'], df['location'])

    with Session.begin() as session:
        for index, row in df.iterrows():
            # MTH: Be careful with this -- default no location should = "  " (2-spaces) in db
            location = row['location'] if row['location'] else "--"
            sncl = "%s.%s.%s.%s" % (row['sta'], row['net'], row['seedchan'], location)
            #print("%s netid:%d on:%s of:%s lgrid:%d lgrfsv:%.1f maxg:%s vperg:%.1f snsid:%d siteid:%d" % 
            print("%s netid:%d on:%s of:%s lgrid:%d lgrfsv:%s maxg:%s vperg:%s snsid:%d siteid:%d" % 
                (sncl, row['netid'], row['ondate'], row['offdate'], row['lgrid'], row['lgrfsv'], row['maxg'], row['vperg'],
                row['snsid'], row['siteid']))
            c_chandata = C_Channeldata(**row)
            #print(c_chandata)
            session.add(c_chandata)

    # MTH: pretty sure that using the context mgr above will 
    #      either commit + close the session 
    #      or will rollback and raise

    #try:
        #logger.info("commit the session")
        #session.commit()
    #except:
        #session.rollback()
        #raise
    #finally:
        #session.close()
    return


def get_channel_data_horizontals(net, sta):
    '''
    '''
    fname = 'get_channel_data_horizontals'

    statement = text("select net, sta, seedchan, location, min(ondate), max(offdate) "
                "from channel_data where seedchan in ('SNN', 'SNE', 'BNN', 'BNE', "
                "'ENN', 'ENE', 'HNN', 'HNE', 'EHN', 'EHE', 'BHN', 'BHE', 'HHN', "
                "'HHE', 'EH1','EH2','BH1','BH2','HH1','HH2') and  "
                "samprate in (20, 40, 50, 80, 100, 200) and net=:net and sta=:sta "
                "group by net, sta, seedchan,location")

    statement = statement.columns(Channel_Data.net,Channel_Data.sta,Channel_Data.seedchan,
                                  Channel_Data.location,Channel_Data.ondate,Channel_Data.offdate)

    # MTH: for some reason the latest sqlalchemy is throwing an error when I try to use rows
    #      after return from this function:
    #sqlalchemy.orm.exc.DetachedInstanceError: Instance <Channel_Data at 0x126715060> is not bound to a Session;
    #attribute refresh operation cannot proceed (Background on this error at: https://sqlalche.me/e/14/bhk3)
    # Try converting to a list to persist ??

    return_list = []
    with Session.begin() as session:
        #rows = session.query(Channel_Data).from_statement(statement).params(net=net, sta=sta).all()
        rows = session.query(Channel_Data).\
                         filter(Channel_Data.net==net).\
                         filter(Channel_Data.sta==sta).\
                         filter(Channel_Data.seedchan.in_(['ENN', 'ENE', 'HNN', 'HNE',
                                                           'EHN', 'EHE', 'BHN', 'BHE',
                                                           'HHN', 'HHE', 'EH1', 'EH2',
                                                           'BH1', 'BH2', 'HH1', 'HH2'])).\
                         order_by(Channel_Data.net, Channel_Data.sta, Channel_Data.seedchan, Channel_Data.location,
                                  Channel_Data.ondate, Channel_Data.offdate).\
                         all()
        """
                         group_by(Channel_Data.net, Channel_Data.sta, Channel_Data.seedchan, Channel_Data.location,
                                  Channel_Data.ondate, Channel_Data.offdate).\
                         filter(Channel_Data.seedchan in ['ENN', 'ENE', 'HNN', 'HNE',
                                                          'EHN', 'EHE', 'BHN', 'BHE',
                                                          'HHN', 'HHE', 'EH1', 'EH2',
                                                          'BH1', 'BH2', 'HH1', 'HH2']).\
                         all()
        """
        session.expunge_all()

        """
        print("MTH: query returned %d rows" % len(rows))
        exit()

        for row in rows:
            return_list.append(row)
            print(row)
        """

    return rows
    #return return_list

def get_appchannels(appname, search_date):
    '''
    '''
    fname = 'get_appchannels'

    with Session.begin() as session:
        rows = session.query(Appchannels, Applications).\
                         filter(Applications.name==appname).\
                         filter(Applications.progid==Appchannels.progid).\
                         filter(Appchannels.ondate<=search_date).\
                         filter(Appchannels.offdate>=search_date).all()
        session.expunge_all()

    appchans = []
    for row in rows:
        appchans.append(row[0])

    if len(appchans) == 0:
        logger.warning("get_appchannels: appname=%s search_date=%s returned 0 channels!" %
                       (appname, search_date))

    return appchans

'''
    # query for qualifying stations
    # g = 980.665cm/sec2; minAcc is % g
    my $cmThresh = $param{minAcc} * 9.81;
    my $mmThresh = 10 * $cmThresh;
    # round duration to integer seconds, per CGS
    my $reqDuration = int(0.5 + $evt->{MAG} * $param{secsPerMag});

    my $sth = $dbh->prepare(qq{
        SELECT DISTINCT a.net, a.sta, wheres.separation_km(sd.lat, sd.lon, o.lat, o.lon) DIST

        FROM assocevampset ev, ampset s, amp a, event e, origin o, applications p, appchannels ac, station_data sd

        WHERE a.ampid = s.ampid AND s.ampsetid = ev.ampsetid
          AND ev.isvalid = 1 AND ev.evid = :evid AND e.evid = ev.evid
          AND e.prefor = o.orid AND p.name = :progname
          AND p.progid = ac.progid AND ac.net = a.net AND ac.sta = a.sta
          AND ac.seedchan = a.seedchan AND ac.location = a.location
          AND to_date(truetime.getstring(o.datetime))
        BETWEEN ac.ondate AND ac.offdate
              AND a.net = sd.net AND a.sta = sd.sta
        AND to_date(truetime.getstring(o.datetime))
              BETWEEN sd.ondate AND sd.offdate
        AND a.amptype LIKE 'PGA'
        AND ((a.units = 'cmss' AND a.amplitude >= :cmThresh)
       OR (a.units = 'mmss' AND a.amplitude >= :mmThresh))
        order by a.net, a.sta
       });
'''
#def get_large_PGA_stations(appname, search_date):
def get_PGA_amps(evid):
    '''
    '''
    fname = 'get_PGA_amps'

    with Session.begin() as session:
        rows = session.query(Amp).join(Ampset, Ampset.ampid==Amp.ampid)\
                                 .join(Assocevampset, Assocevampset.ampsetid==Ampset.ampsetid)\
                                 .filter(Assocevampset.isvalid==1)\
                                 .filter(Assocevampset.evid==evid)\
                                 .filter(Amp.amptype=='PGA').all()
        session.expunge_all()

    return rows


def get_spectralamps(net, sta, cha, loc, starttime, endtime, spectype=None):
    '''
    '''
    fname = 'get_spectralamps'

    with Session.begin() as session:
        query = session.query(SpectralAmp).filter(SpectralAmp.net==net)\
                                          .filter(SpectralAmp.sta==sta)\
                                          .filter(SpectralAmp.seedchan==cha)\
                                          .filter(SpectralAmp.starttime>=starttime)\
                                          .filter(SpectralAmp.starttime<=endtime)
        if spectype:
            query = query.filter(SpectralAmp.spectype==spectype)

        rows = query.all()

        session.expunge_all()

    return rows

