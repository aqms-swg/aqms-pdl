# -*- coding: utf-8 -*-
"""
Functions to read quakeml from file into obspy objects

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import argparse
import io
import logging
import os
import sys
from obspy.core.event import read_events
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg, optional_arg
from aqms_pdl.libs.libs_log import configure_logger, read_config, update_args_with_config

logger = logging.getLogger()

def main():

    r = required_arg(arg=['quakeml'], type=str, description='Path to quakeml.xml file')
    args, parser = parse_cmd_line(required_args=[r], optional_args=[])
    #config, log_msgs = read_config(requireConfigFile=True, debug=False)
    #update_args_with_config(args, config, fname)
    configure_logger(**vars(args))
    logger = logging.getLogger()
    logger.info(sys.argv)

    quakemlfile = args.quakeml

    print("Scan quakemlfile:%s" % quakemlfile)
    summary = scan_quakeml(quakemlfile)
    print(summary)
    '''
    if summary[-1] == '\n':
        print("This has trailing return")
    else:
        print("This has NO trailing return")
    '''

def scan_quakeml(quakemlfile=None):

    event = None
    mag = None
    origin = None
    preferred_origin = False
    # Read quakeml into string and replace anssevent tags:
    try:
        with open(quakemlfile, 'r') as file:
            xml = file.read().replace('anssevent:internalEvent', 'event')
    except FileNotFoundError as e:
        logger.error(f"scan_quakeml: {e.__class__.__name__}: {e}")
        return None
    try:
        #cat = read_events(quakemlfile, format="quakeml")
        cat = read_events(io.BytesIO(bytes(xml, encoding='utf-8')), format="quakeml")
        event = cat[0]
    except:
        raise

    names = {"Catalog":"Cat",
             "Event":"Evt",
             "Origin":"Org",
             "Magnitude":"Mag",
             "FocalMechanism":"Mec",
            }

    string = "quakeml summary: [nPicks:%d nAmps:%d nOrigins:%d nMags:%d nFocalMechs:%s]\n" % \
            (len(event.picks), len(event.amplitudes), len(event.origins), len(event.magnitudes), len(event.focal_mechanisms))
    string += ("%-31s %s %-54s %18s %16s %s\n" % ("         [creation_time]", "[agency]", "     [resource_id]", "[data_id]", "[event_id]", "[P]"))
    string +=("-"*135)
    string += "\n"

    objs = [cat, event] + event.origins + event.magnitudes + event.focal_mechanisms
    for obj in objs:
        cls = type(obj).__name__
        cls_name = "Unk"
        if cls in names:
            cls_name = names[cls]

        dsource = "N/A"
        dataid = "N/A"
        esource = "N/A"
        eventid = "N/A"
        if hasattr(obj, 'extra'):
            if 'datasource' in obj.extra:
                dsource = obj.extra['datasource']['value']
            if 'dataid' in obj.extra:
                dataid = obj.extra['dataid']['value']
            if 'eventsource' in obj.extra:
                esource = obj.extra['eventsource']['value']
            if 'eventid' in obj.extra:
                eventid = obj.extra['eventid']['value']

        #eventid="us_70007v9g_mww"
        creation_time = "N/A"
        agency_id = "--"
        if obj.creation_info:
            creation_time = obj.creation_info.creation_time
            agency_id = obj.creation_info.agency_id

        pref = ""
        flags = None
        if cls == "Origin":
            if event.preferred_origin() and obj.resource_id.id == event.preferred_origin().resource_id.id:
                flags = "*"
            if event.focal_mechanisms:
                fm = event.preferred_focal_mechanism() if event.preferred_focal_mechanism() else event.focal_mechanisms[0]
                if fm.triggering_origin_id and obj.resource_id == fm.triggering_origin_id:
                    if flags:
                        flags += "T"
                    else:
                        flags = "T"

        elif cls == "Magnitude":
            if event.preferred_magnitude() and obj.resource_id.id == event.preferred_magnitude().resource_id.id:
                flags = "*"
        elif cls == "FocalMechanism":
            if event.preferred_focal_mechanism() and obj.resource_id.id == event.preferred_focal_mechanism().resource_id.id:
                flags = "*"

        if flags:
            string += ("%3s: %-28s %1s%2s%1s %-56s %18s %16s [%s]\n" % \
                (cls_name, creation_time, " ",agency_id, " ", obj.resource_id.id, dataid, eventid, flags))
        else:
            string += ("%3s: %-28s %1s%2s%1s %-56s %18s %16s\n" % \
                (cls_name, creation_time, " ",agency_id, " ", obj.resource_id.id, dataid, eventid))

        #if cls == "Event":
            #string += ("%-38s [nPicks:%d nAmps:%d nOrigins:%d nMags:%d nFocalMechs:%d]\n" % \
                #("     -contd-", len(obj.picks), len(obj.amplitudes), len(obj.origins), len(obj.magnitudes), len(obj.focal_mechanisms)))
        if cls == "Origin":
            string += (" OT: %-28s loc: <%5.2f,%7.2f> [h=%.2f km] nArr:%d [mode:%s] [status:%s]\n" % \
                (obj.time, obj.latitude, obj.longitude, obj.depth/1e3, len(obj.arrivals), obj.evaluation_mode, obj.evaluation_status))
        elif cls == "Magnitude":
            string += ("%-38s [%s: %.2f] [mode:%s] [status:%s]\n" % \
                ("     -contd-",obj.magnitude_type, obj.mag, obj.evaluation_mode, obj.evaluation_status))

    return string


def read_quakemlfile(quakemlfile):
    """
    Read quakemlfile (typically coming through PDL)

    If quakemlfile contains multiple events, only read 1st event
    If event has multiple origins(magnitudes), only read preferred_origin and preferred_magnitude
    If no preferred_origin --> scan for mww_origin and/or mww_trigger_origin
    If event has focal mechanisms, only read preferred or 1st
    Drop any arrivals from origin that don't have distance set

    return obspy event, origin, magnitude, focal_mechanism
    """

    event = None
    origin = None
    magnitude = None
    focal_mechanism = None

    try:
        #cat = read_events(quakemlfile, format="quakeml")
        # Read quakeml into string and replace anssevent tags:
        try:
            with open(quakemlfile, 'r') as file:
                xml = file.read()
        except FileNotFoundError as e:
            logger.error(f"read_quakemlfile: {e.__class__.__name__}: {e}")
            return None, None, None, None

        anssInternal = False
        if 'anssevent:internalEvent' in xml:
            anssInternal = True
            logger.info("This is an anssevent:internalEvent quakeml")
            xml = xml.replace('anssevent:internalEvent', 'event')
            #xml = file.read().replace('anssevent:internalEvent', 'event')
        cat = read_events(io.BytesIO(bytes(xml, encoding='utf-8')), format="quakeml")
        if len(cat.events) == 0:
            logger.error("Incoming quakemlfile:%s has 0 events!" % quakemlfile)
            exit(2)
        elif len(cat.events) > 1:
            logger.warning("Incoming quakemlfile:%s has [%d] events!  Only inserting first event!" % (quakemlfile, len(cat.events)))
        event = cat[0]
    except:
        logger.error("Problem reading quakemlfile=[%s]" % quakemlfile)
        raise

    magnitude = event.preferred_magnitude()
    origin = event.preferred_origin()
    if event.focal_mechanisms:
        focal_mechanism = event.preferred_focal_mechanism() or event.focal_mechanisms[0]

    if event.preferred_origin() is None and event.origins is not None:
        mww_origin = None
        mww_trigger_origin = None
        for origin in event.origins:
            # MTH: CI sometimes uses "...query?originid=..." e.g., (ci39727887_mt1)
            #      Here I can scan the origin id, but should this case (of no preferred origin0
            #      be handled ?
            if 'query?originid=' in origin.resource_id.id:
                foo = origin.resource_id.id.split("query?originid=")[-1]
            else:
                foo = origin.resource_id.id.split("/")[-1]
            # MTH: Some other possibilities are: 'mwr', 'mwr_trigger' -- handle these ??
            if foo == 'mww':
                mww_origin = origin
            elif foo == 'mww_trigger':
                mww_trigger_origin = origin
            else:
                logger.warning("Unknown origin id:%s" % foo)
        if mww_trigger_origin:
            logger.info("Set origin = mww_trigger_origin")
            origin = mww_trigger_origin
        elif mww_origin:
            logger.info("Set origin = mww_origin")
            origin = mww_origin

    if origin.arrivals:
        arrivals = []
        # MTH: Drop bad arrivals with no distance set
        for arr in origin.arrivals:
            if not getattr(arr, 'distance', None) or arr.distance <= 0:
                logger.warning("quakemlfile=[%s] Got arrival with bad distance:%s --> Drop arrival!" %
                               (quakemlfile, arr.distance))
            else:
                arrivals.append(arr)
        origin.arrivals = arrivals


    return event, origin, magnitude, focal_mechanism


if __name__ == "__main__":
    main()
