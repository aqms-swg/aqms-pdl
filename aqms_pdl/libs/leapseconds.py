
from obspy import UTCDateTime
import numpy as np

def main():
    x = UTCDateTime('1972-6-30')
    x = UTCDateTime('1973-01-01')
    x = UTCDateTime('2016-12-31')
    #print(x.timestamp)
    #print_timestamps()
    for i in range(10000):
        secs = get_leapseconds_from_timestamp(x.timestamp)
        #secs = get_leapseconds_from_date(x)
    #print(get_leapseconds_from_date(x))


timestamps = [
(  78710400,   UTCDateTime('1972-06-30') ),       #+1 after this date
(  94608000,   UTCDateTime('1972-12-31') ),       # 2
( 126144000,   UTCDateTime('1973-12-31') ),       # 3
( 157680000,   UTCDateTime('1974-12-31') ),       # 4
( 189216000,   UTCDateTime('1975-12-31') ),       # 5
( 220838400,   UTCDateTime('1976-12-31') ),       # 6
( 252374400,   UTCDateTime('1977-12-31') ),       # 7
( 283910400,   UTCDateTime('1978-12-31') ),       # 8
( 315446400,   UTCDateTime('1979-12-31') ),       # 9
( 362707200,   UTCDateTime('1981-06-30') ),       # 10
( 394243200,   UTCDateTime('1982-06-30') ),       # 11
( 425779200,   UTCDateTime('1983-06-30') ),       # 12
( 488937600,   UTCDateTime('1985-06-30') ),       # 13
( 567907200,   UTCDateTime('1987-12-31') ),       # 14
( 631065600,   UTCDateTime('1989-12-31') ),       # 15
( 662601600,   UTCDateTime('1990-12-31') ),       # 16
( 709862400,   UTCDateTime('1992-06-30') ),       # 17
( 741398400,   UTCDateTime('1993-06-30') ),       # 18
( 772934400,   UTCDateTime('1994-06-30') ),       # 19
( 820368000,   UTCDateTime('1995-12-31') ),       # 20
( 867628800,   UTCDateTime('1997-06-30') ),       # 21
( 915062400,   UTCDateTime('1998-12-31') ),       # 22
(1135987200,   UTCDateTime('2005-12-31') ),       # 23
(1230681600,   UTCDateTime('2008-12-31') ),       # 24
(1341014400,   UTCDateTime('2012-06-30') ),       # 25
(1435622400,   UTCDateTime('2015-06-30') ),       # 26
(1483142400,   UTCDateTime('2016-12-31') ),       # 27
]


"""
timetamp is really (Unix) Epoch timestamp since Jan 1,1970
"""
def get_leapseconds_from_timestamp(timestamp):
    x = np.array([timestamp for timestamp, _date in timestamps])
    leapsecs = sum(i for i in x < timestamp)
    return leapsecs

def get_leapseconds_from_date(utcdate):
    x = np.array([_date for timestamp, _date in timestamps])
    leapsecs = sum(i for i in x < utcdate)
    return leapsecs


if __name__ == '__main__':
    main()
