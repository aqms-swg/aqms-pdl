from datetime import datetime
from obspy import UTCDateTime
import os
import pytz
import sys

import math


def main():
    """
    x = 849985
    for n in [6, 5, 4, 3, 2, 1]:
        print("%s %d" % (round_microseconds(x,n), n))
    """

    utcdate = UTCDateTime('2024/12/06 19:31:34.849985')
    utcdate = UTCDateTime('2024/12/06 19:31:34.849999')
    utcdate = UTCDateTime(1733513521.8499985 -27., precision=7)
    utcdate = UTCDateTime(1716209707.540001 -27., precision=7)

    #format_date: utcdate:2024-12-06T19:31:34.849999Z microsecs:850000 newdate:2024-12-06T19:31:34.849999Z

    #for n in [6,5,4,3,2,1]:
    for n in [6,5,4]:
        x = format_date(utcdate, include_T=True, ndigits=n)
        print(x, n)


def round_microseconds(microseconds, ndigits):
    npow = 6-ndigits
    #x = math.ceil(x) Always rounds up to next int
    #x = int(x)       Simply drops fractional part
    #x = round(x,0)   Rounds to nearest int
    return int(10**npow * round(microseconds/10**npow,0))

def utc_to_local(utc):
    offset = datetime.now(pytz.timezone('US/Eastern')).utcoffset().total_seconds()
    return utc + offset

def local_to_utc(local):
    offset = datetime.now(pytz.timezone('US/Eastern')).utcoffset().total_seconds()
    return local - offset

def format_date(utcdate, include_T=True, include_Z=False, ndigits=2, date_separator='-'):
    """
    round/format utcdate to requested number of digits and return as string
    """

    # Round up microsecond before truncating
    # utcdate.microsecond = round_microseconds(utcdate.microsecond, ndigits)
    # obspy is complaining that setting attribs on UTCDateTime will be
    #  deprecated so ... make a new UTCDateTime with correct rounding
    microsecs = round_microseconds(utcdate.microsecond, ndigits)
    newdate = UTCDateTime(utcdate.timestamp - utcdate.microsecond/1e6 + microsecs/1e6)

    date_str = None
    s = 'T' if include_T else ' '
    ds = date_separator

    if ndigits == 0:
        date_str = newdate.datetime.strftime(f"%Y-%m-%d{s}%H:%M:%S")
    elif ndigits == 6:
        date_str = newdate.datetime.strftime(f"%Y-%m-%d{s}%H:%M:%S.%f")
    else:
        #date_str = newdate.datetime.strftime(f"%Y-%m-%d{s}%H:%M:%S.%f")[:ndigits-6]
        date_str = newdate.datetime.strftime(f"%Y{ds}%m{ds}%d{s}%H:%M:%S.%f")[:ndigits-6]

    if include_Z:
        date_str += 'Z'

    return date_str


if __name__=="__main__":
    main()
