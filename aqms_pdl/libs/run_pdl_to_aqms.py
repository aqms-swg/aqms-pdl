# -*- coding: utf-8 -*-
"""
Read in quakeml file + flags and insert into AQMS db
Triggered by PDL

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import logging
import os
from sqlalchemy import exc

from obspy.core.utcdatetime import UTCDateTime

from aqms_pdl.libs.libs_amps import scan_xml_amps, unassoc_amplitude_to_aqms
from aqms_pdl.libs.libs_aqms import (obspy_event_to_aqms, obspy_origin_to_aqms, obspy_magnitude_to_aqms,
                            obspy_arrival_to_aqms, obspy_amplitude_to_aqms,
                            obspy_amplitude_to_assocamm, obspy_amplitude_to_assocamo,
                            obspy_arrival_to_assocaro, obspy_focal_mechanism_to_aqms,
                            aqmsprefor, aqmsprefmag, aqmsprefmec, summarize,
                            compare_quakeml_origin_to_aqms_origin,
                            compare_aqms_origins,
                            calc_diff_between_aqms_origins,
                            associate_PDL_origin_with_RSN_origins,
                            get_assoc_thresh,
                            )
from aqms_pdl.libs.libs_evids import check_eventids
from aqms_pdl.libs.libs_db import (fix_leap_seconds,
                           getSequence, get_eventbyid, call_procedure, get_gtype,
                           query_origins_around_origin_time,
                           query_for_pdl_event_in_aqms,
                           query_assocevents,
                           query_origin_by_evid,
                           fix_unassoc_amp_leap_seconds,
                           arrival_already_in_db,
                           amp_already_in_db,
                           netmag_already_in_db,
                           focalmec_already_in_db,
                          )
from aqms_pdl.libs.libs_obs import scan_quakeml, read_quakemlfile
from aqms_pdl.libs.libs_util import archive_quakeml
from aqms_pdl.libs.schema_aqms_PI import (Eventprefmag, Eventprefor, Eventprefmec, Origin,
                                          Event, Assocevents, Remark, Netmag, Arrival)
from aqms_pdl.libs.util_cls import thresh

KM_PER_DEG = 111.19

logger = logging.getLogger()

fname = 'pdl_to_aqms'

def run_pdl_to_aqms(config, quakemlfile, args):

    RSN_source = config['RSN_source']

    auth   = args.source.upper()
    pdl_id, RSN_pdl_id, other_pdl_ids = check_eventids(args, RSN_source, keep_them_separated=True)

    logger.debug("pdl_id=[%s] RSN_pdl_id=[%s] other_pdl_ids=[%s]" % (pdl_id, RSN_pdl_id, other_pdl_ids))

    #print("run_pdl_to_aqms RSN_source=[%s] pdl_id=[%s] RSN_pdl_id=[%s] other_pdl_ids=[%s]" %
          #(RSN_source, pdl_id, RSN_pdl_id, other_pdl_ids))

    if pdl_id is None:
        logger.error("%s: Must pass --preferred-eventsourcecode=... to set pdl_id ==> Exitting!" % fname)
        exit(2)

    if len(pdl_id) > 12:
        new_id = pdl_id[:12]
        logger.warning("pdl_id=%s has len > 12 ==> shorten to:%s for storage in origin.locevid" %
                       (pdl_id, new_id))
        pdl_id = new_id

    pdl_type = args.type          #  PDL message type, e.g., 'origin', 'phase_data' or 'moment-tensor'
    subsource = 'PDL'
    if args.subsource:
        logger.info("%s: Setting subsource=%s" % (fname, args.subsource))
        subsource = args.subsource

    if pdl_type == 'unassociated-amplitude':
        logger.info("%s: Handle unassociated-amplitude message and exit" % fname)
        handle_unassociated_amp(quakemlfile, auth, subsource)
        exit(0)

    if pdl_type == 'internal-moment-tensor':
        logger.info("%s: pdl_type = internal-moment-tensor --> Expect Mww magnitude" % (fname))

    selectflag = 1
    if args.set_selectflag_zero or ('set_selectflag_zero' in config and config['set_selectflag_zero']==True):
        logger.info("Setting selectflag=0 for PDL events !!")
        selectflag = 0

    assoc_thresh = get_assoc_thresh(config)
    do_association = True if 'associate' in config and config['associate'] else False
    #print("run_pdl: auth:%s pdl_id:%s do_association:%s" % (auth, pdl_id, do_association))

    # Is this pdl_id already in the db (as origin.locevid) ?
    if 'skip_origin_locevid_check' in config and config['skip_origin_locevid_check']:
        logger.info("Skipping found_PDL_orig check")
        found_PDL_orig = None
        do_association = False
    else:
        found_PDL_orig = query_for_pdl_event_in_aqms(pdl_id)

    if found_PDL_orig is None and do_association:
        # Try the other ids pulled from eventids
        for other_id in other_pdl_ids:
            found_PDL_orig = query_for_pdl_event_in_aqms(other_id)
            if found_PDL_orig:
                break

    if found_PDL_orig:
        logger.info("Found match pdl_id:%s evid:%s orid:%s in local db" %
                    (found_PDL_orig.locevid, found_PDL_orig.evid, found_PDL_orig.orid))
    else:
        logger.info("No matching/related pdl_ids found in db")

    if args.archive_quakeml:
        archive_quakeml(args)

# 1. Read in the PDL quakeml file:
    logger.info(scan_quakeml(quakemlfile))
    event, origin, magnitude, focal_mechanism = read_quakemlfile(quakemlfile)

    if event is None:
        logger.error(f"Unable to read event from quakemlfile:{quakemlfile} --> Exitting!")
        exit(2)

    min_dist = None
    if origin.arrivals:
        sorted_arrivals = sorted(origin.arrivals, key=lambda x: x.distance, reverse=False)
        #MTH: obspy distance is in deg and all AQMS lengths are km:
        min_dist = sorted_arrivals[0].distance * KM_PER_DEG

    if args.debug == 'summarize-only':
        summarize(event, origin, magnitude)
        exit(0)

    # Add leap seconds to origin.time and any event.picks:
    fix_leap_seconds(origin, event)

# 2. Figure out if event and/or origin need to be inserted:
    # Defaults
    insert_origin = False
    insert_event = False
    insert_magnitude = False
    insert_mechanism = False
    evid = None
    orid = None
    magid = None
    mecid = None

    if found_PDL_orig:
        evid = found_PDL_orig.evid
        logger.info("found_PDL_orig [orid=%s] has evid=%s --> Use this evid" %
                    (found_PDL_orig.orid, found_PDL_orig.evid))
        if pdl_id == found_PDL_orig.locevid:
            orid = found_PDL_orig.orid
            logger.info("found_PDL_orig [orid=%s] locevid=%s matches pdl_id=%s --> compare origins" %
                        (found_PDL_orig.orid, found_PDL_orig.locevid, pdl_id))
            if origin:
                origin_matches = compare_quakeml_origin_to_aqms_origin(quakeml_origin=origin,
                                                                       aqms_origin=found_PDL_orig)
                if not origin_matches:
                    logger.info("This origin does not match found_PDL_orig [orid=%s] --> update origin" %
                                (found_PDL_orig.orid))
                    insert_origin = True
        else:
            logger.info("found_PDL_orig [orid=%s] locevid=%s DOES NOT MATCH pdl_id=%s --> insert new origin" %
                        (found_PDL_orig.orid, found_PDL_orig.locevid, pdl_id))
            insert_origin = True
    else:
        logger.info("pdl_id:%s not found in local db --> Create new evid + orid" %
                    (pdl_id))
        insert_event = True
        insert_origin = True

    if insert_origin:
        orid = getSequence('origin', 1)[0]
        logger.info("Request new orid from sequence returned: [orid=%s]" % (orid))
    if insert_event:
        evid = getSequence('event', 1)[0]
        logger.info("Request new evid from sequence returned: [evid=%s]" % (evid))

    # MTH: is it possible to get to here with origin = None ?
    if origin:
        logger.info("quakeml file contains [%d] arrivals [%d] amplitudes [%d] picks" % \
                    (len(origin.arrivals), len(event.amplitudes), len(event.picks)))
        insert_arrivals = True if (len(origin.arrivals) > 0) and not args.ignore_arrivals else False
        insert_amplitudes = True if (len(event.amplitudes) > 0) and not args.ignore_amplitudes else False


# Keep a local session for adding/flushing
    createAppSession(config)
    session = appSession()

# 3. Prefetch all ids needed for insertion
#    we should already have evid + orid

    if magnitude:
        aqms_mag  = obspy_magnitude_to_aqms(magnitude, orid=orid, evid=evid, magid=9999, auth=auth,
                                            subsource=subsource, min_dist=min_dist)
        magid = netmag_already_in_db(aqms_mag)
        if magid is None:
            insert_magnitude = True
            magid = getSequence('magnitude', 1)[0]
            aqms_mag.magid = magid
            logger.info("Request magid returned:%s" % (aqms_mag.magid))
            session.add(aqms_mag)
            session.flush()
        else:
            logger.info("netmag magid:%d is a match for this magnitude --> use this magid" % magid)

    if focal_mechanism:
        #TODO: MTH: For moment-tensor msg with no preferred mag, need to find the associated magid
        #           before next step. For now set to None
        aqms_mec = obspy_focal_mechanism_to_aqms(focal_mechanism, event, orid=orid, magid=magid,
                                                 mecid=9999, auth=auth, subsource=subsource)
        mecid = focalmec_already_in_db(aqms_mec)
        if mecid is None:
            insert_mechanism = True
            mecid = getSequence('mechanism', 1)[0]
            aqms_mec.mecid = mecid
            logger.info("Request mecid returned:%s" % (aqms_mec.mecid))
            session.add(aqms_mec)
            session.flush()
        else:
            logger.info("Mec mecid:%d is a match for this mechanism --> use this mecid" % mecid)

    if insert_arrivals:
        aqms_arrivals = []
        assocaro_adds = []

        for arrival in origin.arrivals:
            # agency_id: 'pr'  ?? use this ??
            auth_arr = auth
            if getattr(arrival, 'creation_info', None) and \
               getattr(arrival.creation_info, 'agency_id', None):
                auth_arr = arrival.creation_info.agency_id.upper()
            else:
                logger.debug("Arrival doesn't have creation_info --> use origin auth=[%s] for auth_arr" % (auth))

            aqms_arr = obspy_arrival_to_aqms(arrival, auth=auth_arr, subsource=subsource)

            arid = arrival_already_in_db(aqms_arr, orid)
            if arid is None:
                arid = getSequence('arrival', 1)[0]
                aqms_arr.arid = arid
                logger.info("Insert new arrival arid:%d" % arid)
                aqms_arrivals.append(aqms_arr)
                # assocaro - Data associating arrivals with origins
                assocaro_adds.append(obspy_arrival_to_assocaro(arrival, arid=arid, orid=orid,
                                                               auth=auth_arr, subsource=subsource) )
            else:
                logger.info("Arrival with arid:%d already in db --> don't reinsert!" % arid)

        if aqms_arrivals:
            session.bulk_save_objects(aqms_arrivals)
            logger.info("Insert %d arrivals arid:%d -to- %d" % (len(aqms_arrivals), aqms_arrivals[0].arid, aqms_arrivals[-1].arid))
        else:
            logger.warning("insert_arrivals=True but len(aqms_arrival)==0!!")

        if assocaro_adds:
            session.bulk_save_objects(assocaro_adds)

    if insert_amplitudes:
        aqms_amplitudes = []
        assocamm_adds = []
        assocamo_adds = []
        # MTH: ATODO: Need to test if these amplitudes are in fact coda measurements and fill coda table accordingly
        for amplitude in event.amplitudes:
            auth_amp = auth
            if getattr(amplitude, 'creation_info', None) and \
               getattr(amplitude.creation_info, 'agency_id', None):
                auth_amp = amplitude.creation_info.agency_id.upper()
            else:
                logger.debug("Amplitude doesn't have creation_info --> use origin auth=[%s] for auth_amp" % (auth))

            aqms_amp = obspy_amplitude_to_aqms(amplitude, auth=auth_amp, subsource=subsource)

            ampid = amp_already_in_db(aqms_amp, orid)
            if ampid is None:
                ampid = getSequence('amplitude', 1)[0]
                aqms_amp.ampid = ampid
                logger.info("Insert new amp ampid:%d" % ampid)
                aqms_amplitudes.append(aqms_amp)

                st_mag = None
                for station_mag in event.station_magnitudes:
                    if amplitude.resource_id == station_mag.amplitude_id:
                        st_mag = station_mag
                        break
                # assocamm - associates amps with mag
                assocamm_adds.append( obspy_amplitude_to_assocamm(ampid=ampid, magid=magid,  auth=auth, station_mag=st_mag, subsource=subsource) )
                # assocamo - associates amps with origin
                # Note: This will not set fields: delta & seaz in assocamo
                #       since they are not present in obspy amplitudes
                #       The fix would be to relate ampid scnl to arrival scnl and
                #       pull delta/seaz from the corresponding arrival
                assocamo_adds.append( obspy_amplitude_to_assocamo(ampid=ampid, orid=orid,  auth=auth, subsource=subsource) )

            else:
                logger.info("Amp with ampid:%d already in db --> don't reinsert!" % ampid)

        if aqms_amplitudes:
            session.bulk_save_objects(aqms_amplitudes)
            session.bulk_save_objects(assocamm_adds)
            session.bulk_save_objects(assocamo_adds)
            logger.info("Insert %d amps ampid:%d -to- %d" % (len(aqms_amplitudes), aqms_amplitudes[0].ampid, aqms_amplitudes[-1].ampid))
        else:
            logger.warning("insert_amplitudes=True but len(aqms_amplitudes)==0!!")


# 4. Convert obspy objects to AQMS table equivalents and add to session:
    if insert_event:
        aqms_event = obspy_event_to_aqms(event, orid=orid, evid=evid, magid=magid,
                                        mecid=mecid, auth=auth, subsource=subsource,
                                        selectflag=selectflag)
        if 'event_remark' in config:
            #rmk = Remark(lineno=1, remark=args.event_remark[:79])
            rmk = Remark(lineno=1, remark=config['event_remark'])
            session.add(rmk)
            session.flush()
            aqms_event.commid = rmk.commid
            logger.info("Add commid:%d rmk:%s" % (rmk.commid, rmk.remark))
        #session.add(aqms_event)
        #session.flush()
    else:
        aqms_event = get_eventbyid(evid)

    if insert_origin:
        aqms_orig = obspy_origin_to_aqms(event, orid=orid, evid=evid, magid=magid,
                                         mecid=mecid, auth=auth, pdl_id=pdl_id,
                                         subsource=subsource, min_dist=min_dist)
        #print("run_pdl get_gtype RSN_source:%s lat:%.3f lon:%.3f" %
              #(RSN_source, aqms_orig.lat, aqms_orig.lon))
        gtype = get_gtype(config, RSN_source, aqms_orig.lat, aqms_orig.lon)
        if gtype is not None:
            aqms_orig.gtype = gtype
        #else:
            #logger.info("set_gtype() returned None --> origin.gtype not set")
            #print("set_gtype() returned None --> origin.gtype not set")

        session.add(aqms_orig)
        session.flush()

        aqms_event.prefor = orid # aqms_event is already added to session
        # MTH: Is there ever a time when you would add aqms_event *without* inserting an origin ?
        session.add(aqms_event)
        session.flush()

        # Does this evid already have an eventprefor entry?
        prefor = None
        eventprefor = session.query(Eventprefor).filter(Eventprefor.evid==evid).one_or_none()
        if eventprefor:
            old_prefor = eventprefor.orid
            logger.info("Update eventprefor evid:%s old_orid:%s --> new_orid:%s" % (evid, old_prefor, orid))
            eventprefor.orid = orid
            session.add(eventprefor)

        else:
            logger.info("Unable to locate evid=%s in eventprefor table --> make new entry with orid=%s" %
                       (evid, orid))
            session.add(aqmsprefor(evid, orid))

    else:
        aqms_orig = found_PDL_orig


    if insert_mechanism:
    # MTH: Couldn't insert prefmec before because event was not yet added/inserted
    #      hence that FK didn't yet exist
        prefmec = session.query(Eventprefmec).filter(Eventprefmec.evid==evid,
                                                     Eventprefmec.mechtype==aqms_mec.mechtype
                                                     ).one_or_none()
        if prefmec:
            logger.info("Update eventprefmec evid:%s type:%s old_mecid:%s --> new:%s" % \
                        (evid, prefmec.mechtype, prefmec.mecid, mecid))
            prefmec.mecid = mecid
            session.add(prefmec)
        else:
            logger.info("Unable to locate evid:%s type:%s in eventprefmec table --> insert new mecid:%s" % \
                        (evid, aqms_mec.mechtype, mecid))
            session.add(aqmsprefmec(evid, mecid, mechtype=aqms_mec.mechtype))


    if do_association:

    # 1. The current event/evid is a PDL event with source code != RSN_source
    #    If PDL already associated this evid with a previous msg from RSN_source,
    #    then RSN_pdl_id will be set by check_eventids, either from
    #    eventids[] and/or args.preferred_evenid.
    #    We can do a look up to see if RSN_pdl_id is in the local db and
    #    make an association between evid and RSN_pdl_id

        evid_evidassoc = []

        if RSN_pdl_id:
            RSN_evid = None
            RSN_orig = None
            try:
                RSN_evid = int(RSN_pdl_id)
            except ValueError:
                logger.warning("RSN evid=%s cannot be read as int --> Can't use to associate with PDL evids" %
                               RSN_pdl_id)
            else:
                RSN_orig = query_origin_by_evid(RSN_evid)

            if RSN_orig:
                logger.info("Assocevents [pdl_id] RSN_pdl_id=%s was found in RSN db evid:%d orid:%d" %
                            (RSN_pdl_id, RSN_orig.evid, RSN_orig.orid))
                comment = 'PDL --eventids:'
                comment += ' '.join(args.eventids)

                if query_assocevents(RSN_orig.evid, aqms_orig.evid):
                    logger.info("Assocevents(%d, %d) already in Table --> Do nothing" % (RSN_orig.evid, aqms_orig.evid))
                else:
                    logger.info("Add assocevents(%d, %d)" % (RSN_orig.evid, aqms_orig.evid))
                    commid = None
                    if comment:
                        #rmk = Remark(commid=commid, lineno=1, remark=comment)
                        rmk = Remark(lineno=1, remark=comment[:79])
                        session.add(rmk)
                        session.flush()
                        commid = rmk.commid
                        logger.info("Add commid=%d remark=%s" % (rmk.commid, comment))
                    session.add(Assocevents(evid=RSN_evid, evidassoc=evid, commid=commid))
                    evid_evidassoc.append((RSN_evid, evid))
            else:
                logger.warning("RSN_pdl_id=%s was not found in RSN db origin.evid fields" % RSN_pdl_id)


    # 2. We can also look through the db for any non-PDL (thus, only local RSN evids) 
    #    origins within assoc_thresh of this PDL origin.
    #    For each origin within threshold, insert new assocevents row

        logger.info("Use assoc_thresh: time:%.1f, dist:%.1f km depth:%.1f km" %
                    (assoc_thresh.time, assoc_thresh.km, assoc_thresh.depth))

        # Return list of local (RSN, non-PDL) origin(s) that associate with aqms_orig PDL origin
        local_origins = associate_PDL_origin_with_RSN_origins(aqms_orig, RSN_source, assoc_thresh)

        if local_origins:
            for local_orig in local_origins:
                logger.info("Assocevents [assoc_thresh] RSN local_orig.evid=%d orid:%d associates within assoc_thresh" %
                            (local_orig.evid, local_orig.orid))
                if query_assocevents(local_orig.evid, aqms_orig.evid):
                    logger.info("Assocevents(%d, %d) already in Table --> Do nothing" % (local_orig.evid, aqms_orig.evid))
                elif (local_orig.evid, aqms_orig.evid) in evid_evidassoc:
                    logger.info("Assocevents(%d, %d) already staged to associate --> Do nothing" % (local_orig.evid, aqms_orig.evid))
                    #print("Assocevents(%d, %d) already staged to associate --> Do nothing" % (local_orig.evid, aqms_orig.evid))
                else:
                    od = calc_diff_between_aqms_origins(local_orig, aqms_orig)
                    comment = 'orids:%d and %d are close: dt:%.3f s dx:%.2f km dz:%.2f km' % \
                                (local_orig.orid, aqms_orig.orid, od.dt, od.dx, od.dz)
                    logger.info("Add assocevents(%d, %d)" % (local_orig.evid, aqms_orig.evid))
                    rmk = Remark(lineno=1, remark=comment[:79])
                    session.add(rmk)
                    session.flush()
                    #commid = rmk.commid
                    logger.info("Add commid=%d remark=%s" % (rmk.commid, comment))
                    #print("session.add: Assocevents(evid=%d, evidassoc=%d, commid=%d)" %(local_orig.evid, aqms_orig.evid, rmk.commid))
                    session.add(Assocevents(evid=local_orig.evid, evidassoc=aqms_orig.evid, commid=rmk.commid))

# 5. Commit the session
    logger.info("Commit the session insert_arrivals=%s" % insert_arrivals)
    try:
        logger.info("commit the session")
        session.commit()
    except exc.SQLAlchemyError as e:
        logger.error("Could not commit session:%s" % e)
        logger.error("Rollback session and exit")
        session.rollback()
        raise
    finally:
        session.close()

    # Use stored proc to set preferred magnitude
    if insert_magnitude:
        logger.info("Set the preferred magnitude to  magid=%d" % magid)
        p_evtpref, p_bump, p_commit = 1, 1, 0
        # MTH: Use this version if you want to bump event.version when new event.prefmag gets set:
        #retval = call_procedure('epref.setprefmag_magtype', [evid, magid, p_evtpref, p_bump, p_commit], config)
        retval = call_procedure('epref.setprefmag_magtype', [evid, magid], config)
        if retval == 2:
            logger.info("Stored procedure setprefmag returned val=%d" % retval)
        elif retval == 1:
            logger.warning("Stored procedure setprefmag returned val=%d" % retval)
        else:
            logger.error("Stored procedure setprefmag returned val=%d" % retval)

    logger.info("==== End of pdl_to_aqms  ============================================")

    return evid



def handle_unassociated_amp(xml_amps_dir, auth, subsource):

    amps = scan_xml_amps(xml_amps_dir)
    logger.info("%s: pdl_type:%s scan for amps in xml_dir=[%s] returned:[%d] unassoc amps" % \
                (fname, pdl_type, xml_amps_dir, len(amps)))

    ampids = getSequence('unassocamp', len(amps))

    AQMS_amps = []
    for amp in amps:
        ampid = ampids.pop(0)
        fix_unassoc_amp_leap_seconds(amp)
        AQMS_amps.append(unassoc_amplitude_to_aqms(amp, ampid=ampid, auth=auth, subsource=subsource))

    createAppSession(config)
    session = appSession()
    session.bulk_save_objects(AQMS_amps)
    try:
        logger.info("commit the session")
        session.commit()
    except exc.SQLAlchemyError as e:
        logger.error("Could not commit session:%s" % e)
        logger.error("Rollback session and exit")
        session.rollback()
        raise
    finally:
        session.close()

    logger.info("%s: Done inserting unassocamps --> Exit" % fname)

    return


from sqlalchemy import create_engine, engine_from_config
from sqlalchemy.orm import sessionmaker

appSession = None

def createAppSession(config):
    global appSession

    if appSession:
        logger.warning("appSession is already created")
        return

    try:
        #engine = engine_from_config(config, echo=True)
        engine = engine_from_config(config)
        appSession = sessionmaker(bind=engine)
    except :
        raise


if __name__ == "__main__":
    main()
