# -*- coding: utf-8 -*-
"""
Functions for querying AQMS db PI tables

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime
import psycopg2
import sqlalchemy
from sqlalchemy import engine_from_config
from sqlalchemy import distinct, func, exc
from sqlalchemy import Sequence, text


import logging
#logger = logging.getLogger()
logger = logging.getLogger(__name__)

from obspy.core.utcdatetime import UTCDateTime

#from aqms_pdl.libs.libs_db_IR import *
#from aqms_pdl.libs.libs_db_WF import get_waveroot, get_waveform_list, get_subdir, get_filename

from aqms_pdl.libs.schema_aqms_AP import Epochtimebase, Credit_alias
from aqms_pdl.libs.schema_aqms_PI import (Eventprefmag, Eventprefor, Eventprefmec, Origin,
                                          Arrival, Amp, Event, Assocevents, Remark, Netmag, Mec,
                                          Assocaro, Assocamo)

def is_magid_preferred(evid, magid):
    event=None
    eventprefmag=None
    with Session.begin() as session:
        event = session.query(Event).filter(Event.evid==evid).one_or_none()
        eventprefmag = session.query(Eventprefmag).filter(Eventprefmag.evid==evid).all()
        netmag = session.query(Netmag).filter(Netmag.magid==magid).one_or_none()
        session.expunge_all()

    eventset = eventprefset = False
    if getattr(event, 'prefmag', None) == magid:
        eventset = True

    for eprefmag in eventprefmag:
        if eprefmag.magtype == netmag.magtype:
            if eprefmag.magid == netmag.magid:
                eventprefset = True
            break

    """
    0 - eventprefmag.magtype unset and event.prefmag unset
    1 - eventprefmag.magtype set   but event.prefmag unset
    2 - eventprefmag.magtype set   and event.prefmag set
    3 - eventprefmag.magtype unset but event.prefmag set
    """
    if not eventprefset and not eventset:
        retval = 0
    elif eventprefset and not eventset:
        retval = 1
    elif eventprefset and eventset:
        retval = 2
    elif not eventprefset and eventset:
        retval = 3

    return retval


def force_eventprefmag(evid, magid, bump_version=True):
    event=None
    eventprefmag=None
    with Session.begin() as session:
        event = session.query(Event).filter(Event.evid==evid).one_or_none()
        eventprefmag = session.query(Eventprefmag).filter(Eventprefmag.evid==evid).all()
        netmag = session.query(Netmag).filter(Netmag.magid==magid).one_or_none()
        #session.expunge_all()
        #print("event:%s" % event)

        event.prefmag = magid
        if bump_version:
            old = event.version
            event.version = old + 1
            logger.info("force_eventprefmag bump version old:%d new:%d" % (old, event.version))
        #session.add(event)
        for eprefmag in eventprefmag:
            if eprefmag.magtype == netmag.magtype:
                #print("Found match magtype:%s" % netmag.magtype)
                eprefmag.magtype = netmag.magtype
                eprefmag.magid = netmag.magid
                #session.add(eprefmag)
                break
        try:
            session.commit()
        except exc.SQLAlchemyError as e:
            logger.error("Could not commit session:%s" % e)
            logger.error("Rollback session and exit")
            return_msg = "Could not commit session:%s" % e
            return {'retval':-1, 'retmsg':return_msg}

    retval = is_magid_preferred(evid, magid)
    return {'retval':retval, 'retmsg':None}



def unset_eventprefmag(evid, config, ignore_existing_prefmag=False):

# MTH: o epref.unsetprefmag runs bestMagidOfEvent
#      o  magpref.magpref_bestMagidOfEvent
#          - only includes netmags with origin.evid=this_evid
#          - it won't include foreign netmags like US Mww
#          - it sorts the query by priority + lddate desc and takes the first row
#          - hence if you have 2 netmags of equal priority it keeps the most recent
#      o The problem is that the aqms-tdmt-app inserts netmags (e.g., NC Mw)
#        of equal priority so the most recent wins and you can never
#        unset it back to the previous one
#
#      o The answer was to create a new SP = magpref.magpref_bestMagidOfEventWithoutMagid
#        that does not consider the passed in p_magid in the search for best
#        and then just pass in the currently set prefmag magid
#
#      epref.unsetprefmagToPrior looks for best magid *excluding* currently set prefmag
#
#      o Once you have a v_bestmagid the SPs call:
#        1. epref.setprefmag_magtype(evid, v_bestmagid)  // Inserts into eventprefmag if not already there
#        2. magpref.setprefmagofevent(evid)              // Set event.prefmag from best mag of eventprefmag rows

    fname = "unset_eventprefmag"
    with Session.begin() as session:
        magid = session.query(Event.prefmag).filter(Event.evid==evid).one_or_none()
        session.expunge_all()

    magid = magid[0] if magid else None

    stored_proc = 'epref.unsetprefmag'
    if magid and ignore_existing_prefmag:
        stored_proc = 'epref.unsetprefmagToPrior'

    retval = call_procedure(stored_proc, [evid, magid], config)
    return_codes = {
        -1: "magid is bestmagid --> Don't unset it",
         0: "event.prefmag is already set to bestmagid",
         1: "event.prefmag successfully unset",
    }

    retmsg = "Unknown return code:%s" % retval
    if retval in return_codes:
        retmsg = return_codes[retval]

    logger.debug("stored_proc:%s returned retval:%d retmsg:%s" % (stored_proc, retval, retmsg))

    return {'retval':retval, 'retmsg':retmsg}



def set_eventprefmag(evid, magid, config, force=False):

    if force:
        return force_eventprefmag(evid, magid, bump_version=True)

    return_msg = None

    # MTH: assuming we want to bump the event.version each time eventprefmag is set (?)
    p_evtpref, p_bump, p_commit = 1, 1, 0    # p_commit is leftover from Oracle, not used by PG
    retval = call_procedure('epref.setprefmag_magtype', [evid, magid, p_evtpref, p_bump, p_commit], config)
    logger.info("setprefmag_magtype(evid=%d magid=%d) returned retval=%s" %
                (evid, magid, retval))

    # retval = 2 indicates both event.prefmag and eventprefmag.magtype successfully set
    # retval = 1 could mean that only 1 of these was set -or- that current magid is *already* set (?)

    # MTH: overwrite retval to be: 0-3
    retval = is_magid_preferred(evid, magid)
    logger.info("is_magid_preferred(%d,%d) returned:%d" % (evid, magid, retval))

    #if retval in [2, 1]:
    if retval not in [2]:
        logger.warn("setprefmag_magtype(evid:%d, magid:%d) returned retval:%d" %
                    (evid, magid, retval))

        new_magid = magid
        new_priority = call_procedure('magpref.getMagPriority', [new_magid], config)

        with Session.begin() as session:
            event = session.query(Event).filter(Event.evid==evid).one_or_none()
            eventprefmag = session.query(Eventprefmag).filter(Eventprefmag.evid==evid).all()
            new_netmag = session.query(Netmag).filter(Netmag.magid==new_magid).one_or_none()

            if getattr(event, 'prefmag', None) is None:
                return_msg = "evid:%d has NO existing event.prefmag" % (evid)
            elif event.prefmag == new_magid:
                pass
            else:
                old_magid = event.prefmag
                old_netmag = session.query(Netmag).filter(Netmag.magid==old_magid).one_or_none()
                old_priority = call_procedure('magpref.getMagPriority',[old_magid], config)
                #highest_priority = call_procedure('magpref.highest_priority', [old_magid, new_magid], config)
                #logger.info("evid:%d highest_priority magid:%d" % (evid, highest_priority))
                #return_msg = "evid:%d has existing prefmag:%d [type:%s mag:%.2f priority:%d] > new_magid:%d [type:%s mag:%.2f priority:%d]" % \
                                #(evid, old_magid, old_netmag.magtype, old_netmag.magnitude, old_priority,
                                       #new_magid, new_netmag.magtype, new_netmag.magnitude, new_priority)
                return_msg = "existing prefmag:%d type:%s mag:%.2f priority:%2d\n" \
                             "    >  new_magid:%d type:%s mag:%.2f priority:%2d" % \
                                (old_magid, old_netmag.magtype, old_netmag.magnitude, old_priority,
                                 new_magid, new_netmag.magtype, new_netmag.magnitude, new_priority)

        #if len(eventprefmag) > 0:
            #magtypes = ', '.join(["type:%s magid:%d" % (x.magtype, x.magid) for x in eventprefmag])
            #logger.info("evid:%d has %d eventprefmag entries:%s" % (evid, len(eventprefmag), magtypes))
    return {'retval':retval, 'retmsg':return_msg}


def call_procedure(function_name, params, config):
    ''' Generic way to call a stored procedure (=function_name) with params
    '''
    #print("call_procedure: params:%s config:%s" % (params, config))
    #connection = cloudsql.Engine.raw_connection()
    engine = engine_from_config(config)
    connection = engine.raw_connection()
    buf = None
    try:
        cursor = connection.cursor()
        cursor.callproc(function_name, params)
        row = cursor.fetchone()
        buf = row[0]
        cursor.close()
        connection.commit()
        #return buf
    except Exception as e:
        print("call_procedure caught exception:%s type:%s" %
              (e, type(e)))
    finally:
        connection.close()

    return buf


def call_procedure2(function_name, params, config):
    ''' Generic way to call a stored procedure (=function_name) with params
    '''
    #print("call_procedure: params:%s config:%s" % (params, config))
    #connection = cloudsql.Engine.raw_connection()

    engine = engine_from_config(config)

    p = ','.join([str(x) for x in params])
    spcall = "%s(%s)" % (funcname, p)
    #stmts = """\
            #SET NOCOUNT ON;
    stmt = "select * from %s;" % spcall

    with engine.begin() as conn:
        result = conn.exec_driver_sql(stmt).all()
        print("call_procedure2 result:%s" % result)
        logger.warning("call_procedure2 result:%s" % result)

    return result


def get_gtype(config, source, lat, lon):
    '''
      returns:
          l - if lat,lon within authoritative region for this source (RSN)
          r - if lat,lon not within authoritative region for this source (RSN)
          None - if it can't be determined (e.g., if source not set)
    '''
    gtype = None

    logger.debug("get_gtype called with source=%s lat=%f lon=%f" % (source, lat, lon))
    retval = call_procedure('geo_region.inside_border', [source, lat, lon], config)
    if retval == 1:
        gtype = 'l'
    elif retval == 0:
        gtype = 'r'
    # MTH: let calling function handle return None and decide action
    #else:
        #logger.warning("gtype was not successfully set by SP!")
    return gtype

# MTH: The db queries from here down rely on have a Session.session() available

def fix_Session(session_in=None):
    global Session
    Session = session_in

def set_Session():
    global Session
    from .. import Session
    from aqms_pdl.libs.libs_db_IR import set_Session_IR
    from aqms_pdl.libs.libs_db_WF import set_Session_WF
    for s in [set_Session_IR, set_Session_WF]:
        s(Session)
        #setSession(Session)

def table_exists(tablename, config):
    ''' Return true if table exists in db'''
    engine = engine_from_config(config)
    return sqlalchemy.inspect(engine).has_table(tablename)

def test_connection(config):

    if config['conn_string'] is None:
        return False

    try:
        conn = psycopg2.connect(config['conn_string'])
    except psycopg2.OperationalError as ex:
        logger.error("test_connection: Caught error:[%s]" % repr(ex))
        return False

    return True

def pg_dbase_is_read_only():

    with Session.begin() as session:
        readonly = list(session.execute(text("select pg_is_in_recovery()")))
        session.expunge_all()
    return readonly[0][0]


def get_leapsecs(epoch=None):

    if epoch is None:
        epoch = UTCDateTime.now().timestamp

    # Query to see if this db is applying leapsecs or not
    # Note that epochtimebase table typically has:
    # ondate='1900-01-01' (timestamp=-2208970800) and offdate='3000-01-01' (timetamp=32503698000)
    with Session.begin() as session:
        rows = session.query(Epochtimebase).all()
        base = None
        for row in rows:
            on  = row.ondate.timestamp()
            off = row.offdate.timestamp()
            if epoch >= on and epoch <= off:
                base = row.base
                break

        if base == 'T':
            epoch_base=list(session.execute("select truetime.nominal2true(%d)" % int(epoch)))
            fixed_epoch = epoch_base[0][0]
            leapsecs = fixed_epoch - int(epoch)
            #logger.debug("Epochtimebase base=T ==> Apply [%d] leapsecs" % leapsecs)
        elif base == 'N':
            logger.debug("Epochtimebase base=N ==> Do NOT apply leapsecs")
            leapsecs = 0
        else:
            logger.error("Epochtimebase base=%s ==> Cannot determine whether to apply leapsecs. Exitting!" % base)
            exit(2)

    return leapsecs


def getSequence(name, count):
    sequences = {'event':'evseq',
                 'origin':'orseq',
                 'arrival':'arseq',
                 'amplitude':'ampseq',
                 'magnitude':'magseq',
                 'mechanism':'mecseq',
                 'mecdata':'mecdataseq',
                 'mecfreq':'mecfreqseq',
                 'unassocamp':'unassocseq',
                 'remark':'commseq',
                 'dm':'dmseq',
                 'dc':'dcseq',
                 'pz':'pzseq',
                 'pn':'poseq',
                 'd_comment':'comseq',
                 }

    if name not in sequences:
        logger.error("getSequence: Error - Unrecognized sequence name=[%s]" % name)
        return None

    nextids = []
    with Session.begin() as session:
        seq = Sequence(sequences[name])
        for i in range(count):
            try:
                nextids.append(session.execute(seq))
            except exc.SQLAlchemyError as e:
                logger.error("Caught SQLalchemyError:%s" % e)
                logger.error("*** Insert event FAILED --> Early Exit!!")
                exit(2)

    return nextids


def get_eventbyid(evid):
    '''
        Return event from aqms Event table matching evid
    '''
    event=None
    with Session.begin() as session:
        #event = session.query(Event).filter(Event.evid==evid).all()[0]
        event = session.query(Event).filter(Event.evid==evid).one_or_none()
        session.expunge_all()
    return event


def query_last_n_events(n):
    with Session.begin() as session:
        nevents = session.query(func.count(distinct(Event.evid))).scalar()
        rows = session.query(Origin).join(Event, Event.prefor == Origin.orid).\
                order_by(Origin.datetime.desc()).limit(n).all()
        # Some ESAs have empty eventprefor table
        #rows = session.query(Origin).join(Eventprefor).filter(Origin.orid==Eventprefor.orid).\
                #order_by(Origin.datetime.desc()).limit(n).all()
        session.expunge_all()

    return nevents, rows


def get_catalog_contributors():
    contributors = []
    with Session.begin() as session:
        rows = session.query(Event.auth).distinct()
        for row in rows:
            contributors.append(row[0])
    return contributors



def query_events_sort_by_lddate():
    with Session.begin() as session:
        rows = session.query(Event).order_by(Event.lddate.desc()).all()
        session.expunge_all()

    return rows


def query_origins_around_origin_time(origin_time, dt, use_preferred_origins=True):
    '''
    return list of origins in local db within dt seconds
           of origin_time
    Only include origins with evid that points to Event.selectflag=1
    '''

    t1 = float(origin_time) - dt
    t2 = float(origin_time) + dt
    logger.debug("query_origins around origin.datetime:%s +/- %s" %
                 (origin_time, dt))

    with Session.begin() as session:
        #rows = session.query(Origin).join(Eventprefor).filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()
        # MTH: PRSN eventprefor is not set for RT1, Jiggle, EBird events!
        #rows = session.query(Origin).filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()
        #rows = session.query(Origin, Event).filter(Origin.evid == Event.evid).filter(Event.selectflag == 1).\
                            #filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()
        if use_preferred_origins:
            rows = session.query(Origin).join(Event, Origin.evid == Event.evid).filter(Event.selectflag == 1).\
                            filter(Origin.orid == Event.prefor).\
                            filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()
        else:
            rows = session.query(Origin).join(Event, Origin.evid == Event.evid).filter(Event.selectflag == 1).\
                            filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()

        session.expunge_all()

    logger.debug("query_origins returned %d rows" % len(rows))
    return rows

def query_for_pdl_event_in_aqms(pdl_id):
    '''
        Query origin table for origin.locevid == pdl_id
        If found return latest origin with this locevid
    '''
    origin = None
    with Session.begin() as session:
    # Get the most recently loaded origin that has this locevid (it may not be preferred):
        found = session.query(Origin).filter(Origin.locevid==pdl_id).order_by(Origin.lddate.desc()).all()
        if found:
            origin = found[0]
            session.expunge_all()

    return origin


def amp_already_in_db(aqms_amp, orid):
    """
    See if this aqms amp is already in the db for this origin orid
    If so, return the ampid
    """
    with Session.begin() as session:
        rows = session.query(Amp).join(Assocamo, Assocamo.ampid==Amp.ampid).\
                                  filter(Assocamo.orid==orid).all()
        session.expunge_all()

    '''
    logger.info("Amp query returns %d rows" % len(rows))
    for row in rows:
        logger.info("Amp row sta:%s seedchan:%s iphase:%s datetime:%s" %
            (row.sta, row.seedchan, row.iphase, row.datetime));
        if row == aqms_amp:
            logger.info("**** AMP FOUND IT **** ")
            break
    '''

    for row in rows:
        if row == aqms_amp:
            return row.ampid

    return None


def get_arrivals_for_evid(evid):

    with Session.begin() as session:
        orig = session.query(Origin).join(Event, Event.evid==evid).filter(Event.prefor==Origin.orid).one_or_none()

        session.expunge_all()

    if orig is None:
        logger.error("ERROR: No origin found for evid:%d --> Exitting!" % evid)
        return None

    return get_arrivals_for_orid(orig.orid)

def get_arrivals_for_orid(orid):

    arrivals = []
    with Session.begin() as session:
        #arrivals = session.query(Arrival).join(Assocaro, Assocaro.orid==orid).\
        rows = session.query(Arrival, Assocaro).filter(Assocaro.orid==orid).\
                                          filter(Arrival.arid==Assocaro.arid).\
                                          order_by(Arrival.sta, Arrival.seedchan).all()
        session.expunge_all()

    for arr, ass in rows:
        #print("%s.%s arr.azimuth=%s ass.seaz=%s" % (arr.sta, arr.seedchan, arr.azimuth, ass.seaz))
        if arr.azimuth is None:
            arr.azimuth = ass.seaz
        if arr.ema is None:
            arr.ema = ass.ema
        arr.distance = ass.delta
        arrivals.append(arr)

    return arrivals


def arrival_already_in_db(aqms_arr, orid):
    """
    See if this aqms arrival is already in the db for this origin orid
    If so, return the arid
    """
    with Session.begin() as session:
        rows = session.query(Arrival).join(Assocaro, Assocaro.arid==Arrival.arid). \
                                      filter(Assocaro.orid==orid).all()
        session.expunge_all()

    for row in rows:
        if row == aqms_arr:
            return row.arid

    return None

def netmag_already_in_db(aqms_mag):
    '''
    '''
    with Session.begin() as session:
        rows = session.query(Netmag).filter(Netmag.orid==aqms_mag.orid).all()
        session.expunge_all()

    for row in rows:
        # compare this Netmag to aqms_mag
        if row == aqms_mag:
            return row.magid

    return None


def focalmec_already_in_db(aqms_mec):
    '''
    '''
    with Session.begin() as session:
        rows = session.query(Mec).filter(Mec.oridin==aqms_mec.oridin).all()
        session.expunge_all()

    for row in rows:
        if row == aqms_mec:
            #print("This is a match!")
            return row.mecid

    return None


def query_assocevents(evid, evidassoc):

    with Session.begin() as session:
        found = session.query(Assocevents).get((evid, evidassoc))
        if found:
            logger.info("query_assocevents: found.evid=%s found.evidassoc=%s" % (found.evid, found.evidassoc))
            return True
    return False

def query_origin_by_evid(evid):
    '''
        Look up event evid in the origin table
        If found, return the preferred origin
    '''
    fname = "query_origin_by_evid"
    origin = None
    # MTH: 2024-01-23 Can't remember why this is here - it certainly is possible to have multiple rows!
    # e.g., eventprefor could have a row for each type (h, a, u, etc)
    with Session.begin() as session:
        try:
            origin = session.query(Origin).join(Eventprefor).filter(Origin.evid==evid).one_or_none()
        except sqlalchemy.exc.MultipleResultsFound as e:
            logger.warning("%s: Caught error:%s" % (fname, e))
            rows = session.query(Eventprefor).filter(Eventprefor.evid==evid).all()
            logger.info("The following rows are in eventprefor:")
            for row in rows:
                logger.info(row)
        session.expunge_all()

    if origin is None:
        logger.info("%s: Will fall back on event.prefor query instead" % fname)
        with Session.begin() as session:
            origin = session.query(Origin).join(Event, Event.evid==evid).filter(Event.prefor==Origin.orid).one_or_none()
            session.expunge_all()

    if origin is None:
        logger.info("%s: Unable to find origin for evid:%d in db ==> return None!" % (fname, evid))
    return origin

def fix_unassoc_amp_leap_seconds(amp_dict):
    '''
        If db epochtimebase.base='T' then add leapsecs to amp datetime + wstart fields
    '''
    epoch = amp_dict['wstart'].timestamp
    orig_utc = amp_dict['wstart']

    with Session.begin() as session:
        # Query to see if this db is applying leapsecs or not
        # Note that epochtimebase table typically has:
        # ondate='1900-01-01' (timestamp=-2208970800) and offdate='3000-01-01' (timetamp=32503698000)
        rows = session.query(Epochtimebase).all()
        session.expunge_all()

    base = None
    for row in rows:
        on  = row.ondate.timestamp()
        off = row.offdate.timestamp()
        if epoch >= on and epoch <= off:
            base = row.base
            break

    if base == 'T':
        #logger.debug("Epochtimebase base=T ==> Apply leapsecs")
        with Session.begin() as session:
            epoch_base=list(session.execute("select truetime.nominal2true(%d)" % int(epoch)))
            session.expunge_all()

        fixed_epoch = epoch_base[0][0]
        leap_secs = fixed_epoch - int(epoch)
        logger.info("fix_unassoc_amp wstart=[%s] --> epoch=[%s] + leap_secs=[%d] = [%s]" % \
                    (amp_dict['wstart'], epoch, leap_secs, (amp_dict['wstart'].timestamp + leap_secs)))
        amp_dict['wstart'] = amp_dict['wstart'].timestamp + leap_secs
        amp_dict['datetime'] = amp_dict['datetime'].timestamp + leap_secs

    elif base == 'N':
        logger.debug("Epochtimebase base=N ==> Do NOT apply leapsecs")
    else:
        logger.error("Epochtimebase base=%s ==> Cannot determine whether to apply leapsecs. Exitting!" % base)
        exit(2)

    return


def fix_leap_seconds(origin, event):
    '''
        If db epochtimebase.base='T' then add leapsecs to origin + pick times
    '''
    leapsecs = get_leapsecs(origin.time.timestamp)

    if leapsecs:
        otime = origin.time
        origin.time += leapsecs
        logger.info("fix_leap: origin.time=[%s] + leapsecs=[%d] = new origin.time=[%s]" % \
                    (otime, leapsecs, origin.time))

        for pick in event.picks:
            pick.time += leapsecs

    return


def delete_assocevent(evid, evidassoc):

    with Session.begin() as session:

        #assocevent = session.query(Assocevents).get((evid, evidassoc)).one_or_none()
        assocevent = session.query(Assocevents).get((evid, evidassoc))

        if assocevent:
            #if assocevent.commid > 0:
            if assocevent.commid:
                #Need to give a line no as part of the Remark key, so either hard code to '1' or delete them all:
                #remark = session.query(Remark).get((assocevent.commid, 1))
                remarks = session.query(Remark).filter_by(commid=assocevent.commid).all()
                for remark in remarks:
                    session.delete(remark)
            session.delete(assocevent)
            session.commit()

    return


def insert_assocevent(evid, evidassoc, remark=None):

    with Session.begin() as session:
        if query_assocevents(evid, evidassoc):
            retmsg = "insert_assocevent: Assocevents(%d, %d) already in Table --> Do nothing" % (evid, evidassoc)
            logger.info(retmsg)
            return {'retval':0, 'retmsg':retmsg}

        if remark:
            rmk = Remark(lineno=1, remark=remark)
            session.add(rmk)
            session.flush()
            assocevent = Assocevents(evid=evid, evidassoc=evidassoc, commid=rmk.commid)
        else:
            assocevent = Assocevents(evid=evid, evidassoc=evidassoc)
        session.add(assocevent)
        try:
            session.commit()
        except exc.SQLAlchemyError as e:
            retmsg = "Could not commit session:%s" % e
            logger.error(retmsg)
            logger.error("Rollback session and exit")
            print(retmsg)
            return {'retval':-1, 'retmsg':retmsg}

    return {'retval':200, 'retmsg':'Success'}

def create_and_insert_assocevents(RSN_evid, assoc_origins):

    RSN_orig = query_origin_by_evid(RSN_evid)

    with Session.begin() as session:

        for orig, comment in assoc_origins:
            if query_assocevents(RSN_orig.evid, orig.evid):
                logger.info("Assocevents(%d, %d) already in Table --> Do nothing" % (RSN_orig.evid, orig.evid))
            #elif RSN_orig.evid == orig.evid:
                #logger.info("Assocevents(%d, %d) evid==evidassoc --> Don't insert" % (RSN_orig.evid, orig.evid))
                #print("Assocevents(%d, %d) evid==evidassoc --> Don't insert" % (RSN_orig.evid, orig.evid))
            else:
                logger.info("Add assocevents(%d, %d)" % (RSN_orig.evid, orig.evid))
                rmk = Remark(lineno=1, remark=comment[:79])
                session.add(rmk)
                session.flush()
                commid = rmk.commid
                logger.info("Add commid=%d remark=%s" % (rmk.commid, comment))
                session.add(Assocevents(evid=RSN_orig.evid, evidassoc=orig.evid, commid=commid))
        try:
            session.commit()
        except exc.SQLAlchemyError as e:
            logger.error("Could not commit session:%s" % e)
            logger.error("Rollback session and exit")

    return


def get_event_info(evid):
    '''
    '''
    fname = 'get_event_info'

    with Session.begin() as session:
        rows = session.query(Event, Origin, Netmag).\
                         filter(Event.evid==evid).\
                         filter(Event.evid==Origin.evid).\
                         filter(Event.prefor==Origin.orid).\
                         filter((Origin.orid==Netmag.orid) | \
                                (Event.prefmag==Netmag.magid)) \
                                .all()
                         #filter(Event.prefmag==Netmag.magid).all()
        session.expunge_all()

    logger.info("%s: query for evid=%s returned %d results" % (fname, evid, len(rows)))

    d = None
    event = None
    if rows:
        event, origin, _netmag = rows[0]
        event.preferred_origin = origin
        origin.leapsecs = get_leapsecs(origin.datetime)

        event.netmags = []
        for _evt, _org, netmag in rows:
            if event.prefmag == netmag.magid:
                event.preferred_magnitude = netmag
            event.netmags.append(netmag)

        event.preferred_origin.leapsecs = get_leapsecs(event.preferred_origin.datetime)

        logger.info("%s: evid:%d found netmag type:%s mag:%.2f" % (fname, evid, netmag.magtype, netmag.magnitude))
    else:
        logger.error("%s: query for evid=%d returned No results --> Return None!" %
                    (fname, evid))

    return event

def get_event_info_prsn(evid):
    '''
    2023-01-11 PRSN db doesn't have event.prefmag set!
    '''
    fname = 'get_event_info'

    with Session.begin() as session:
        rows = session.query(Event, Origin, Netmag).\
                         filter(Event.evid==evid).\
                         filter(Event.evid==Origin.evid).\
                         filter(Event.prefor==Origin.orid).\
                         all()
                         #filter(Event.prefmag==Netmag.magid).all()
        session.expunge_all()

    logger.info("%s: query for evid=%s returned %d results" % (fname, evid, len(rows)))

    d = None
    event = None
    if len(rows) == 1:
        event, origin, netmag = rows[0]
        event.preferred_origin = origin
        event.preferred_magnitude = netmag
        d = {}
        d['origin'] = {}
        d['netmag'] = {}
        d['origin']['datetime'] = origin.datetime
        d['origin']['lat'] = origin.lat
        d['origin']['lon'] = origin.lon
        d['origin']['depth'] = origin.depth
        d['origin']['locevid'] = origin.locevid
        d['origin']['prefmag'] = origin.prefmag
        d['origin']['auth'] = origin.auth
        d['netmag']['auth'] = netmag.auth
        d['netmag']['magnitude'] = netmag.magnitude

    #return d
    return event


def get_credit_aliases():
    '''
    '''
    with Session.begin() as session:
        rows = session.query(Credit_alias).all()
        session.expunge_all()

    aliases = [row.alias for row in rows]
    return aliases

