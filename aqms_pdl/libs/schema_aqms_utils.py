# -*- coding: utf-8 -*-
"""
    Common helpers for Classes that describe AQMS tables

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime
import numpy as np
import sys

import logging
logger = logging.getLogger()

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text
from sqlalchemy import Column, DateTime, Integer, Numeric, String, ForeignKey

# create the base class of all ORM classes
Base = declarative_base()

DEFAULT_ENDDATE = datetime.datetime(3000,1,1)

#FLOAT_DIFF_THRESHOLD = 1e-12
FLOAT_DIFF_THRESHOLD = 1e-10

from sqlalchemy.orm import declarative_mixin
from sqlalchemy.orm import declared_attr

'''
  Defaults aren't applied until Insert or Update in sqlalchemy
  This is a work around so you can use/compare them before that
'''
"""
from aqms_pdl.libs.util_cls import get_tables
def init(self, **kwargs):
    tables = get_tables()
    if 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    cls = getattr(sys.modules[__name__], self.__class__.__name__)
    fix_location(kwargs)
    return super(cls, self).__init__(**kwargs)
"""

def init(self, **kwargs):
    tables = get_tables()
    #for t in sorted(tables.keys()):
        #print(t)
    cls_tbl = tables[self.__tablename__]
    table_cols = [x.name for x in cls_tbl.__table__.c]

    has_offdate = 'offdate' in table_cols
    has_location = 'location' in table_cols
    #print("table:%20s has_location:%s has_offdate:%s" %
          #(self.__tablename__, has_location, has_offdate))

    if has_offdate and 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    if has_location:
        fix_location(kwargs)
    cls = getattr(sys.modules[__name__], self.__class__.__name__)
    return super(cls, self).__init__(**kwargs)

def get_tables():
    Base.TBLNAME_TO_CLASS = {}

    for mapper in Base.registry.mappers:
        cls = mapper.class_
        classname = cls.__name__
        if not classname.startswith('_'):
            tblname = cls.__tablename__
            #print(tblname, cls)
            Base.TBLNAME_TO_CLASS[tblname] = cls
    return Base.TBLNAME_TO_CLASS

#def has_ondate(tablename):


# The location_code coming in from stationxml/obspy may be empty string,
# but we want 2-spaces to represent no location in aqms db:
EMPTY_LOCATION_CODE = "  "
def fix_location(kwargs):
    empty_locations = ['', '--']
    if 'location' not in kwargs or kwargs['location'] in empty_locations:
        kwargs['location'] = EMPTY_LOCATION_CODE


def truncate_strings(cls_tbl, kwargs):

    # Resize any input strings to fit db column length
    for col in cls_tbl.__table__.c:
        name = col.name
        #print(col.name, col.type)
        if isinstance(col.type, String):
            #field_length = cls_tbl.name.type.length
            field_length = col.type.length
            input_string = kwargs.get(name)
            if input_string and len(input_string) > field_length:
                kwargs[name] = input_string[:field_length]
                #print("tablename:%s col:%s ==> Resize input string=[%s] to fit %d varchars=[%s]" %
                               #(cls_tbl.__tablename__, name, input_string, field_length, kwargs[name]))
                logger.warning("tablename:%s col:%s ==> Resize input string=[%s] to fit %d varchars=[%s]" %
                               (cls_tbl.__tablename__, name, input_string, field_length, kwargs[name]))


@declarative_mixin
class MyMixin:
    '''
    Common bits to add to classes that define matching_fields that
      can be compared for equality to decide if 
      table row in memory matches row in db
    '''

    '''
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    net = Column(String(8),  primary_key=True, nullable=False)
    sta = Column(String(6),  primary_key=True, nullable=False)
    #seedchan = Column(String(3), primary_key=True, nullable=False)
    #location = Column(String(2), primary_key=True, nullable=False)
    #ondate = Column('ondate', DateTime, info='timestamp without time zone', primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    offdate = Column(DateTime, default=DEFAULT_ENDDATE)
    lddate  = Column(DateTime, server_default=text('NOW()'))

    __table_args__ = {'mysql_engine': 'InnoDB'}
    __mapper_args__= {'always_refresh': True}
    '''

    def __eq__(self, other):
        matching = True
        exclude_fields = getattr(self, 'exclude_fields_from_comparison', None)
        if exclude_fields:
            logger.debug("Found exclude_fields=%s" % exclude_fields)
            matching_fields = [key for key in self.__table__.columns.keys() if key not in exclude_fields]
        elif getattr(self, 'matching_fields', None):
            matching_fields = self.matching_fields
        else:
            logger.error("Can't compare objects of type:%s without exclude_fields -or- matching_fields" % type(self))
            exit()

        #logger.debug("Use matching_fields=%s" % matching_fields)

        #for field in self.matching_fields:
        for field in matching_fields:
            _matching = check_matching(self, other, field)
            if not _matching:
                matching = False

        return matching

    def __hash__(self):
        return hash(repr(self))

def float_diff_exceeds_thresh(x,y,thresh=FLOAT_DIFF_THRESHOLD):
    """
    How similar do 2 floats need to be for us to call them equal
    """

    if x == 0:
        if y == 0:
            return False
        else:
            return True

    #logger.debug("x=%f y=%f diff=%f thresh=%f" %
                #(x,y,np.abs(x-y)/np.abs(x), thresh))

    return np.abs(x-y)/np.abs(x) > thresh



def check_matching(self, other, field):
    '''
    Compare self.field to other.field
    Return True (self.field == other.field) or False
    '''

    tablename = self.__tablename__

    x = getattr(self, field, None)
    y = getattr(other, field, None)

    matching = True

    table_cols = [x.name for x in self.__table__.c]
    has_scnl = 'seedchan' in table_cols

    if x is not None and y is not None:
        #print("check_matching field:%s x=%s type=%s isinstance float=%s" % (field, x, type(x), isinstance(x, float)))
        #if type(x) is float:
        if isinstance(x, float):
            if float_diff_exceeds_thresh(float(x),y):
                matching = False
                if has_scnl:
                    logger.debug("%s FLOAT DIFF net:%s sta:%s cha:%s loc:%s mismatch: field:%s %s != %s" %
                        (tablename, self.net, self.sta, self.seedchan, self.location, field,
                         getattr(self, field, None), getattr(other, field, None)))
                else:
                    logger.debug("%s FLOAT DIFF mismatch: field:%s %s != %s" %
                        (tablename, field, getattr(self, field, None), getattr(other, field, None)))
        elif x != y:
            if has_scnl:
                logger.debug("%s net:%s sta:%s cha:%s loc:%s mismatch: field:%s %s != %s" %
                    (tablename, self.net, self.sta, self.seedchan, self.location, field,
                     getattr(self, field, None), getattr(other, field, None)))
            matching = False

    elif x is not None:
        if has_scnl:
            logger.debug("%s net:%s sta:%s cha:%s loc:%s other has empty field:%s != self.%s:%s" %
                        (tablename, self.net, self.sta, self.seedchan, self.location, field, field, x))
        matching = False
    elif y is not None:
        if has_scnl:
            logger.debug("%s net:%s sta:%s cha:%s loc:%s self has empty field:%s != other.%s:%s" %
                        (tablename, self.net, self.sta, self.seedchan, self.location, field, field, y))
        matching = False

    #if not matching:
        #print("field:%s x=%s != y=%s" % (field, x, y))

    return matching


#from aqms_pdl.libs.schema_aqms_PI import *
from sqlalchemy.inspection import inspect
def fix_int_fields_MOVED_TO_schema_aqms_PI(aqms_obj):
    """
    Check for table cols that are INTEGER or NUMERIC(n, 0)
      incoming fields (from quakeml/obspy) may not be ints
      We want to fix that here so that in memory object fields
      compare correctly to db rows for __eq__
    """
    print("MTH: fix_int_fields")
    #mapper = inspect(Mec)
    #mapper = inspect(globals()[aqms_obj.__class__.__name__])
    mapper = Mec
    #col = mapper.c.rake1
    for col in mapper.c:
        #print("col.key=%s" % col.key)
        val = getattr(aqms_obj, col.key, None)
        if getattr(aqms_obj, col.key, None) and isinstance(col.type, Numeric):
            precision = col.type.precision
            scale = col.type.scale
            if scale is not None:
                setattr(aqms_obj, col.key, round(val, scale))
                #print("col_key=%s --> val=%s [precision=%s, scale=%s]-->[%s]" %
                        #(col.key, val, precision, scale, getattr(aqms_mec, col.key, None)))
        elif getattr(aqms_obj, col.key, None) and isinstance(col.type, Integer):
            setattr(aqms_obj, col.key, int(round(val, 0)))

    return aqms_obj


def main():
    print("welcome to schema_aqms_utils !")


if __name__ == '__main__':
    main()
