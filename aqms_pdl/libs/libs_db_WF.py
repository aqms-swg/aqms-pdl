# -*- coding: utf-8 -*-
"""
functions for querying AQMS db WF tables

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime
import logging

logger = logging.getLogger(__name__)

from aqms_pdl.libs.schema_aqms_WF import Waveform, AssocWaE, Filename, Waveroots, Subdir


def set_Session_WF(SessionIn):
    global Session
    Session = SessionIn

def get_waveroot():
    '''
    '''
    fname = 'get_waveroot'

    waveroot = None

    with Session.begin() as session:
        rows = session.query(Waveroots).all()
        session.expunge_all()

    for wv in rows:
        if wv.status == 'T':
            waveroot = wv
            logger.info("Use waveroot.status='T'")

    if waveroot is None:
        if len(rows) == 1:
            waveroot = rows[0]
            logger.info("len(waveroot)=1 --> use waveroot status='%s'" % waveroot.status)

    return waveroot

def get_waveform_list(evid, only_accelerometers=True):
    '''
        Return list of aqms Waveform rows for this evid
    '''
    fname = 'get_waveforms'
    waveform_list = []

    with Session.begin() as session:
        rows = session.query(Waveform, AssocWaE).filter(AssocWaE.evid==evid).\
                                                 filter(AssocWaE.wfid==Waveform.wfid).\
                                                 order_by(Waveform.sta, Waveform.seedchan).all()
                                                 #filter(AssocWaE.wfid==Waveform.wfid).all()
        session.expunge_all()

    for wf, assoc in rows:
        if only_accelerometers:
            if wf.seedchan[0:2] == 'HN' or wf.seedchan[0:2] == 'BN':   # Only keep accelerometer channels (HNZ, HNE, etc)
                waveform_list.append(wf)
        else:
                waveform_list.append(wf)

    return waveform_list


def get_subdir(subdirid):
    '''
    '''
    fname = 'get_subdir'
    with Session.begin() as session:
        subdir = session.query(Subdir).filter(Subdir.subdirid==subdirid).one()
        session.expunge_all()

    if subdir:
        logger.debug("get_subdir subdirid:%s returned subdir.subdirname:%s" %
                    (subdirid, subdir.subdirname))
    else:
        logger.error("get_subdir subdirid:%s returned None" % subdirid)

    return subdir

def get_filename(fileid):
    '''
    '''
    fname = 'get_filename'

    with Session.begin() as session:
        filename = session.query(Filename).filter(Filename.fileid==fileid).one()
        session.expunge_all()

    return filename

