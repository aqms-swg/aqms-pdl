# -*- coding: utf-8 -*-
"""
Read in quakeml file + flags and insert into AQMS db
Triggered by PDL

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

class thresh:
    def __init__(self, time, km, depth):
        self.time = time
        self.km = km
        self.depth = depth

class diff:
    def __init__(self, dt, dx, dz):
        self.dt = dt
        self.dx = dx
        self.dz = dz


from aqms_pdl.libs.schema_aqms_utils import Base
"""
from aqms_pdl.libs.schema_aqms_PI import *
from aqms_pdl.libs.schema_aqms_IR import *
from aqms_pdl.libs.schema_aqms_AP import *
from aqms_pdl.libs.schema_aqms_C_ import *
from aqms_pdl.libs.schema_aqms_WF import *
"""

def init(self, **kwargs):
    tables = get_tables()
    #for t in sorted(tables.keys()):
        #print(t)
    cls_tbl = tables[self.__tablename__]
    table_cols = [x.name for x in cls_tbl.__table__.c]

    has_offdate = 'offdate' in table_cols
    has_location = 'location' in table_cols
    print("table:%20s has_location:%s has_offdate:%s" %
          (self.__tablename__, has_location, has_offdate))

    if has_offdate and 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    if has_location:
        fix_location(kwargs)
    cls = getattr(sys.modules[__name__], self.__class__.__name__)
    return super(cls, self).__init__(**kwargs)

def get_tables():
    Base.TBLNAME_TO_CLASS = {}

    for mapper in Base.registry.mappers:
        cls = mapper.class_
        classname = cls.__name__
        if not classname.startswith('_'):
            tblname = cls.__tablename__
            #print(tblname, cls)
            Base.TBLNAME_TO_CLASS[tblname] = cls
    return Base.TBLNAME_TO_CLASS

#def has_ondate(tablename):

