import copy
import logging
import numpy as np
import os
import sys

from timezonefinder import TimezoneFinder
import pytz
import datetime

from obspy.core.event import Catalog
from obspy.core.event.base import CreationInfo
from obspy.core.utcdatetime import UTCDateTime

from aqms_pdl.libs.libs_aqms_to_obspy import aqms_event_to_obspy, aqms_origin_to_obspy
from aqms_pdl.libs.libs_aqms_to_obspy import aqms_mec_to_obspy, aqms_netmag_to_obspy
from aqms_pdl.libs.date_formats import format_date


def write_quakeml(events, resource_id_base, ns_catalog):

    qml_events = []
    for event in events:
        event.remark=None
        evid = event.evid
        dataid = "%s%d" % (event.auth, evid)
        extra = {'datasource': {'value': event.auth, 'type': 'attribute', 'namespace': ns_catalog},
                 'dataid': {'value': dataid, 'type': 'attribute', 'namespace': ns_catalog},
                 'eventid': {'value': evid, 'type': 'attribute', 'namespace': ns_catalog},
                 'eventsource': {'value': event.auth, 'type': 'attribute', 'namespace': ns_catalog}
                }

        qml_event = aqms_event_to_obspy(event, prefix=os.path.join(resource_id_base,
                                               "Event/%s/%s" % (event.auth, str(event.evid))))
        qml_event.extra = extra

        qml_event.origins = []
        if len(event.origins) == 0:
            print("evid:%d has no origins!" % event.evid)
            exit()
        for origin in event.origins:
            qml_origin = aqms_origin_to_obspy(origin,
                            prefix=os.path.join(resource_id_base, "Origin/%s/%s" %
                                               (origin.auth, str(origin.orid)))
                         )
            qml_origin.extra = copy.deepcopy(extra)
            earth_model_id = os.path.join(resource_id_base,"velmodel/%s/%s"%(origin.auth,origin.crust_model))
            qml_origin.extra['earth_model_id'] = {'value': earth_model_id, 'type': 'attribute', 'namespace': ns_catalog}
            qml_origin.arrivals = origin.arrivals
            qml_event.origins.append(qml_origin)

            if origin.orid == event.prefor:
                qml_event.preferred_origin_id = qml_origin.resource_id

        qml_event.magnitudes = []
        for netmag in event.netmags:
            prefix=os.path.join(resource_id_base, "Netmag/%s/%s" % (netmag.auth, str(netmag.magid)))
            qml_magnitude = aqms_netmag_to_obspy(netmag, prefix=prefix)
            qml_magnitude.origin_id = os.path.join(resource_id_base, "Origin/%s/%s" % (origin.auth, str(netmag.orid)))

            if getattr(netmag, 'station_magnitude_contributions', None):
                qml_magnitude.station_magnitude_contributions = netmag.station_magnitude_contributions
            qml_event.magnitudes.append(qml_magnitude)

            if netmag.magid == event.prefmag:
                qml_event.preferred_magnitude_id = qml_magnitude.resource_id

        if getattr(event, 'picks', None):
            qml_event.picks = event.picks
        if getattr(event, 'amplitudes', None):
            qml_event.amplitudes = event.amplitudes
        if getattr(event, 'station_magnitudes', None):
            qml_event.station_magnitudes = event.station_magnitudes
        if getattr(event, 'focal_mechanisms', None) and len(event.focal_mechanisms):
            qml_event.focal_mechanisms = event.focal_mechanisms

        qml_events.append(qml_event)

    creation_info = CreationInfo(agency_id=events[0].auth, creation_time=UTCDateTime.utcnow())

    return Catalog(events=qml_events, creation_info=creation_info)


from dataclasses import dataclass
@dataclass
class missing_netmag():
    magtype = "U"
    # MTH: 2024-12-06 For now we return missing magnitude=0. to not break the eqk portal
    #                 even though this is a terrible idea.  Once that POS gets fixed, revert
    #                 to N/A !
    #missing_magnitude_string = "N/A"
    missing_magnitude_string = "0."
    missing_magtype_string = " "
    magnitude = 0.
    lddate = None
    nsta = None
    uncertainty = None
    subsource = None
    magalgo = None
    def __init__(self, auth="N/A"):
        self.auth = auth


def write_text(events, catalog):
    """
    This is equivalent to: dbselect -F detail -I | event_text.pl

#EventID | Time | Latitude | Longitude | Depth/km | Author | Catalog | Contributor | ContributorID | MagType | Magnitude | MagAuthor | EventLocationName
10001|2024-03-21T17:47:24.3100|31.94867|-102.03983|6.940|PR|EGS|hypomag||Ml|2.9|PR|
10011|2024-03-23T00:27:15.1900|32.41633|-101.90134|4.790|PR|EGS|hypomag||Ml|0.6|PR|

Note: EventLocationName = Flinn-Engdahl region
      This is not implemented - apparently even USGS no longer uses this
    """

    text = []
    hdr_str = "#EventID | Time | Latitude | Longitude | Depth/km | Author | Catalog | Contributor | ContributorID | MagType | Magnitude | MagAuthor | EventLocationName"
    text.append(hdr_str)
    for event in events:
        origin = event.preferred_origin
        #netmag = event.preferred_magnitude if getattr(event, 'preferred_magnitude', None) else missing_netmag(auth=origin.auth)
        netmag = event.preferred_magnitude
        magnitude = "%.1f" % netmag.magnitude if not isinstance(netmag, missing_netmag) else netmag.missing_magnitude_string

        ot = format_date(UTCDateTime(origin.datetime - origin.leapsecs, precision=7), include_T=True, ndigits=4)

        #Catalog = Identifier of source catalog (= db name)
        #Contributor = Identifier of event information contributor
        #ContributorID = Event ID as reported by the contributor

        contributor = event.auth
        contributorID = event.evid

        if origin.subsource.upper() == 'PDL':
            contributorID = origin.locevid

        text.append("%d|%s|%.5f|%.5f|%.3f|%s|%s|%s|%s|M%s|%s|%s|" %
              (event.evid, ot, origin.lat, origin.lon, origin.depth,
               origin.auth, catalog, contributor, contributorID,
               netmag.magtype, magnitude, netmag.auth))

    return '\n'.join(text)


def write_phase(events):
    """
    This is equivalent to: get_all_phaseinfo.py

    events = list of aqms events
    event.origins = list of aqms.origins
    origin.arrivals = list of *obspy* Arrivals
    """

    text = []
    hdr_str = "net,sta,chan,loc,phase,residual,distance(km),weight,quality,traveltime(s),arrivaltime,evid"
    text.append(hdr_str)
    for event in events:
        picks = event.picks
        for origin in event.origins:
            for arr in origin.arrivals:
                pick = arr.pick_id.get_referred_object()
                w = pick.waveform_id
                net, sta, cha, loc = w.network_code, w.station_code, w.channel_code, w.location_code
                # MTH: obspy arr time should not have leapsecs
                #      but aqms origin still does
                ot = origin.datetime - origin.leapsecs
                tt = pick.time.timestamp - ot
                qlty = ""
                if arr.comments:
                    comment = arr.comments[0].text
                    if 'quality' in comment:
                        fields = comment.split()
                        for field in fields:
                            if 'quality' in field:
                                qlty = field.split(':')[1]
                            elif 'arid' in field:
                                arid = field.split(':')[1]
                line = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%.3f,%s,%s" % \
                    (net, sta, cha, loc, arr.phase, arr.time_residual, arr.distance, arr.time_weight,
                     qlty, tt, pick.time.timestamp,event.evid)
                text.append(line)

    return '\n'.join(text)


def write_ncedc(events, debug=False):
    """
    (aqms-python) [aqms@tx-ovintiv-presidio ~]$ dbselect -F ncedc | head
    Date       Time              Lat        Lon   Depth   Mag Magt  Nst Gap  Clo  RMS  SRC   Event ID
    -------------------------------------------------------------------------------------------------
    2024/03/21 17:47:25.27  32.01500 -102.04583   8.260  2.91   ML   15 255   14 0.20   PR      10001
    2024/03/23 00:27:15.19  32.41633 -101.90134   4.790  0.64   ML    9 136    5 0.05   PR      10011
    2024/03/23 02:50:31.29  32.40467 -102.05917   6.480  1.43   ML   29 121    8 0.13   PR      10016
    2024/03/23 06:20:21.30  32.25950 -101.93350   6.210  1.07   ML   26  48    4 0.10   PR      10021
    2024/03/23 07:36:02.24  32.26017 -101.93517   6.170  2.90   ML   37  48    4 0.10   PR      10026

                                                 magt ndf gap dmin sdob sour evid qloc
 867  printf ("%.19s%.3s%10.5f %10.5f %7.3f %5.2f %4s %4d%4.0f%5.0f%5.2f %4s %10d %s\n",
 868    ndatetime, cnb_sec, lat, lon, depth, magnitude, ncedc_magtype, ndef, gap, distance,
 869    sdobs, ncedc_source, evid, qlocation);
 870 }
    """

    fmt = {
        'lat': '10.5f', 'lon': '10.5f', 'dep': '7.3f',
        'mag': '5.2f', 'ncedc_magtype':'>4s', 'ncedc_source': '>4s',
        'ndef': '4d', 'evid': '10d',
        'gap': '4.0f', 'dmin': '5.0f', 'rms': '5.2f',
        'qloc': 's',
    }

    ncedc_magtypes = {'a':'Mx', 'd':'Md', 'dl':'Mdl', 'h':'Mh', 'l':'ML', 'w':'Mw'}

    text = []
    hdr_str = "Date       Time              Lat        Lon   Depth   Mag Magt  Nst Gap  Clo  RMS  SRC   Event ID"
    text.append(hdr_str)
    text.append("-" * len(hdr_str))
    dbselect_example = f"2024/03/21 17:47:25.27  32.01500 -102.04583   8.260  2.91   ML   15 255   14 0.20   PR      10001"
    if debug:
        text.append(dbselect_example)
        events.insert(0, example_event())

    for event in events:
        origin = event.preferred_origin
        netmag = event.preferred_magnitude
        magtype   = "%s" % netmag.magtype if not isinstance(netmag, missing_netmag) else netmag.missing_magtype_string
        ncedc_magtype = ncedc_magtypes[magtype] if magtype in ncedc_magtypes else 'Unk'
        ncedc_source = "NCSN" if origin.auth == 'NC' else origin.auth

        ot = format_date(UTCDateTime(origin.datetime - origin.leapsecs, precision=7), ndigits=2, date_separator='/')

        dmin = f"{origin.distance:{fmt['dmin']}}" if origin.distance else " " * get_len(fmt['dmin'])
        gap  = f"{origin.gap:{fmt['gap']}}" if origin.gap else " " * get_len(fmt['gap'])
        ndef = f"{origin.ndef:{fmt['ndef']}}" if origin.ndef else " " * get_len(fmt['ndef'])
        rms  = f"{origin.wrms:{fmt['rms']}}" if origin.wrms else " " * get_len(fmt['rms'])

        line = (
            f"{ot}{origin.lat:{fmt['lat']}} {origin.lon:{fmt['lon']}} {origin.depth:{fmt['dep']}} "
            f"{netmag.magnitude:{fmt['mag']}} {ncedc_magtype:{fmt['ncedc_magtype']}} "
            f"{ndef}{gap}{dmin}{rms} "
            f"{ncedc_source:{fmt['ncedc_source']}} {event.evid:{fmt['evid']}} "
        )

        text.append(line)

    return '\n'.join(text)



def write_detail(events, debug=False):
    """
    This is equivalent to: dbselect -F detail
  Date       Time            Lat      Lon        Depth   SRC  Subsrc   Gap    Clo    Ndef ErTim RMS  ErHor ErDep  Qual  Mag   Mt  Nst DbId     JtId       Typ SF
  --------------------------------------------------------------------------------------------------------------------------------------------------------------
  2024/03/21 17:47:25.2700   32.01500 -102.04583 8.260   PR   hypomag  255.00 14.00  15   0.00  0.20  0.99  2.11   0.75  2.91  l   007    10001            re  1
  2024/03/23 00:27:15.1900   32.41633 -101.90134 4.790   PR   hypomag  136.00 5.00   9    0.00  0.05  0.42  1.19   1.00  0.64  l   008    10011            re  1
  2024/03/23 02:50:31.2900   32.40467 -102.05917 6.480   PR   hypomag  121.00 8.00   29   0.00  0.13  0.28  0.53   1.00  1.43  l   015    10016            re  1

                                            auth subsrc  gap   dmin  ndef stime  sdobs  erhor  sdep   quality  mag  magt  nsta evid
1116  printf ("%.19s%s %10.5f %10.5f %-7.3f %-4s %-8s %-6.2f %-6.2f %-4d %-5.2f %-5.2f %-5.2f %-6.2f %-5.2f %-5.2f %-3s %03d %8d %-10s %-2s  %d %s\n"
1117          ndatetime, cnb_sec, lat, lon, depth, auth, subsource, gap, distance, ndef, sttime, sdobs,
1118          erhor, sdepth, quality, magnitude, detail_magtype, nsta, evid, locevid, new_etype, slctflg, qlocation);
1119 }

    """
    fmt = {
        'lat': '10.5f',
        'lon': '10.5f',
        'dep': '<7.3f',
        'auth': '<4s',
        'subsrc': '<8s',
        'gap': '<6.2f',
        'dmin': '<6.2f',
        'ndef': '<4d',
        'rms': '<5.2f',
        'erhor': '<5.2f',
        'erver': '<6.2f',
        'ertim': '<5.2f',
        'mag': '<5.2f',
        'magtype':'<3s',
        'qual': '<5.2f',
        'nsta': '03d',
        'evid': '8d',
        'locevid': '<10s',
    }
    text = []
    hdr_str = "Date       Time            Lat      Lon        Depth   SRC  Subsrc   Gap    Clo    Ndef ErTim RMS  ErHor ErDep  Qual  Mag   Mt  Nst DbId     JtId       Typ SF"
    text.append(hdr_str)
    text.append("-" * len(hdr_str))

    dbselect_example = (
        f"2024/03/21 17:47:25.2700   32.01500 -102.04583 8.260   PR   hypomag  255.00 14.00  15   0.00  0.20  0.99  2.11   0.75  2.91  l   007    10001            re  1"
    )
    if debug:
        text.append(dbselect_example)
        events.insert(0, example_event())

    for event in events:
        origin = event.preferred_origin
        #netmag = event.preferred_magnitude if getattr(event, 'preferred_magnitude', None) else missing_netmag(auth=origin.auth)
        netmag = event.preferred_magnitude
        ot = format_date(UTCDateTime(origin.datetime - origin.leapsecs, precision=7), ndigits=4, date_separator='/')

        new_etype = get_new_etype(event, origin)

        dmin = f"{origin.distance:{fmt['dmin']}}" if origin.distance else " " * get_len(fmt['dmin'])
        gap  = f"{origin.gap:{fmt['gap']}}" if origin.gap else " " * get_len(fmt['gap'])
        ndef = f"{origin.ndef:{fmt['ndef']}}" if origin.ndef else " " * get_len(fmt['ndef'])
        stime= f"{origin.stime:{fmt['ertim']}}" if origin.stime else " " * get_len(fmt['ertim'])
        rms  = f"{origin.wrms:{fmt['rms']}}" if origin.wrms else " " * get_len(fmt['rms'])
        erhor= f"{origin.erhor:{fmt['erhor']}}" if origin.erhor else " " * get_len(fmt['erhor'])
        erver= f"{origin.sdep:{fmt['erver']}}" if origin.sdep else " " * get_len(fmt['erver'])
        qual = f"{origin.quality:{fmt['qual']}}" if origin.quality else " " * get_len(fmt['qual'])
        nsta = f"{netmag.nsta:{fmt['nsta']}}" if netmag.nsta else " " * get_len(fmt['nsta'])
        locevid = f"{origin.locevid:{fmt['locevid']}}" if origin.locevid else " " * get_len(fmt['locevid'])

        line = (
            f"{ot.replace('T',' ')} {origin.lat:{fmt['lat']}} {origin.lon:{fmt['lon']}} {origin.depth:{fmt['dep']}} {origin.auth:{fmt['auth']}} {origin.subsource:{fmt['subsrc']}} "
            f"{gap} {dmin} {ndef} {stime} {rms} "
            f"{erhor} {erver} {qual} "
            f"{netmag.magnitude:{fmt['mag']}} {netmag.magtype:{fmt['magtype']}} {nsta} {event.evid:{fmt['evid']}} "
            f"{locevid} {new_etype}  {event.selectflag}"
        )

        text.append(line)


    return '\n'.join(text)


def write_geojson(events, url_call=None):

    """
{"type":"Feature","properties":{
"mag":0.06,
"time":1735881032,
"tz":-600,
"net":"hv",
"code":45221,
"nst":7,
"rms":0.04,
"gap":79.00,
"magType":"l",
"etype":"le",
"sources":"PR",
"subSrc":"hypomag",
"erHor":0.29,
"erDep":1.05,
"ndef":0,
"qual":1,
"clos":4
},
"geometry":{
"type":"Point",
"coordinates":[
-102.04517,
32.40583,
4.97
]},
"id":"45221"
},
    """

    metadata = {'generated':UTCDateTime.now().timestamp,
                'title': "ISTI Earthquakes",
                'status':200,
                'count':len(events)}

    if url_call:
        metadata['url'] = url_call

    geojson = {'type':'FeatureCollection',
               'metadata':metadata,
               'features':[]
               }

    for event in events:
        origin = event.preferred_origin
        netmag = event.preferred_magnitude

        new_etype = get_new_etype(event, origin)

        feature = {'type':'Feature'}
        feature['properties'] = {}
        feature['geometry'] = {}
        feature['id'] = event.evid

        pdict = feature['properties']
        pdict['type'] = "earthquake"
        pdict['time'] = origin.datetime - origin.leapsecs
        pdict['tz'] = get_offset_mins(origin.lat, origin.lon)

        pdict['mag'] = netmag.magnitude
        pdict['magType'] = f"M{netmag.magtype}"

        pdict['status'] = "reviewed" if origin.rflag.lower() in ['h','f'] else "preliminary"

        pdict['net'] = origin.auth
        pdict['code'] = event.evid
        pdict['nst'] = netmag.nsta

        if origin.distance:
            pdict['dmin'] = origin.distance
        if origin.gap:
            pdict['gap'] = origin.gap
        if origin.ndef:
            pdict['ndef'] = origin.ndef
        if origin.wrms:
            pdict['rms'] = origin.wrms
        if origin.subsource:
            pdict['subSrc'] = origin.subsource
        if origin.erhor:
            pdict['erHor'] = origin.erhor
        if origin.sdep:
            pdict['erDep'] = origin.sdep
        if origin.ndef:
            pdict['ndef'] = origin.ndef
        if origin.quality:
            pdict['qual'] = origin.quality

        gdict = feature['geometry']
        gdict['type'] = "Point"
        gdict['coordinates'] = [origin.lon, origin.lat, origin.depth]

        geojson['features'].append(feature)

    return geojson


def write_json(events):

    """
[{"event":{
    "rss_utc":"Tue Jan 28 17:04:43 EST 2025",
    "depth_km":"8.260",
    "depth_mi":"5.1",
    "auth":"HV",
    "version":"1",
    "evid":"10001",
    "lng":"-102.04583",
    "lat":"32.01500",
    "magnitude":"2.91",
    "event_time_utc":"2024/03/21 17:47:25",
    "etype":"le",
    "event_time_local":"2024/03/21 07:47:25",
    "event_time_epoch":"1711043245"
}},{"event":{
    """

    miles_per_km = 1./1.60934

    json_dicts = []
    for event in events:
        origin = event.preferred_origin
        netmag = event.preferred_magnitude
        ot = UTCDateTime(origin.datetime - origin.leapsecs, precision=7)
        offset_mins = get_offset_mins(origin.lat, origin.lon)
        local_ot = ot + float(offset_mins)*60

        e = {}
        e['rss_utc'] = UTCDateTime.now().datetime.strftime("%a %b %-d %H:%M:%S %Z %Y")
        e['depth_km'] = f"{origin.depth:{'.3f'}}"
        e['depth_mi'] = f"{origin.depth * miles_per_km : {'.1f'}}"
        e['auth'] = origin.auth
        e['version'] = event.version
        e['evid'] = event.evid
        e['lng'] = f"{origin.lon:{'.5f'}}"
        e['lat'] = f"{origin.lat:{'.5f'}}"
        #e['magnitude'] = netmag.magnitude
        e['magnitude'] = f"{netmag.magnitude:{'.2f'}}"
        e['event_time_utc'] = format_date(ot, ndigits=0, include_T=False, date_separator='/')
        e['event_time_local'] = format_date(local_ot, ndigits=0, include_T=False, date_separator='/')
        e['event_time_epoch'] = ot.timestamp
        e['etype'] = get_new_etype(event, origin)
        json_dicts.append({'event':e})

    return json_dicts


def write_csv(events):

#time,latitude,longitude,depth,mag,magType,nst,gap,dmin,rms,net,id,updated,place,type,horizontalError,depthError,magError,magNst,status,locationSource,magSource,eventSource
#                                                       nst,gap,dmin,rms,net,id,updated,place,type,horizontalError,depthError,magError,magNst,status,locationSource,magSource,eventSource
#2019-12-08T21:32:44.860Z,35.88417,-97.41634,5.810,2.05,l,23,77.00,10.00,0.08,OH,70000003,2019-12-09T06:19:23.000Z,"",eq,0.23,0.44,0.20,17,I,OH,OK,RT1
#2019-12-09T04:46:04.130Z,36.26150,-97.49866,7.650,1.34,l,14,101.00,50.00,0.13,OK,70000008,2019-12-09T06:15:16.000Z,"",eq,0.40,16.36,0.13,4,I,OK,OK,RT1
    s = ','
    fmt = {
        'lat': '.5f',
        'lon': '.5f',
        'dep': '.3f',
        'gap': '.2f',
        'dmin': '.2f',
        'rms': '.2f',
        'erhor': '.2f',
        'erver': '.2f',
        'mag': '.2f',
        'magerr': '.2f',
    }
    header_cols = ["time","latitude","longitude","depth","mag","magType","nst","gap",
                   "dmin","rms","net","id","updated","place","type",
                   "horizontalError","depthError","magError","magNst","status",
                   "locationSource","magSource","eventSource",
                   ]
    csv_lines = []
    csv_lines.append(','.join(header_cols))

    # MTH: dbselect returns 0.0 when the following are missing: origin.gap, origin.distance, origin.rms, origin.erhor, origin.sdep,
    #                                                           netmag.uncertainty, netmag.nsta - e.g., when netmag itself is missing!
    missing = ""
    missing_float = 0.
    missing_int = 0

    for event in events:
        origin = event.preferred_origin
        netmag = event.preferred_magnitude
        ot = format_date(UTCDateTime(origin.datetime - origin.leapsecs, precision=7), include_T=True, include_Z=True, ndigits=3)

        gap  = origin.gap if origin.gap else missing_float
        dmin = origin.distance if origin.distance else missing_float
        rms  = origin.wrms if origin.wrms else missing_float
        erhor = origin.erhor if origin.erhor else missing_float
        erver = origin.sdep if origin.sdep else missing_float
        magerr = netmag.uncertainty if netmag.uncertainty else missing_float
        nsta   = origin.ndef if origin.ndef else missing_int
        magauth = netmag.auth if netmag.magtype != 'U' else missing
        magtype = netmag.magtype if netmag.magtype != 'U' else "Unk"
        magnsta = netmag.nsta if netmag.nsta else missing_int

        orflag= origin.rflag
        update_date = max([event.lddate, origin.lddate, netmag.lddate]) if netmag.lddate \
                 else max([event.lddate, origin.lddate])
        update_date = format_date(UTCDateTime(update_date, precision=7), include_T=True, include_Z=True, ndigits=3)
        #qlocation
        # EXEC SQL SELECT WHERES.getNearestTown(:lat, :lon) INTO :qlocation:qlocation_ind FROM DUAL;
        place='""'
        net=origin.auth

        """
dbselect.pc

1014  if ((!strcmp(mag_type,"")) || (!strcmp(mag_type,"n")))
1015         strcpy (detail_magtype, "Unk");
1016  else   strcpy (detail_magtype, mag_type);
1017
1018  /* Update date */
1019  if (strcmp (olddate, mlddate) > 0)
1020   strcpy (update_date, olddate);
1021  else   strcpy (update_date, mlddate);
1022  update_date[4] = '-';
1023  update_date[7] = '-';
1024  update_date[10] = 'T';
1025
1026  printf ("%.19s%s,%.5f,%.5f,%.3f,%.2f,%s,%d,%.2f,%.2f,%.2f,%s,%d,%.19s.000Z,\"%s\",%s,%.2f,%.2f,%.2f,%d,%c,%s,%s",
1027          ndatetime, cnb_sec, lat, lon, depth, magnitude, detail_magtype, ndef, gap, distance, sdobs, auth, evid,
1028    update_date ,qlocation, eventype, erhor, sdepth, maguncertainty, nsta, orflag, auth, magauth);
1029
1030  if (includeEventSource)
1031   printf(",%s", esubsource);
1032  puts("");
        """
        line = (
            f"{ot}{s}{origin.lat:{fmt['lat']}}{s}{origin.lon:{fmt['lon']}}{s}{origin.depth:{fmt['dep']}}"
            f"{s}{netmag.magnitude:{fmt['mag']}}{s}{magtype}"
            f"{s}{nsta}{s}{gap:{fmt['gap']}}{s}{dmin:{fmt['dmin']}}{s}{rms:{fmt['rms']}}"
            f"{s}{net}{s}{event.evid}{s}{update_date}{s}{place}{s}{event.etype}"
            f"{s}{erhor:{fmt['erhor']}}{s}{erver:{fmt['erver']}}{s}{magerr:{fmt['magerr']}}"
            f"{s}{magnsta}{s}{origin.rflag}{s}{origin.auth}{s}{magauth}{s}{event.subsource}"
        )
        csv_lines.append(line)

    return '\n'.join(csv_lines)


def write_fpfit(events, mechtype='FP'):

    fmt = {
        'latd': '2d', 'latm': '5.2f',
        'lond': '3d', 'lonm': '5.2f',
        'dep' : '>7.2f',
        'gap' : '4d',
        'dmin': '5.1f',
        'rms' : '5.2f',
        'herror': '5.1f',
        'zerror': '5.1f',
        'strike': '3d', 'dip': '2d', 'rake':'4d',
        'unstrike': '2d', 'undip': '2d', 'unrake':'2d',
        'ndef': '3d',
        'pvr' : '4.2f',
        'mag' : '7.2f',
        'magtype': '>3',
    }

    missing = ""

    lines = []

# 10016 | 24636 | 2024/03/23 02:50:31.2900 | 32.404666666666664 | -102.05916666666667
#                                          dep        ndef gap dist wrms erhor sdepl st  d  rak  pvr    nsta               us ud ur       evid
# 20240323 0250 31.29 32 24.28 102  3.55   6.48   1.43 29 121  8.0 0.13  0.3  0.5  L 215 55 -50  0.00   9                  43 35 35       10016

    nlat = ' ' # This could get set to 'N' or 'S'
    nlon = ' ' # This could get set to 'E' or 'W'

    #print("20240323 0250 31.29 32 24.28 102  3.55   6.48   1.43 29 121  8.0 0.13  0.3  0.5  L 215 55 -50  0.00   9                  43 35 35       10016")

    for event in events:
        origin = event.preferred_origin
        netmag = event.preferred_magnitude

        match(netmag.magtype):
            case 'a':
                magtype = 'X'
            case 'w'|'l'|'h'|'B':
                magtype = netmag.magtype.upper()
            case 'm':
                magtype = 'B'
            case _:
                magtype = ' '
            #case 'd':

        ndef  = f"{origin.ndef:{fmt['ndef']}}" if origin.ndef else missing_str(fmt['ndef'])
        gap   = f"{int(origin.gap):{fmt['gap']}}" if origin.gap else missing_str(fmt['gap'])
        # MTH: dbselect -F fpfit defaults to dmin=0.0 when origin.distance is null
        dmin  = f"{origin.distance:{fmt['dmin']}}" if origin.distance else missing_str(fmt['dmin'])
        rms   = f"{origin.wrms:{fmt['rms']}}" if origin.wrms else missing_str(fmt['rms'])
        erhor = f"{origin.erhor:{fmt['herror']}}" if origin.erhor else missing_str(fmt['herror'])
        erver = f"{origin.sdep:{fmt['zerror']}}" if origin.sdep else missing_str(fmt['zerror'])

        ot = UTCDateTime(origin.datetime - origin.leapsecs)
        yyyymmdd = ot.datetime.strftime("%Y%m%d")
        hhmm = ot.datetime.strftime("%H%M")
        sec = "%05.2f" % float(ot.datetime.strftime("%S.%f"))

        lat = np.abs(origin.lat)
        lon = np.abs(origin.lon)

        latd  = f"{int(lat):{fmt['latd']}}"
        latm  = f"{(lat - int(lat))*60.:{fmt['latm']}}"
        lond  = f"{int(lon):{fmt['lond']}}"
        lonm  = f"{(lon - int(lon))*60.:{fmt['lonm']}}"

        prefmecid = getattr(event, 'prefmec', None)

        for i, mec in enumerate(event.mecs):

            print_asterisk = False

            if len(event.mecs) > 1:
                if prefmecid:
                    if mec.mecid == prefmecid:
                        print_asterisk = True
                elif i == len(event.mecs)-1:
                    print_asterisk = True

            asterisk = '*' if print_asterisk else ' '

            #print("i:%d mecid:%d" % (i, mec.mecid))

            if mec.mechtype == mechtype:
                strike1 = mec.strike1 + 90
                strike1 %= 360
                pvr  = "%4.2f" % ((100 - mec.pvr)/100.) if mec.pvr else "    "
                nsta = "%3d" % mec.nsta if mec.nsta else "    "

                unstrike1 = f"{int(mec.unstrike1):{fmt['unstrike']}}" if mec.unstrike1 else ""
                undip1 = f"{int(mec.undip1):{fmt['undip']}}" if mec.undip1 else ""
                unrake1 = f"{int(mec.unrake1):{fmt['unrake']}}" if mec.unrake1 else ""

                line = (
                    f"{yyyymmdd} {hhmm} {sec} {latd}{nlat}{latm} {lond}{nlon}{lonm}"
                    f"{origin.depth:{fmt['dep']}}{netmag.magnitude:{fmt['mag']}}{ndef}{gap}{dmin}{rms}{erhor}{erver}"
                    f"{magtype:{fmt['magtype']}} "
                    f"{strike1:{fmt['strike']}} {mec.dip1:{fmt['dip']}}{mec.rake1:{fmt['rake']}}"
                    f"  {pvr} {nsta}"
                    f"{18 * ' '}"
                    #f"{int(mec.unstrike1):{fmt['unstrike']}} {int(mec.undip1):{fmt['undip']}} {int(mec.unrake1):{fmt['unrake']}}"
                    f"{unstrike1} {undip1} {unrake1}"
                    f" {asterisk}  "
                    #f"{4 * ' '}"
                    #f"{7 * ' '}"
                    f"{event.evid}"
                )
                lines.append(line)

    return '\n'.join(lines)


def write_kml(events):

    kml = get_kml_preamble()

    for event in events:
        origin = event.preferred_origin
        netmag = event.preferred_magnitude

        ot = format_date(UTCDateTime(origin.datetime - origin.leapsecs, precision=7), ndigits=4,
                         date_separator='-', include_T=True)

        gap  = origin.gap if origin.gap else ""
        ndef = origin.ndef if origin.ndef else ""
        dmin = f"{origin.distance:{'.1f'}}" if origin.distance else ""
        rms  = f"{origin.wrms:{'.2f'}}" if origin.wrms else ""
        erhor= f"{origin.erhor:{'.2f'}}" if origin.erhor else ""
        erver= f"{origin.sdep:{'.2f'}}" if origin.sdep else ""

        text = (
            f"    <Placemark>\n"
            f"    <styleUrl>#pointStyleMap</styleUrl>\n"
            f"        <ExtendedData>\n"
            f"        <SchemaData schemaUrl=\"#HypoSummary\">\n"
            f"            <SimpleData name=\"Evid\"> {event.evid} </SimpleData>\n"
            f"            <SimpleData name=\"Origin\">{ot}</SimpleData>\n"
            f"            <SimpleData name=\"Lat\">{origin.lat}</SimpleData>\n"
            f"            <SimpleData name=\"Long\">{origin.lon}</SimpleData>\n"
            f"            <SimpleData name=\"Depth\">{origin.depth}</SimpleData>\n"
            f"            <SimpleData name=\"MAG\">{netmag.magnitude}</SimpleData>\n"
            f"            <SimpleData name=\"Number_Picks\">{ndef}</SimpleData>\n"
            f"            <SimpleData name=\"Azimuthal_Gap\">{gap}</SimpleData>\n"
            f"            <SimpleData name=\"Nearest_Station\">{dmin}</SimpleData>\n"
            f"            <SimpleData name=\"RMS\">{rms}</SimpleData>\n"
            f"            <SimpleData name=\"Error_H\">{erhor}</SimpleData>\n"
            f"            <SimpleData name=\"Error_Z\">{erver}</SimpleData>\n"
            f"        </SchemaData> \n"
            f"        </ExtendedData> \n"
            f"        <Point> \n"
            f"            <coordinates>{origin.lon}, {origin.lat} </coordinates> \n"
            f"        </Point> \n"
            f"    </Placemark>\n"
        )
        kml += text
    kml += f"</Document></kml>"

    return kml


def get_kml_preamble():

    kml_preamble="""<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
<Document>
  <name>Hypoinverse Location</name>
  <Schema name="HypoinverseSummary" id="HypoSummary">
    <SimpleField type="string" name="Evid"><displayName>&lt;b&gt;Evid&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="string" name="Origin"><displayName>&lt;b&gt;Origin&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="double" name="Lat"><displayName>&lt;b&gt;Lat&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="double" name="Long"><displayName>&lt;b&gt;Long&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="double" name="Depth"><displayName>&lt;b&gt;Depth&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="double" name="MAG"><displayName>&lt;b&gt;MAG (Md)&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="int" name="Number_Picks"><displayName>&lt;b&gt;Number Picks&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="int" name="Azimuthal_Gap"><displayName>&lt;b&gt;Azimuthal Gap&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="double" name="Nearest_Station"><displayName>&lt;b&gt;Nearest Station&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="double" name="RMS"><displayName>&lt;b&gt;RMS&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="double" name="Error_H"><displayName>&lt;b&gt;Error-H&lt;/b&gt;</displayName></SimpleField>
    <SimpleField type="double" name="Error_Z"><displayName>&lt;b&gt;Error-Z&lt;/b&gt;</displayName></SimpleField>
  </Schema>
  <StyleMap id="pointStyleMap">
    <Pair>
        <key>normal</key>
        <styleUrl>#hlightPointStyle</styleUrl>
    </Pair>
    <Pair>
        <key>highlight</key>
        <styleUrl>#normPointStyle</styleUrl>
    </Pair>
  </StyleMap>
  <Style id="normPointStyle">
    <IconStyle>
        <color>ff00ffff</color>
        <scale>2.0</scale>
        <Icon> <href>http://maps.google.com/mapfiles/kml/shapes/shaded_dot.png</href> </Icon>
    </IconStyle>
    <BalloonStyle>
        <text><![CDATA[<table border="0">
            <tr><td><b>Origin</b></td><td>$[HypoinverseSummary/Origin]</td></tr>
            <tr><td><b>Lat</b></td><td>$[HypoinverseSummary/Lat]</td></tr>
            <tr><td><b>Long</b></td><td>$[HypoinverseSummary/Long]</td></tr>
            <tr><td><b>Depth</b></td><td>$[HypoinverseSummary/Depth]</td></tr>
            <tr><td><b>MAG (Md)</b></td><td>$[HypoinverseSummary/MAG]</td></tr>
            <tr><td><b>Number Picks</b></td><td>$[HypoinverseSummary/Number_Picks]</td></tr>
            <tr><td><b>Azimuthal Gap</b></td><td>$[HypoinverseSummary/Azimuthal_Gap]</td></tr>
            <tr><td><b>Nearest Station</b></td><td>$[HypoinverseSummary/Nearest_Station]</td></tr>
            <tr><td><b>RMS</b></td><td>$[HypoinverseSummary/RMS]</td></tr>
            <tr><td><b>Error-H</b></td><td>$[HypoinverseSummary/Error_H]</td></tr>
            <tr><td><b>Error-Z</b></td><td>$[HypoinverseSummary/Error_Z]</td></tr>
        </table> ]]></text>
    </BalloonStyle>
    <ListStyle>
    </ListStyle>
  </Style>
  <Style id="hlightPointStyle">
    <IconStyle>
      <color>ff0000ff</color>
      <scale>0.6</scale>
      <Icon> <href>http://maps.google.com/mapfiles/kml/shapes/shaded_dot.png</href> </Icon>
    </IconStyle>
    <BalloonStyle>
        <text><![CDATA[<table border="0">
            <tr><td><b>Origin</b></td><td>$[HypoinverseSummary/Origin]</td></tr>
            <tr><td><b>Lat</b></td><td>$[HypoinverseSummary/Lat]</td></tr>
            <tr><td><b>Long</b></td><td>$[HypoinverseSummary/Long]</td></tr>
            <tr><td><b>Depth</b></td><td>$[HypoinverseSummary/Depth]</td></tr>
            <tr><td><b>MAG (Md)</b></td><td>$[HypoinverseSummary/MAG]</td></tr>
            <tr><td><b>Number Picks</b></td><td>$[HypoinverseSummary/Number_Picks]</td></tr>
            <tr><td><b>Azimuthal Gap</b></td><td>$[HypoinverseSummary/Azimuthal_Gap]</td></tr>
            <tr><td><b>Nearest Station</b></td><td>$[HypoinverseSummary/Nearest_Station]</td></tr>
            <tr><td><b>RMS</b></td><td>$[HypoinverseSummary/RMS]</td></tr>
            <tr><td><b>Error-H</b></td><td>$[HypoinverseSummary/Error_H]</td></tr>
            <tr><td><b>Error-Z</b></td><td>$[HypoinverseSummary/Error_Z]</td></tr>
        </table> ]]></text>
    </BalloonStyle>
    <ListStyle>
    </ListStyle>
  </Style>
    """
    return kml_preamble


def missing_str(fmt):
    width = fmt.split('.')[0].replace('>','') if '.' in fmt else fmt.replace('>','').replace('d','')
    #print("fmt=%s width=[%s]" % (fmt, width))
    width = int(width)
    if width > 0:
        return width * ' '
    else:
        return ""


from aqms_pdl.libs.schema_aqms_PI import Event, Origin, Netmag, Origin_Error
from aqms_pdl.libs.leapseconds import get_leapseconds_from_timestamp
def example_event():
    event = Event(evid=10001)
    event.etype = 'eq'
    event.selectflag = 1
    timestamp = UTCDateTime("2024/03/21T17:47:25.2700").timestamp
    origin = Origin(datetime=timestamp)
    origin.leapsecs = get_leapseconds_from_timestamp(timestamp)
    origin.lat=32.015007
    origin.lon=-102.04583221
    origin.depth=8.2601
    origin.auth='PR'
    origin.subsource='hypomag'
    origin.gap=255.001
    origin.distance=14.00
    origin.ndef=15
    origin.stime=0.
    origin.wrms=0.20
    origin.erhor = 0.99
    origin.sdep  = 2.11
    origin.quality = 0.75
    origin.gtype = 'r'
    origin.locevid=None
    netmag = Netmag()
    netmag.magnitude = 2.91
    netmag.magtype = 'l'
    netmag.nsta = 7

    event.preferred_magnitude = netmag
    event.preferred_origin = origin
    return event


def get_len(fmt):
    _len = None
    try:
        _str = fmt
        if '.' in fmt:
            _str = fmt.split('.')[0]

        n = "".join(filter(str.isdigit, _str))
        _len = int(n)
    except ValueError as e:
        raise
    return _len



# MTH: this is a slow creation so only do once
tf = TimezoneFinder()
def get_offset_mins(lat, lon):

    timezone_str = tf.timezone_at(lng=lon, lat=lat)
    timezone = pytz.timezone(timezone_str)
    utc_offset = timezone.utcoffset(datetime.datetime.now())

    #print(utc_offset)  # Output: -08:00
    #-1 day, 19:00:00

    if "-1 day," in str(utc_offset):
        hh, mm, ss = str(utc_offset).replace("-1 day,","").split(':')
        offset_mins = int(hh)*60 + int(mm) - 24 * 60
    else:
        hh, mm, ss = str(utc_offset).split(':')
        offset_mins = int(hh)*60 + int(mm)

    #print(timezone_str, utc_offset, offset_mins)
    return offset_mins



def get_new_etype(event, origin):
    geotype = origin.gtype if origin.gtype else ''
    if event.etype == 'eq':
        if geotype == 'l':
            new_etype = 'le'
        elif geotype == 'r':
            new_etype = 're'
        else:
            new_etype = 'ts'
    else:
        new_etype = event.etype
    return new_etype


def main():

    fmts = {
        'lat': '10.5f', 'lon': '10.5f', 'dep': '<7.3f',
        'mag': '5.2f', 'ncedc_magtype':'>4s', 'ncedc_source': '>4s',
        'ndef': '4d', 'evid': '10d',
    }

    for field, fmt in fmts.items():
        _len = get_len(fmt)
        print(field, fmt, _len)

    places = {
        'Boston':(42.355, -71.0565),
        'LA':(34.0549, -118.2426),
        'Houston':(29.7601, -95.3701),
        'Vienna':(48.2091, 16.3713),
    }

    for place, x in places.items():
        print(place, get_offset_mins(x[0], x[1]))


if __name__ == "__main__":
    main()
