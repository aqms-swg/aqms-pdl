# -*- coding: utf-8 -*-
"""
    Classes that describe AQMS IR tables

    CISN Instrument Response Schema

    http://ncedc.org/db/Documents/NewSchemas/IR/v1.5.4/IR.1.5.4/index.htm

:copyright:
    Mike Hagerty (m.hagerty@isti.com), 2022
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""

import datetime
import sys

import logging
logger = logging.getLogger()

from sqlalchemy import text
from sqlalchemy import Column, DateTime, Integer, Numeric, String, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import CheckConstraint, UniqueConstraint

from aqms_pdl.libs.schema_aqms_utils import MyMixin, DEFAULT_ENDDATE, Base, check_matching, fix_location
from aqms_pdl.libs.schema_aqms_utils import truncate_strings

import sqlalchemy.types
#class EquipmentType(sqlalchemy.types.UserDefinedType):
#class EquipmentType(sqlalchemy.types.UserDefinedType):

#from aqms_pdl.libs.util_cls import get_tables, init
#from aqms_pdl.libs.schema_aqms_utils import init
def get_tables():
    Base.TBLNAME_TO_CLASS = {}

    for mapper in Base.registry.mappers:
        cls = mapper.class_
        classname = cls.__name__
        if not classname.startswith('_'):
            tblname = cls.__tablename__
            #print(tblname, cls)
            Base.TBLNAME_TO_CLASS[tblname] = cls
    return Base.TBLNAME_TO_CLASS

'''
Defaults aren't applied until Insert or Update in sqlalchemy
This is a work around so you can use/compare them before that
'''
def init(self, **kwargs):
    tables = get_tables()
    #for t in sorted(tables.keys()):
        #print(t)
    cls_tbl = tables[self.__tablename__]
    truncate_strings(cls_tbl, kwargs)

    table_cols = [x.name for x in cls_tbl.__table__.c]
    has_offdate = 'offdate' in table_cols
    has_location = 'location' in table_cols
    #print("table:%20s has_location:%s has_offdate:%s" %
          #(self.__tablename__, has_location, has_offdate))

    if has_offdate and 'offdate' not in kwargs:
         kwargs['offdate'] = self.__table__.c.offdate.default.arg
    if has_location:
        fix_location(kwargs)
    cls = getattr(sys.modules[__name__], self.__class__.__name__)

# MTH: 2023-11-29 Set matching_fields here for classes that don't have them
#                 as they are needed in libs_epochs.insert_update_or_remove
    if getattr(self, 'matching_fields', None) is None:
        exclude_fields = getattr(self, 'exclude_fields_from_comparison', None)
        if exclude_fields:
            matching_fields = [key for key in self.__table__.columns.keys() if key not in exclude_fields]
            #print("** init cls:%s exclude_fields:%s ==> matching_fields:%s" % (cls, exclude_fields, matching_fields))
            self.matching_fields = matching_fields

# MTH: 2024-03-07 Set table.channel = table.seedchan & table.channelsrc = "SEED" **If not set in kwargs**
    # print("Inside init tablename:%s seedchan:%s" % (self.__tablename__, kwargs.get('seedchan')))
    if 'seedchan' in kwargs:
        if 'channel' not in kwargs:
            kwargs['channel'] = kwargs['seedchan']
        if 'channelsrc' not in kwargs:
            kwargs['channelsrc'] = "SEED"

    return super(cls, self).__init__(**kwargs)


from sqlalchemy.dialects.postgresql import ARRAY
class Equipment(Base, MyMixin):
    __tablename__ = "equipment"

    _id = Column('id', Integer, Sequence('eqseq'), info='auto nextval from sequence EQSEQ', primary_key=True, nullable=False)
    net = Column(String(8), info='Network name', primary_key=True, nullable=False)
    sta = Column(String(6), info='Station name', primary_key=True, nullable=False)
    seedchan = Column(String(3), info='Channel name', primary_key=True, nullable=False)
    location = Column(String(2), info='Location code', primary_key=True, nullable=False)
    ondate = Column(DateTime, info='timestamp without time zone', primary_key=True, nullable=False)
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Channel name')
    # MTH: This is actually an enum called equipment_type that must be one of
    #      {'sensor', 'datalogger', 'preamplifier'}
    _type = Column('type', String(80), info='sensor, datalogger or preamplifier')
    description = Column(String(80), info='equipment description')
    manufacturer = Column(String(80), info='equipment manufacturer')
    model = Column(String(80), info='equipment model')
    vendor = Column(String(80), info='equipment vendor')
    serialnumber = Column(String(80), info='equipment serialnumber')
    calibration_dates = Column(ARRAY(DateTime), info='equipment calibration dates')
    offdate = Column(DateTime, info='timestamp without time zone', default=DEFAULT_ENDDATE)
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'channel', 'channelsrc',
                       'description', 'manufacturer', 'vendor',
                       'serialnumber', 'model',
                       'ondate', 'offdate',
                       '_type',
                      ]

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        s = "%s net:%s sta:%s seedchan:%s loc:%s on:%s off:%s id:%s type:%s description:%s manufacturer:%s model:%s S/N:%s calibn:%s" %\
            (self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, self.offdate,
             self._id, self._type, self.description, self.manufacturer, self.model, self.serialnumber,
             self.calibration_dates)
        return s
        """
        s = "%s net:%s sta:%s seedchan:%s loc:%s on:%s off:%s id:%s type:%s description:%s manufacturer:%s model:%s S/N:%s" %\
            (self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, self.offdate,
             self._id, self._type, self.description, self.manufacturer, self.model, self.serialnumber)
        """
        """
        return "Equipment: net={} sta={} seedchan={} location={} on:{} off:{} id={} type={} description={}".\
                format(self.net, self.sta, self.seedchan, self.location, self.ondate, self.offdate,
                       self._id, self._type, self.description)
        """

''' Summary information on a channel
'''

class Channel_Data(Base, MyMixin):
    __tablename__ = "channel_data"

    net = Column(String(8), info='Network name', primary_key=True, nullable=False)
    sta = Column(String(6), info='Station name', primary_key=True, nullable=False)
    seedchan = Column(String(3), info='Channel name', primary_key=True, nullable=False)
    location = Column(String(2), info='Location code', primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, info='timestamp without time zone', primary_key=True, nullable=False)
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Channel name')
    inid = Column(Integer, info='Instrument identifier')
    remark = Column(String(60), info='Channel remark')
    unit_signal = Column(Integer, info='Unit lookup key for units of signal response')
    unit_calib  = Column(Integer, info='Unit lookup key for units of calibration input')
    lat = Column(Numeric(asdecimal=False), info='Locations north of the equator have positive latitudes')
    lon = Column(Numeric(asdecimal=False), info='Longitudes are measured positive east of the Greenwich meridian')
    elev = Column(Numeric(asdecimal=False),info='elevation')
    edepth = Column(Numeric(asdecimal=False), info='Emplacement depth relative to elevation')
    azimuth = Column(Numeric(asdecimal=False),info='Azimuth of instrument from North')
    dip = Column(Numeric(asdecimal=False),info='Dip of instrument down from horizontal')
    format_id = Column(Integer, info='Data format lookup key')
    record_length = Column(Integer, info='The exponent (as a power of 2) of the data record length')
    samprate = Column(Numeric(asdecimal=False), info='Data sampling rate [samples/second]')
    clock_drift = Column(Numeric(asdecimal=False), info='Tolerance value [seconds/sample] used as threshold for time error detection')
    flags = Column(String(27), info='Channel flags')
    offdate = Column(DateTime, info='timestamp without time zone', default=DEFAULT_ENDDATE)
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))

    CheckConstraint('azimuth >= 0 and azmiuth <= 360', name='chd01')
    CheckConstraint('clock_drift >= 0.0', name='chd02')
    CheckConstraint('dip >= -90 and dip <= 90', name='chd03')
    #CheckConstraint('edepth >= 0.0', name='chd04')
    CheckConstraint('lat >= -90.0 and lat <= 90', name='chd06')
    CheckConstraint('lon >= -180.0 and lon <= 180', name='chd07')
    CheckConstraint('record_length >= 8 and record_length <= 12', name='chd08')

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'channel', 'channelsrc',
                       'lat', 'lon', 'elev', 'edepth',
                       'inid', 'azimuth', 'dip',
                       'format_id', 'record_length',
                       'samprate', 'clock_drift',
                       'ondate', 'offdate'
                      ]

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        if self.lat and self.lon and self.elev:
            s = "%s net:%s sta:%s seedchan:%s loc:%s ondate:%s offdate:%s lat:%.3f lon:%.3f elev:%.1f azim:%s" %\
                (self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, self.offdate,
                self.lat, self.lon, self.elev, self.azimuth)
        else:
            s = "%s net:%s sta:%s seedchan:%s loc:%s ondate:%s offdate:%s lat:%s lon:%s elev:%s azim:%s" %\
                (self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, self.offdate,
                self.lat, self.lon, self.elev, self.azimuth)
        return s


class Station_Comment(Base, MyMixin):
    __tablename__ = "station_comment"

    net = Column(String(8), info='Network name', primary_key=True, nullable=False)
    sta = Column(String(6), info='Station name', primary_key=True, nullable=False)
    ondate = Column(DateTime, info='timestamp without time zone', primary_key=True, nullable=False)
    comment_id = Column(Integer, ForeignKey('d_comment.id'), info='key of association d_comment entry', primary_key=True, nullable=False)
    comment_level = Column(Integer, info='numeric value (if any) associated with the Units of comment level')
    offdate = Column(DateTime, info='timestamp without time zone', default=DEFAULT_ENDDATE)
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))

    matching_fields = ['net', 'sta', 'ondate', 'offdate']

# MTH: added init on 2023-11-29
    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "Station_Comment: net={} sta={} on:{} off:{} comment_id={} comment_level={}".\
                format(self.net, self.sta, self.ondate, self.offdate, self.comment_id, self.comment_level)


class D_Comment(Base, MyMixin):
    __tablename__ = "d_comment"

    id = Column(Integer, Sequence('comseq'), primary_key=True, nullable=False,
                  info='A cross reference code to indicate this particular dictionary entry')
    _class = Column('class', String(1), info='A single letter that determines to what the code refers',
                   nullable=False)
    description = Column(String(70), info='This is a description of a comment')
    unit = Column(Integer, info='Units of comment level',
                  nullable=False)

    matching_fields = ['_class', 'description', 'unit']

# MTH: added init on 2023-11-29
    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "D_Comment: id={} class={} unit={} description={}".\
                format(self.id, self._class, self.unit, self.description)

class Channel_Comment(Base, MyMixin):
    __tablename__ = "channel_comment"

    net = Column(String(8), info='Network name', primary_key=True, nullable=False)
    sta = Column(String(6), info='Station name', primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column(DateTime, info='timestamp without time zone', primary_key=True, nullable=False)
    comment_id = Column(Integer, ForeignKey(D_Comment.id), info='key of association d_comment entry', primary_key=True, nullable=False)
    comment_level = Column(Integer, info='numeric value (if any) associated with the Units of comment level')
    comment_level = Column(Integer, info='numeric value (if any) associated with the Units of comment level')
    channel = Column('channel', String(8),info='Channel name')
    channelsrc = Column('channelsrc', String(8),info='Domain for the channel (name). e.g., SEED, USGS, etc')
    offdate = Column(DateTime, info='timestamp without time zone', default=DEFAULT_ENDDATE)
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))

    matching_fields = ['net', 'sta', 'seedchan', 'location', 'ondate', 'offdate',
                       'channel', 'channelsrc',
                      ]

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "Channel_Comment: net={} sta={} seedchan={} location={} on:{} off:{} comment_id={} comment_level={}".\
                format(self.net, self.sta, self.seedchan, self.location, self.ondate, self.offdate,
                       self.comment_id, self.comment_level)

''' Abbreviation Dictionary
'''
class D_Abbreviation(Base):
    __tablename__ = "d_abbreviation"

    id = Column(Integer, Sequence('abbseq'), primary_key=True, nullable=False,
                  info='A cross reference code to indicate this particular dictionary entry')
    description = Column(String(70), info='This is a description of an abbreviation')

    def __repr__(self):
        return "D_Abbreviation: id={}, description={}".\
                format(self.id, self.description)


class D_Format(Base):
    __tablename__ = "d_format"

    id = Column('id', Integer, Sequence('forseq'), primary_key=True, nullable=False)
    name = Column('name', String(80), default="UNKNOWN")
    family = Column('family', Integer, nullable=False, default=50)
    ms_id = Column('ms_id', Integer, nullable=False, default=0)

    def __repr__(self):
        return "D_Format: id={}, name={}, family={}, ms_id={}".format(\
                self.id, self.name, self.family, self.ms_id)


class D_Unit(Base):
    __tablename__ = "d_unit"

    id = Column('id', Integer, Sequence('uniseq'), primary_key=True, nullable=False)
    name = Column('name', String(80))
    description = Column('description', String(70))

    def __repr__(self):
        return "D_Unit: id={}, name={}, description={}".format(\
                self.id, self.name, self.description)


class Poles_Zeros(Base, MyMixin):
    __tablename__ = "poles_zeros"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=DEFAULT_ENDDATE)
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    #stage_seq = Column('stage_seq', Integer)
    stage_seq = Column('stage_seq', Integer, primary_key=True, nullable=False)
    tf_type  = Column('tf_type', String(1))

    pz_key   = Column('pz_key', ForeignKey('pz.key'),
                      info="key to PZ to get to list of PZ_Data rows", nullable=False)

    unit_in  = Column('unit_in', Integer, nullable=False)
    unit_out = Column('unit_out', Integer, nullable=False)
    ao = Column('ao', Numeric(asdecimal=False), nullable=False)
    af = Column('af', Numeric(asdecimal=False), nullable=False)
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'channel', 'channelsrc',
                       'ondate', 'offdate',
                       'ao', 'af',
                       'stage_seq',
                       'tf_type',
                       'unit_in', 'unit_out'    # ~Everything *except* pz_key!
                      ]

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "{}: net={} sta={} seedchan={} location={} ondate={} " \
                "offdate={} stage_seq={} ao={} af={} unit_in={} unit_out={}".\
                format(self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, \
                self.offdate, self.stage_seq, self.ao, self.af, self.unit_in, self.unit_out)


class PZ(Base, MyMixin):
    __tablename__ = "pz"

    key  = Column('key', Integer, Sequence('pzseq'), primary_key=True, nullable=False)
    name = Column('name', String(80))
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    exclude_fields_from_comparison = ['key', 'lddate']

# MTH: Added on 2023-11-29
    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "class PZ: key={}, name=[{}]".format(self.key, self.name)


class PZ_Data(Base, MyMixin):
    __tablename__ = "pz_data"

    key = Column(Integer, ForeignKey(PZ.key), primary_key=True, nullable=False)
    row_key = Column('row_key', Integer, primary_key=True, nullable=False)
    pztype = Column('type', String(1))
    r_value = Column('r_value', Numeric(asdecimal=False), nullable=False)
    r_error = Column('r_error', Numeric(asdecimal=False))
    i_value = Column('i_value', Numeric(asdecimal=False), nullable=False)
    i_error = Column('i_error', Numeric(asdecimal=False))

    matching_fields = ['pztype',
                       'r_value', 'i_value',
                       'r_error', 'i_error',
                      ]
# MTH: Added on 2023-11-29
    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "PZ_Data: key={}, row_key={}, pztype={}, r_value={}, i_value={}".\
                format(self.key, self.row_key, self.pztype, self.r_value, self.i_value)

class Polynomial(Base, MyMixin):
    __tablename__ = "polynomial"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=DEFAULT_ENDDATE)
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    stage_seq = Column('stage_seq', Integer, primary_key=True, nullable=False)
    tf_type  = Column('tf_type', String(1))
    pn_key   = Column('pn_key', ForeignKey('pn.key'),
                      info="key to PN to get to list of PN_Data rows", nullable=False)
    unit_in  = Column('unit_in', Integer, nullable=False)
    unit_out = Column('unit_out', Integer, nullable=False)

    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'channel', 'channelsrc',
                       'ondate', 'offdate',
                       'stage_seq',
                       'tf_type',
                       'unit_in', 'unit_out'
                      ]

# MTH: Added on 2023-11-29
    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "{}: net={} sta={} seedchan={} location={} ondate={} " \
                "offdate={} stage_seq={} unit_in={} unit_out={}".\
                format(self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, \
                self.offdate, self.stage_seq, self.unit_in, self.unit_out)


class PN(Base, MyMixin):
    __tablename__ = "pn"

    key  = Column('key', Integer, Sequence('poseq'), primary_key=True, nullable=False)
    name = Column('name', String(80))
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    poly_type = Column(String(1), info="The type of polynomial approximation: {'C','L','M'}")
    # MTH: This table should probably also have frequency_upper/lower_bound
    lower_bound = Column(Numeric(asdecimal=False), info="Polynomial lower bound")
    upper_bound = Column(Numeric(asdecimal=False), info="Polynomial upper bound")
    max_error = Column(Numeric(asdecimal=False), info="Max error of polynomial approximation")

    CheckConstraint("poly_type in ('C','L','M')", name='PN01')

    exclude_fields_from_comparison = ['key', 'lddate']

# MTH: Added on 2023-11-29
    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "class PN: key={}, name=[{}] poly_type={}".format(self.key, self.name, self.poly_type)


class PN_Data(Base, MyMixin):
    __tablename__ = "pn_data"

    key = Column(Integer, ForeignKey(PN.key), primary_key=True, nullable=False)
    row_key = Column(Integer, primary_key=True, nullable=False)
    pn_value = Column(Numeric(asdecimal=False), nullable=False, info='Value of polynomial coefficient')

    matching_fields = ['pn_value',
                      ]

    def __repr__(self):
        return "PN_Data: key={}, row_key={}, pn_value={}".\
                format(self.key, self.row_key, self.pn_value)


class Coefficients(Base, MyMixin):
    __tablename__ = "coefficients"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    stage_seq = Column('stage_seq', Integer, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=DEFAULT_ENDDATE)
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    tf_type  = Column('tf_type', String(1))
    dc_key   = Column('dc_key', ForeignKey('dc.key'),
                      info="key to DC to get to list of DC_Data rows=coefficients", nullable=False)
    unit_in  = Column('unit_in', Integer, nullable=False)
    unit_out = Column('unit_out', Integer, nullable=False)
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'channel', 'channelsrc',
                       'ondate', 'offdate',
                       'stage_seq',
                       'tf_type',
                       'unit_in', 'unit_out',
                      ]

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "{}: net={} sta={} seedchan={} location={} ondate={} " \
               "offdate={} stage_seq={} unit_in={} unit_out={}".\
                format(self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, \
                self.offdate, self.stage_seq, self.unit_in, self.unit_out)


class DC(Base, MyMixin):
    __tablename__ = "dc"

    key  = Column('key', Integer, Sequence('dcseq'), primary_key=True, nullable=False)
    name = Column('name', String(80))
    symmetry = Column('symmetry', String(1))
    storage = Column('storage', String(1))
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    matching_fields = ['name', 'symmetry', 'storage']

# MTH: Added on 2023-11-29
    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "class DC: key={}, name=[{}] symmetry={} storage={}".format(self.key, self.name,
                                                                           self.symmetry, self.storage)

class DC_Data(Base, MyMixin):
    __tablename__ = "dc_data"

    key = Column(Integer, ForeignKey(DC.key), primary_key=True, nullable=False)
    row_key = Column('row_key', Integer, primary_key=True, nullable=False)
    dctype = Column('type', String(1))
    coefficient = Column('coefficient', Numeric(asdecimal=False), nullable=False)
    error = Column('error', Numeric(asdecimal=False))

    matching_fields = ['dctype', 'coefficient', 'error']

    def __repr__(self):
        return "DC_Data: key={} row_key={} dctype={} coefficient={} error={}".\
                format(self.key, self.row_key, self.dctype, self.coefficient, self.error)


class Decimation(Base, MyMixin):
    __tablename__ = "decimation"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=DEFAULT_ENDDATE)
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    stage_seq = Column('stage_seq', Integer, primary_key=True, nullable=False)

    dm_key   = Column('dm_key', ForeignKey('dm.key'),
                      info="key to DC to get to list of DC_Data rows=coefficients", nullable=False)

    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'channel', 'channelsrc',
                       'ondate', 'offdate', 'stage_seq',
                      ]

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "{}: net={} sta={} seedchan={} location={} ondate={} offdate={} stage_seq={}".\
                format(self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, \
                self.offdate, self.stage_seq)

class DM(Base, MyMixin):
    __tablename__ = "dm"

    key  = Column('key', Integer, Sequence('dmseq'), primary_key=True, nullable=False)
    name = Column('name', String(80))
    samprate = Column('samprate', Numeric(asdecimal=False), nullable=False,
                      info="Sampling rate. This attribute is the sample rate in samples/second")
    factor = Column('factor', Integer, nullable=False,
                    info="The decimation factor. When this number of samples are read in, one final sample comes out.")
    i_offset = Column('i_offset', Integer,
                    info="This field determines which sample is chosen for use. The value of this field "\
                         "has to be greater than or equal to zero, but less than the decimation factor")
    delay = Column('delay', Numeric(asdecimal=False), info="The estimated pure delay for the stage")
    correction = Column('correction', Numeric(asdecimal=False), nullable=False,
                        info="The time shift applied to the time tag due to delay at this stage of the filter; "\
                             "a negative number indicating the amount of time added to the former time tag. "\
                             "The actual delay is difficult to estimate, and the correction applied neglects dispersion."\
                             "This field allows to know how much correction was used, in case a more accurate correction "\
                             "is to be applied later. A zero here implies no correction was done")

    lddate = Column('lddate', DateTime, server_default=text('NOW()'))


    matching_fields = ['name', 'samprate', 'factor', 'i_offset',
                       'delay', 'correction',
                      ]
    # MTH: Added on 2023-11-29
    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "class DM: key={} name=[{}] samprate={} factor={} offset={} delay={} correction={}".\
                format(self.key, self.name, self.samprate, self.factor, self.i_offset, self.delay, self.correction)


class Sensitivity(Base, MyMixin):
    __tablename__ = "sensitivity"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    stage_seq = Column('stage_seq', Integer, primary_key=True, nullable=False)
    offdate = Column('offdate', DateTime, default=datetime.datetime(3000,1,1))
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    sensitivity = Column('sensitivity', Numeric(asdecimal=False))
    frequency = Column('frequency', Numeric(asdecimal=False))
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'channel', 'channelsrc',
                       'ondate', 'offdate',
                       'stage_seq', 'sensitivity', 'frequency',
                      ]

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "{}: net={} sta={} seedchan={} location={} ondate={} " \
               "offdate={} stage_seq={} sensitivity={} frequency={}".\
                format(self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.ondate, \
                self.offdate, self.stage_seq, self.sensitivity, self.frequency)


class SimpleResponse(Base, MyMixin):
    __tablename__ = "simple_response"

    net = Column('net', String(8), primary_key=True, nullable=False)
    sta = Column('sta', String(6), primary_key=True, nullable=False)
    seedchan = Column('seedchan', String(3), primary_key=True, nullable=False)
    location = Column('location', String(2), primary_key=True, nullable=False)
    ondate = Column('ondate', DateTime, primary_key=True, nullable=False)
    channel = Column('channel', String(8))
    channelsrc = Column('channelsrc', String(8), default="SEED")
    natural_frequency = Column('natural_frequency', Numeric(asdecimal=False))
    damping_constant = Column('damping_constant', Numeric(asdecimal=False))
    gain = Column('gain', Numeric(asdecimal=False))
    gain_units = Column('gain_units', String)
    low_freq_corner = Column('low_freq_corner', Numeric(asdecimal=False))
    high_freq_corner = Column('high_freq_corner', Numeric(asdecimal=False))
    offdate = Column('offdate', DateTime, default=datetime.datetime(3000,1,1))
    lddate = Column('lddate', DateTime, server_default=text('NOW()'))
    dlogsens = Column('dlogsens', Numeric(asdecimal=False))

    matching_fields = ['net', 'sta', 'seedchan', 'location',
                       'channel', 'channelsrc',
                       'ondate', 'offdate',
                       'natural_frequency', 'damping_constant',
                       'gain', 'gain_units',
                       'low_freq_corner', 'high_freq_corner',
                       'dlogsens'
                      ]

    def __init__(self, **kwargs):
        return init(self, **kwargs)

    def __repr__(self):
        return "{}: net={} sta={} seedchan={} location={} src={} ondate={} " \
               "offdate={} gain={} (f:{}) low_freq_corner={} high_freq_corner={} " \
               "natural_frequency={}, damping_constant={}".\
                format(self.__tablename__, self.net, self.sta, self.seedchan, self.location, self.channelsrc, self.ondate, \
                self.offdate, self.gain, self.gain_units, self.low_freq_corner, self.high_freq_corner, \
                self.natural_frequency, self.damping_constant)


class Station_Data(Base, MyMixin):
    __tablename__ = "station_data"

    net = Column(String(8), info='Network name', primary_key=True, nullable=False)
    sta = Column(String(6), info='Station name', primary_key=True, nullable=False)
    ondate = Column(DateTime, info='timestamp without time zone', primary_key=True, nullable=False)
    # MTH: by default sqlalchemy creates Numeric(asdecimal=True) which has to be converted to
    #      float before comparing with another float:
    lat = Column(Numeric(asdecimal=False), info='Locations north of the equator have positive latitudes')
    lon = Column(Numeric(asdecimal=False), info='Longitudes are measured positive east of the Greenwich meridian')
    elev = Column(Numeric(asdecimal=False),info='Station elevation')
    staname = Column(String(60), info='Station name')
    net_id = Column(Integer, info='Network id')
    word_32 = Column(Integer, info='', nullable=False, default=3210)
    word_16 = Column(Integer, info='', nullable=False, default=10)
    offdate = Column(DateTime, info='timestamp without time zone', default=DEFAULT_ENDDATE)
    lddate  = Column(DateTime, info='timestamp without time zone', server_default=text('NOW()'))

    CheckConstraint('lat >= -90.0 and lat <= 90', name='std02')
    CheckConstraint('lon >= -180.0 and lon <= 180', name='std03')


    #matching_fields = ['net', 'sta',
                       #'lat', 'lon', 'elev',
                       #'ondate', 'offdate']
    # MTH: 2023-01-26
    exclude_fields_from_comparison = ['lddate']

    '''
    Defaults aren't applied until Insert or Update in sqlalchemy
    This is a work around so you can use/compare them before that
    '''
    def __init__(self, **kwargs):
        if 'word_32' not in kwargs:
             kwargs['word_32'] = self.__table__.c.word_32.default.arg
        if 'word_16' not in kwargs:
             kwargs['word_16'] = self.__table__.c.word_16.default.arg
        if 'offdate' not in kwargs:
             kwargs['offdate'] = self.__table__.c.offdate.default.arg

        #cls = getattr(sys.modules[__name__], self.__class__.__name__)
        #super(cls, self).__init__(**kwargs)
        # MTH: Added on 2023-11-29
        return init(self, **kwargs)


    def __repr__(self):
        s = "%s net:%s sta:%s ondate:%s offdate:%s lat:%.3f lon:%.3f elev:%.1f" %\
            (self.__tablename__, self.net, self.sta, self.ondate, self.offdate, self.lat, self.lon, self.elev)
        #s = "%s net:%s sta:%s (%s) ondate:%s lat:%.3f lon:%.3f elev:%.1f w32:%s w16:%s" %\
            #(self.__tablename__, self.net, self.sta, self.staname, self.ondate, self.lat, self.lon, self.elev,
             #self.word_32, self.word_16)
        #s+= "\n         word_32:%d word_16:%d" % (self.word_32, self.word_16)
        return s


def main():
    print("congrats - you're in schema_aqms_IR!")
#        1         2         3         4         5         6         7         8
    name = """
12345678901234567890123456789012345678901234567890123456789012345678901234567890abcd
    """
    pz = PZ(name=name.strip())
    print(pz)
    print(pz.name)

if __name__ == '__main__':
    main()
