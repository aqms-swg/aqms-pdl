import argparse
import logging
import os
import sys

from aqms_pdl import createSession
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg
from aqms_pdl.libs.libs_aqms import get_assoc_thresh, calc_diff_between_aqms_origins
from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.libs_db import query_origin_by_evid, create_and_insert_assocevents
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config, read_config


def main():
    """
        The purpose of this app is to compare the difference in time, lat,lon and depth
        between 2 evids in the db.
        i.e., it runs the same code was the aqms-pdl associator but does *not* try to
              insert anything, just shows you how far/close the events are

        Usage: >python compare_evids.py -e evid1,evid2   // where evid1,2 are integer evids
    """

    fname = 'compare_evids'
    r = required_arg(arg=['-e', '--evids'], type=str, description='evid1,evid2', help='--evids evid1,evid2')

    args, parser = parse_cmd_line(required_args=[r])
    config, log_msgs = read_config(debug=False, requireConfigFile=True, configfile=args.configfile)
    update_args_with_config(args, config, fname)

    configure_logger(**vars(args))
    logger = logging.getLogger()

    logger.info(sys.argv)
    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    try:
        evids = list_str(args.evids)
    except:
        raise
    if len(evids) != 2:
        logger.error("Unable to parse 2 evids from:%s" % args.evids)
        parser.print_help()
        exit(2)

    # Configure the Session for the whole module in __init__.py:
    createSession(config)
    # import the configured Session into the libs_db pkg for use
    set_Session()

    orig1 = query_origin_by_evid(evids[0])
    if not orig1:
        logger.error("No such evid:%d" % evids[0])
        parser.print_help()
        exit(2)
    orig2 = query_origin_by_evid(evids[1])
    if not orig2:
        logger.error("No such evid:%d" % evids[1])
        parser.print_help()
        exit(2)

    od = calc_diff_between_aqms_origins(orig1, orig2)
    print("compare-evids: evid1:%d orid1:%d evid2:%d orid2:%d" %
          (evids[0], orig1.orid, evids[1], orig2.orid))
    print(orig1)
    print(orig2)
    print("compare-evids: dt=%.1f (sec) dx=%.1f (km) dz=%.1f (km)" %
          (od.dt, od.dx, od.dz))


def list_str(values):
    try:
        l = [int(val) for val in values.split(',')]
    except ValueError as e:
        print("Error: parsing evids as ints from cmd line:%s" % e)
        #raise
        exit(2)
    return l

if __name__ == "__main__":
    main()
