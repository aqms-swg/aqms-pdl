#!/home/aqms/anaconda3/envs/aqms/bin/python
import argparse
import logging
import os
import shutil
import sys

from hashwrap import hashwrapper
from hashwrap.read_phase_formats import read_fpfit_file
from hashwrap.read_sp_ratios import read_sp_ratios

import toml
from obspy import UTCDateTime

import numpy as np
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from obspy.imaging.beachball import NodalPlane, beach, pol2cart, xy2patch, D2R
from obspy.core.event.event import Event
from obspy.core.event.origin import Origin
from obspy.core.event.resourceid import ResourceIdentifier

from aqms_pdl import createSession
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg, optional_arg
from aqms_pdl.libs.libs_aqms import get_assoc_thresh, obspy_focal_mechanism_to_aqms
from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.libs_db import query_origin_by_evid, create_and_insert_assocevents, get_arrivals_for_evid
from aqms_pdl.libs.libs_db import get_arrivals_for_orid, get_event_info
from aqms_pdl.libs.libs_db import focalmec_already_in_db, getSequence, get_leapsecs
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config, read_config
from aqms_pdl.libs.schema_aqms_PI import Mec, Mecdata, Mecchannel, Remark
from aqms_pdl.libs.schema_aqms_PI import Origin as Orig

from aqms_pdl.helpers.calc_amps import calc_amp_ratios

from dataclasses import dataclass

@dataclass
class fm:
    sta: str
    cha: str
    iph: str
    fm: str
    az: float
    ema: float

logger = None

def main():
    """
        Usage: >python focal_mech EVID    [-a/--use-amplitude-ratios]  // Calc/use S/P amp ratios + P polarities
                                          [-f/--ampfile FILENAME]      // Read S/P ratios from FILENAME (else calculate on fly)
                                          [-p/--insert-db]             // Insert Mec/Remark into db
                                          [-y/--generate-plot]         // Generate stereo focal plot = evid.png
                                          [-w/--workdir]               // Specify workdir for stdout + files
                                          [-m/--min-mag]               // Specify min magnitude to run
                                          [-g/--max-agap]              // Specify max azimuthal gap to run
        Help: >python -h or >python --help

        Common optional args:
            [--loglevel DEBUG INFO WARN ERROR]
            [--logconsole]
            [--logfile /path/to/logfile]
            [-c/--configfile /path/to/yml/configfile (DEFAULT=./config.yml)]
    """

    fname = 'focal_mech'
    #r = required_arg(arg=['-e', '--evid'], type=int, description='EVID', help='evid to generate focal mech for')
    r = required_arg(arg=['evid'], type=int, description='EVID', help='evid to generate focal mech for')
    args, parser = parse_cmd_line(required_args=[r], optional_args=get_optional_args())
    #parser.description=....
    config, log_msgs = read_config(debug=False, requireConfigFile=True, configfile=args.configfile)
    if args.logdir is not None and args.logfile is None:
        # Interpret this to mean user wants a default logfile name, not spec'd in the config.yml (?)
        # Otherwise, if args.logfile is empty and config['logfile'] is not, it will use config['logdir']
        args.logfile = "%s.log" % fname

    update_args_with_config(args, config, fname)

    if args.workdir:
        if not os.path.exists(args.workdir):
            try:
                os.makedirs(args.workdir)
            except:
                raise
        printfile = os.path.join(args.workdir, '%s.out' % args.evid)
        sys.stdout = open(printfile, 'w')

    configure_logger(**vars(args))
    global logger
    logger = logging.getLogger()

    logger.info(sys.argv)
    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    log_args(args)

    # Configure the Session for the whole module in __init__.py:
    createSession(config)
    # import the configured Session into the libs_db pkg for use
    set_Session()

    if args.tomlfile:
        settings = toml.load(args.tomlfile)
        plfile = settings['input_files']['plfile']
        fpfile = settings['input_files']['fpfile']
        delmax = settings['hash_parameters']['delmax']

        n = 1
        if n == 1:
            events = read_fpfit_file(fpfile=fpfile, plfile=plfile, delmax=delmax)
            #               012345678901234567890
            #'event_info': '1994-01-21 11:04 <34.242, -118.618>
            event = events[0]
            poor_ot = UTCDateTime(event['event_info'][0:16])
            print(events[0]['sname'])
            print(events[0]['p_pol'])
            reversals = check_polarity(plfile)
            print(reversals)
            print(poor_ot)
            for i, sta in enumerate(events[0]['sname']):
                p_pol = events[0]['p_pol'][i]
                if sta in reversals:
                    print(sta, reversals[sta])
                    start, end = reversals[sta]
                    if poor_ot >= start and poor_ot <= end:
                        print("Reverse polarity: p_pol:%d --> %d" % (p_pol, -1*p_pol))
                    else:
                        print("No need to reverse polarity: p_pol:%d" % (p_pol))

            outputs = hashwrapper.calc_focal_mechanisms(events, settings['hash_parameters'], phase_format='FPFIT')
            print("Here come the outputs:")
            print(outputs)
        exit()

    if args.reverse_polarity_file:
        reversals = check_polarity(args.reverse_polarity_file)

    #orig = query_origin_by_evid(args.evid)
    event = get_event_info(args.evid)
    if event is None:
        logger.error("%s: Unable to find evid:%d in db --> Exitting!" %
                     (fname, args.evid))
        print("%s: Unable to find evid:%d in db --> Exitting!" %
                     (fname, args.evid))
        exit(2)

    orig = event.preferred_origin
    mag = event.preferred_magnitude
    if mag and args.min_mag:
        if mag.magnitude < args.min_mag:
            logger.info("%s: evid:%d pref mag:%.2f < minmag [%.2f] ==> Exitting!" %
                        (fname, args.evid, mag.magnitude, args.min_mag))
            exit()
    leapsecs = get_leapsecs(orig.datetime)
    #arrivals = get_arrivals_for_evid(args.evid)
    arrivals = get_arrivals_for_orid(orig.orid)
    logger.info("evid:%d orid:%d (leapsecs:%d) n_arrs:%d" % (args.evid, orig.orid, leapsecs, len(arrivals)))

    # Remove stations with unclear P first motion - this will also affect amplitudes!
    remove_stations = []
    for arrival in arrivals:
        logger.info("Arr sta:%4s cha:%s [%s] dist:%.1f az:%.1f ema:%.1f" %
                    (arrival.sta, arrival.seedchan, arrival.iphase, arrival.distance, arrival.azimuth, arrival.ema))
        if arrival.iphase == 'P':
            if 'c' not in arrival.fm.lower() and 'd' not in arrival.fm.lower():
                logger.warning("sta:%4s has P arrival.fm=[%s] --> Remove this station from processing!" %
                               (arrival.sta, arrival.fm))
                remove_stations.append(arrival.sta)
    arrivals = [arrival for arrival in arrivals if arrival.sta not in remove_stations]

    # Collect arrivals by station
    arrs = {}
    for arrival in arrivals:
        if arrival.sta not in arrs:
            arrs[arrival.sta] = {}
        d = arrs[arrival.sta]
        if arrival.iphase == 'P':
            d['P'] = arrival
        elif arrival.iphase == 'S':
            d['S'] = arrival

    # Remove any stations that don't have P arrival since we need it for
    #     either FM or SP_ratio
    for sta in list(arrs):
        if 'P' not in arrs[sta]:
            logger.warning("stn:%s has no P arrival --> delete this station" % sta)
            del arrs[sta]

    sname=[]
    p_pol=[]
    p_qual=[]
    qdist=[]
    qazi=[]
    qthe=[]
    sazi=[]
    sthe=[]
    first_motions = []

    # Arrival pairs we'll use to compute S/P for station
    amp_arrivals = []
    mecdata_polarities = []
    mecdata_pick_times = []
    mecchan_nscl = []
    for sta, d in arrs.items():
        if 'P' in d and 'S' in d:
            amp_arrivals.append(d['P'])
            amp_arrivals.append(d['S'])
        sname.append(sta)
        # Not sure why we would continue without a P arrival (?)
        #arrival = d['P'] if 'P' in d else d['S']
        arrival = d['P']

        # Get FM for this sta else set to 0

        if 'P' in d and getattr(d['P'], 'fm', None):
            if "c" in arrival.fm.lower():
                pol = 1
                mecdata_polarities.append("U")
            elif "d" in arrival.fm.lower():
                pol = -1
                mecdata_polarities.append("D")
            else:
                logger.warning("arrival.fm=[%s] --> Skip this!" % arrival.fm)
                continue

            if sta in reversals.keys():
                start, end = reversals[sta]
                _time = UTCDateTime(d['P'].datetime)
                if _time >= start and time <= end:
                    logger.info("sta:%s [%s, %s] ==> reverse polarity!" % (sta, start, end))
                    pol *= -1

            p_pol.append(pol)
            first = fm(sta=arrival.sta, cha=arrival.seedchan, iph=arrival.iphase,
                       fm=arrival.fm, az=float(arrival.azimuth), ema=float(arrival.ema))
            first_motions.append(first)
            mecdata_pick_times.append(d['P'].datetime)
            mecchan_nscl.append("%s.%s.%s.%s" % (d['P'].net, d['P'].sta, d['P'].seedchan, d['P'].location))
        else:
            p_pol.append(0)


        # FOCALMC wants takeoff angle wrt Up=0, <90 upgoing, >90 downgoing
        qthe.append(180.-float(arrival.ema))
        qazi.append(float(arrival.azimuth))
        qdist.append(float(arrival.distance))
        # p_qual: 0=impulsive, 1=emergent
        p_qual.append(0)
        sazi.append(1.)
        sthe.append(10.)

    event = {}
    event['event']={}
    event['event']['icusp']=args.evid
    event['event']['sez']=orig.sdep
    event['event']['seh']=orig.erhor
    event['event']['qdep']= orig.depth
    event['event']['qlat']= orig.lat
    event['event']['qlon']= orig.lon
    event['event_info'] = UTCDateTime(orig.datetime).strftime('%Y-%m-%d %H:%M:%S')

    for key in ['sname', 'p_pol', 'p_qual', 'qdist', 'qazi', 'qthe', 'sazi', 'sthe']:
        event[key]=eval(key)

    logger.info("len(sname)=%d len(pol)=%d len(qthe)=%d" %
          (len(event['sname']), len(event['p_pol']), len(event['qthe'])))
    logger.info("len(mecdata)=%d len(qthe)=%d" %
          (len(mecdata_polarities), len(qthe)))
    for k,name in enumerate(event['sname']):
        logger.info("POL sta:%s pol:%2s  [%s] dist:%3.1f az:%5.1f ema:%4.1f the:%4.1f" %
                   (name, p_pol[k], mecdata_polarities[k], qdist[k], qazi[k], (180.-qthe[k]), qthe[k]))

        #print(name, p_pol[k], mecdata_polarities[k], UTCDateTime(mecdata_pick_times[k] - leapsecs, precision=3),
              #qdist[k], qazi[k], qthe[k])

    # An example file for default params:
    #settings = toml.load('/Users/mth/mth/python_pkgs/aqms-pdl/aqms_pdl/resources/example1.toml')
    # MTH: hack to get resources dir relative to dir focal_mech.py is in
    dir_path = os.path.dirname(os.path.realpath(__file__))
    settings = toml.load(os.path.join(dir_path, '../resources/example1.toml'))
    settings = settings['hash_parameters']
    settings['npolmin'] = 5
    settings['max_agap'] = 180
    settings['max_pgap'] = 80
    settings['badfrac'] = .00001
    settings['badfrac'] = 0.

    # If you want to set more HASH params via configfile:
    # 1. In config.yml, add "MAX_PGAP: 22.5"
    # 2. This will automagically get converted to args.max_pgap
    # 3. Set settings['max_pgap'] = args.max_pgap here
    #if args.max_agap:
        #settings['max_agap'] = args.max_agap

    for arg in vars(args):
        if arg in settings:
            val = getattr(args, arg, None)
            if val is not None:
                settings[arg] = val

    logger.info(f"{'=' * 10 + 'hash settings' + '=' * 10}")
    for k,v in settings.items():
        logger.info(f"{k+':':>20} {v}")
    logger.info(f"{'=' * 20} {'=' * 20}")

    events_sp = None
    # Calc S/P amp ratios
    if args.use_amplitudes and amp_arrivals:

        if args.ampfile:
            logger.info("Read in amp file:%s", args.ampfile)
            event_sp = read_ampfile(args.ampfile)
        else:

            #ratios = calc_amp_ratios(amp_arrivals, args, config)
            event_sp = calc_amp_ratios(amp_arrivals, args, config, save=True)
            """
               print("calc_amps sta:%s  Dist:%.2f Az:%.2f i:%.2f FM:%s P_amp:%.2f S_amp:%.2f S/P:%.2f" %
                       (sta, x, az, ema, fm, P_amp, S_amp, (S_amp/P_amp)))
            """
        if args.workdir:
            for ampfile in ["%s.amps" % args.evid, "%s.amps.json" % args.evid]:
                target = os.path.join(args.workdir, ampfile)
                if os.path.exists(ampfile):
                    shutil.move(ampfile, target)

        for k,sta in enumerate(event_sp['sname']):
            logger.info("sta:%s az:%5.2f ema:%5.2f ih:%5.2f dist:%5.2f p_amp:%.2f s_amp:%.2f sp_ratio:%.2f" %
                (sta, event_sp['qazi'][k], event_sp['emas'][k], event_sp['qthe'][k], event_sp['dist'][k],
                event_sp['P_amp'][k], event_sp['S_amp'][k], event_sp['sp_ratio'][k]))

            for key in ['P_amp', 'S_amp']:
                for cha,d in event_sp['arrs'][sta][key].items():
                    pick = UTCDateTime(d['pick_time'], precision=3)
                    logger.info("      cha:%s pick_time:%s pre_pick:%.2f pos_pick:%.2f max:%8.4e min:%8.4e" %
                        (cha, pick, d['pre_pick'], d['pos_pick'], d['maxval'], d['minval']))


        event_sp['icusp']=args.evid
        event_sp['sazi']=[1.] * len(event_sp['qazi'])
        event_sp['sthe']=[1.] * len(event_sp['qthe'])
        event_sp['sp_ratio']=[np.log10(np.abs(sp_ratio)) for sp_ratio in event_sp['sp_ratio'] ]

        events_sp = [event_sp]

    """
    print("**** Call calc_focal_mechanisms ****")
    print("len(event['sname'])=%d len(events_sp['sname'])=%d len(events_sp['qthe']=%d" %
          (len(event['sname']), len(events_sp[0]['sname']), len(events_sp[0]['qthe'])))
    """
    output = hashwrapper.calc_focal_mechanisms([event], settings, events_sp=events_sp, phase_format='FPFIT',
                                               logger_in=logger)

    if not output:
        logger.warning("hashwrapper.calc_focal_mechanisms returned empty output --> Maybe there aren't enough picks ?!")
        logger.warning("Exitting!")
        exit()


    if 1:
        #hashwrapper.write_outputs_to_file('tst.out', output)
        hashwrapper.printout(output, filename='tst.out')

    output = output[0]
    if args.generate_plot:
        sdr = [round(output['strike']), round(output['dip']), round(output['rake'])]
        #sdr = [120, 60, -50]
        title='evid:%d' % args.evid
        if args.use_amplitudes:
            title='evid:%d + s/p ratios' % args.evid
        filename = '%d.amps.png' % args.evid if args.use_amplitudes else '%d.noamps.png' % args.evid
        if args.workdir:
            filename = os.path.join(args.workdir, filename)

        test_stereo(first_motions,sdr,title=title, filename=filename)
        #plot(first_motions,sdr)

    if args.insert_db:
        logger.info("Insert into db")
        mecds = []
        mecchans = []
        for k, name in enumerate(event['sname']):
            mecd = Mecdata(mecid=9999, polarity=mecdata_polarities[k], time1=mecdata_pick_times[k])
            #print(name, mecchan_nscl[k], event['p_pol'][k], mecdata_polarities[k], UTCDateTime(mecdata_pick_times[k] - leapsecs, precision=3),
                #mecd.mecdataid, event['qdist'][k], event['qazi'][k], event['qthe'][k])
            net, sta, cha, loc = mecchan_nscl[k].split('.')
            mecchan = Mecchannel(mecdataid=mecd.mecdataid, net=net, sta=sta, seedchan=cha, location=loc)
            mecds.append(mecd)
            mecchans.append(mecchan)
        insert_mec_db(output, orig, mecds, mecchans)


    return


def get_optional_args():

    # Optional args - follow flag names from fprun
    o = []
    o.append(optional_arg(arg=['-a', '--use-amplitudes', '--use-amplitude-ratios'], type=bool, description='',
                           help='Use S/P amp ratios in addition to P polarities'))
    o.append(optional_arg(arg=['-f', '--ampfile'], type=str, description='',
                           help='Path to file to read S/P amp ratios from, else compute them on the fly'))
    o.append(optional_arg(arg=['-p', '--insert-db'], type=bool, description='',
                           help='Insert Mec into aqms table'))
    o.append(optional_arg(arg=['-y', '--generate-plot'], type=bool, description='',
                           help='Generate focalmech (stereo) plot'))

    # --configfile, --logfile, --loglevel, --logdir are all added automagically

    o.append(optional_arg(arg=['-w', '--workdir', '--work-dir'], type=str, description='',
                          help='Path to workdir - can also read from config:WORK_DIR'))
    o.append(optional_arg(arg=['-m', '--min-mag', '--minmag'], type=float, description='',
                           help='minimum mag to compute focal mech'))
    o.append(optional_arg(arg=['-g', '--max-agap', '--maxagap',], type=float, description='',
                           help='maximum azimuthal gap to compute focal mech'))

    o.append(optional_arg(arg=['-r', '--reverse-polarity-file'], type=str, description='',
                           help='Path to reverse polarity file'))

    o.append(optional_arg(arg=['-i', '--tomlfile'], type=str, description='',
                           help='Path to hashdriver style tomlfile settings with paths to plfile, fpfile, etc'))

    return o

def insert_mec_db(output, orig, mecds, mecchans):
    from aqms_pdl import Session
    focal_mechanism = obspy_focalmech(output)
    obspy_event = Event(origins= [Origin(time=UTCDateTime(orig.datetime))])
    with Session.begin() as session:
        if focal_mechanism.comments:
            n = len(focal_mechanism.comments)
            commid = getSequence('remark', 1)[0]
            #for i in range(n):
            for i, remark in enumerate(focal_mechanism.comments):
                lineno = i+1
                #print("Lineno [%d]: %s" % (lineno, remark.text))
                rmk = Remark(commid=commid, lineno=lineno, remark=remark.text[:80])
                session.add(rmk)
                session.flush()
                logger.info("Add commid:%d lineno:%d rmk:%s" % (rmk.commid, rmk.lineno, rmk.remark))

        aqms_mec = obspy_focal_mechanism_to_aqms(focal_mechanism, obspy_event, orid=orig.orid,
                                                    mecid=9999, auth=orig.auth, subsource='HashRun')
        aqms_mec.commid = commid
        mecid = focalmec_already_in_db(aqms_mec)
        if mecid is None:
            insert_mechanism = True
            mecid = getSequence('mechanism', 1)[0]
            #mecid = 1234567
            aqms_mec.mecid = mecid
            #print("Request mecid returned:%s" % (aqms_mec.mecid))
            logger.info("Request mecid returned:%s" % (aqms_mec.mecid))

            mecdata = []

            session.add(aqms_mec)
            for mecd, mecchan in zip(mecds, mecchans):
                mecd.mecid = mecid
                session.add(mecd)
                session.flush()
                mecchan.mecdataid=mecd.mecdataid
                session.add(mecchan)
                session.flush()
                logger.info("mecid:%d mecd.mecdataid:%d mecchan:%s.%s.%s.%s" %
                           (mecid, mecd.mecdataid, mecchan.net, mecchan.sta, mecchan.seedchan, mecchan.location))
                #print(name, mecchan_nscl[k], event['p_pol'][k], mecdata_polarities[k], UTCDateTime(mecdata_pick_times[k] - leapsecs, precision=3),
                    #mecd.mecdataid, event['qdist'][k], event['qazi'][k], event['qthe'][k])

            #for k, name in enumerate(event['sname']):
                #mecd = Mecdata(mecid=mecid, polarity=mecdata_polarities[k], time1=mecdata_pick_times[k])
                #session.add(mecd)
                #session.flush()
                #print(name, mecchan_nscl[k], event['p_pol'][k], mecdata_polarities[k], UTCDateTime(mecdata_pick_times[k] - leapsecs, precision=3),
                    #mecd.mecdataid, event['qdist'][k], event['qazi'][k], event['qthe'][k])
                #net, sta, cha, loc = mecchan_nscl[k].split('.')
                #mecchan = Mecchannel(mecdataid=mecd.mecdataid, net=net, sta=sta, seedchan=cha, location=loc)
                #session.add(mecchan)
                #session.flush()

        else:
            logger.info("Mec mecid:%d is a match for this mechanism --> use this mecid" % mecid)

    return


from obspy.core.event.source import FocalMechanism, NodalPlane, NodalPlanes
from obspy.core.event.base import Comment
from obspy.imaging.beachball import aux_plane

def obspy_focalmech(out):
    p1 = NodalPlane(strike=out['strike'], dip=out['dip'], rake=out['rake'])
    s,d,r = aux_plane(out['strike'], out['dip'], out['rake'])
    p2 = NodalPlane(strike=s, dip=d, rake=r)

    comments = []
    comments.append("HASH v1.2 Quality=[%s] use_S/P_amp_ratios=%s" % (out['quality'], out['use_amplitudes']))
    comments.append("azimuthal_gap=%.2f" % out['azim_gap'])
    comments.append("takeoff_gap=%.2f" % out['theta_gap'])
    comments.append("number_P_polarities=%d" % (out['number_P_polarities']))
    if out['use_amplitudes']:
        comments.append("number_SP_amp_ratios=%d S_P_log10_misfit=%.2f" % (out['number_SP_amp_ratios'], out['S_P_log10_misfit']))
        comments.append("stdr=%.2f misfit=%.2f mavg=%.2f rms_angle=%.2f" %
                (out['stdr'], out['misfit'], out['mavg'], out['rms_angle']))

    comments = [Comment(text=comment) for comment in comments]

    fc = FocalMechanism(nodal_planes = NodalPlanes(nodal_plane_1=p1, nodal_plane_2=p2),
                        azimuthal_gap = out['azim_gap'],
                        station_polarity_count = out['station_polarity_count'],
                        station_distribution_ratio = out['stdr'],
                        misfit = out['misfit'],
                        evaluation_mode = 'automatic',
                        evaluation_status = 'preliminary',
                        comments = comments,
                        method_id=ResourceIdentifier('HASH_v1.2')
                        #comments = [Comment(text="HASH v1.2 Quality=[%s]" % out['quality'])]
                       )

    return fc



def plot(first_motions, sdr=[]):

    outputfile=None
    format="png"
    BAD = "red"
    GOOD = "black"
    facecolor = "lightgrey"
    PHI = 90.
    width = 1000 # width of the whole figure

    if sdr:
        s1,d1,r1 = sdr
        mec = NodalPlane(s1,d1,r1)

    # A figure for each mechanism
    fig = plt.figure(figsize=(5,5), dpi=100)
    fig.subplots_adjust(left=0, bottom=0, right=1, top=1)
    fig.set_figheight(width // 100)
    fig.set_figwidth(width // 100)
    ax = fig.add_subplot(111, aspect='equal')

    # expand the axes beyong the beachball to plot labels and text
    ax.set_ylim(-200,200)
    ax.set_xlim(-150,300)

    # legend
    xdown = 160 * np.cos(15*D2R)
    ydown = 160 * np.sin(15*D2R)
    xup = xdown
    yup = ydown - 15
    ax.plot(xdown,ydown, marker='o', markersize=10, mew=1, mec='black', color='none')
    ax.plot(xup,yup,marker='x', markersize=10, mew=1, mec='black', color='none')
    ax.annotate('Down first-motion (dilatation)', (xdown,ydown), (xdown+10,ydown))
    ax.annotate('Up first-motion (compression)', (xup,yup), (xup+10,yup))
    ax.plot(xup,yup-15, marker='o', markersize=10, mew=2, mec='black', color='none')
    ax.annotate('Boldface: upgoing rays', (xup,yup-15), (xup+10,yup-15))
    ax.annotate('(take-off > 90, with 0=down)', (xup,yup), (xup+10,yup-25))
    ax.plot(xup,yup-40, marker='x', markersize=10, mew=1, mec='red', color='none')
    ax.annotate('Red font: discrepant observations', (xup,yup-40), (xup+10,yup-40))
    ax.annotate('*: reversed channel', (xup,yup-55), (xup+10,yup-55))
    ax.annotate('(polarity corrected)', (xup,yup-55), (xup+10,yup-65))

    # axes + ticks (on=True)
    ax.axison = False

    # plot the mechanism and associated data

    #outputfile='81001888.png'

    if mec:
        collection = beach(mec,facecolor=facecolor,zorder=1)
        ax.add_collection(collection)
        ax.grid()

# sta=LR01 chan=HHZ az=340.52360234072 ainc=116 pol=U
    for fm in first_motions:
        az = float(fm.az)
        ema = float(fm.ema)
        if ema > 90:
            ema = 180. - ema
            az = 180. + az
        r = np.sqrt(2) * np.sin(0.5*ema*D2R)
        #r = np.sqrt(2 * np.sin(0.5*ema*D2R))
        #r = np.sin(ema*D2R)
        if 'c' in fm.fm:
            marker='x'
        else:
            marker='o'

        #print("call pol2cart((PHI=%s -az=%s)*D2R=%s,100*r=%s" % (PHI, az, D2R, r))
        x,y = pol2cart((PHI-az)*D2R,100*r)
        ax.plot(x,y, marker=marker, markersize=10, mew=2, mec=GOOD, color='none', zorder=3)
        ax.annotate(fm.sta,(x,y), (0.01*x,0.01*y), textcoords='offset points', zorder=3)


    # label fault-planes
    """
    shift = 30 # in points
    x1,y1 = pol2cart((PHI-120)*D2R,100)
    x2,y2 = pol2cart((PHI-240)*D2R,100)
    ax.annotate('(120, 60, -50)', (x1,y1), (0.2*x1 - (100-x1)/100*shift,0.2*y1), textcoords='offset points', arrowprops={'arrowstyle' : 'simple'}, zorder=4)
    ax.annotate('(240, 48, -138)', (x2,y2), (0.2*x2 - (100-x2)/100*shift,0.2*y2), textcoords = 'offset points', arrowprops={'arrowstyle' : 'simple'}, zorder=4)
    """
    # mark the center of the beachball
    ax.plot(0,0, marker='+', mec='black', zorder=3)

    ax.autoscale_view(tight=False, scalex=True, scaley=True)
    # export
    if outputfile:
        if format:
            fig.savefig(outputfile, dpi=100, transparent=True, format=format)
        else:
            fig.savefig(outputfile, dpi=100, transparent=True)
    else:
        plt.show()

    return



def test_stereo(first_motions,sdr=[], title='title', filename=None):
    '''
    Plots points with given azimuths, takeoff angles, and
    polarities on a stereonet. Will also plot both planes
    of a double-couple given a strike/dip/rake
    '''
    import matplotlib.pyplot as plt
    import mplstereonet
    from obspy.imaging.beachball import aux_plane

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='equal_area_stereonet')

    for fm in first_motions:
        if fm.iph == 'P':
            logger.debug("sta:%s cha:%s iph:%s --> fm=%s" % (fm.sta, fm.cha, fm.iph, fm.fm))
        else:
            logger.debug("sta:%s cha:%s iph:%s --> Skip" % (fm.sta, fm.cha, fm.iph))

        az = fm.az
        takeoff = fm.ema
        logger.debug("Before stn:%s az:%.1f takeoff:%.1f" % (fm.sta, az, takeoff))

        if takeoff > 90:
            takeoff = 180 - takeoff
            az = 180 + az

        logger.debug(" After stn:%s az:%.1f takeoff:%.1f 90-takeoff:%.1f" % (fm.sta, az, takeoff, 90-takeoff))
        #ax.line(plunge, az)
        if "c" in fm.fm:
            ax.line(90-takeoff, az, 'rx')
        elif "d" in fm.fm:
            ax.line(90-takeoff, az, 'bo')

        #print("stn:%s az:%.1f takeoff:%.1f 90-takeoff:%.1f" %
              #(fm.sta, az, takeoff, 90-takeoff))

        # ax.line seems to take plunge but pole() takes takeoff ??
        lon, lat = mplstereonet.pole(az+90, takeoff)
        ax.annotate(fm.sta, ha='center', va='center', xy=(lon,lat),
                    xytext=(0, 5), textcoords='offset points')

    ax.grid()

    if sdr:
        s1,d1,r1 = sdr
        s2,d2,r2 = aux_plane(*sdr)
        s2 = round(s2)
        d2 = round(d2)
        r2 = round(r2)
        logger.info("str dip rake")
        logger.info("%3d %3d %3d" % (s1, d1, r1))
        logger.info("%3d %3d %3d" % (s2, d2, r2))

        ax.plane(s1,d1,'g')
        ax.rake(s1,d1,r1, 'g+')
        ax.pole(s1,d1, 'g.')

        ax.plane(s2,d2, 'r')
        ax.pole(s2,d2, 'r.')
        ax.rake(s2,d2,r2, 'r+')
        #plt.suptitle("(%s, %s, %s)" % (s1,d1,r1), fontsize=18)
        #plt.title("(%s, %s, %s)" % (s1,d1,r1), y=-0.1, x=-0.1, fontsize=10)
        #plt.annotate("(%s, %s, %s)" % (s1,d1,r1), (-2.,-1.576), horizontalalignment='right', fontsize=10)

        voff = 0
        if s1 <= 180:
            hoff = 10
            ha = 'left'
        else:
            hoff = -10
            ha = 'right'
        lon, lat = mplstereonet.pole(s1+90, 90)
        ax.annotate("[%s, %s, %s]" % (s1,d1,r1), ha='left', va='center', xy=(lon,lat),
                    xytext=(hoff, voff), textcoords='offset points')
        if s2 <= 180:
            hoff = 5
            ha = 'left'
        else:
            hoff = -5
            ha = 'right'

        lon, lat = mplstereonet.pole(s2+90, 90)
        ax.annotate("[%s, %s, %s]" % (s2,d2,r2), ha=ha, va='center', xy=(lon,lat),
                    xytext=(hoff, voff), textcoords='offset points')

    ax.set_title(title, y=1.07, fontsize=10)


    if filename:
        plt.savefig(filename)
    else:
        plt.show()

def check_polarity(reversed_file):

    """
AQUA 19920101 19921231
BAC  19950624 19960124
BAHA 19940101 0
    """
    with open(reversed_file, 'r') as f:
        lines = f.readlines()
    #reversals = []
    reversals = {}
    for line in lines:
        name, start, end = line.strip().split()
        start = UTCDateTime('1970-01-01') if start == '0' else UTCDateTime(start)
        end   = UTCDateTime('3000-01-01') if end == '0' else UTCDateTime(end)
        #print(name, start, end)
        #reversals.append((name, start, end))
        reversals[name] = (start, end)

    return reversals

def read_ampfile(ampfile):
    """
 sta:RW01 az:16.62 ih:82.00 dist: 3.77 p_amp:9.34 s_amp:412.24 sp_ratio:44.12
      cha:HHZ pick_time:2022-04-01T06:08:26.910Z pre_pick:0.15 pos_pick:0.25 max:9.3437e+00 min:-7.3442e+00
      cha:HHE pick_time:2022-04-01T06:08:28.060Z pre_pick:0.15 pos_pick:0.25 max:2.5322e+02 min:-3.1194e+02
      cha:HHN pick_time:2022-04-01T06:08:28.060Z pre_pick:0.15 pos_pick:0.25 max:2.6200e+02 min:-2.6951e+02
 sta:RW02 az:23.34 ih:67.00 dist: 2.72 p_amp:-20.17 s_amp:725.53 sp_ratio:-35.97
      cha:HHZ pick_time:2022-04-01T06:08:26.760Z pre_pick:0.15 pos_pick:0.25 max:1.7777e+01 min:-2.0168e+01
      cha:HHE pick_time:2022-04-01T06:08:27.790Z pre_pick:0.15 pos_pick:0.25 max:2.9182e+02 min:-3.5490e+02
      cha:HHN pick_time:2022-04-01T06:08:27.790Z pre_pick:0.15 pos_pick:0.25 max:6.3280e+02 min:-4.8052e+02

    #with open(ampfile, "r") as f:
        #lines = f.readlines()
    """

    import json
    contents = open(ampfile, "r").read()
    event = json.loads(contents)

    return event

def log_args(args):
    logger.info(f"{'=' * 15 + ' arg:':>20} {'value ====='}")
    for k,v in vars(args).items():
        logger.info(f"{k+':':>20} {v}")
    logger.info(f"{'=' * 20} {'=' * 20}")

if __name__ == "__main__":
    main()

        #plunge = 20    # plunge = 90 - takeoff
        #bearing = 50   # bearing = azimuth
        # These are equivalent:
        #ax.rake(bearing-90, plunge,90, 'gx')
        #ax.line(plunge, bearing, 'b+')

