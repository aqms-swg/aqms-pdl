import argparse
import logging
logger = logging.getLogger()
import os
import sys

from obspy import UTCDateTime
from obspy import read

import numpy as np
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
#from obspy.imaging.beachball import NodalPlane, beach, pol2cart, xy2patch, D2R

from aqms_pdl import createSession
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg, optional_arg
from aqms_pdl.libs.libs_aqms import get_assoc_thresh, associate_RSN_origin_with_PDL_origins

from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.libs_db import query_origin_by_evid, create_and_insert_assocevents
from aqms_pdl.libs.libs_db import get_arrivals_for_evid, get_event_info
from aqms_pdl.libs.libs_db import get_leapsecs, call_procedure
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config, read_config

from aqms_mkv0.libs.libs_db import get_filename, get_subdir, get_waveroot, get_waveform_list
from aqms_mkv0.libs.libs_db import fix_Session as fix_Session_mkv0

#from aqms_pdl.libs.schema_aqms_PI import *
#from aqms_pdl.libs.schema_aqms_WF import *

#from waveform import WaveformPlotting as wp

def main():
    """
    calc_amps - Calculate P, S, S/P amplitudes from aqms waveforms for input evid
    """
    fname = 'calc_amps'

    r = required_arg(arg=['-e', '--evid'], type=int, description='EVID', help='evid to calc amps for')
    #required_args = {['-e', '--evid']: 'EVID'}
    o = required_arg(arg=['-s'], type=bool, description='', help='write amp calcs to file=evid.amps')
    args, parser = parse_cmd_line(required_args=[r], optional_args=[o])

    config, log_msgs = read_config(debug=False, requireConfigFile=True, configfile=args.configfile)
    update_args_with_config(args, config, fname)

    configure_logger(**vars(args))
    logger = logging.getLogger()

    logger.info(sys.argv)
    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    # Configure the Session for the whole module in __init__.py:
    createSession(config)
    # import the configured Session into the libs_db pkg for use
    set_Session()

    arrivals = get_arrivals_for_evid(args.evid)
    if not arrivals:
        logger.error("Found no arrivals for evid:%s" % args.evid)
        exit(2)

    save = True if args.s else False
    calc_amp_ratios(arrivals, args, config, save=save)



def calc_amp_ratios(arrivals, args, config, save=False):

    fname = 'calc_amp_ratios'

    arrs = {}

    for arrival in arrivals:
        if arrival.sta not in arrs:
            arrs[arrival.sta] = {}
        d = arrs[arrival.sta]
        if arrival.iphase == 'P':
            d['P'] = arrival
        elif arrival.iphase == 'S':
            d['S'] = arrival
        logger.info("%s: %s.%s.%s.%s [%s]" % (fname, arrival.net, arrival.sta, arrival.seedchan,
                                              arrival.location, arrival.iphase))

    amp_arrs = {}
    for sta, d in arrs.items():
        if 'P' in d and 'S' in d:
            logger.debug("sta:%s has P & S --> include" % sta)
            amp_arrs[sta]=d
        else:
            logger.debug("sta:%s Does NOT have both P & S --> remove" % sta)

    arrs = amp_arrs

    '''
    for k,v in arrs.items():
        print(k, v['P'].seedchan, v['P'].datetime, v['P'].iphase,
                 v['S'].seedchan, v['S'].datetime, v['S'].iphase)
    '''

    # set Session in aqms_mkv0.libs.libs_db so we can use the WF functions there
    from .. import Session
    fix_Session_mkv0(Session)

    evid = int(args.evid)
    wvroot = get_waveroot()
    if wvroot is None:
        logger.error("db waveroot is empty --> exitting!")
        exit(2)
    fileroot = wvroot.fileroot

    event = get_event_info(evid)
    if event is None:
        logger.error("Unable to locate evid:%d in aqms db! --> Exitting!" % evid)
        exit(2)

    leapsecs = get_leapsecs(event.preferred_origin.datetime)
    event.preferred_origin.leapsecs = leapsecs
    waveform_list = get_waveform_list(evid=evid, only_accelerometers=False)
    logger.info("get_waveform_list returned n=%d" % len(waveform_list))

    for w in waveform_list:
        #print(w.sta, w.seedchan, w.nbytes, w.fileid)

        if w.sta not in arrs:
            logger.debug("calc_amps: sta:%s *not* in arrs --> Skip" % w.sta)
            continue

        arr = arrs[w.sta]
        if w.seedchan[2] == 'Z':
            a = arr['P']
        else:
            a = arr['S']

        # Both arrival time and waveform.datetime_on/_off have been adjusted by leapsecs!

        filename = get_filename(w.fileid)
        if filename.subdirid:
            subdir = get_subdir(filename.subdirid)
            subdirname = subdir.subdirname
        else:
            # subdir table doesn't seem to be populated on okesa3 at least
            subdirname = str(evid)

        filepath = os.path.join(fileroot, subdirname, filename.dfile)

        logger.info("waveform scnl:%s.%s archive:%s fileid:%s dfile:%s filename.on:%s filename.off:%s filepath:%s" %
                    (w.sta, w.seedchan, w.archive, w.fileid, filename.dfile, filename.datetime_on,
                    filename.datetime_off, filepath))

        call_params = [filepath, w.traceoff, w.nbytes, w.datetime_on, w.datetime_off]

        logger.info("Call wave.get_waveform_blob fname:%s w.traceoff:%s w.nbytes:%d w.start:%s w.end:%s" %
                    (filepath, w.traceoff, w.nbytes, w.datetime_on, w.datetime_off))
        ms_blob = call_procedure('wave.get_waveform_blob', call_params, config)
        if len(ms_blob) == 0:
            logger.error("get_waveform_blob returned None!")
            continue

        #plot_sta = 'KK06'

        with open('test.ms', 'wb') as f:
            # But ms_blob is returned with leapsecs REMOVED from mseed start/endtime
            f.write(bytes(ms_blob))

        if 1:
            st = read('test.ms')
            trace = st[0]
            h = trace.stats
            sncl = "%s.%s.%s.%s" % (h.station, h.network, h.channel, h.location)
            logger.info("mseed hdr:%s start:%s end:%s npts:%d srate:%.1f filesize:%d" %
                       (sncl, h.starttime, h.endtime, h.npts, h.sampling_rate, h.mseed.filesize))
            st.detrend('linear')
            st.filter('highpass', freq=10)
            st.integrate(method='spline')
            #if h.station == plot_sta:
                #st.plot(type='relative')
            #waveform = wp(stream=st, color='k', type='relative', xlabel='Seconds',number_of_ticks=8, tick_rotation=0, title='Something else!', label='Heres the label', addOverlay=False, extras=extras)
            pick_time = a.datetime - leapsecs
            pre_pick = 0.15
            pos_pick = 0.25
            #logger.info("mseed hdr:%s pick_time:%s ==> trim from %s to %s" %
                       #(sncl, UTCDateTime(pick_time), UTCDateTime(pick_time - pre_pick), UTCDateTime(pick_time + pos_pick)))
            st.trim(starttime=UTCDateTime(pick_time - pre_pick), endtime=UTCDateTime(pick_time + pos_pick))
            if len(st) == 0:
                logger.error("mseed hdr:%s trim from %s to %s Returned 0 traces! ==> Remove stn=%s from P/S amps arrs" %
                            (sncl, UTCDateTime(pick_time - pre_pick), UTCDateTime(pick_time + pos_pick), h.station))
                key = arrs.pop(h.station)
                #print("pop key=%s" % key)
                continue
                #break

            maxval = np.max(st[0].data)
            minval = np.min(st[0].data)
            logger.info("pick_time=%s phase:%s maxval:%.2f minval:%.2f max:%.2f" %
                  (pick_time, a.iphase, maxval, minval, max(minval, maxval, key=abs)))
            #st.plot()
            extras={}
            extras['pick_time'] = pick_time
            extras['pick_phase'] = a.iphase
            #waveform = wp(stream=st, color='k', type='relative', xlabel='Seconds',number_of_ticks=8, tick_rotation=0, title='Something else!', label='Heres the label', addOverlay=False, extras=extras)
            #waveform.plot_waveform()
            #exit()

            key = "%s_amp" % a.iphase
            if key not in arr:
                arr[key] = {}
            d = arr[key]
            d[w.seedchan] = {}
            d[w.seedchan]['maxval']=maxval
            d[w.seedchan]['minval']=minval
            d[w.seedchan]['pick_time']=pick_time
            d[w.seedchan]['pre_pick']=pre_pick
            d[w.seedchan]['pos_pick']=pos_pick

            #print(arr)

            #exit()
        os.remove('test.ms')

    #sp_ratio = {}
    sname = []
    qazi = []
    qthe = []
    emas = []
    dist = []
    sp_ratio = []
    p_amp = []
    s_amp = []
    fms = []
    event = {}
    event['sname'] = sname
    event['qazi'] = qazi
    event['qthe'] = qthe
    event['emas'] = emas
    event['dist'] = dist
    event['sp_ratio'] = sp_ratio
    event['P_amp'] = p_amp
    event['S_amp'] = s_amp
    event['processing'] = {}
    event['processing']['filter'] = 'highpass f > 10Hz'
    event['processing']['ground_motion'] = 'velocity'
    event['fms'] = fms

    event['arrs'] = arrs

    for sta, arr in arrs.items():
        logger.info("Get P/S amps for sta=%s" % sta)
        for key in ['P_amp', 'S_amp']:
            for cha, amp in arr[key].items():
                maxval = amp['maxval']
                minval = amp['minval']
                amax = max(minval, maxval, key=abs)
                logger.info("sta:%s  %s  cha:%s  maxval:%.2f  minval:%.2f  amax:%.2f" %
                           (sta, key, cha, maxval, minval, amax))

        p_seedchan = [*arr['P_amp']][0]
        p_dict = arr['P_amp'][p_seedchan]
        P_amp = max(p_dict['minval'], p_dict['maxval'], key=abs)
        #P_amp = max(arr['P_amp']['HHZ']['minval'], arr['P_amp']['HHZ']['maxval'], key=abs)
        #S_E = max(arr['S_amp']['HHE']['minval'], arr['S_amp']['HHE']['maxval'], key=abs)
        #S_N = max(arr['S_amp']['HHN']['minval'], arr['S_amp']['HHN']['maxval'], key=abs)

        for seedchan, s_dict in arr['S_amp'].items():
            if seedchan[2] in ['E', '2']:
                S_E = max(s_dict['minval'], s_dict['maxval'], key=abs)
                logger.info("%s: sta:%s S_E measured on seedchan:%s" % (sta, fname, seedchan))
            elif seedchan[2] in ['N', '1']:
                S_N = max(s_dict['minval'], s_dict['maxval'], key=abs)
                logger.info("%s: sta:%s S_N measured on seedchan:%s" % (sta, fname, seedchan))
            else:
                logger.error("%s: sta:%s Unexpected S_amp seedchan:[%s]" % (sta, fname, seedchan))
                exit(2)

        S_amp = np.sqrt(S_E*S_E + S_N*S_N)
        #sp_ratio[sta] = S_amp/P_amp

        az = arr['P'].azimuth
        ema = arr['P'].ema
        fm = arr['P'].fm
        x = arr['P'].distance

        sname.append(sta)
        qazi.append(float(az))
        qthe.append(180.-float(ema))
        emas.append(float(ema))
        dist.append(float(x))
        sp_ratio.append(S_amp/P_amp)
        p_amp.append(P_amp)
        s_amp.append(S_amp)
        fms.append(fm)

        logger.info("calc_amps sta:%s  Dist:%.2f Az:%.2f i:%.2f FM:%s P_amp:%.2f S_amp:%.2f S/P:%.2f" %
                   (sta, x, az, ema, fm, P_amp, S_amp, (S_amp/P_amp)))

    if save:
        write_amps(event, "%d.amps" % evid)

    return event


def write_amps(event, outfile):

    # Serializing json
    import json
    event_copy = event.copy()
    # MTH: This has to be done since arr['P'] = aqms arrival is not serializable
    for sta, arr in event_copy['arrs'].items():
        arr.pop('P')
        arr.pop('S')
    with open('%s.json' % outfile, 'w', encoding='utf-8') as f:
        json.dump(event, f, ensure_ascii=False, indent=4)

    with open(outfile, 'w') as f:

        for k,sta in enumerate(event['sname']):
            f.write("sta:%s az:%.2f ema:%.2f ih:%.2f dist:%.2f fm:%s p_amp:%.2f s_amp:%.2f sp_ratio:%.2f\n" %
                (sta, event['qazi'][k], event['emas'][k], event['qthe'][k], event['dist'][k], event['fms'][k],
                event['P_amp'][k], event['S_amp'][k], event['sp_ratio'][k]))

            for key in ['P_amp', 'S_amp']:
                for cha,d in event['arrs'][sta][key].items():
                    pick = UTCDateTime(d['pick_time'], precision=3)
                    f.write("      cha:%s pick_time:%s pre_pick:%.2f pos_pick:%.2f max:%.4e min:%.4e\n" %
                        (cha, pick, d['pre_pick'], d['pos_pick'], d['maxval'], d['minval']))


if __name__ == "__main__":
    main()
