import argparse
import logging
import os
import sys
from sqlalchemy import exc
from sqlalchemy import sql

from aqms_pdl import createSession
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg
from aqms_pdl.libs.libs_aqms import get_assoc_thresh, associate_RSN_origin_with_PDL_origins
from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.libs_db import query_origin_by_evid, create_and_insert_assocevents
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config, read_config
from aqms_pdl.libs.schema_aqms_PI import Magprefpriority


def main():
    """
    Usage: >python magprefpriority -f ../resources/magprefpriority_NC -c /path/to/config.yml --loglevel DEBUG
    Read in template file (which looks like the magprefpriority table)
    Loop through lines and use to create a new Magprefpriority table in the db

    Usage: >python magprefpriority.py -f /path/to/template_file

    See: resources/magprefpriotity_NC for an example templatet_file
    """

    fname = 'magprefpriority'
    r = required_arg(arg=['-f', '--template'], type=str, description='table', help='path to magprefpriority template file')
    args, parser = parse_cmd_line(required_args=[r])

    magprefile = args.template

    config, log_msgs = read_config(debug=False, requireConfigFile=True, configfile=args.configfile)
    update_args_with_config(args, config, fname)

    configure_logger(**vars(args))
    logger = logging.getLogger()

    logger.info(sys.argv)
    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    if not os.path.isfile(magprefile):
        logger.error("Filepath:%s does not exist! --> Exitting!" % magprefile)
        exit(2)

    logger.warning("Now it gets real")
    #exit()
    # Configure the Session for the whole module in __init__.py:
    createSession(config)
    # import the configured Session into the libs_db pkg for use
    set_Session()

    update_magprefpriority_table(magprefile)


def update_magprefpriority_table(magprefile):

    with open(magprefile, 'r') as f:
        lines = f.readlines()

    from aqms_pdl import Session

    with Session.begin() as session:
        # Delete all existing rows
        session.query(Magprefpriority).delete(synchronize_session=False)

        NULL = sql.null()

        for line in lines[3:-1]:
            print(line.strip())
            cols = [col.strip() for col in line.split('|')]
            #for i, col in enumerate(cols):
                #print("col[%2d]=[%s]" % (i, col))

            region_name    = cols[0]
            datetime_on    = cols[1]
            datetime_off   = cols[2] if cols[2] else NULL
            priority       = float(cols[3])
            magtype        = cols[4]
            auth           = cols[5]
            subsource      = cols[6] if cols[6] else NULL
            magalgo        = cols[7] if cols[7] else NULL
            minmag         = float(cols[8]) if cols[8] else NULL
            maxmag         = float(cols[9]) if cols[8] else NULL
            minreadings    = float(cols[10]) if cols[10] else NULL
            maxuncertainty = float(cols[11]) if cols[11] else NULL
            quality        = float(cols[12]) if cols[12] else NULL

            magpref = Magprefpriority(region_name=region_name, datetime_on=datetime_on, datetime_off=datetime_off,
                            priority=priority, magtype=magtype, auth=auth, subsource=subsource,
                            magalgo=magalgo, minmag=minmag, maxmag=maxmag, minreadings=minreadings,
                            maxuncertainty=maxuncertainty, quality=quality)
            session.add(magpref)
            print(magpref)

        try:
            #logger.info("commit the session")
            session.commit()
        except exc.SQLAlchemyError as e:
            logger.error("Could not commit session:%s" % e)
            logger.error("Rollback session and exit")
            session.rollback()
            raise

    return


if __name__ == "__main__":
    main()
