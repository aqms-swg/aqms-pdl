import argparse
import logging
import os
import sys

from aqms_pdl import createSession
from aqms_pdl.libs.cmd_line import parse_cmd_line
from aqms_pdl.libs.libs_db import set_Session, query_last_n_events, get_leapsecs
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config, read_config

from aqms_pdl.libs.libs_db import get_leapsecs

def main():
    """
        Simple app to help debug/test connection to aqms db
        If successful, will print out 1-liner summary of 3 most recent events

        Usage: >python test_connection.py
    """

    fname = 'test-connection'
    args, parser = parse_cmd_line()
    config, log_msgs = read_config(debug=True, requireConfigFile=True, configfile=args.configfile)
    update_args_with_config(args, config, fname)

    args.logfile = None
    args.logconsole = True

    configure_logger(**vars(args))
    logger = logging.getLogger()

    logger.info(sys.argv)
    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    # Configure the Session for the whole module in __init__.py:
    createSession(config)
    # import the configured Session into the libs_db pkg for use
    set_Session()


    nevents, origins = query_last_n_events(3)

    print("Found total of [%d] events in db. Here are last [%d]" %
         (nevents, len(origins)))

#          2021-12-20 20:10:21.40 40.30 -124.62  9.0 EW PDL 1640031020
    print("YYYY-MM-DD HH:MM:SS.ss  Lat   Lon    Dep  AU SUB LOCEVID")
    print("========================================================")
    import datetime
    for origin in origins:
       date = datetime.datetime.utcfromtimestamp(origin.datetime - get_leapsecs(origin.datetime))
       #print("%.1f %s" % (origin.datetime, date.strftime('%Y-%m-%d %H:%M:%S.%f')[:-4]))
       print("%s %.2f %.2f %4.1f %s %s %s" %
             (date.strftime('%Y-%m-%d %H:%M:%S.%f')[:-4], origin.lat, origin.lon, origin.depth,
              origin.auth, origin.subsource, origin.locevid))



if __name__ == "__main__":
    main()
