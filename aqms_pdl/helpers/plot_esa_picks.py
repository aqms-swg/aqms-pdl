import argparse
import logging
import os
import sys

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

from obspy import UTCDateTime

from aqms_pdl import createSession
from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config
from aqms_pdl.libs.libs_util import read_config
from aqms_pdl.libs.schema_aqms_PI import Event, Origin, Arrival, Assocaro

from cmd_line import processCmdLine, required_arg, optional_arg


def main():
    """
        Usage: >python plot_esa_picks -c picks.RW [--min-lat 35.74] [--max-lat 35.88]
                                                  [--min-lon -97.97] [--max-lon -97.83]
                                                  [--starttime 2022-05-04T02] [--endtime 2022-05-25]

        where picks.RW is a yaml config file:
            DB_HOST: okesa2.isti.com
            DB_NAME: oklahoma
            DB_PORT: xxxx
            DB_USER: xxxx
            DB_PASSWORD: xxxxx

            ESA: 'RW'

            starttime: 2022-02-22T00
            endtime: 2022-05-14T15:30

            min_lat: 35.8828
            max_lat: 36.0446
            min_lon: -98.2595
            max_lon: -98.1221

        and any cmd line args will take precedence

    """

    fname = 'plot_esa_picks'
    #r = required_arg(arg=['-e', '--evid'], type=int, description='EVID', help='evid to generate focal mech for')
    # Optional args - follow flag names from fprun
    # -c configfile is already an optional arg specified in cmd_line.py
    r = required_arg(arg=['--configfile', '--config-file', '--configFile', '--config', '-c'], type=str, description='configfile', help='config yml file')
    required_args = [r]
    o = []

    o.append(optional_arg(arg=['--min-lat', '--min_lat', '--minimum-latitude', '--minimum_latitude'], type=float, description='',
                           help='minimum latitude'))
    o.append(optional_arg(arg=['--max-lat', '--max_lat', '--maximum-latitude', '--maximum_latitude'], type=float, description='',
                           help='maximum latitude'))
    o.append(optional_arg(arg=['--min-lon', '--min_lon', '--minimum-longitude', '--minimum_longitude'], type=float, description='',
                           help='minimum longitude'))
    o.append(optional_arg(arg=['--max-lon', '--max_lon', '--maximum-longitude', '--maximum_longitude'], type=float, description='',
                           help='maximum longitude'))

    o.append(optional_arg(arg=['--starttime', '--start', '--start-date', '--start_date'], type=UTCDateTime, description='',
                           help='start date'))
    o.append(optional_arg(arg=['--endtime', '--end', '--end-date', '--end_date'], type=UTCDateTime, description='',
                           help='end date'))

    args, parser = processCmdLine(required_args=required_args, optional_args=o)

    config, log_msgs = read_config(debug=False, requireConfigFile=True, configfile=args.configfile)

    if config:
        if 'logfile' not in config:
            config['logfile'] = "%s.log" % fname
        configure_logger(**config)
    else:
        configure_logger(**{'logconsole':True})
    logger = logging.getLogger()
    logger.debug(sys.argv)

    if config is None and "-h" not in sys.argv:
        for msg in log_msgs['info']:
            logger.info(msg)
        for msg in log_msgs['error']:
            logger.error(msg)
        logger.error("Can't continue without valid config to specify DB connection.  Exitting!")
        parser.print_help()
        exit(2)

    update_args_with_config(args, config, fname)
    configure_logger(**vars(args))
    logger = logging.getLogger()

    # If not on the cmd line then these fields better be in the config file:
    required_args = ['min_lat', 'max_lat', 'min_lon', 'max_lon', 'starttime', 'endtime', 'ESA']
    for k in required_args:
        if getattr(args, k, None) is None:
            if k not in config:
                logger.error("Can't proceed without param:%s.  Please specify this either on the cmd line or in the config file" % k)
                parser.print_help()
                exit(2)
            else:
                setattr(args, k, config[k])

    args.starttime = UTCDateTime(args.starttime)
    args.endtime = UTCDateTime(args.endtime)
    #print(args)

    logger.info(sys.argv)
    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    # Configure the Session for the whole module in __init__.py:
    createSession(config)
    # import the configured Session into the libs_db pkg for use
    set_Session()

    esa = args.ESA

    rt1 = pauls_query(args, 'RT1')
    ecs = pauls_query(args, 'ECS')

    rt1_nP = [ rt1[sta]['P'] for sta in rt1.keys() if 'P' in rt1[sta]]
    rt1_nS = [ rt1[sta]['S'] for sta in rt1.keys() if 'S' in rt1[sta]]

    ecs_nP = [ ecs[sta]['P'] for sta in ecs.keys() if 'P' in ecs[sta]]
    ecs_nS = [ ecs[sta]['S'] for sta in ecs.keys() if 'S' in ecs[sta]]
    stations = [sta for sta in ecs.keys()]
    title = '%s Station Picks' % esa
    for phase in ['P', 'S']:
        outfile = '%s.%s.png' % (esa, phase)
        rt1 = rt1_nP if phase == 'P' else rt1_nS
        ecs = ecs_nP if phase == 'P' else ecs_nS
        picks = '%s picks' % phase
        plot(rt1, ecs, stations, outfile=outfile, title=title, picks=picks)



def pauls_query(args, subsource):

    from aqms_pdl import Session
    with Session.begin() as session:

        # MTH: ignoring leapsecs in this query ...
        results = session.query(Event, Origin, Arrival, Assocaro).filter(Event.evid == Origin.evid).\
                                filter(Arrival.arid == Assocaro.arid).\
                                filter(Assocaro.subsource == subsource).\
                                filter(Assocaro.orid == Origin.orid).\
                                filter(Origin.rflag == 'A').\
                                filter(Origin.lon >= args.min_lon).\
                                filter(Origin.lon <= args.max_lon).\
                                filter(Origin.lat >= args.min_lat).\
                                filter(Origin.lat <= args.max_lat).\
                                filter(Origin.datetime >= args.starttime.timestamp).\
                                filter(Origin.datetime <= args.endtime.timestamp).\
                                order_by(Arrival.sta).\
                                all()
        session.expunge_all()

    return get_dict(results)


def get_dict(results):
    # 73002013 3003658 ECS 3027058 ECS P MC02 HHZ
    stadict = {}
    for event, origin, arrival, assocaro in results:
        ot = UTCDateTime(origin.datetime)
        if 1:
        #if ot >= start_date and ot <= end_date:
            #print(event.evid, origin.orid, origin.subsource, arrival.arid, arrival.subsource,
                #arrival.iphase, arrival.sta, arrival.seedchan)
            if arrival.sta not in stadict:
                stadict[arrival.sta] = {}
            d = stadict[arrival.sta]

            if arrival.iphase not in d:
                d[arrival.iphase] = 0

            d[arrival.iphase] += 1

    return stadict

def plot(rt1, ecs, labels, picks='P picks', outfile='mth.png', title='Station Picks'):

    N = len(ecs) if ecs else len(rt1)
    y1 = rt1
    y2 = ecs
    ind = np.arange(N)
    width = 0.35
    if y1:
        plt.bar(ind, y1, width, label='RT1 %s' % picks)
    if y2:
        plt.bar(ind + width, y2, width, label='ECS %s' % picks)

    plt.ylabel('Number picks/station')
    plt.title(title)

    plt.xticks(rotation = 45)

    plt.xticks(ind + width / 2, labels)
    plt.legend(loc='best')
    #plt.show()
    plt.savefig(outfile)
    plt.clf()

if __name__ == '__main__':
    main()
