import argparse
import logging
import os
import sys

from aqms_pdl import createSession
from aqms_pdl.libs.cmd_line import parse_cmd_line, required_arg
from aqms_pdl.libs.libs_aqms import get_assoc_thresh, associate_RSN_origin_with_PDL_origins
from aqms_pdl.libs.libs_db import set_Session
from aqms_pdl.libs.libs_db import query_origin_by_evid, create_and_insert_assocevents
from aqms_pdl.libs.libs_log import configure_logger, update_args_with_config, read_config


def main():
    """
        This app can be run with -e RSN_evid to look for possible matching PDL events/origins
             that may have arrived *before* the RSN_evid was inserted into the db (by some unknown
             process, eg. RT1 or Jiggle).

             Matches within assoc_thresh will trigger insertion into Assocevents table

        Usage: >python associate_origin.py -e EVID      # associate_origin -h for help
    """

    fname = 'associate_origin'
    r = required_arg(arg=['-e', '--evid'], type=int, description='EVID', help='evid to look for possible associations')
    #required_args = {['-e', '--evid']: 'EVID'}
    args, parser = parse_cmd_line(required_args=[r])

    config, log_msgs = read_config(debug=False, requireConfigFile=True, configfile=args.configfile)
    update_args_with_config(args, config, fname)

    configure_logger(**vars(args))
    logger = logging.getLogger()

    logger.info(sys.argv)
    for msg in log_msgs['info']:
        logger.info(msg)

    if config is None:
        for msg in log_msgs['error']:
            logger.error(msg)
        print()
        parser.print_help()
        exit(2)

    # Configure the Session for the whole module in __init__.py:
    createSession(config)
    # import the configured Session into the libs_db pkg for use
    set_Session()

    RSN_evid = args.evid

    RSN_orig = query_origin_by_evid(RSN_evid)
    if RSN_orig is None:
        logger.error("ERROR: No origin found for RSN_evid:%d --> Exitting!" % RSN_evid)
        exit(2)
    assoc_thresh = get_assoc_thresh(config)
    # Return list of PDL origin(s) that associate with this RSN origin
    assoc_origins = associate_RSN_origin_with_PDL_origins(RSN_orig, assoc_thresh)
    if assoc_origins:
        logger.info("%s: Found %d PDL origins that associate with RSN evid:%d" % (fname, len(assoc_origins), RSN_evid))
        create_and_insert_assocevents(RSN_evid, assoc_origins)
    else:
        logger.info("%s: Found no PDL origins that associate with RSN evid:%d" % (fname, RSN_evid))


if __name__ == "__main__":
    main()
