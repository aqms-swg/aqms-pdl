import io
import numpy as np
import os
import sys

from obspy.core.event import read_events

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

from aqms_pdl import installation_dir

from aqms_pdl.libs.libs_log import configure_logger
from aqms_pdl.libs.libs_cmd import process_cmd_line
from aqms_pdl.libs.libs_util import read_config

from aqms_pdl.libs.libs_obs import scan_quakeml


def main():
    fname = 'pdl-to-SA'

    config = read_config()
    configure_logger(config, logfile="%s.log" % fname, levelString=None)
    quakemlfile, args = process_cmd_line(fname, config)

    auth = args.source.upper()    # 'us' --> 'US'
    code = args.code              # 'us70007lf3'. This is the same as catalog:dataid in the quakeml
    pdl_type = args.type          #  PDL msg type: 'origin', 'phase_data' or 'moment-tensor', etc

    logger.info("%s: main: process pdl_type:%s code:%s" % (fname, pdl_type, code))

# 1. Read in the PDL quakeml file:
    event, origin, magnitude, focal_mechanism = read_quakemlfile(quakemlfile)

    SA = quakeml_to_SA(code, event)

    if not os.path.exists(config['EVENT_DIR']):
     try:
         os.makedirs(config['EVENT_DIR'])
     except:
         raise

    fname = "%s.xml" % code
    outfile = os.path.join(config['EVENT_DIR'], fname)

    with open(outfile, "w") as f:
        f.write(SA)

    return


'''
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<event_message alg_vers="3.1.4-2018-11-08" category="live" instance="epic@eew-bk-prod1" message_type="update" orig_sys="epic" ref_id=">
  <core_info id="7277">
    <mag units="Mw">6.1622</mag>
    <mag_uncer units="Mw">0.2317</mag_uncer>
    <lat units="deg">35.7725</lat>
    <lat_uncer units="deg">0.0465</lat_uncer>
    <lon units="deg">242.3903</lon>
    <lon_uncer units="deg">0.0465</lon_uncer>
    <depth units="km">8.0000</depth>
    <depth_uncer units="km">5.0000</depth_uncer>
    <orig_time units="UTC">2019-07-06T03:19:53.221Z</orig_time>
    <orig_time_uncer units="sec">0.5154</orig_time_uncer>
    <likelihood>1.0000</likelihood>
    <num_stations>21</num_stations>
  </core_info>
</event_message>
'''

def quakeml_to_SA(evid, event):
    origin = event.preferred_origin() or event.origins[0]
    magnitude = None
    magnitude = event.preferred_magnitude() or event.magnitudes[0]

    timestamp = None
    version = None
    if getattr(origin, 'creation_info', None):
        timestamp = origin.creation_info.creation_time
        version = origin.creation_info.version

    mag_uncer = magnitude.mag_errors.uncertainty if magnitude.mag_errors.uncertainty else 0.
    lat_uncer = origin.latitude_errors.uncertainty if origin.latitude_errors.uncertainty else 0.
    lon_uncer = origin.longitude_errors.uncertainty if origin.longitude_errors.uncertainty else 0.
    dep_uncer = origin.depth_errors.uncertainty if origin.depth_errors.uncertainty else 0.
    time_uncer = origin.time_errors.uncertainty if origin.time_errors.uncertainty else 0.

    num_stations = len(origin.arrivals)

    SA =  '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n'
    SA += '<event_message alg_vers="3.1.4-2018-11-08" category="live" ' \
          'instance="epic@eew-bk-prod1" message_type="update" orig_sys="PDL" ref_id="0" ref_src="" ' \
          'timestamp="%s" version="%s">\n' % (timestamp, version)
    SA += '  <core_info id="%s">\n' % evid
    if magnitude:
        # MTH: Temp hack since gfast only accepts units=Mw for mag:
        #SA += '    <mag units="%s">%f</mag>\n' % ("Mw", magnitude.mag)
        SA += '    <mag units="%s">%f</mag>\n' % (magnitude.magnitude_type, magnitude.mag)
        SA += '    <mag_uncer units="%s">%f</mag_uncer>\n' % (magnitude.magnitude_type, mag_uncer)
    SA += '    <lat units="deg">%f</lat>\n' % origin.latitude
    SA += '    <lat_uncer units="deg">%f</lat_uncer>\n' % lat_uncer
    SA += '    <lon units="deg">%f</lon>\n' % origin.longitude
    SA += '    <lon_uncer units="deg">%f</lon_uncer>\n' % lon_uncer

    SA += '    <depth units="km">%f</depth>\n' % (origin.depth/1000.)
    SA += '    <depth_uncer units="km">%f</depth_uncer>\n' % (dep_uncer/1000.)

    SA += '    <orig_time units="UTC">%s</orig_time>\n' % origin.time
    SA += '    <orig_time_uncer units="UTC">%s</orig_time_uncer>\n' % time_uncer
    SA += '    <likelihood>1.0000</likelihood>\n'
    SA += '    <num_stations>%d</num_stations>\n' % num_stations
    SA += '  </core_info>\n'
    SA += '</event_message>'

    return SA

def read_quakemlfile(quakemlfile):

    event = None
    origin = None
    magnitude = None
    focal_mechanism = None

    try:
        #cat = read_events(quakemlfile, format="quakeml")
        # Read quakeml into string and replace anssevent tags:
        with open(quakemlfile, 'r') as file:
            xml = file.read()
        anssInternal = False
        if 'anssevent:internalEvent' in xml:
            anssInternal = True
            logger.info("This is an anssevent:internalEvent quakeml")
            xml = xml.replace('anssevent:internalEvent', 'event')
            #xml = file.read().replace('anssevent:internalEvent', 'event')
        cat = read_events(io.BytesIO(bytes(xml, encoding='utf-8')), format="quakeml")
        event = cat[0]
    except:
        logger.error("Problem reading quakemlfile=[%s]" % quakemlfile)
        raise

    magnitude = event.preferred_magnitude()
    origin = event.preferred_origin()
    if event.focal_mechanisms:
        focal_mechanism = event.preferred_focal_mechanism() or event.focal_mechanisms[0]

    if event.preferred_origin() is None and event.origins is not None:
        mww_origin = None
        mww_trigger_origin = None
        for origin in event.origins:
            foo = origin.resource_id.id.split("/")[-1]
            if foo == 'mww':
                mww_origin = origin
            elif foo == 'mww_trigger':
                mww_trigger_origin = origin
            else:
                logger.warning("Unknown origin id:%s" % foo)
        if mww_trigger_origin:
            logger.info("Set origin = mww_trigger_origin")
            origin = mww_trigger_origin
        elif mww_origin:
            logger.info("Set origin = mww_origin")
            origin = mww_origin

    return event, origin, magnitude, focal_mechanism

if __name__ == "__main__":
    main()
